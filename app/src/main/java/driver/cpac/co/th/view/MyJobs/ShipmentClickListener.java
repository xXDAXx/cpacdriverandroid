package driver.cpac.co.th.view.MyJobs;

import driver.cpac.co.th.Model.ShipmentInfo;

public interface ShipmentClickListener {
    void passContent(ShipmentInfo shipmentInfo);
}
