package driver.cpac.co.th.view.dashboard;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;

import driver.cpac.co.th.Model.DashboardInfo;
import driver.cpac.co.th.Model.DriverInfo;
import driver.cpac.co.th.Model.GetMstTypeRes;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;

import java.util.List;

import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardViewModel extends ViewModel {
    private CargoLinkAPI cargoLinkAPI;

    public MutableLiveData<Boolean> showLoading;
    public MutableLiveData<Integer> countUnRead;
    public MutableLiveData<List<JobDetailResV2>> listMutableLiveData;
    public MutableLiveData<DashboardInfo> dashboardInfoLiveData;

    public MutableLiveData<Integer> getCountUnRead() {
        if (countUnRead == null) {
            countUnRead = new MutableLiveData<>();
        }
        return countUnRead;
    }

    public MutableLiveData<List<JobDetailResV2>> getListMutableLiveData() {
        if (listMutableLiveData == null) {
            listMutableLiveData = new MutableLiveData<>();
        }
        return listMutableLiveData;
    }

    public MutableLiveData<DashboardInfo> getDashboardInfoLiveData() {
        if (dashboardInfoLiveData == null) {
            dashboardInfoLiveData = new MutableLiveData<>();
        }
        return dashboardInfoLiveData;
    }

    public MutableLiveData<Boolean> getShowLoading() {
        if (showLoading == null) {
            showLoading = new MutableLiveData<>();
        }
        return showLoading;
    }

    public void setListMutableLiveData(List<JobDetailResV2> list) {
        listMutableLiveData.setValue(list);
    }

    public void getDashboardInfo(String Token) {
        cargoLinkAPI = ApiUtils.getAPIService();
        try {
            cargoLinkAPI.getDashboardInfo(Token)
                    .enqueue(new Callback<DashboardInfo>() {
                        @Override
                        public void onResponse(Call<DashboardInfo> call, Response<DashboardInfo> response) {

                            if (response.isSuccessful()) {
                                dashboardInfoLiveData.setValue(response.body());
                            } else {

                            }
                        }

                        @Override
                        public void onFailure(Call<DashboardInfo> call, Throwable t) {
                            //showLoading.setValue(false);
                        }
                    });
        } catch (Exception ex) {
            Log.println(0, "", ex.toString());
        }

    }

    public void countUnreadNotifications(String Token) {
        cargoLinkAPI = ApiUtils.getAPIService();
        try {
            cargoLinkAPI.countUnreadNotifications(Token).enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {
                    if (response.isSuccessful()) {
                        countUnRead.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<Integer> call, Throwable t) {

                }
            });

        } catch (Exception ex) {
            Log.println(0, "", ex.toString());
        }

    }

    public void getMstType(String Token) {
        cargoLinkAPI = ApiUtils.getAPIService();
        try {
            cargoLinkAPI.getMstType(Token).enqueue(new Callback<GetMstTypeRes>() {
                @Override
                public void onResponse(Call<GetMstTypeRes> call, Response<GetMstTypeRes> response) {
                    if (response.isSuccessful()) {
                        GetMstTypeRes res = response.body();
                        if (res != null) {
                            // Set sharePreferences here
                            Gson gson = new Gson();
                            String json = gson.toJson(res);
                            PreferenceHelper preferenceHelper = new PreferenceHelper();
                            preferenceHelper.putString(Constant.MST_TYPE, json);
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetMstTypeRes> call, Throwable t) {

                }
            });

        } catch (Exception ex) {
            Log.println(0, "", ex.toString());
        }
    }
}