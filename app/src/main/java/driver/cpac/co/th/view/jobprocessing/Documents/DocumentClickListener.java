package driver.cpac.co.th.view.jobprocessing.Documents;

import driver.cpac.co.th.Model.Documents;

public interface DocumentClickListener {
    void passContent(Documents documents);
}
