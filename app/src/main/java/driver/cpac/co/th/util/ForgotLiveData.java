package driver.cpac.co.th.util;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

public class ForgotLiveData<D> extends MediatorLiveData<D> {
    private boolean isChanged;

    @Override
    public void postValue(D value){
        isChanged = true;
        super.postValue(value);
    }

    @Override
    public void setValue(D value) {
        isChanged = true;
        super.setValue(value);
    }

    @Override
    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<? super D> observer) {
        super.observe(owner, responseErrorState -> {
            if (isChanged){
                observer.onChanged(responseErrorState);
                isChanged = false;
            }
        });
    }
}
