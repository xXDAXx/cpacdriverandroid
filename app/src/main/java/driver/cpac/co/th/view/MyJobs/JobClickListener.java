package driver.cpac.co.th.view.MyJobs;

import driver.cpac.co.th.Model.ListJobRes;

public interface JobClickListener {
    void passContent(ListJobRes listJobRes);
}
