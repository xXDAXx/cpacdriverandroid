package driver.cpac.co.th.WebApi;

import driver.cpac.co.th.Model.ActiveUserReqReq;
import driver.cpac.co.th.Model.DriverListIssueRes;
import driver.cpac.co.th.Model.DriverNewIssueDocumentReq;
import driver.cpac.co.th.Model.DriverNewIssueReq;
import driver.cpac.co.th.Model.GetMstTypeRes;
import driver.cpac.co.th.Model.SetPasswordReq;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import driver.cpac.co.th.Model.JobUploadDocumentReq;
import driver.cpac.co.th.Model.Login;
import driver.cpac.co.th.Model.RealmObject.AppVersionCheckRes;
import driver.cpac.co.th.util.Constant;

import com.google.gson.JsonObject;
import driver.cpac.co.th.Model.AppVersionCheckReq;
import driver.cpac.co.th.Model.ChangePass;
import driver.cpac.co.th.Model.DashboardInfo;
import driver.cpac.co.th.Model.DriverNotificationRes;
import driver.cpac.co.th.Model.GpsUpdateReq;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.JobStatusUpdateReq;
import driver.cpac.co.th.Model.OfflineGpsInfo;
import driver.cpac.co.th.Model.UpdateDriverInfoReq;
import driver.cpac.co.th.Model.UploadFileResponse;
import driver.cpac.co.th.Model.google.Directions;

import java.util.List;


public interface CargoLinkAPI {
    @Headers({
            "Content-Type: application/json"
    })
    //API List

    //Json Login chuẩn


    @POST("mobile/v1/auth/login")
    Call<JsonObject> logincall(@Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language, @Body Login bodyparam);

    @POST("mobile/v1/auth/validLogin")
    Call<JsonObject> validLoginCall(@Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language, @Body Login bodyparam);

    @POST("v1/signup/activeDriver")
    Call<JsonObject> activeNewUser(@Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language, @Body ActiveUserReqReq req);

    @POST("v1/signup/setPasswordDriver")
    Call<JsonObject> setPasswordUser(@Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language, @Body SetPasswordReq req);

    @GET("v1/signup/verifyActiveDriverPhoneNumber/{mobileNumber}")
    Call<Void> verifyActiveDriverPhoneNumber(@Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language, @Path("mobileNumber") String mobileNumber);

    //Logout
    @Headers(Constant.CONTENT_TYPE_JSON)
    @POST("mobile/v1/auth/logout")
    Call<JsonObject> logoutcall(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language);

    ///CheckToken
    @Headers(Constant.CONTENT_TYPE_JSON)
    @POST("v1/drivers/appVersionCheck")
    Call<AppVersionCheckRes> appVersionCheck(@Body AppVersionCheckReq req);

    //CheckToken
    @Headers(Constant.CONTENT_TYPE_JSON)
    @POST("mobile/v1/auth/checkToken")
    Call<Void> checkToken(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language);

    //resetpassword
    @Headers(Constant.CONTENT_TYPE_JSON)
    @POST("mobile/v1/auth/resetPasswordRequest")
    Call<JsonObject> resetpassword(@Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language, @Body Login bodyparam);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @POST("mobile/v1/user/driverChangePassword")
    Call<JsonObject> changepassword(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language, @Body ChangePass bodyparam);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @POST("v1/drivers/updateDriverInfo")
    Call<JsonObject> updateDriverInfo(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language, @Body UpdateDriverInfoReq bodyparam);

    @Multipart
    @POST("v1/drivers/uploadAvatar")
    Call<Void> UploadFile(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language, @Part MultipartBody.Part file);

    @POST("v1/drivers/updateAvatar")
    Call<Void> updateAvatar(@Header(Constant.HEADER_TOKEN) String authorization,
                                       @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language,
                                          @Body UploadFileResponse req);

    @GET("v1/media/download/{url}")
    Call<ResponseBody>downloadImg(@Header(Constant.HEADER_TOKEN) String authorization, @Path("url") String url);


    @Multipart
    @POST("v1/media/uploadPublic")
    Call<UploadFileResponse> uploadPublic(@Header(Constant.HEADER_TOKEN) String authorization,
                                          @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language,
                                          @Part MultipartBody.Part file,
                                          @Part("oldFile") RequestBody oldFile);

    @Multipart
    @POST("v1/media/upload")
    Call<UploadFileResponse> upload(@Header(Constant.HEADER_TOKEN) String authorization,
                            @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language,
                            @Part MultipartBody.Part file,
                            @Part("oldFile") RequestBody oldFile);

    @GET("directions/json")
    Call<Directions> getDirections(@Query("origin") String origin, @Query("destination") String destination, @Query("key") String key);

    @GET("directions/json")
    Call<Directions> getDirectionsWithWayPoints(@Query("origin") String origin, @Query("destination") String destination, @Query("waypoints") String waypoints, @Query("key") String key);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @GET("v1/drivers/getNewJobs")
    Call<List<JobDetailResV2>> getNewJobs(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @GET("v1/drivers/getMyJobs")
    Call<List<JobDetailResV2>> getMyJobs(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @GET("v1/drivers/getAllJobs")
    Call<List<JobDetailResV2>> getAllJobs(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @GET("v1/drivers/getJobsHistory")
    Call<List<JobDetailResV2>> getJobsHistory(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @GET("v1/drivers/getDetailWaybill/{waybillId}")
    Call<JobDetailResV2> getDetailWaybill(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language, @Path("waybillId") String id);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @POST("v1/drivers/updateStatusWaybill")
    Call<Void> updateStatusWaybill(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language, @Body JobStatusUpdateReq job);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @POST("v1/drivers/updateStatusDetailWaybill")
    Call<Void> updateStatusDetailWaybill(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language, @Body JobStatusUpdateReq job);


    @Headers(Constant.CONTENT_TYPE_JSON)
    @POST("v1/drivers/waitasec")
    Call<Void> waitasec(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @GET("v1/drivers/getListNotifications")
            Call<List<DriverNotificationRes>>getListNotifications(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX)String language);

    @Multipart
    @POST("v1/drivers/uploadJobDocuments")
    Call<Void> uploadJobDocuments(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language,
                                  @Part MultipartBody.Part file,
                                  @Part("waybillId") RequestBody waybillId,
                                  @Part("detailWaybillId") RequestBody detailWaybillId,
                                  @Part("type") RequestBody type,
                                  @Part("documentName") RequestBody documentName);

    @POST("v1/drivers/updateJobDocuments")
    Call<Void> updateJobDocuments(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX) String language,
                                  @Body JobUploadDocumentReq req);


    @Headers(Constant.CONTENT_TYPE_JSON)
    @GET("v1/drivers/setReadNotifications/{notificationId}")
    Call<Void>setReadNotifications(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX)String language,@Path("notificationId") String id);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @POST("v1/drivers/changeLanguage/{language}")
    Call<Void>changeLanguage(@Header(Constant.HEADER_TOKEN) String authorization, @Path("language") String languageId);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @DELETE("v1/drivers/deleteAllNotifications")
    Call<Void>deleteAllNotifications(@Header(Constant.HEADER_TOKEN) String authorization, @Header(Constant.ACCEPT_LANG_HEADER_PREFIX)String language);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @GET("v1/drivers/getDashboardInfo")
    Call<DashboardInfo>getDashboardInfo(@Header(Constant.HEADER_TOKEN) String authorization);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @GET("v1/drivers/countUnreadNotifications")
    Call<Integer>countUnreadNotifications(@Header(Constant.HEADER_TOKEN) String authorization);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @POST("gps/sendMsg")
    Call<JsonObject> updateGPS(@Body GpsUpdateReq req);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @POST("gps/sendDataHis")
    Call<JsonObject> updateOfflineGPS(@Body OfflineGpsInfo req);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @GET("v1/drivers/getIssueList")
    Call<List<DriverListIssueRes>>getIssueList(@Header(Constant.HEADER_TOKEN) String authorization);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @POST("v1/drivers/createNewIssueReport")
    Call<Void> createNewIssueReport(@Header(Constant.HEADER_TOKEN) String authorization,
                                    @Body DriverNewIssueReq req);

    @Headers(Constant.CONTENT_TYPE_JSON)
    @GET("v1/drivers/getMstType")
    Call<GetMstTypeRes>getMstType(@Header(Constant.HEADER_TOKEN) String authorization);
}
