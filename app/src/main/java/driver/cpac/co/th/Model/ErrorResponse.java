package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ErrorResponse implements Serializable {
    @SerializedName("status") @Expose
    private int status;

    @SerializedName("internal") @Expose
    private String internal;

    @SerializedName("msgList") @Expose
    private List<String> msgList;

    @SerializedName("validMsgList") @Expose
    private ValidMsg validMsgList;

    public class ValidMsg implements Serializable {
        @SerializedName("driverAccLocked") @Expose
        private List<String> driverAccLocked;

        public List<String> getDriverAccLocked() {
            return driverAccLocked;
        }

        public void setDriverAccLocked(List<String> driverAccLocked) {
            this.driverAccLocked = driverAccLocked;
        }

        public ValidMsg(List<String> driverAccLocked) {
            this.driverAccLocked = driverAccLocked;
        }

    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getInternal() {
        return internal;
    }

    public void setInternal(String internal) {
        this.internal = internal;
    }

    public List<String> getMsgList() {
        return msgList;
    }

    public void setMsgList(List<String> msgList) {
        this.msgList = msgList;
    }

    public ValidMsg getValidMsgList() {
        return validMsgList;
    }

    public void setValidMsgList(ValidMsg validMsgList) {
        this.validMsgList = validMsgList;
    }

    public ErrorResponse(int status, String internal, List<String> msgList, ValidMsg validMsgList) {
        this.status = status;
        this.internal = internal;
        this.msgList = msgList;
        this.validMsgList = validMsgList;
    }


}


