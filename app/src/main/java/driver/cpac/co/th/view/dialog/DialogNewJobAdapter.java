package driver.cpac.co.th.view.dialog;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.DetailWaybill;
import driver.cpac.co.th.Model.JobDetailResV2;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.tmg.th.R;
import com.tmg.th.databinding.ItemPopupNewjobLayoutBinding;
import com.tmg.th.databinding.ItemShipmentLayoutBinding;

import driver.cpac.co.th.Model.ShipmentInfo;
import driver.cpac.co.th.WebApi.MyGlide;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.CargoType;
import driver.cpac.co.th.util.enumClass.TruckType;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.jobdetail.DestinationListAdapter;

import java.util.ArrayList;
import java.util.List;

public class  DialogNewJobAdapter extends RecyclerView.Adapter<DialogNewJobAdapter.MyViewHolder> {

    protected List<JobDetailResV2> jobList;
    private Context context;
    private JobClickListenerV2 mListener;
    private ItemPopupNewjobLayoutBinding binding;
    PreferenceHelper preferenceHelper;
    DestinationListAdapter adapter;

    public DialogNewJobAdapter(Context context, List<JobDetailResV2> jobList, JobClickListenerV2 listener) {
        this.context = context;
        this.jobList = jobList;
        this.mListener = listener;
        preferenceHelper = new PreferenceHelper(context);
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemPopupNewjobLayoutBinding itemBinding =
                ItemPopupNewjobLayoutBinding.inflate(layoutInflater, viewGroup, false);
        return new MyViewHolder(itemBinding, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        JobDetailResV2 currentShipment = jobList.get(position);
        MyViewHolder shipmentHolder = viewHolder;
        //shipmentHolder.itemView.setOnClickListener(v -> {
        //   mListener.passContent(currentShipment);
        //});
        viewHolder.bind(currentShipment, position);
    }

    @Override
    public int getItemCount() {
        return jobList == null ? 0 : jobList.size();
    }

    public void updateAdapter(ArrayList<JobDetailResV2> arrayList) {
        this.jobList.clear();
        this.jobList = arrayList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemPopupNewjobLayoutBinding binding;

        public MyViewHolder(ItemPopupNewjobLayoutBinding binding, JobClickListenerV2 listener) {
            super(binding.getRoot());
            mListener = listener;
            this.binding = binding;
        }

        public void bind(JobDetailResV2 item, int position) {
            if(item.getAccepted() == null ) {
                binding.linearlayoutButton.setVisibility(View.VISIBLE);
                binding.layoutResponed.setVisibility(View.GONE);
            } else {
                binding.linearlayoutButton.setVisibility(View.GONE);
                binding.layoutResponed.setVisibility(View.VISIBLE);
                if(item.getAccepted()) {
                    binding.lblAccpeted.setVisibility(View.VISIBLE);
                    binding.lblRejected.setVisibility(View.GONE);
                } else {
                    binding.lblAccpeted.setVisibility(View.GONE);
                    binding.lblRejected.setVisibility(View.VISIBLE);
                }

            }
            //RCV Destination:
            ((SimpleItemAnimator) binding.rcvDestinationList.getItemAnimator()).setSupportsChangeAnimations(true);
            adapter = new DestinationListAdapter(context, item, false, false);
            binding.rcvDestinationList.setLayoutManager(new LinearLayoutManager(context));
            binding.rcvDestinationList.setAdapter(adapter);

            //SHIPPER INFO
            if (item.getShipperAvatarUrl() != null && !item.getShipperAvatarUrl().isEmpty())
                App.self().getPicasso().load(item.getShipperAvatarUrl()).placeholder(R.drawable.account_white).into(binding.imgAvatarShipper);
            binding.shipperName.setText(item.getShipperName()); //+ "  POS: " + String.valueOf(position));

            //Truck Info
            binding.truckInfo.setText(TruckType.GetTruckType(item.getTruckType(), context).toUpperCase() + " - " + item.getTruckNumber().toUpperCase());
            App.self().getPicasso().load(TruckType.GetTruckTypeImg(item.getTruckType(), context)).placeholder(R.drawable.others).into(binding.truckIcon);
            App.self().getPicasso().get().load(CargoType.GetCargoTypeImg(item.getTypeOfCargo(), context)).placeholder(R.drawable.carrgo).into(binding.cargoIcon);
            // GOODS INFO
            String goodInfo = item.getTotalWeight().toString() + " " + context.getString(R.string.common_tons)  // 20 tons
                    + " " + CargoType.GetCargoType(item.getTypeOfCargo(), context);  // FOOODS
            if (item.getQuantity() > 0) {
                goodInfo += " - " + item.getQuantity().toString() + " " + Util.GetUnitType(item.getUnit(), context).toLowerCase()
                        + " ";
                if (!(item.getSizeOfCargo().equals("0 x 0 x 0m") || item.getSizeOfCargo().equals("- x - x -m"))
                        || (item.getSizeOfCargo().equals("0.0 x 0.0 x 0.0m")) || (item.getSizeOfCargo().equals("0.00 x 0.00 x 0.00m")))
                    goodInfo += item.getSizeOfCargo();
            }
            binding.goodsInfo.setText(goodInfo);

            //binding.txtNoteToDriver.setText(jobDetailRes.getHandlingInstruction());

            if (((item.isPrimaryDriverAccepted() && item.getPrimaryDriver().getId() == preferenceHelper.getInt(Constant.USER_ID, 0))
                    || (item.isSecondaryDriverAccepted() && item.getSecondaryDriver().getId() == preferenceHelper.getInt(Constant.USER_ID, 0)))
                    && item.getSecondaryDriver() != null) {
                binding.linearlayoutButton.setVisibility(View.INVISIBLE);
            }
            binding.acceptBtn.setOnClickListener(v -> {
                mListener.passContent(item, true,position);
                //binding.linearlayoutButton.setVisibility(View.GONE);
            });
            binding.rejectBtn.setOnClickListener(v -> {
                mListener.passContent(item, false,position);
            });
        }
    }

    public interface JobClickListenerV2 {
        void passContent(JobDetailResV2 item,boolean AcceptedClicked,int pos);
    }

}

