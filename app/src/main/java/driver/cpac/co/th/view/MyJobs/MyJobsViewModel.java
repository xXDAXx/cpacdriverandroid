package driver.cpac.co.th.view.MyJobs;

import android.os.Handler;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import driver.cpac.co.th.Model.BaseResponse;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.LangUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyJobsViewModel extends ViewModel {
    public MutableLiveData<List<JobDetailResV2>> listShipmentInfo;
    public MutableLiveData<List<JobDetailResV2>> listJobRes;
    public MutableLiveData<Boolean> showLoading = new MutableLiveData<>();
    public MutableLiveData<Boolean> showEmpty = new MutableLiveData<>();
    public MutableLiveData<BaseResponse> baseResponse;
    public MutableLiveData<JobDetailResV2> jobDetailLiveData;
    private CargoLinkAPI mCargoLinkAPI;

    public MutableLiveData<BaseResponse> getBaseRes() {
        if (baseResponse == null) {
            baseResponse = new MutableLiveData<>();
        }
        return baseResponse;
    }

    public MutableLiveData<List<JobDetailResV2>> getListShipmentInfo() {
        if (listShipmentInfo == null) {
            listShipmentInfo = new MutableLiveData<>();
        }
        return listShipmentInfo;
    }

    public MutableLiveData<List<JobDetailResV2>> getlistJobRes() {
        if (listJobRes == null) {
            listJobRes = new MutableLiveData<>();
        }
        return listJobRes;
    }

    public MutableLiveData<Boolean> getShowLoading() {
        if (showLoading == null) {
            showLoading = new MutableLiveData<>();
        }
        return showLoading;
    }

    public MutableLiveData<Boolean> getShowEmpty() {
        if (showEmpty == null) {
            showEmpty = new MutableLiveData<>();
        }
        return showEmpty;
    }

    public MutableLiveData<JobDetailResV2> getjobDetailLiveData() {
        if (jobDetailLiveData == null) {
            jobDetailLiveData = new MutableLiveData<>();
        }
        return jobDetailLiveData;
    }

    //API getJob
    public void getMyJobs(String Token) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.getAllJobs(Token, LangUtils.getCurrentLanguage()).enqueue(new Callback<List<JobDetailResV2>>() {
            @Override
            public void onResponse(Call<List<JobDetailResV2>> call, Response<List<JobDetailResV2>> response) {
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        listJobRes.setValue(response.body());
                    } else {
                        showEmpty.setValue(true);
                        showLoading.setValue(false);
                    }
                } else {
                    showEmpty.setValue(true);
                    showLoading.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<List<JobDetailResV2>> call, Throwable t) {
                showEmpty.setValue(true);
                showLoading.setValue(false);
            }
        });

    }

    public void getDetailJob(String Token, String jobId) {
        mCargoLinkAPI = ApiUtils.getAPIService();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCargoLinkAPI.getDetailWaybill(Token, LangUtils.getCurrentLanguage(), jobId)
                        .enqueue(new Callback<JobDetailResV2>() {
                            @Override
                            public void onResponse(Call<JobDetailResV2> call, Response<JobDetailResV2> response) {
                                if (response.body() != null) {
                                    jobDetailLiveData.setValue(response.body());
                                } else {
                                    showLoading.setValue(false);
                                }
                            }

                            @Override
                            public void onFailure(Call<JobDetailResV2> call, Throwable t) {
                                showLoading.setValue(false);
                            }
                        });
            }
        }, 1000);

    }
    private void simulateDelay() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
            }
        }, 2000);
    }
}
