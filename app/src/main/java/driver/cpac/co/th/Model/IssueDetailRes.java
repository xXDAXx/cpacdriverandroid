package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IssueDetailRes {
    @SerializedName("waybillId")
    @Expose
    private int waybillId;
    @SerializedName("issueId")
    @Expose
    private int issueId;
    @SerializedName("issueType")
    @Expose
    private int issueType;
    @SerializedName("documentUrl")
    @Expose
    private String documentUrl;
    @SerializedName("contentStr")
    @Expose
    private String contentStr;
    @SerializedName("documentName")
    @Expose
    private String documentName;
    @SerializedName("documentType")
    @Expose
    private String documentType;
    @SerializedName("fileType")
    @Expose
    private String fileType;
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("submittedTime")
    @Expose
    private String submittedTime;

    @SerializedName("createdByName")
    @Expose
    private String createdByName;

    @SerializedName("createdById")
    @Expose
    private int createdById;

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    public int getCreatedById() {
        return createdById;
    }

    public void setCreatedById(int createdById) {
        this.createdById = createdById;
    }

    public String getSubmittedTime() {
        return submittedTime;
    }

    public void setSubmittedTime(String submittedTime) {
        this.submittedTime = submittedTime;
    }

    public int getWaybillId() {
        return waybillId;
    }

    public void setWaybillId(int waybillId) {
        this.waybillId = waybillId;
    }

    public int getIssueId() {
        return issueId;
    }

    public void setIssueId(int issueId) {
        this.issueId = issueId;
    }

    public int getIssueType() {
        return issueType;
    }

    public void setIssueType(int issueType) {
        this.issueType = issueType;
    }

    public String getDocumentUrl() {
        return documentUrl;
    }

    public void setDocumentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
    }

    public String getContentStr() {
        return contentStr;
    }

    public void setContentStr(String contentStr) {
        this.contentStr = contentStr;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public IssueDetailRes(int waybillId, int issueId, int issueType, String documentUrl, String contentStr, String documentName, String documentType, String fileType, int status, String submittedTime, String createdByName, int createdById) {
        this.waybillId = waybillId;
        this.issueId = issueId;
        this.issueType = issueType;
        this.documentUrl = documentUrl;
        this.contentStr = contentStr;
        this.documentName = documentName;
        this.documentType = documentType;
        this.fileType = fileType;
        this.status = status;
        this.submittedTime = submittedTime;
        this.createdByName = createdByName;
        this.createdById = createdById;
    }
}
