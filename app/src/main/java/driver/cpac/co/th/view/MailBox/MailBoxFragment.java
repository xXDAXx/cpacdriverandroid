package driver.cpac.co.th.view.MailBox;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tmg.th.R;
import com.tmg.th.databinding.FragmentMailboxBinding;

import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.enumClass.NotificationStatus;
import driver.cpac.co.th.view.Home.HomeActivity;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;
import driver.cpac.co.th.view.dialog.DialogWait2;

public class MailBoxFragment extends Fragment implements DialogNoticeCallback.CallBackDialogNotice {
    private FragmentMailboxBinding binding;
    private static View view;
    private MailboxViewModel viewModel;
    private ListNotificationAdapter adapter;
    Menu myMenu;
    MenuItem item_delete;
    DialogWait2 dialogWait;
    DialogNoticeCallback dialogNoticeCallback;
    PreferenceHelper preferenceHelper;
    HomeActivity parent;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_mailbox, container, false);
        view = binding.getRoot();
        viewModel = ViewModelProviders.of(this).get(MailboxViewModel.class);
        binding.setLifecycleOwner(this);
        binding.rcvNotification.setVisibility(View.INVISIBLE);
        binding.deleteBtn.setOnClickListener(v -> {
            dialogNoticeCallback = new DialogNoticeCallback(getContext().getString(R.string.mailbox_noticeLabel_deleteMessages).toString(), getActivity(), this);
            dialogNoticeCallback.show();
        });
        preferenceHelper = new PreferenceHelper(getContext());
        //
        parent = (HomeActivity) getActivity();
        viewModel = ViewModelProviders.of(this).get(MailboxViewModel.class);
        OserverSetup();
        viewModel.getMyNotify(preferenceHelper.getString(Constant.TOKEN, ""));
        return view;
    }

    public void OserverSetup() {
        viewModel.getListMutableLiveData().observe(this, driverNotificationRes -> {
            adapter = new ListNotificationAdapter(getContext(), driverNotificationRes, listener);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            layoutManager.setOrientation(RecyclerView.VERTICAL);
            binding.rcvNotification.setLayoutManager(layoutManager);
            binding.rcvNotification.setAdapter(adapter);
        });

        viewModel.getShowEmpty().observe(this, aBoolean -> {
            if (aBoolean) showEmpty();
            else hideEmtpy();
        });

        viewModel.getLoading().observe(this, aBoolean -> {
            if (aBoolean) showLoading();
            else hideLoading();
        });
    }

    NotifyClickListener listener = driverNotificationRes -> {

        driverNotificationRes.setStatus(NotificationStatus.read.getValue());
        adapter.notifyDataSetChanged();
        viewModel.setReadNotify(preferenceHelper.getString(Constant.TOKEN, ""), driverNotificationRes.getHashId());
        Intent intent = new Intent(getContext(), NotificationDetailActivityV3.class);
        intent.putExtra("notify", driverNotificationRes);
        startActivity(intent);
    };

    public void showEmpty() {
        binding.rcvNotification.setVisibility(View.GONE);
        binding.textEmpty.setVisibility(View.VISIBLE);
        binding.imgEmpty.setVisibility(View.VISIBLE);
        if (item_delete != null)
            item_delete.setVisible(false);
    }

    public void hideEmtpy() {
        binding.rcvNotification.setVisibility(View.VISIBLE);
        binding.textEmpty.setVisibility(View.GONE);
        binding.imgEmpty.setVisibility(View.GONE);
        if (item_delete != null)
            item_delete.setVisible(true);
    }

    @Override
    public void BtnOkDiaComClick() {
        dialogNoticeCallback.dismiss();
        viewModel.deleteAllNotifications(preferenceHelper.getString(Constant.TOKEN, ""));
    }

    public void deleteAllNotifications() {
        viewModel.deleteAllNotifications(preferenceHelper.getString(Constant.TOKEN, ""));
    }

    public void showLoading() {
        parent.showLoading();
    }

    public void hideLoading() {
        parent.hideLoading();
    }
}