package driver.cpac.co.th.view.base;


import androidx.fragment.app.Fragment;

import driver.cpac.co.th.view.dialog.DialogNotice;

public class BaseFragment extends Fragment {

    public void NoticeShow(String msg) {
        DialogNotice dialogNotice = new DialogNotice(msg, getActivity());
        dialogNotice.show();
    }
}
