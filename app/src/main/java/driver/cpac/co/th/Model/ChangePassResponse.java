package driver.cpac.co.th.Model;
import com.google.gson.JsonObject;
import driver.cpac.co.th.util.Constant;

public class ChangePassResponse {
    private JsonObject jobject;
    private String notice_title;
    public JsonObject ChangePassResponse() {
        return jobject;
    }

    public void setChangePassResponse(JsonObject jobject) {
        this.jobject = jobject;
    }

    public String getStringNotice()
    {
        if (jobject==null)
            return null;
        else
            return jobject.get(Constant.TITLE).getAsString();
    }

    public void setStringNotice(String notice_title) {
        this.notice_title = notice_title;
    }
}
