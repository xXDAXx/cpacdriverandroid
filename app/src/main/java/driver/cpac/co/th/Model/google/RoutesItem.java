package driver.cpac.co.th.Model.google;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RoutesItem implements Serializable{
    @SerializedName("copyrights")
    @Expose
    private String copyrights;

    @SerializedName("legs")
    @Expose
    private List<LegsItem> legs;

    @SerializedName("bounds")
    @Expose
    private Bounds bounds;

    @SerializedName("overview_polyline")
    @Expose
    private OverviewPolyline overviewPolyline;

    public RoutesItem(String copyrights, List<LegsItem> legs, Bounds bounds, OverviewPolyline overviewPolyline) {
        this.copyrights = copyrights;
        this.legs = legs;
        this.bounds = bounds;
        this.overviewPolyline = overviewPolyline;
    }

    public String getCopyrights() {
        return copyrights;
    }

    public void setCopyrights(String copyrights) {
        this.copyrights = copyrights;
    }

    public List<LegsItem> getLegs() {
        return legs;
    }

    public void setLegs(List<LegsItem> legs) {
        this.legs = legs;
    }

    public Bounds getBounds() {
        return bounds;
    }

    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }

    public OverviewPolyline getOverviewPolyline() {
        return overviewPolyline;
    }

    public void setOverviewPolyline(OverviewPolyline overviewPolyline) {
        this.overviewPolyline = overviewPolyline;
    }
}
