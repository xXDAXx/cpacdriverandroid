package driver.cpac.co.th.view.issueReport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import driver.cpac.co.th.util.enumClass.IssueType;

public class ItemNewIssue implements Serializable {
    public IssueType getIssueType() {
        return issueType;
    }

    public void setIssueType(IssueType issueType) {
        this.issueType = issueType;
    }

    @SerializedName("title")
    @Expose
    String title;

    @SerializedName("type")
    @Expose
    int type;

    @SerializedName("issueType")
    @Expose
    IssueType issueType;




    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public ItemNewIssue(String title, int type, IssueType issueType) {
        this.title = title;
        this.type = type;
        this.issueType = issueType;
    }
}
