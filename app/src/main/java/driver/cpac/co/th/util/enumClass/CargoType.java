package driver.cpac.co.th.util.enumClass;

import android.content.Context;

import com.google.gson.Gson;
import com.tmg.th.R;

import java.sql.PreparedStatement;
import java.util.List;

import driver.cpac.co.th.Model.GetMstTypeRes;
import driver.cpac.co.th.Model.MstTypeItem;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.util.PreferenceHelper;

public enum CargoType {
    constructionMaterial(1),
    agriculturalProduct(2),
    food(3),
    consumerGood(4),
    furniture(5),
    houseware(6),
    electronicEquipment(7),
    machineryEquipment(8),
    fertilizer(9),
    chemicalProduct(10),
    liquid(11),
    oilGas(12),
    logWood(13),
    medicine(14),
    refrigeratedProduct(15),
    others(16);

    private final int value;

    private CargoType(int value) {
        this.value = value;
    }

    public short getValue() {
        return (short) value;
    }


    public static CargoType getEnumType(int value) {
        for(CargoType type:CargoType.values()){
            if (type.getValue()==value) return type;
        }
        return null;
    }

    /*
    public static int GetCargoTypeImg(int cargoType, Context context) {
        CargoType type = CargoType.getEnumType(cargoType);
        int drawableId = 0;
        switch (type) {
            case food:
                drawableId = R.drawable.cargo_type_food_beverages;
                break;
            case liquid:
                drawableId = R.drawable.cargo_type_liquids;
                break;
            case oilGas:
                drawableId = R.drawable.cargo_type_oil_gas;
                break;
            case logWood:
                drawableId = R.drawable.cargo_type_wood;
                break;
            case medicine:
                drawableId = R.drawable.cargo_type_pharmaceuticals;
                break;
            case furniture:
                drawableId = R.drawable.cargo_type_furniture;
                break;
            case houseware:
                drawableId = R.drawable.cargo_type_clothes_shoes;
                break;
            case fertilizer:
                drawableId = R.drawable.cargo_type_fertilizer;
                break;
            case consumerGood:
                drawableId = R.drawable.cargo_type_consumer_goods;
                break;
            case chemicalProduct:
                drawableId = R.drawable.cargo_type_chemical_products;
                break;
            case machineryEquipment:
                drawableId = R.drawable.cargo_type_machinery;
                break;
            case agriculturalProduct:
                drawableId = R.drawable.cargo_type_agriculture;
                break;
            case electronicEquipment:
                drawableId = R.drawable.cargo_type_electronics;
                break;
            case refrigeratedProduct:
                drawableId = R.drawable.cargo_type_refrigerated_products;
                break;
            case constructionMaterial:
                drawableId = R.drawable.cargo_type_constructions;
                break;

            case others:
                drawableId = R.drawable.cargo_type_consumer_goods;
                break;
        }
        return drawableId;
    }
    */

    public static String GetCargoTypeImg(int cargoType, Context context) {
        String urlRet = "";
        PreferenceHelper preferenceHelper = new PreferenceHelper(context);
        Gson gson = new Gson();
        GetMstTypeRes res = gson.fromJson(preferenceHelper.getString(Constant.MST_TYPE,""),GetMstTypeRes.class);
        if(res != null) {
            for (MstTypeItem item : res.getCargoType()) {
                if (item.getType() == cargoType) {
                    urlRet = Constant.API_BASE_URL.replace("api/","") + item.getImgUrl();
                    break;
                }
            }
        }
        return urlRet;
    }

    public static String GetCargoType(int cargoType, Context context) {
        String typeRet = "";
        PreferenceHelper preferenceHelper = new PreferenceHelper(context);
        Gson gson = new Gson();
        GetMstTypeRes res = gson.fromJson(preferenceHelper.getString(Constant.MST_TYPE,""),GetMstTypeRes.class);
        if(res != null ) {
            for (MstTypeItem item : res.getCargoType()) {
                if (item.getType() == cargoType) {
                    if (LangUtils.getCurrentLanguage().equals("en")) {
                        typeRet = item.getNameEng();
                    } else if (LangUtils.getCurrentLanguage().equals("th")) {
                        typeRet = item.getNameThai();
                    }
                    break;
                }
            }
        }
        return typeRet;
    }


}

