package driver.cpac.co.th.view.MailBox;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tmg.th.R;
import com.tmg.th.databinding.ActivityMailboxBinding;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.enumClass.NotificationStatus;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;

public class MailBoxActivity extends OldBaseActivity implements DialogNoticeCallback.CallBackDialogNotice {
    private ActivityMailboxBinding binding;
    private MailboxViewModel viewModel;
    private ListNotificationAdapter adapter;
    Menu myMenu;
    MenuItem item_delete;
    DialogNoticeCallback dialogNoticeCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_mailbox);
        setSupportActionBar(binding.appBarLayout.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.title_mailbox);
        preferenceHelper = new PreferenceHelper(this);
        //
        viewModel = ViewModelProviders.of(this).get(MailboxViewModel.class);
        OserverSetup();
        viewModel.getMyNotify(preferenceHelper.getString(Constant.TOKEN, ""));
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void OserverSetup() {
        viewModel.getListMutableLiveData().observe(this, driverNotificationRes -> {
            adapter = new ListNotificationAdapter(this, driverNotificationRes, listener);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(RecyclerView.VERTICAL);
            binding.rcvNotification.setLayoutManager(layoutManager);
            binding.rcvNotification.setAdapter(adapter);
        });

        viewModel.getShowEmpty().observe(this, aBoolean -> {
            if (aBoolean) showEmpty();
            else hideEmtpy();
        });

        viewModel.getLoading().observe(this, aBoolean -> {
            if (aBoolean) showLoading();
            else hideLoading();
        });
    }

    NotifyClickListener listener = driverNotificationRes -> {

        driverNotificationRes.setStatus(NotificationStatus.read.getValue());
        adapter.notifyDataSetChanged();
        viewModel.setReadNotify(preferenceHelper.getString(Constant.TOKEN, ""), driverNotificationRes.getHashId());
        Intent intent = new Intent(this, NotificationDetailActivityV3.class);
        intent.putExtra("notify", driverNotificationRes);
        startActivity(intent);
    };

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        myMenu = menu;
        getMenuInflater().inflate(R.menu.menu_mailbox, menu);
        item_delete = (MenuItem) menu.findItem(R.id.action_delete);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle status filter & action filter or search
        switch (item.getItemId()) {
            case R.id.action_delete:
                dialogNoticeCallback = new DialogNoticeCallback(getText(R.string.mailbox_noticeLabel_deleteMessages).toString(), this, this);
                dialogNoticeCallback.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showEmpty() {
        binding.rcvNotification.setVisibility(View.GONE);
        binding.textEmpty.setVisibility(View.VISIBLE);
        binding.imgEmpty.setVisibility(View.VISIBLE);
        if (item_delete != null)
            item_delete.setVisible(false);
    }

    public void hideEmtpy() {
        binding.rcvNotification.setVisibility(View.VISIBLE);
        binding.textEmpty.setVisibility(View.GONE);
        binding.imgEmpty.setVisibility(View.GONE);
        if (item_delete != null)
            item_delete.setVisible(true);
    }

    @Override
    public void BtnOkDiaComClick() {
        dialogNoticeCallback.dismiss();
        viewModel.deleteAllNotifications(preferenceHelper.getString(Constant.TOKEN, ""));
    }
}
