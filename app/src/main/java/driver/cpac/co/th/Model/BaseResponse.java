package driver.cpac.co.th.Model;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import driver.cpac.co.th.util.Constant;

import okhttp3.ResponseBody;

public class BaseResponse {
    private JsonObject jobject;
    private String responcode;

    public JsonObject BaseResponse() {
        return jobject;
    }

    public void setBaseResponse(JsonObject jobject) {
        this.jobject = jobject;
    }

    public void setErrorResponse(ResponseBody responseBody) {
        Gson gson = new Gson();
        this.jobject = gson.fromJson(responseBody.charStream(), JsonObject.class);
    }

    public String getStringNotice() {
        if (jobject.size() >= 1) {
            if (jobject == null)
                return null;
            else
                return jobject.get(Constant.TITLE).getAsString();
        } else
            return null;
    }

    public String getStringStatus() {
        if (jobject.size() > 1) {
            if (jobject == null)
                return null;
            else
                return jobject.get(Constant.STATUS).getAsString();
        } else
            return null;
    }

    public String getStringType() {
        if (jobject.size() > 1) {
            if (jobject == null)
                return null;
            else
                return jobject.get(Constant.TYPE).getAsString();
        } else
            return null;
    }

    public String getResponcode() {
        return responcode;
    }

    public void setResponcode(String responcode) {
        this.responcode = responcode;
    }
}
