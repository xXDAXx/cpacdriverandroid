package driver.cpac.co.th.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.tmg.th.R;
import com.tmg.th.databinding.DialogNewjobBinding;

import java.util.List;

import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.JobStatusUpdateReq;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DialogNewJob extends Dialog {
    public Activity activity;
    public Dialog dialog;
    DialogDissmiss dialogDissmiss;
    private DialogNewjobBinding binding;
    LangUtils langUtils;
    List<JobDetailResV2> listJobs;
    DialogNewJobAdapter adapter;
    private CargoLinkAPI mCargoLinkAPI;

    public DialogNewJob(Activity a, List<JobDetailResV2> listJobs) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
        this.listJobs = listJobs;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        binding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_newjob, (ViewGroup) activity.findViewById(android.R.id.content), false);
        setContentView(binding.getRoot());
        binding.btnCancel.setOnClickListener(v -> {
            dismiss();
            //dialogDissmiss.RefreshContent();
        });

        binding.closeBtn.setOnClickListener(v -> {
            dismiss();
        });
        adapter = new DialogNewJobAdapter(activity.getBaseContext(), listJobs, listener);
        binding.dlgConfirmTittle.setText( listJobs.size() + " x " + binding.dlgConfirmTittle.getText() );
        binding.rcvNewJob.setItemViewCacheSize(listJobs.size());
        binding.rcvNewJob.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.rcvNewJob.setLayoutManager(layoutManager);
        binding.rcvNewJob.setAdapter(adapter);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(binding.rcvNewJob);
    }

    DialogNewJobAdapter.JobClickListenerV2 listener = (item, AcceptedClicked, pos) -> {
        int status = 0;
                if(AcceptedClicked) {
                    status = WaybillStatus.driverAccepted.getValue();
                } else {
                    status = WaybillStatus.driverRejected.getValue();
                }
        updateJobStatus(item, status, AcceptedClicked, pos);
    };

    public void updateJobStatus(JobDetailResV2 item, int status,Boolean AcceptedClicked, int pos) {
        showLoading(true);
        PreferenceHelper preferenceHelper = new PreferenceHelper(getContext());
        String token = preferenceHelper.getString(Constant.TOKEN,"");
        mCargoLinkAPI = ApiUtils.getAPIService();
        try {
            mCargoLinkAPI.updateStatusWaybill(token, LangUtils.getCurrentLanguage(),new JobStatusUpdateReq(item.getHashId(), WaybillStatus.driverAccepted.getValue(),null))
                    .enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            showLoading(false);
                            if (response.isSuccessful()) {
                                for (JobDetailResV2 job : listJobs) {
                                    if (job.getId() == item.getId()) {
                                        job.setAccepted(AcceptedClicked);
                                        adapter.notifyDataSetChanged();
                                        if (pos < (adapter.getItemCount() - 1)) {
                                            binding.rcvNewJob.post(() -> binding.rcvNewJob.smoothScrollToPosition(pos + 1));
                                        }
                                    }
                                    if (pos == adapter.getItemCount() - 1) {
                                        dismiss();
                                        //dialogDissmiss.RefreshContent();
                                    }
                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            showLoading(false);
                        }
                    });
        } catch (Exception ex){
            Log.println(0,"",ex.toString());
        }
    }

    public void showLoading(Boolean aBoolean) {
        if (aBoolean) {
            binding.waitView.setVisibility(View.VISIBLE);
        } else {
            binding.waitView.setVisibility(View.GONE);
        }
    }


    public interface DialogDissmiss {
        void RefreshContent();
    }

}
