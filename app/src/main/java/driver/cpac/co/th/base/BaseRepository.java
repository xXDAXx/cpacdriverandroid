package driver.cpac.co.th.base;

import driver.cpac.co.th.Model.ResponseErrorState;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.ForgotLiveData;
import driver.cpac.co.th.util.PreferenceHelper;

import java.io.EOFException;
import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.HttpException;

public abstract class BaseRepository {
    protected final ForgotLiveData<ResponseErrorState> responseStateMutableLiveData;
    protected PreferenceHelper preferenceHelper;
    private Util util;

    public BaseRepository(PreferenceHelper preferenceHelper) {
        this.preferenceHelper = preferenceHelper;
        responseStateMutableLiveData = new ForgotLiveData<>();

    }

    public ForgotLiveData<ResponseErrorState> getResponseStateMutableLiveData() {
        return responseStateMutableLiveData;
    }


    protected void handleErrorCode(int errorCode) {
        if (errorCode == 409) {
            responseStateMutableLiveData.postValue(ResponseErrorState.UNAUTHORIZED);
        }
    }

    protected void handlerError(Throwable throwable) {

        if (throwable instanceof SocketTimeoutException) {
            responseStateMutableLiveData.postValue(ResponseErrorState.TIME_OUT);
        } else if (throwable instanceof EOFException) {
            responseStateMutableLiveData.postValue(ResponseErrorState.UNKNOWN);
        } else if (throwable instanceof IOException) {
            // check internet connection
            responseStateMutableLiveData.postValue(ResponseErrorState.NO_CONNECTION);
        } else if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            if (httpException.code() == 401 || httpException.code() == 409) {
                responseStateMutableLiveData.postValue(ResponseErrorState.UNAUTHORIZED);
            }
        } else {
            responseStateMutableLiveData.postValue(ResponseErrorState.UNKNOWN);
        }
    }

    protected String checkTokenNull() {
        if (getToken() == null) {
            responseStateMutableLiveData.postValue(ResponseErrorState.UNAUTHORIZED);
        }
        return getToken();
    }

    // always get token new
    protected String getToken() {
        return preferenceHelper.getString(Constant.TOKEN, null);
    }


}
