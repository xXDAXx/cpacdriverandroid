package driver.cpac.co.th.view.jobprocessing.JobHistory;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.Documents;
import driver.cpac.co.th.Model.JobDetailResV2;

import com.squareup.picasso.Picasso;
import com.tmg.th.R;
import com.tmg.th.databinding.FragmentJobHistoryV2Binding;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.CargoType;
import driver.cpac.co.th.util.enumClass.TruckType;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.MapView.MapViewActivity;
import driver.cpac.co.th.view.documentViewer.WebViewerActivity;
import driver.cpac.co.th.view.jobdetail.DestinationListAdapter;
import driver.cpac.co.th.view.jobprocessing.Documents.ListDocumentsAdapterV2;

import java.util.List;

public class JobHistoryFragment extends Fragment {
    FragmentJobHistoryV2Binding binding;
    public JobDetailResV2 jobDetail;
    private static View view;
    PreferenceHelper preferenceHelper;
    DestinationListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_job_history_v2, container, false);
        view = binding.getRoot();
        binding.setLifecycleOwner(this);
        jobDetail = (JobDetailResV2) getArguments().getSerializable("jobDetail");
        preferenceHelper = new PreferenceHelper(getContext());
        onDisplay_Detail_Shipment();
        binding.dropList.cardViewMapButton.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), MapViewActivity.class);
            intent.putExtra("jobDetail", this.jobDetail);
            startActivity(intent);
        });
        return view;
    }

    public void setupRcvDocument(JobDetailResV2 jobDetailRes) {
        List<Documents> documentsList = jobDetailRes.getDocuments();
        ListDocumentsAdapterV2 mAdapter = new ListDocumentsAdapterV2(getContext(), documentsList, viewClickListener, deleteClickListener, WaybillStatus.getEnumType(jobDetailRes.getStatus()));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        binding.rcvDocuments.setLayoutManager(layoutManager);
        binding.rcvDocuments.setAdapter(mAdapter);
    }

    ListDocumentsAdapterV2.DocumentViewClickListener viewClickListener = documents -> {
        Intent intent = new Intent(getContext(), WebViewerActivity.class);
        intent.putExtra("content", documents);
        startActivity(intent);
    };

    ListDocumentsAdapterV2.DocumentDeleteClickListener deleteClickListener = documents -> {

    };

    public void onDisplay_Detail_Shipment() {
        if (jobDetail.getEstimateDistance() == null || jobDetail.getEstimateDistance().isEmpty()) {
            binding.dropList.distanceInfo.setVisibility(View.GONE);
            binding.dropList.distanceIcon.setVisibility(View.GONE);
        } else
            binding.dropList.distanceInfo.setText(getString(R.string.common_est) + " " + jobDetail.getEstimateDistance());
        if (jobDetail != null) {
            //binding.dropList.cargoIcon.setImageResource(CargoType.GetCargoTypeImg(jobDetail.getTypeOfCargo(), getContext()));
            Picasso.get().load(CargoType.GetCargoTypeImg(jobDetail.getTypeOfCargo(), getContext())).placeholder(R.drawable.others).into( binding.dropList.cargoIcon);
            //SHIPPER INFO
            if (jobDetail.getShipperAvatarUrl() != null && !jobDetail.getShipperAvatarUrl().isEmpty())
                App.self().getPicasso().load(jobDetail.getShipperAvatarUrl()).placeholder(R.drawable.account_white).into(binding.imgAvatarShipper);
            binding.shipperName.setText(jobDetail.getShipperName());

            /// GOODS INFO
            String goodInfo = jobDetail.getTotalWeight().toString() + " " + getString(R.string.common_tons)  // 20 tons
                    + " " + CargoType.GetCargoType(jobDetail.getTypeOfCargo(), getContext());  // FOOODS
            if (jobDetail.getQuantity() > 0) {
                goodInfo += " - " + jobDetail.getQuantity().toString() + " " + Util.GetUnitType(jobDetail.getUnit(), getContext()).toLowerCase()
                        + " " ;
                if (!(jobDetail.getSizeOfCargo().equals("0 x 0 x 0m") || jobDetail.getSizeOfCargo().equals("- x - x -m")
                        || jobDetail.getSizeOfCargo().equals("0.0 x 0.0 x 0.0m")|| jobDetail.getSizeOfCargo().equals("0.00 x 0.00 x 0.00m")))
                    goodInfo += jobDetail.getSizeOfCargo();
            }
            binding.dropList.goodsInfo.setText(goodInfo);
            //binding.txtNoteToDriver.setText(jobDetailRes.getHandlingInstruction());

            //Driver Info
            int userId = preferenceHelper.getInt(Constant.USER_ID, 0);
            if (jobDetail.getSecondaryDriver() != null) {   // IF TRIP HAVE 2 DRIVER
                if (userId == jobDetail.getSecondaryDriver().getId()) { // DRIVER IS PRIMARY DRIVER --> SHOW SECOND DRIVER INFO
                    if (jobDetail.getPrimaryDriver().getAvaUrl() != null)
                        App.self().getPicasso().load(jobDetail.getPrimaryDriver().getAvaUrl()).placeholder(R.drawable.account).into(binding.dropList.imgAvatar);
                    binding.dropList.driverName.setText(jobDetail.getPrimaryDriver().getDriverName());
                    binding.dropList.driverRole.setText(R.string.common_label_primaryDriver);
                    binding.dropList.btnDrvCall.setOnClickListener(v -> {
                        String phone = jobDetail.getPrimaryDriver().getDriverContact();
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                        startActivity(intent);
                    });
                    binding.dropList.btnDrvSms.setOnClickListener(v -> {
                        String number = jobDetail.getPrimaryDriver().getDriverContact();  // The number on which you want to send SMS
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                    });
                } else {  // DRIVER IS PRIMARY DRIVER --> SHOW SECOND DRIVER INFO
                    if (jobDetail.getSecondaryDriver().getAvaUrl() != null)
                        App.self().getPicasso().load(jobDetail.getSecondaryDriver().getAvaUrl()).placeholder(R.drawable.account).into(binding.dropList.imgAvatar);
                    binding.dropList.driverName.setText(jobDetail.getSecondaryDriver().getDriverName());
                    binding.dropList.driverRole.setText(R.string.common_label_secondDriver);
                    binding.dropList.btnDrvCall.setOnClickListener(v -> {
                        String phone = jobDetail.getSecondaryDriver().getDriverContact();
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                        startActivity(intent);
                    });
                    binding.dropList.btnDrvSms.setOnClickListener(v -> {
                        String number = jobDetail.getSecondaryDriver().getDriverContact();  // The number on which you want to send SMS
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                    });
                }
            } else {
                binding.dropList.cardViewDriverInfo.setVisibility(View.GONE);
            }

            //DOcument list
            setupRcvDocument(jobDetail);

            //Binding list Destination
            ((SimpleItemAnimator) binding.dropList.rcvDestinationList.getItemAnimator()).setSupportsChangeAnimations(true);
            adapter = new DestinationListAdapter(getContext(), jobDetail, true, true);
            binding.dropList.rcvDestinationList.setLayoutManager(new LinearLayoutManager(getContext()));
            binding.dropList.rcvDestinationList.setAdapter(adapter);

            //Truck Info
            binding.dropList.truckInfo.setText(TruckType.GetTruckType(jobDetail.getTruckType(),getContext()).toUpperCase() + " - " + jobDetail.getTruckNumber().toUpperCase());
            App.self().getPicasso().load(TruckType.GetTruckTypeImg(jobDetail.getTruckType(),getContext())).placeholder(R.drawable.others).into(binding.dropList.truckIcon);
        }

    }
}
