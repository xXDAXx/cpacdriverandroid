package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SetPasswordReq implements Serializable
    {
        @SerializedName("phoneNumber") @Expose private String mobileNumer;
        @SerializedName("verificationCode") @Expose private String activeCode;
        @SerializedName("password") @Expose private String password;
        @SerializedName("passwordConfirm") @Expose private String passwordConfirm;

        public String getMobileNumer() {
            return mobileNumer;
        }

        public void setMobileNumer(String mobileNumer) {
            this.mobileNumer = mobileNumer;
        }

        public String getActiveCode() {
            return activeCode;
        }

        public void setActiveCode(String activeCode) {
            this.activeCode = activeCode;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPasswordConfirm() {
            return passwordConfirm;
        }

        public void setPasswordConfirm(String passwordConfirm) {
            this.passwordConfirm = passwordConfirm;
        }

        public SetPasswordReq(String mobileNumer, String activeCode, String password, String passwordConfirm) {
            this.mobileNumer = mobileNumer;
            this.activeCode = activeCode;
            this.password = password;
            this.passwordConfirm = passwordConfirm;
        }
    }
