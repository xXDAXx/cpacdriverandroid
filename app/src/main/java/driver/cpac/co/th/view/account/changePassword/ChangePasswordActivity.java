package driver.cpac.co.th.view.account.changePassword;

import android.app.Activity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import driver.cpac.co.th.Model.BaseResponse;
import driver.cpac.co.th.Model.ChangePass;
import com.tmg.th.R;
import com.tmg.th.databinding.ActivityChangePasswordBinding;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogNotice;
import driver.cpac.co.th.view.dialog.DialogWait;
import driver.cpac.co.th.view.dialog.DialogWait2;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChangePasswordActivity extends OldBaseActivity {
    private ActivityChangePasswordBinding binding;
    ChangePasswordViewModel changePasswordViewModel;
    DialogNotice dialogNotice;
    DialogWait2 dialogWait;
    Util util;
    Activity activity;
    ChangePass changePass;
    private Pattern pattern;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.change_password);
        util = Util.getInstance();
        activity = this;
        preferenceHelper = new PreferenceHelper(this);
        changePasswordViewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel.class);
        dialogWait = new DialogWait2(this);
        binding.okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BtnChangeClick();
            }
        });
        binding.cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BackClick();
            }
        });

        binding.showHidePassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    binding.newPwdConfirm.setInputType(InputType.TYPE_CLASS_TEXT);
                    binding.newPwd.setInputType(InputType.TYPE_CLASS_TEXT);
                    binding.currentPwd.setInputType(InputType.TYPE_CLASS_TEXT);
                } else {
                    binding.newPwdConfirm.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    binding.newPwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    binding.currentPwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });

        changePasswordViewModel.getBaseResponse().observe(this, new Observer<BaseResponse>() {
            @Override
            public void onChanged(@Nullable BaseResponse baseResponse) {
                dialogWait.hide();
                if (baseResponse != null) {
                    dialogNotice = new DialogNotice(baseResponse.getStringNotice(), activity);
                    binding.newPwdConfirm.setText("");
                    binding.newPwd.setText("");
                    binding.currentPwd.setText("");
                    binding.showHidePassword.setChecked(false);
                } else {
                    dialogNotice = new DialogNotice(getString(R.string.commonErrorMsg), activity);
                }
                /*
                if (baseResponse.getStringStatus().equals("500")){
                    dialogNotice = new DialogNotice(baseResponse.getStringNotice(), activity);
                } else {
                    dialogNotice = new DialogNotice(getString(R.string.common_err), activity);
                }
                */
                dialogNotice.show();
            }

        });
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    public void BtnChangeClick() {
        if (checkPassChange()) {
            changePass = new ChangePass(null, binding.currentPwd.getText().toString(), binding.newPwd.getText().toString(), binding.newPwdConfirm.getText().toString());
            changePass.setLoginId(preferenceHelper.getString(Constant.USER_NAME, ""));
            String token = preferenceHelper.getString(Constant.TOKEN, "");
            dialogWait.show();
            changePasswordViewModel.callChangePassword(changePass, token);
        }
    }

    public void ClearPassword() {
        binding.currentPwd.setText("");
        binding.newPwd.setText("");
        binding.newPwdConfirm.setText("");
    }


    public void BackClick() {
        finish();
    }

    public boolean checkPassChange() {
        if (TextUtils.isEmpty(Objects.requireNonNull(binding.currentPwd.getText().toString()))) {
            binding.currentPwd.setError(getString(R.string.login_errorMsg_emptyPassword));
            binding.currentPwd.requestFocus();
        } else if (!(binding.currentPwd.getText().length() > 3)) {
            binding.currentPwd.setError(getString(R.string.login_errorMsg_passwordIsShort));
            binding.currentPwd.requestFocus();
        } else if (TextUtils.isEmpty(Objects.requireNonNull(binding.newPwd.getText().toString()))) {
            binding.newPwd.setError(getString(R.string.login_errorMsg_emptyPassword));
            binding.newPwd.requestFocus();
   /*     } else if (!(binding.newPwd.getText().length() > 7)) {
            binding.newPwd.setError(getString(R.string.login_errorMsg_passwordIsShort));
            binding.newPwd.requestFocus();
        } else if (TextUtils.isEmpty(Objects.requireNonNull(binding.newPwdConfirm.getText().toString()))) {
            binding.newPwdConfirm.setError(getString(R.string.login_errorMsg_emptyPassword));
            binding.newPwdConfirm.requestFocus();
        } else if (!(binding.newPwdConfirm.getText().length() > 7)) {
            binding.newPwdConfirm.setError(getString(R.string.login_errorMsg_passwordIsShort));
            binding.newPwdConfirm.requestFocus();
        } else if (!(binding.newPwdConfirm.getText().length() <= 20)) {
            binding.newPwdConfirm.setError(getString(R.string.login));
            binding.newPwdConfirm.requestFocus();*/
        } else if (!checkRulePassword(binding.newPwd.getText().toString())) {
            binding.newPwd.setError(getString(R.string.changePassword_errorMsg_passwordError));
            binding.newPwd.requestFocus();
        } else if (!checkRulePassword(binding.newPwdConfirm.getText().toString())) {
            binding.newPwdConfirm.setError(getString(R.string.changePassword_errorMsg_passwordError));
            binding.newPwdConfirm.requestFocus();
        } else if (binding.currentPwd.getText().toString().equals(binding.newPwd.getText().toString())) {
            binding.newPwd.setError(getString(R.string.changePassword_errorMsg_sameWithOld));
            binding.newPwd.requestFocus();
        } else if (!binding.newPwdConfirm.getText().toString().equals(binding.newPwd.getText().toString())) {
            binding.newPwdConfirm.setError(getString(R.string.changePassword_errorMsg_confrimNotMatch));
            binding.newPwdConfirm.requestFocus();
        } else
            return true;
        return false;
    }

    public boolean checkRulePassword(String pwd) {
        String regexp = "^(?=.*[a-zA-Z0-9]).{4,20}$";
        this.pattern = Pattern.compile(regexp);
        Matcher m = pattern.matcher(pwd);
        if (m.matches()) {
            return true;
        }
        return false;
    }
}
