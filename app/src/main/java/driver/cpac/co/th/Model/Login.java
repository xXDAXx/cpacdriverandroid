package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;

public class Login {

    @Expose
    private String loginId;
    @Expose
    private String password;
    @Expose
    private String deviceToken;
    @Expose
    private int deviceType;
    @Expose
    private int userType;



    public Login(String loginId, String password, String deviceToken, int deviceType,int userType) {
        this.loginId = loginId;
        this.password = password;
        this.setDeviceToken(deviceToken);
        this.setDeviceType(deviceType);
        this.setUserType(userType);
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getEmail() {
        return loginId;
    }

    public void setEmail(String customerId) {
        this.loginId = customerId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }
}
