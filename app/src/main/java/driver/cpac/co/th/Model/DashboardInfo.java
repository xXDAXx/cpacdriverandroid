package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DashboardInfo implements Serializable {
    @SerializedName("myJobCount")
    @Expose
    private int myJobCount;
    @SerializedName("newJobCount")
    @Expose
    private int newJobCount;
    @SerializedName("finishedJobCount")
    @Expose
    private int finishedJobCount;
    @SerializedName("rejectedJobCount")
    @Expose
    private int rejectedJobCount;
    @SerializedName("averageRating")
    @Expose
    private double averageRating;
    @SerializedName("rateCount")
    @Expose
    private int rateCount;
    @SerializedName("finishedJobWeekCount")
    @Expose
    private int finishedJobWeekCount;
    @SerializedName("finishedJobMonthCount")
    @Expose
    private int finishedJobMonthCount;
    @SerializedName("finishedJobYearCount")
    @Expose
    private int finishedJobYearCount;

    public DashboardInfo(int myJobCount, int newJobCount, int finishedJobCount, int rejectedJobCount, double averageRating, int rateCount, int finishedJobWeekCount, int finishedJobMonthCount, int finishedJobYearCount) {
        this.myJobCount = myJobCount;
        this.newJobCount = newJobCount;
        this.finishedJobCount = finishedJobCount;
        this.rejectedJobCount = rejectedJobCount;
        this.averageRating = averageRating;
        this.rateCount = rateCount;
        this.finishedJobWeekCount = finishedJobWeekCount;
        this.finishedJobMonthCount = finishedJobMonthCount;
        this.finishedJobYearCount = finishedJobYearCount;
    }

    public int getFinishedJobWeekCount() {
        return finishedJobWeekCount;
    }

    public void setFinishedJobWeekCount(int finishedJobWeekCount) {
        this.finishedJobWeekCount = finishedJobWeekCount;
    }

    public int getFinishedJobMonthCount() {
        return finishedJobMonthCount;
    }

    public void setFinishedJobMonthCount(int finishedJobMonthCount) {
        this.finishedJobMonthCount = finishedJobMonthCount;
    }

    public int getFinishedJobYearCount() {
        return finishedJobYearCount;
    }

    public void setFinishedJobYearCount(int finishedJobYearCount) {
        this.finishedJobYearCount = finishedJobYearCount;
    }

    public int getMyJobCount() {
        return myJobCount;
    }

    public void setMyJobCount(int myJobCount) {
        this.myJobCount = myJobCount;
    }

    public int getNewJobCount() {
        return newJobCount;
    }

    public void setNewJobCount(int newJobCount) {
        this.newJobCount = newJobCount;
    }

    public int getFinishedJobCount() {
        return finishedJobCount;
    }

    public void setFinishedJobCount(int finishedJobCount) {
        this.finishedJobCount = finishedJobCount;
    }

    public int getRejectedJobCount() {
        return rejectedJobCount;
    }

    public void setRejectedJobCount(int rejectedJobCount) {
        this.rejectedJobCount = rejectedJobCount;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    public int getRateCount() {
        return rateCount;
    }

    public void setRateCount(int rateCount) {
        this.rateCount = rateCount;
    }
}
