package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WaybillHistoryRes implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("type")
    @Expose
    private int type;

    @SerializedName("user")
    @Expose
    private String user;

    @SerializedName("note")
    @Expose
    private String note;

    @SerializedName("formattedCreatedDate")
    @Expose
    private String formattedCreatedDate;

    public String getFormattedCreatedDate() {
        return formattedCreatedDate;
    }

    public void setFormattedCreatedDate(String formattedCreatedDate) {
        this.formattedCreatedDate = formattedCreatedDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public WaybillHistoryRes(int id, String createdDate, int type, String user, String note, String formattedCreatedDate) {
        this.id = id;
        this.createdDate = createdDate;
        this.type = type;
        this.user = user;
        this.note = note;
        this.formattedCreatedDate = formattedCreatedDate;
    }
}
