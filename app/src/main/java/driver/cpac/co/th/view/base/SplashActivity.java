package driver.cpac.co.th.view.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import driver.cpac.co.th.Model.AppVersionCheckReq;
import driver.cpac.co.th.Model.GetMstTypeRes;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.RealmObject.AppVersionCheckRes;

import com.google.gson.Gson;
import com.tmg.th.BuildConfig;
import com.tmg.th.R;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.Home.HomeActivity;
import driver.cpac.co.th.view.MainActivity;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;
import driver.cpac.co.th.view.dialog.DialogUpdateVersion;
import driver.cpac.co.th.view.jobdetail.JobDetailActivityV3;
import driver.cpac.co.th.view.jobprocessing.JobProcessingActivityV3;

import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity implements DialogNoticeCallback.CallBackDialogNotice, DialogUpdateVersion.DialogUpdateCallback {
    private final int SPLASH_DISPLAY_LENGTH = 2000;
    private CargoLinkAPI mCargoLinkAPI;
    String jobID, notifyType;
    JobDetailResV2 jobDetail;
    Intent intent;
    String token;
    DialogNoticeCallback dialogNoticeCallback;
    DialogUpdateVersion dialogUpdateVersion;

    @Inject
    protected PreferenceHelper preferenceHelper;


    @Override
    protected void attachBaseContext(Context newBase) {
        Context context = LangUtils.wrap(newBase, new Locale(LangUtils.getCurrentLanguage()));
        super.attachBaseContext(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        //Check sharePreference
        preferenceHelper = new PreferenceHelper(this);
        LangUtils.loadLocale(this);
        intent = getIntent();
        token = preferenceHelper.getString(Constant.TOKEN, "");
        //checkVersion();

    }

    public void checkToken(String token) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.checkToken(token, LangUtils.getCurrentLanguage()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    getMstType(token);
                    /*
                    if (intent.hasExtra("notifyType")) {
                        notifyType = intent.getStringExtra("notifyType");
                        if (notifyType.equals("1") || notifyType.equals("2")) {
                            String dataStr = intent.getStringExtra("listData");
                            String[] listData = dataStr.split("\\|\\|\\|");
                            jobID = listData[0];
                            getDetailJob(token, jobID);
                        }
                    } else
                        MoveToHome();
                        */
                } else {
                    MoveToLogin();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                MoveToLogin();

            }
        });
    }

    public void checkVersion() {
        mCargoLinkAPI = ApiUtils.getAPIService();
        AppVersionCheckReq req = new AppVersionCheckReq(getPackageName().replace(".staging",""), BuildConfig.VERSION_NAME);
        mCargoLinkAPI.appVersionCheck(req).enqueue(new Callback<AppVersionCheckRes>() {
            @Override
            public void onResponse(Call<AppVersionCheckRes> call, Response<AppVersionCheckRes> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        AppVersionCheckRes res = response.body();
                        //if (res.isHaveUpdate()) {
                        if (false) {
                            if (dialogUpdateVersion == null || (dialogUpdateVersion != null && !dialogUpdateVersion.isShowing())) {
                                dialogUpdateVersion = new DialogUpdateVersion(SplashActivity.this, res.isForceUpdate(), res.getLastestVersion(), SplashActivity.this::btnCancelClick);
                                dialogUpdateVersion.show();
                            }
                        } else if (token != null && !token.equals(""))
                            checkToken(token);
                        else
                            MoveToLogin();
                    }
                } else {
                    Log.println(1, "abc", "ggg");
                }
            }

            @Override
            public void onFailure(Call<AppVersionCheckRes> call, Throwable t) {
                Log.println(1, "abc", "ggg");
            }
        });
    }

    public void getMstType(String Token) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        try {
            mCargoLinkAPI.getMstType(Token).enqueue(new Callback<GetMstTypeRes>() {
                @Override
                public void onResponse(Call<GetMstTypeRes> call, Response<GetMstTypeRes> response) {
                    if (response.isSuccessful()) {
                        GetMstTypeRes res = response.body();
                        if (res != null) {
                            // Set sharePreferences here
                            Gson gson = new Gson();
                            String json = gson.toJson(res);
                            PreferenceHelper preferenceHelper = new PreferenceHelper();
                            preferenceHelper.putString(Constant.MST_TYPE, json);
                        }
                        //if (intent.hasExtra("notifyType")) {
                        if(false) {
                            notifyType = intent.getStringExtra("notifyType");
                            if (notifyType.equals("1") || notifyType.equals("2")) {
                                String dataStr = intent.getStringExtra("listData");
                                String[] listData = dataStr.split("\\|\\|\\|");
                                jobID = listData[0];
                                getDetailJob(token, jobID);
                            }
                        } else
                            MoveToHome();
                    }
                }
                @Override
                public void onFailure(Call<GetMstTypeRes> call, Throwable t) {
                    Log.println(1, "abc", "ggg");
                }
            });

        } catch (Exception ex) {
            Log.println(0, "", ex.toString());
        }
    }

    public void MoveToHome() {
        Intent mainIntent = new Intent(SplashActivity.this, HomeActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
    }

    public void MoveToLogin() {
        Intent loginIntent = new Intent(SplashActivity.this, MainActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    public void MoveToDetailActivity() {
        Intent detailActivityIntent = new Intent(SplashActivity.this, JobDetailActivityV3.class);
        detailActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        detailActivityIntent.putExtra("content", jobDetail);
        startActivity(detailActivityIntent);
    }

    public void MoveToProcessActivity() {
        Intent detailActivityIntent = new Intent(SplashActivity.this, JobProcessingActivityV3.class);
        detailActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        detailActivityIntent.putExtra("jobDetail", jobDetail);
        startActivity(detailActivityIntent);
    }

    public void getDetailJob(String Token, String jobId) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCargoLinkAPI = ApiUtils.getAPIService();
                mCargoLinkAPI.getDetailWaybill(Token, LangUtils.getCurrentLanguage(), jobId)
                        .enqueue(new Callback<JobDetailResV2>() {
                            @Override
                            public void onResponse(Call<JobDetailResV2> call, Response<JobDetailResV2> response) {
                                if (response.body() != null) {
                                    jobDetail = (response.body());
                                    WaybillStatus status = WaybillStatus.getEnumType(jobDetail.getStatus());
                                    int userId = preferenceHelper.getInt(Constant.USER_ID, 0);
                                    if (status == WaybillStatus.waitingDriver) {
                                        MoveToDetailActivity();
                                    } else if (Util.jobBelongToUser(jobDetail, userId) && (status == WaybillStatus.driverAccepted || status == WaybillStatus.issued
                                            || status == WaybillStatus.arrived || status == WaybillStatus.loaded
                                            || status == WaybillStatus.finished)) {
                                        MoveToProcessActivity();
                                    } else if (status == WaybillStatus.cancelled || status == WaybillStatus.driverRejected
                                            || status == WaybillStatus.shipperRejected) {
                                        showRejectedNotice(status);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<JobDetailResV2> call, Throwable t) {
                            }
                        });
            }
        }, 1000);
    }

    public void showRejectedNotice(WaybillStatus status) {
        String content = "This waybill was canceled or rejected and could not be viewed.";
        dialogNoticeCallback = new DialogNoticeCallback(content, this, this);
        dialogNoticeCallback.show();
    }

    @Override
    public void BtnOkDiaComClick() {
        MoveToHome();
    }

    @Override
    public void btnCancelClick() {
        dialogUpdateVersion.dismiss();
        if (token != null && !token.equals(""))
            checkToken(token);
        else
            MoveToLogin();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (dialogUpdateVersion == null || (dialogUpdateVersion != null && !dialogUpdateVersion.isShowing())) {
            checkVersion();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dialogUpdateVersion != null) {
            dialogUpdateVersion.dismiss();
            dialogUpdateVersion = null;
        }
        if (dialogNoticeCallback != null) {
            dialogNoticeCallback.dismiss();
            dialogNoticeCallback = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (dialogUpdateVersion != null) {
            dialogUpdateVersion.dismiss();
            dialogUpdateVersion = null;
        }
        if (dialogNoticeCallback != null) {
            dialogNoticeCallback.dismiss();
            dialogNoticeCallback = null;
        }
    }
}
