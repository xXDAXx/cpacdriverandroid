package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;

public class LoginResponse {
    @Expose
    private
    int id;
    @Expose
    private
    String email;
    @Expose
    private
    String password;
    @Expose
    private
    String fullname;
    @Expose
    private
    boolean enabled;
    @Expose
    private
    String equipment;
    @Expose
    private
    String licenseNumber;
    @Expose
    private
    String licenseNumberDocument;
    @Expose
    private
    String phoneNumber;
    @Expose
    private
    String platform;
    @Expose
    private
    boolean receiveOffers;
    @Expose
    private
    int carrierId;
    @Expose
    private
    String backgroundImage;
    @Expose
    private
    boolean isDeleted;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getLicenseNumberDocument() {
        return licenseNumberDocument;
    }

    public void setLicenseNumberDocument(String licenseNumberDocument) {
        this.licenseNumberDocument = licenseNumberDocument;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public boolean isReceiveOffers() {
        return receiveOffers;
    }

    public void setReceiveOffers(boolean receiveOffers) {
        this.receiveOffers = receiveOffers;
    }

    public int getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(int carrierId) {
        this.carrierId = carrierId;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
