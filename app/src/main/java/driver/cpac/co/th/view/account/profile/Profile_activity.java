package driver.cpac.co.th.view.account.profile;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.net.Uri;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.BaseResponse;
import driver.cpac.co.th.Model.UpdateDriverInfoReq;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.view.account.avatar.AvatarActivity;
import com.tmg.th.R;
import com.tmg.th.databinding.ActivityProfileV2Binding;

import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogNotice;
import driver.cpac.co.th.view.dialog.DialogWait;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import driver.cpac.co.th.view.dialog.DialogWait2;
import jp.wasabeef.blurry.Blurry;
import jp.wasabeef.picasso.transformations.BlurTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Profile_activity extends OldBaseActivity {
    View.OnClickListener listener;
    private ActivityProfileV2Binding binding;
    public static final int REQUEST_CODE_GALLERY = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int REQUEST_CODE_CROP_IMAGE = 0x3;
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.png";
    private File mFileTemp;
    //private TruckTypeAdapter adapter;
    PreferenceHelper preferenceHelper;
    ProfileViewModel profileViewModel;
    private CargoLinkAPI mCargoLinkAPI;
    BaseResponse cPresponse;
    private int checkedLicense;

    @Inject
    public DialogWait2 dialogWait;
    DialogNotice dialogNotice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_profile);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile_v2);
        setSupportActionBar(binding.toolbar);
        cPresponse = new BaseResponse();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.common_label_myprofile);
        preferenceHelper = new PreferenceHelper(this);
        dialogWait = new DialogWait2(this);
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        profileViewModel.getUpdateSuccessfull().observe(this, aBoolean -> {
            hideLoading();
            if (!aBoolean)
                showDialogError();
            else preferenceHelper.putInt(Constant.DRIVER_LICENCE,checkedLicense);
        });
        initData();
        loadImageFromStorage();
        binding.imgAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.editbtn.getText().equals(getString(R.string.save_button))) {
                    openGallery();
                }
                //Intent mainIntent = new Intent(Profile_activity.this, AvatarActivity.class);
                //startActivity(mainIntent);
            }
        });
        String eState = Environment.getExternalStorageState();
        if (
                eState.equals(Environment.MEDIA_MOUNTED)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
        } else {
            mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }
        //
        Button editbtn = binding.editbtn;
        listener = v -> {
            if (editbtn.getText().equals(getString(R.string.edit_button))) {
                editbtn.setText(getString(R.string.save_button));
                //Enable info
                enableEditInfo();
            } else {
                if (checkUpdateInfo()) {
                    updateDriverInfo();
                    editbtn.setText(R.string.edit_button);
                    disableEditInfo();
                }
            }
        };

        editbtn.setOnClickListener(listener);

        binding.radioLicense.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == binding.rdoType1.getId())
                    checkedLicense = 1;
                else if (i == binding.rdoType2.getId())
                    checkedLicense = 2;
                else if (i == binding.rdoType3.getId())
                    checkedLicense = 3;
                else if (i == binding.rdoType4.getId())
                    checkedLicense = 4;
            }
        });

    }


    @Override

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void enableEditInfo() {
        binding.imgAvatar.setBorderColor(getResources().getColor(R.color.cargolink_yellow));
        binding.licensenumber.setEnabled(false);
        binding.mobilenumtxt.setEnabled(false);
        binding.mobilenumtxt.requestFocus();
        binding.radioLicense.setEnabled(true);
        for(int i = 0; i < binding.radioLicense.getChildCount(); i++){
            binding.radioLicense.getChildAt(i).setEnabled(true);
        }
    }

    public void disableEditInfo() {
        binding.imgAvatar.setBorderColor(getResources().getColor(R.color.cargolink_white));
        binding.licensenumber.setEnabled(false);
        //binding.emailtxt.setEnabled(false);
        binding.mobilenumtxt.setEnabled(false);
        binding.imgEdit.setVisibility(View.INVISIBLE);
        //adapter.enableCheckBox(false);
        binding.radioLicense.setEnabled(false);
        binding.radioLicense.setEnabled(true);
        for(int i = 0; i < binding.radioLicense.getChildCount(); i++){
            binding.radioLicense.getChildAt(i).setEnabled(false);
        }
    }

    public void initData() {
        disableEditInfo();
        binding.mobilenumtxt.setText(preferenceHelper.getString(Constant.PHONE_NUMBER, ""));
        binding.licensenumber.setText(preferenceHelper.getString(Constant.LICENSE_NUMBER, ""));
        binding.fullName.setText(preferenceHelper.getString(Constant.FULLNAME, ""));
        int driverLicense = preferenceHelper.getInt(Constant.DRIVER_LICENCE,1);
        switch ( driverLicense) {
            case 1:
                binding.rdoType1.setChecked(true);
                break;
            case 2:
                binding.rdoType2.setChecked(true);
                break;
            case 3:
                binding.rdoType3.setChecked(true);
                break;
            case 4:
                binding.rdoType4.setChecked(true);
                break;
        }
    }

    public void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != this.RESULT_OK) {
            return;
        }
        File file;
        Bitmap bitmap;
        switch (requestCode) {

            case REQUEST_CODE_GALLERY:
                try {
                    Uri uri = data.getData();
                    Intent intent = new Intent(this, AvatarActivity.class);
                    intent.putExtra("IMAGE_URI", uri);
                    startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
                } catch (Exception e) {
                }

                break;
            case REQUEST_CODE_TAKE_PICTURE:
                Uri uri = data.getData();
                Intent intent = new Intent(this, AvatarActivity.class);
                intent.putExtra("IMAGE_URI", getImageUri(this, uri));
                startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
                break;

            case REQUEST_CODE_CROP_IMAGE:
                uploadAvatar();
                loadImageFromStorage();
                break;
        }
        //super.onActivityResult(requestCode, resultCode, data);
    }

    public void uploadAvatar() {
        showLoading();
        ContextWrapper cw = new ContextWrapper(this.getApplicationContext());
        File directory = cw.getDir("avatars", Context.MODE_PRIVATE);
        File f = new File(directory, "avatar0.jpg");
        profileViewModel.uploadAvatar(preferenceHelper.getString(Constant.TOKEN, ""), f);
    }

    public boolean checkUpdateInfo() {
        if (binding.mobilenumtxt.length() == 0) {
            binding.mobilenumtxt.setError(getString(R.string.profileEdit_errorMsg_emptyPhone));
            binding.mobilenumtxt.requestFocus();
            return false;
        } else if (binding.licensenumber.length() == 0) {
            binding.licensenumber.setError(getString(R.string.profileEdit_errorMsg_emptyLicense));
            binding.licensenumber.requestFocus();
            return false;
        } else return true;
    }


    public void showDialogError() {
        dialogNotice = new DialogNotice(getString(R.string.commonErrorMsg), this);
        dialogNotice.show();
    }

    public void showDialogTittle(String tittle) {
        dialogNotice = new DialogNotice(tittle, this);
        dialogNotice.show();
    }

    public void saveInfoUpdated() {
        //preferenceHelper.setList(Constant.TRUCK_TYPE, getTruckType());
        preferenceHelper.putString(Constant.PHONE_NUMBER, binding.mobilenumtxt.getText().toString());
        preferenceHelper.putString(Constant.PHONE_NUMBER, binding.licensenumber.getText().toString());
        //adapter.notifyDataSetChanged();
    }

    public void updateDriverInfo() {
        dialogWait.show();
        mCargoLinkAPI = ApiUtils.getAPIService();
        UpdateDriverInfoReq updateDriverInfoReq;
        updateDriverInfoReq = new UpdateDriverInfoReq(preferenceHelper.getInt(Constant.USER_ID, 0), binding.mobilenumtxt.getText().toString(), binding.licensenumber.getText().toString(), new ArrayList<>(),checkedLicense);
        mCargoLinkAPI.updateDriverInfo(preferenceHelper.getString(Constant.TOKEN, ""), LangUtils.getCurrentLanguage(), updateDriverInfoReq).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    dialogWait.hide();
                    cPresponse.setBaseResponse(response.body());
                    showDialogTittle(cPresponse.getStringNotice());
                    saveInfoUpdated();
                    preferenceHelper.putInt(Constant.DRIVER_LICENCE,checkedLicense);
                } else {
                    dialogWait.hide();
                    showDialogError();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialogWait.hide();
                showDialogError();
            }
        });
    }

    private void loadImageFromStorage() {
        //Util.initializeSSLContext(this);
        try {
            ContextWrapper cw = new ContextWrapper(this);
            File directory = cw.getDir("avatars", Context.MODE_PRIVATE);
            File f = new File(directory, "avatar0.jpg");
            if (f.exists()) {
                Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                binding.imgAvatar.setImageBitmap(b);
                Blurry.with(this).radius(10).sampling(8).color(Color.argb(50, 0, 0, 0)).async().from(b).into(binding.imgBlur);
            } else if (preferenceHelper.getString(Constant.AVATAR, "").length() > 1) {
                App.self().getPicasso().load(preferenceHelper.getString(Constant.AVATAR, ""))
                        .placeholder(R.drawable.ic_avatar_grey).into(binding.imgAvatar);
                App.self().getPicasso().load(preferenceHelper.getString(Constant.AVATAR, ""))
                        .transform(new BlurTransformation(this, 25, 1))
                        .placeholder(R.drawable.ic_avatar_grey).into(binding.imgBlur);
            } else {
                binding.imgAvatar.setImageBitmap(BitmapFactory
                        .decodeResource(getResources(), R.drawable.ic_avatar_grey));
                Blurry.with(this).radius(10).sampling(8).color(Color.argb(50, 0, 0, 0)).async().from(BitmapFactory
                        .decodeResource(getResources(), R.drawable.ic_avatar_grey)).into(binding.imgBlur);
            }
        } catch (IOException ex) {
            // e.printStackTrace();
            binding.imgAvatar.setImageBitmap(BitmapFactory
                    .decodeResource(getResources(), R.drawable.ic_avatar_grey));
            Blurry.with(this).radius(10).sampling(8).color(Color.argb(50, 0, 0, 0)).async().from(BitmapFactory
                    .decodeResource(getResources(), R.drawable.ic_avatar_grey)).into(binding.imgBlur);

        }

    }

    public Uri resizeImage(Uri imageUri) {
        return null;
    }

    private Uri getImageUri(Context context, Uri imageUri) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            Bitmap bitmapOuput = downscaleToMaxAllowedDimension(bitmap);
            bitmapOuput.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
            String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmapOuput, "temp0", null);
            return Uri.parse(path);
        } catch (Exception ex) {
            return null;
        }
    }

    private static Bitmap downscaleToMaxAllowedDimension(Bitmap bitmap) {
        int MAX_ALLOWED_RESOLUTION = 1024;
        int outWidth;
        int outHeight;
        int inWidth = bitmap.getWidth();
        int inHeight = bitmap.getHeight();
        if (inWidth > inHeight) {
            outWidth = MAX_ALLOWED_RESOLUTION;
            outHeight = (inHeight * MAX_ALLOWED_RESOLUTION) / inWidth;
        } else {
            outHeight = MAX_ALLOWED_RESOLUTION;
            outWidth = (inWidth * MAX_ALLOWED_RESOLUTION) / inHeight;
        }
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, outWidth, outHeight, false);
        return resizedBitmap;
    }

}
