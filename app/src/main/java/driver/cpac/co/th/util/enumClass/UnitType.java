package driver.cpac.co.th.util.enumClass;

public enum UnitType {
    cartonBox(1),
    bag(2),
    bundle(3),
    can(4),
    drum(5),
    unit(6),
    bulkCargo(7),
    other(8);

    private final int value;

    private UnitType(int value) {
        this.value = value;
    }

    public short getValue() {
        return (short) value;
    }
}
