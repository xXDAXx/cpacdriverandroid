package driver.cpac.co.th.view.login;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.tmg.th.R;
import com.tmg.th.databinding.LoginLayoutBinding;
import com.tmg.th.databinding.LoginLayoutV2Binding;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import driver.cpac.co.th.Model.Login;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.view.Home.HomeActivity;
import driver.cpac.co.th.view.MainActivity;
import driver.cpac.co.th.view.account.changePassword.ChangePasswordActivity;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogChangeLanguage;
import driver.cpac.co.th.view.dialog.DialogNotice;
import driver.cpac.co.th.view.dialog.DialogWait;
import driver.cpac.co.th.view.dialog.DialogWait2;
import driver.cpac.co.th.view.dialog.ProgressDialogFragment;
import driver.cpac.co.th.view.login.old.ForgotPassword_Fragment;
import okhttp3.ResponseBody;

public class Login_Fragment extends Fragment implements DialogChangeLanguage.DialogDissmiss {
    private LoginViewModel loginViewModel;
    private LoginLayoutV2Binding binding;
    public Fragment thisFragment;
    private static View view;
    DialogChangeLanguage dialogChangeLanguage;
    private DialogWait2 dialogWait;
    private DialogNotice dialogNotice;
    private static LinearLayout thislayout;
    private String deviceToken;
    PreferenceHelper preferenceHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.login_layout_v2, container, false);
        view = binding.getRoot();
        thisFragment = this;
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        binding = DataBindingUtil.setContentView(this.getActivity(), R.layout.login_layout_v2);
        binding.setLifecycleOwner(this);
        preferenceHelper = new PreferenceHelper(getContext());
        dialogChangeLanguage = new DialogChangeLanguage((OldBaseActivity)getActivity(), this);
        binding.imgLang.setOnClickListener(v -> dialogChangeLanguage.show());
        binding.ccp.setCustomMasterCountries("th,vn");
        //init layout
        thislayout = (LinearLayout) this.getActivity().findViewById(R.id.Rlogin_layout);
        dialogWait = new DialogWait2(getActivity());

        binding.btnLogin.setOnClickListener(view1 -> loginClick());

        binding.activeBtn.setOnClickListener(view1 -> activeClick());

        binding.forgotPassword.setOnClickListener(view1 -> forgotClick());

        loginViewModel.getResponseDownloadAvatar().observe(this, responseBody -> saveBitmapToStorage(responseBody));

        loginViewModel.getLoginSuccess().observe(this, aBoolean -> {
            if (aBoolean == false) {
                dialogWait.hide();
                dialogNotice = new DialogNotice(getString(R.string.login_errorMsg_loginFailed), getActivity());
                dialogNotice.show();
            } else {
                moveToHome();
            }
        });

        loginViewModel.getErrorActiveRes().observe(this,activeErrMsg -> {
            dialogWait.hide();
            dialogNotice = new DialogNotice(activeErrMsg.getMessage(), getActivity());
            dialogNotice.show();
        });

        binding.showHidePassword.setOnCheckedChangeListener((compoundButton, b) -> {
            if(b) binding.password.setInputType(InputType.TYPE_CLASS_TEXT);
            else binding.password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        });

        // FireBase
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w(ProgressDialogFragment.TAG, "getInstanceId failed", task.getException());
                        return;
                    }
                    // Get new Instance ID token
                    deviceToken = task.getResult().getToken();
                    Log.d(ProgressDialogFragment.TAG, deviceToken);
                });
        return view;
    }

    @Override
    public void RefreshContent() {
        binding.btnLogin.setText(R.string.logout_btn);
        binding.forgotPassword.setText(R.string.forgot);
        binding.showHidePassword.setText(R.string.show_pwd);
        binding.phoneNumber.setHint(R.string.mobileNumber);
        binding.password.setHint(R.string.password);
        binding.lblLogin.setText(R.string.login);
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void saveBitmapToStorage(ResponseBody body) {
        try {
            if (body != null)
                dialogWait.hide();
                Dexter.withActivity(thisFragment.getActivity())
                        .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener() {
                                          @Override
                                          public void onPermissionGranted(PermissionGrantedResponse response) {
                                              ContextWrapper cw = new ContextWrapper(getContext());
                                              // path to /data/data/yourapp/app_data/imageDir
                                              File directory = cw.getDir("avatars", Context.MODE_PRIVATE);
                                              // Create imageDir
                                              File mypath = new File(directory, "avatar0.jpg");
                                              try {
                                                  mypath.createNewFile();
                                              } catch (IOException e) {
                                                  e.printStackTrace();
                                              }
                                              FileOutputStream fos = null;
                                              try {
                                                  InputStream in = body.byteStream();
                                                  fos = new FileOutputStream(mypath);
                                                  // Use the compress method on the BitMap object to write image to the OutputStream
                                                  int c;

                                                  while ((c = in.read()) != -1) {
                                                      fos.write(c);
                                                  }
                                              } catch (Exception e) {
                                                  e.printStackTrace();
                                              } finally {
                                                  try {
                                                      fos.close();
                                                  } catch (IOException ex) {
                                                  }
                                              }
                                          }

                                          @Override
                                          public void onPermissionDenied(PermissionDeniedResponse response) {
                                              Log.println(1, "abc", "permission denied");
                                          }

                                          @Override
                                          public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                              Log.println(1, "abc", "onPermissionRationaleShouldBeShown");
                                          }
                                      }
                        ).check();
            Context ctx = getContext();
            Intent myIntent;
            myIntent = new Intent(ctx, HomeActivity.class);
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(myIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loginClick() {
        //hide keyboard
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(thislayout.getWindowToken(), 0);
        //
        if (TextUtils.isEmpty(Objects.requireNonNull(binding.phoneNumber.getText()))) {
            binding.phoneNumber.setError(getString(R.string.login_errorMsg_emptyMobileNumber));
            binding.phoneNumber.requestFocus();
        } else if (TextUtils.isEmpty(Objects.requireNonNull(binding.password.getText()))) {
            binding.password.setError(getString(R.string.login_errorMsg_emptyPassword));
            binding.password.requestFocus();
        } else if (!Util.checkRulePassword(binding.password.getText().toString())) {
            binding.password.setError(getString(R.string.changePassword_errorMsg_passwordError));
            binding.password.requestFocus();
        } else {
            dialogWait.show();
            loginViewModel.validLogin(new Login(binding.ccp.getFullNumberWithPlus() + binding.phoneNumber.getText().toString(), binding.password.getText().toString(), deviceToken,1,2));
        }
    }

    public void activeClick() {
        FragmentTransaction fragmentTransaction;
        fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        ActiveUserFragment fragment = new ActiveUserFragment();
        fragment.mode = ActiveUserActivity.ActiveMode.ACTIVE_MODE;
        fragmentTransaction.replace(R.id.Rlogin_layout, fragment).commit();
        fragmentTransaction.addToBackStack(null);
    }

    public void forgotClick() {
        FragmentTransaction fragmentTransaction;
        fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        ActiveUserFragment fragment = new ActiveUserFragment();
        fragment.mode = ActiveUserActivity.ActiveMode.FORGOT_PASSWORD_MODE;
        fragmentTransaction.replace(R.id.Rlogin_layout, fragment).commit();
        fragmentTransaction.addToBackStack(null);
    }

    public void moveToHome() {
        Context ctx = getContext();
        Intent myIntent;
        myIntent = new Intent(ctx, HomeActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        dialogWait.hide();
        startActivity(myIntent);
    }
}