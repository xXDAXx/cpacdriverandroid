package driver.cpac.co.th.customComponent;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.tmg.th.R;
import com.tmg.th.databinding.CardviewFinishedJobsBinding;
import com.tmg.th.databinding.SearchBoxAdvanceBinding;

public class SearchAdvancedView extends ConstraintLayout {
    public SearchBoxAdvanceBinding binding;
    Context context;

    public int getColorBgrHighlighted() {
        return colorBgrHighlighted;
    }

    public void setColorBgrHighlighted(int colorBgrHighlighted) {
        this.colorBgrHighlighted = colorBgrHighlighted;
    }

    public int getColorBgr() {
        return colorBgr;
    }

    public void setColorBgr(int colorBgr) {
        this.colorBgr = colorBgr;
    }

    public int getColortxtHighlighted() {
        return colortxtHighlighted;
    }

    public void setColortxtHighlighted(int colortxtHighlighted) {
        this.colortxtHighlighted = colortxtHighlighted;
    }

    public int getColortxt() {
        return colortxt;
    }

    public void setColortxt(int colortxt) {
        this.colortxt = colortxt;
    }

    public int getWeekCounter() {
        return weekCounter;
    }

    public void setWeekCounter(int weekCounter) {
        this.weekCounter = weekCounter;
    }

    public int getMonthCounter() {
        return monthCounter;
    }

    public void setMonthCounter(int monthCounter) {
        this.monthCounter = monthCounter;
    }

    public int getYearCounter() {
        return yearCounter;
    }

    public void setYearCounter(int yearCounter) {
        this.yearCounter = yearCounter;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    int colorBgrHighlighted;
    int colorBgr;
    int colortxtHighlighted;
    int colortxt;
    int weekCounter = 0;
    int monthCounter = 1;
    int yearCounter = 2;
    int mode = 1;
    int curentCounter = 0;

    public SearchAdvancedView(Context context) {
        super(context);
        this.context = context;
    }

    public SearchAdvancedView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
        setupComponents(attrs);
    }

    private void init() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(infService);
        binding = SearchBoxAdvanceBinding.inflate(layoutInflater, this, true);
    }

    private void setupComponents(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CardViewFinshedJob);
        setColorBgr(a.getInt(R.styleable.CardViewFinshedJob_colorBgr, 0));
        setColorBgrHighlighted(a.getInt(R.styleable.CardViewFinshedJob_colorBgrHighlighted, 0));
        setColortxt(a.getInt(R.styleable.CardViewFinshedJob_colortxt, 0));
        setColortxtHighlighted(a.getInt(R.styleable.CardViewFinshedJob_colortxtHighlighted, 0));
        setWeekCounter(a.getInt(R.styleable.CardViewFinshedJob_weekCounter, 0));
        setMonthCounter(a.getInt(R.styleable.CardViewFinshedJob_monthCounter, 0));
        setYearCounter(a.getInt(R.styleable.CardViewFinshedJob_yearCounter, 0));
        setMode(a.getInt(R.styleable.CardViewFinshedJob_mode, 1));
        //Setup();
    }

    /*public void Setup() {
        switch (mode) {
            case 1: //week
                animateTextView(curentCounter, weekCounter, binding.counterFinished);
                curentCounter = weekCounter;
                binding.lblWeek.setTextColor(getColortxtHighlighted());
                binding.lblYear.setTextColor(getColortxt());
                binding.lblMonth.setTextColor(getColortxt());
                binding.lblWeek.setBackgroundColor(getColorBgrHighlighted());
                binding.lblYear.setBackgroundColor(getColorBgr());
                binding.lblMonth.setBackgroundColor(getColorBgr());
                break;
            case 2: //month
                animateTextView(curentCounter, monthCounter, binding.counterFinished);
                curentCounter = monthCounter;
                binding.counterFinished.setText(String.valueOf(monthCounter));
                binding.lblWeek.setTextColor(getColortxt());
                binding.lblYear.setTextColor(getColortxt());
                binding.lblMonth.setTextColor(getColortxtHighlighted());
                binding.lblWeek.setBackgroundColor(getColorBgr());
                binding.lblYear.setBackgroundColor(getColorBgr());
                binding.lblMonth.setBackgroundColor(getColorBgrHighlighted());
                break;
            case 3: //years
                animateTextView(curentCounter, yearCounter, binding.counterFinished);
                curentCounter = yearCounter;
                binding.counterFinished.setText(String.valueOf(yearCounter));
                binding.lblWeek.setTextColor(getColortxt());
                binding.lblYear.setTextColor(getColortxtHighlighted());
                binding.lblMonth.setTextColor(getColortxt());
                binding.lblWeek.setBackgroundColor(getColorBgr());
                binding.lblYear.setBackgroundColor(getColorBgrHighlighted());
                binding.lblMonth.setBackgroundColor(getColorBgr());
                break;
            default:
                break;

        }
    }*/

    public void animateTextView(int initialValue, int finalValue, final TextView textview) {
        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(500);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                textview.setText(valueAnimator.getAnimatedValue().toString());
            }
        });
        valueAnimator.start();

    }
}
