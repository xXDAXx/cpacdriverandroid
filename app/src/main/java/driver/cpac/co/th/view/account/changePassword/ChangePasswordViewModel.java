package driver.cpac.co.th.view.account.changePassword;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.JsonObject;
import driver.cpac.co.th.Model.BaseResponse;
import driver.cpac.co.th.Model.ChangePass;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.LangUtils;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordViewModel  extends ViewModel {
    @Inject
    public ChangePasswordViewModel() {

    }
    private CargoLinkAPI mCargoLinkAPI;
    private MutableLiveData<ChangePass> changePassParam = new MutableLiveData<>();
    public MutableLiveData<BaseResponse> baseResponse;

    public void changePassCall(ChangePass pwdChange) {
        changePassParam.postValue(pwdChange);
    }

    public MutableLiveData<BaseResponse> getBaseResponse() {

        if (baseResponse == null) {
            baseResponse = new MutableLiveData<>();
        }
        return baseResponse;
    }

    //Call API
    public void callChangePassword(ChangePass changePass, String Token) {
        //set user login
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.changepassword(Token, LangUtils.getCurrentLanguage(), new ChangePass(changePass.getLoginId(),
                changePass.getCurrentPassword(), changePass.getNewPassword(), changePass.getConfirmPassword()))
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (response.isSuccessful()) {
                            BaseResponse cPresponse = new BaseResponse();
                            cPresponse.setBaseResponse(response.body());
                            baseResponse.setValue(cPresponse);
                        } else {
                            BaseResponse cPresponse = new BaseResponse();
                            //cPresponse.setErrorResponse(response.errorBody());
                            //baseResponse.setValue(cPresponse);
                            baseResponse.setValue(null);
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        baseResponse.setValue(null);
                    }
                });

    }

}