package driver.cpac.co.th.view.MailBox;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import driver.cpac.co.th.Model.DriverNotificationRes;
import driver.cpac.co.th.Model.JobDetailResV2;
import com.tmg.th.R;
import com.tmg.th.databinding.ActivityNotificationDetailBinding;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.NotificationType;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;
import driver.cpac.co.th.view.jobdetail.JobDetailActivityV3;
import driver.cpac.co.th.view.jobprocessing.JobProcessingActivityV3;

public class NotificationDetailActivity extends OldBaseActivity implements DialogNoticeCallback.CallBackDialogNotice {
    private ActivityNotificationDetailBinding binding;
    private MailboxViewModel viewModel;
    private JobDetailResV2 shipmentInfo;
    private DriverNotificationRes driverNotificationRes;
    DialogNoticeCallback dialogNoticeCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification_detail);
        setSupportActionBar(binding.appBarLayout.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        driverNotificationRes = (DriverNotificationRes) getIntent().getSerializableExtra("notify");
        //getSupportActionBar().setTitle(shipmentInfo.getId().toString() + " - " + shipmentInfo.getShipperName().toUpperCase());
        preferenceHelper = new PreferenceHelper(this);
        //
        BindingNotify();
        binding.btnAction.setVisibility(View.INVISIBLE);
        viewModel = ViewModelProviders.of(this).get(MailboxViewModel.class);
        viewModel.getDetailJob(preferenceHelper.getString(Constant.TOKEN, ""), driverNotificationRes.getWaybillHashId());

        OserverSetup();
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void OserverSetup() {
        viewModel.getjobDetailLiveData().observe(this, jobDetailResV2 -> {
            shipmentInfo = jobDetailResV2;
            /*
            if (jobDetailResV2.getStatus() == WaybillStatus.waitingDriver.getValue()) {
                binding.btnAction.setVisibility(View.VISIBLE);
                binding.btnAction.setOnClickListener(v->{
                    Intent intent = new Intent(this, JobDetailActivityV3.class);
                    intent.putExtra("content", shipmentInfo);
                    startActivity(intent);
                });
            } else if (jobDetailResV2.getStatus() != WaybillStatus.waitingDriver.getValue()) {
                binding.btnAction.setVisibility(View.VISIBLE);
                binding.btnAction.setOnClickListener(v->{
                    Intent intent = new Intent(this, JobProcessingActivityV3.class);
                    intent.putExtra("jobDetail", shipmentInfo);
                    startActivity(intent);
                });
            }
            */
            binding.btnAction.setVisibility(View.VISIBLE);
            WaybillStatus status = WaybillStatus.getEnumType(shipmentInfo.getStatus());
            int userId = preferenceHelper.getInt(Constant.USER_ID,0);
            if(status == WaybillStatus.waitingDriver) {
                binding.btnAction.setOnClickListener(v-> {MoveToDetailActivity();});
            } else if(Util.jobBelongToUser(shipmentInfo,userId) && (status == WaybillStatus.driverAccepted || status == WaybillStatus.issued
                    || status == WaybillStatus.arrived || status == WaybillStatus.loaded
                    || status == WaybillStatus.finished)) {
                binding.btnAction.setOnClickListener(v-> {MoveToProcessActivity();});
            } else if( status == WaybillStatus.cancelled || status == WaybillStatus.driverRejected
                    || status == WaybillStatus.shipperRejected) {
                binding.btnAction.setOnClickListener(v-> {showRejectedNotice(status);});
            }
        });
    }

    public void BindingNotify() {
        NotificationType type = NotificationType.getEnumType(driverNotificationRes.getType());
        switch (type) {
            case jobNotification:
                binding.titleNotify.setText(driverNotificationRes.getSummary());
                binding.contentBody.setText(driverNotificationRes.getContent());
                binding.webview.setVisibility(View.GONE);
                binding.btnAction.setText("VIEW DETAIL");
                break;
            case systemNotification:


                break;
            case others:


                break;
        }
    }

    public void MoveToDetailActivity() {
        Intent detailActivityIntent = new Intent(this, JobDetailActivityV3.class);
        detailActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        detailActivityIntent.putExtra("content",shipmentInfo );
        startActivity(detailActivityIntent);
    }

    public void MoveToProcessActivity() {
        Intent detailActivityIntent = new Intent(this, JobProcessingActivityV3.class);
        detailActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        detailActivityIntent.putExtra("jobDetail",shipmentInfo );
        startActivity(detailActivityIntent);
    }

    public void showRejectedNotice(WaybillStatus status) {
        String content = "This waybill was canceled or rejected and could not be viewed.";
        dialogNoticeCallback = new DialogNoticeCallback(content,this,this);
    }

    @Override
    public void BtnOkDiaComClick() {
        onBackPressed();
    }
}
