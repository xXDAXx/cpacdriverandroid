package driver.cpac.co.th.view.dashboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import driver.cpac.co.th.App;
import com.tmg.th.databinding.ItemNewsLayoutBinding;


import java.util.List;

public class NewAdapter extends RecyclerView.Adapter<NewAdapter.MyViewHolder> {
    protected List<String> listImg;
    private Context context;
    private ItemNewsLayoutBinding binding;

    public NewAdapter(Context context, List<String> list) {
        this.context = context;
        this.listImg = list;
    }

    @NonNull
    @Override
    public NewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemNewsLayoutBinding itemBinding =
                ItemNewsLayoutBinding.inflate(layoutInflater, viewGroup, false);
        return new NewAdapter.MyViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull NewAdapter.MyViewHolder viewHolder, int position) {
        String imgUrl =listImg.get(position);
        NewAdapter.MyViewHolder myHolder = (NewAdapter.MyViewHolder) viewHolder;
        viewHolder.bind(imgUrl, position);
    }

    @Override
    public int getItemCount() {
        return listImg == null ? 0 : listImg.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemNewsLayoutBinding binding;

        public MyViewHolder(ItemNewsLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(String url, int position) {
            App.self().getPicasso().load(url).into(binding.photoNews);
        }
    }
}

