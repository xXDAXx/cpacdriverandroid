package driver.cpac.co.th.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.databinding.DataBindingUtil;

import com.tmg.th.R;
import com.tmg.th.databinding.DialogRejectReasonBinding;

public class DialogRejectReason extends Dialog {
    public Activity activity;
    public Dialog dialog;
    public String stringContent;
    private DialogRejectReasonBinding binding;
    CallBackDialogReject callBackDialogReject;

    public DialogRejectReason(Activity a, CallBackDialogReject callBackDialogReject) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
        this.callBackDialogReject =callBackDialogReject;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_reject_reason, (ViewGroup) activity.findViewById(android.R.id.content), false);
        setContentView(binding.getRoot());

        binding.rdoGroup.setOnCheckedChangeListener((group, checkedId) -> {
            if(binding.rdo1.isChecked()) stringContent = binding.rdo1.getText().toString();
            if(binding.rdo2.isChecked()) stringContent = binding.rdo1.getText().toString();
            if(binding.rdo3.isChecked()) stringContent = binding.rdo1.getText().toString();

        });

        binding.okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.rdoOther.isChecked())
                    stringContent = binding.inputOther.getText().toString();
                else stringContent = binding.inputReason.getText().toString();
                callBackDialogReject.BtnRejectSubmitClick(stringContent);
                dismiss();
            }
        });
        binding.cancelBtn.setOnClickListener(v -> {dismiss();});
        binding.rdoOther.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked) {
                binding.inputOther.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                binding.inputOther.requestLayout();
                binding.inputOther.setVisibility(View.VISIBLE);
            }else{
                //binding.inputOther.setHeight(0);
                binding.inputOther.setVisibility(View.GONE);
            }
        });
    }

    public interface CallBackDialogReject {
        void BtnRejectSubmitClick(String reason);
    }
}
