package driver.cpac.co.th.util.enumClass;

import android.content.Context;

public enum WaybillHistoryStatus {
    waybillDraft(0),
    waybillWaitingfordriver(1),
    waybillDriveraccepted(2),
    waybillIssued(3),
    waybillArrived(4),
    waybillLoaded(5),
    waybillFinished(6),
    waybillCancelled(7),
    waybillRejected(8),
    shipperRejected(9),
    waybillStarted(10);
    private final int value;

    private WaybillHistoryStatus(int value) {
        this.value = value;
    }

    public short getValue() {
        return (short) value;
    }

    public static WaybillHistoryStatus getEnumType(int value) {
        for(WaybillHistoryStatus type: WaybillHistoryStatus.values()){
            if (type.getValue()==value) return type;
        }
        return null;
    }

    public static String GetWaybillHistoryStatus(int status, Context context) {
        String returnStatus = null;
        for (WaybillHistoryStatus sts : WaybillHistoryStatus.values()) {
            if (status == sts.getValue()) {
                String prefix = "carrier.menu.";
                String resName = prefix + sts.name();
                int id = context.getResources().getIdentifier(resName, "string", context.getPackageName());
                return context.getString(id);
            }
        }
        return returnStatus;
    }
}