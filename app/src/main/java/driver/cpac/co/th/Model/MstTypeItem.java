package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MstTypeItem implements Serializable {
    @SerializedName("type")
    @Expose
    private int type;

    @SerializedName("imgUrl")
    @Expose
    private String imgUrl;

    @SerializedName("nameEng")
    @Expose
    private String nameEng;

    @SerializedName("nameThai")
    @Expose
    private String nameThai;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public String getNameThai() {
        return nameThai;
    }

    public void setNameThai(String nameThai) {
        this.nameThai = nameThai;
    }

    public MstTypeItem(int type, String imgUrl, String nameEng, String nameThai) {
        this.type = type;
        this.imgUrl = imgUrl;
        this.nameEng = nameEng;
        this.nameThai = nameThai;
    }
}
