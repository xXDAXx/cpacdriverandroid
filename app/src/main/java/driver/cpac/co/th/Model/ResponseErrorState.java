package driver.cpac.co.th.Model;

public enum ResponseErrorState {
    NO_CONNECTION,
    BAD_URL,
    TIME_OUT,
    UNKNOWN,
    UNAUTHORIZED,
    SERVER_NO_CONNECTION
}