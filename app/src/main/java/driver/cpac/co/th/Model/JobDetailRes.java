package driver.cpac.co.th.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobDetailRes implements Serializable, Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("hashId")
    @Expose
    private Integer hashId;
    @SerializedName("addressOrg")
    @Expose
    private String addressOrg;
    @SerializedName("addressDest")
    @Expose
    private String addressDest;
    @SerializedName("shipperName")
    @Expose
    private String shipperName;
    @SerializedName("fullnameOrg")
    @Expose
    private String fullnameOrg;
    @SerializedName("fullnameDest")
    @Expose
    private String fullnameDest;
    @SerializedName("phoneOrg")
    @Expose
    private String phoneOrg;
    @SerializedName("phoneDest")
    @Expose
    private String phoneDest;
    @SerializedName("timeFromPickupOrg")
    @Expose
    private String timeFromPickupOrg;
    @SerializedName("timeToPickupOrg")
    @Expose
    private String timeToPickupOrg;
    @SerializedName("timeFromDeliveryOrg")
    @Expose
    private String timeFromDeliveryOrg;
    @SerializedName("timeToDeliveryOrg")
    @Expose
    private String timeToDeliveryOrg;
    @SerializedName("nameOfCargo")
    @Expose
    private String nameOfCargo;
    @SerializedName("typeOfCargo")
    @Expose
    private Integer typeOfCargo;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("unit")
    @Expose
    private short unit;
    @SerializedName("totalWeight")
    @Expose
    private Integer totalWeight;
    @SerializedName("sizeOfCargo")
    @Expose
    private String sizeOfCargo;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("handlingInstruction")
    @Expose
    private String handlingInstruction;
    @SerializedName("truckType")
    @Expose
    private int truckType;
    @SerializedName("truckRegistrationNumber")
    @Expose
    private String truckRegistrationNumber;
    @SerializedName("documents")
    @Expose
    private List<Documents> documents;
    @SerializedName("primaryDriver")
    @Expose
    private DriverInJob primaryDriver;
    @SerializedName("secondaryDriver")
    @Expose
    private DriverInJob secondaryDriver;
    @SerializedName("status")
    @Expose
    private short status;
    @SerializedName("shipperAvatarUrl")
    @Expose
    private String shipperAvatarUrl;
    @SerializedName("primaryDriverAccepted")
    @Expose
    private boolean primaryDriverAccepted;
    @SerializedName("secondaryDriverAccepted")
    @Expose
    private boolean secondaryDriverAccepted;

    public Integer getHashId() {
        return hashId;
    }

    public void setHashId(Integer hashId) {
        this.hashId = hashId;
    }

    public boolean getPrimaryDriverAccepted() {
        return primaryDriverAccepted;
    }

    public void setPrimaryDriverAccepted(boolean primaryDriverAccepted) {
        this.primaryDriverAccepted = primaryDriverAccepted;
    }

    public boolean getSecondaryDriverAccepted() {
        return secondaryDriverAccepted;
    }

    public void setSecondaryDriverAccepted(boolean secondaryDriverAccepted) {
        this.secondaryDriverAccepted = secondaryDriverAccepted;
    }

    public String getShipperAvatarUrl() {
        return shipperAvatarUrl;
    }

    public void setShipperAvatarUrl(String shipperAvatarUrl) {
        this.shipperAvatarUrl = shipperAvatarUrl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddressOrg() {
        return addressOrg;
    }

    public void setAddressOrg(String addressOrg) {
        this.addressOrg = addressOrg;
    }

    public String getAddressDest() {
        return addressDest;
    }

    public void setAddressDest(String addressDest) {
        this.addressDest = addressDest;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public String getFullnameOrg() {
        return fullnameOrg;
    }

    public void setFullnameOrg(String fullnameOrg) {
        this.fullnameOrg = fullnameOrg;
    }

    public String getFullnameDest() {
        return fullnameDest;
    }

    public void setFullnameDest(String fullnameDest) {
        this.fullnameDest = fullnameDest;
    }

    public String getPhoneOrg() {
        return phoneOrg;
    }

    public void setPhoneOrg(String phoneOrg) {
        this.phoneOrg = phoneOrg;
    }

    public String getPhoneDest() {
        return phoneDest;
    }

    public void setPhoneDest(String phoneDest) {
        this.phoneDest = phoneDest;
    }

    public String getTimeFromPickupOrg() {
        return timeFromPickupOrg;
    }

    public void setTimeFromPickupOrg(String timeFromPickupOrg) {
        this.timeFromPickupOrg = timeFromPickupOrg;
    }

    public String getTimeToPickupOrg() {
        return timeToPickupOrg;
    }

    public void setTimeToPickupOrg(String timeToPickupOrg) {
        this.timeToPickupOrg = timeToPickupOrg;
    }

    public String getTimeFromDeliveryOrg() {
        return timeFromDeliveryOrg;
    }

    public void setTimeFromDeliveryOrg(String timeFromDeliveryOrg) {
        this.timeFromDeliveryOrg = timeFromDeliveryOrg;
    }

    public String getTimeToDeliveryOrg() {
        return timeToDeliveryOrg;
    }

    public void setTimeToDeliveryOrg(String timeToDeliveryOrg) {
        this.timeToDeliveryOrg = timeToDeliveryOrg;
    }

    public String getNameOfCargo() {
        return nameOfCargo;
    }

    public void setNameOfCargo(String nameOfCargo) {
        this.nameOfCargo = nameOfCargo;
    }

    public Integer getTypeOfCargo() {
        return typeOfCargo;
    }

    public void setTypeOfCargo(Integer typeOfCargo) {
        this.typeOfCargo = typeOfCargo;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public short getUnit() {
        return unit;
    }

    public void setUnit(short unit) {
        this.unit = unit;
    }

    public Integer getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Integer totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getSizeOfCargo() {
        return sizeOfCargo;
    }

    public void setSizeOfCargo(String sizeOfCargo) {
        this.sizeOfCargo = sizeOfCargo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getHandlingInstruction() {
        return handlingInstruction;
    }

    public void setHandlingInstruction(String handlingInstruction) {
        this.handlingInstruction = handlingInstruction;
    }

    public int getTruckType() {
        return truckType;
    }

    public void setTruckType(int truckType) {
        this.truckType = truckType;
    }

    public String getTruckRegistrationNumber() {
        return truckRegistrationNumber;
    }

    public void setTruckRegistrationNumber(String truckRegistrationNumber) {
        this.truckRegistrationNumber = truckRegistrationNumber;
    }

    public List<Documents> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Documents> documents) {
        this.documents = documents;
    }

    public DriverInJob getPrimaryDriver() {
        return primaryDriver;
    }

    public void setPrimaryDriver(DriverInJob primaryDriver) {
        this.primaryDriver = primaryDriver;
    }

    public DriverInJob getSecondaryDriver() {
        return secondaryDriver;
    }

    public void setSecondaryDriver(DriverInJob secondaryDriver) {
        this.secondaryDriver = secondaryDriver;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.addressOrg);
        dest.writeString(this.addressDest);
        dest.writeString(this.shipperName);
        dest.writeString(this.fullnameOrg);
        dest.writeString(this.fullnameDest);
        dest.writeString(this.phoneOrg);
        dest.writeString(this.phoneDest);
        dest.writeString(this.timeFromPickupOrg);
        dest.writeString(this.timeToPickupOrg);
        dest.writeString(this.timeFromDeliveryOrg);
        dest.writeString(this.timeToDeliveryOrg);
        dest.writeString(this.nameOfCargo);
        dest.writeValue(this.typeOfCargo);
        dest.writeValue(this.quantity);
        dest.writeInt(this.unit);
        dest.writeValue(this.totalWeight);
        dest.writeString(this.sizeOfCargo);
        dest.writeString(this.photo);
        dest.writeString(this.handlingInstruction);
        dest.writeInt(this.truckType);
        dest.writeString(this.truckRegistrationNumber);
        dest.writeList(this.documents);
        dest.writeParcelable(this.primaryDriver, flags);
        dest.writeParcelable(this.secondaryDriver, flags);
        dest.writeInt(this.status);
        dest.writeString(this.shipperAvatarUrl);
        dest.writeByte(this.primaryDriverAccepted ? (byte) 1 : (byte) 0);
        dest.writeByte(this.secondaryDriverAccepted ? (byte) 1 : (byte) 0);
    }

    public JobDetailRes() {
    }

    protected JobDetailRes(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.addressOrg = in.readString();
        this.addressDest = in.readString();
        this.shipperName = in.readString();
        this.fullnameOrg = in.readString();
        this.fullnameDest = in.readString();
        this.phoneOrg = in.readString();
        this.phoneDest = in.readString();
        this.timeFromPickupOrg = in.readString();
        this.timeToPickupOrg = in.readString();
        this.timeFromDeliveryOrg = in.readString();
        this.timeToDeliveryOrg = in.readString();
        this.nameOfCargo = in.readString();
        this.typeOfCargo = (Integer) in.readValue(Integer.class.getClassLoader());
        this.quantity = (Integer) in.readValue(Integer.class.getClassLoader());
        this.unit = (short) in.readInt();
        this.totalWeight = (Integer) in.readValue(Integer.class.getClassLoader());
        this.sizeOfCargo = in.readString();
        this.photo = in.readString();
        this.handlingInstruction = in.readString();
        this.truckType = in.readInt();
        this.truckRegistrationNumber = in.readString();
        this.documents = new ArrayList<Documents>();
        in.readList(this.documents, Documents.class.getClassLoader());
        this.primaryDriver = in.readParcelable(DriverInJob.class.getClassLoader());
        this.secondaryDriver = in.readParcelable(DriverInJob.class.getClassLoader());
        this.status = (short) in.readInt();
        this.shipperAvatarUrl = in.readString();
        this.primaryDriverAccepted = in.readByte() != 0;
        this.secondaryDriverAccepted = in.readByte() != 0;
    }

    public static final Creator<JobDetailRes> CREATOR = new Creator<JobDetailRes>() {
        @Override
        public JobDetailRes createFromParcel(Parcel source) {
            return new JobDetailRes(source);
        }

        @Override
        public JobDetailRes[] newArray(int size) {
            return new JobDetailRes[size];
        }
    };
}
