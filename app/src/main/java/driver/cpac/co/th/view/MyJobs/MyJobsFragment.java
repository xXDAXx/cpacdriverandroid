package driver.cpac.co.th.view.MyJobs;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tmg.th.R;
import com.tmg.th.databinding.FragmentMyjobsBinding;

import java.util.ArrayList;
import java.util.List;

import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.view.Home.HomeActivity;
import driver.cpac.co.th.view.dialog.DialogNewJob;
import driver.cpac.co.th.view.dialog.DialogNotice;
import driver.cpac.co.th.view.dialog.DialogWait;
import driver.cpac.co.th.view.dialog.DialogWait2;
import driver.cpac.co.th.view.jobprocessing.JobProcessingActivityV3;

public class MyJobsFragment extends Fragment {
    private MyJobsViewModel myJobsViewModel;
    private static View view;
    private FragmentMyjobsBinding binding;
    private DialogWait2 dialogWait;
    private DialogNotice dialogNotice;
    private ListJobAdapterV2 adapter;
    private List<JobDetailResV2> shipmentInfoList = new ArrayList<>();
    private int i = 0;
    private HomeActivity parent;
    PreferenceHelper preferenceHelper;
    private JobDetailResV2 jobDetailRes;
    private JobDetailResV2 shipmentInfo;
    boolean clicked;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        preferenceHelper = new PreferenceHelper(getContext());
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_myjobs, container, false);
        view = binding.getRoot();
        myJobsViewModel = ViewModelProviders.of(this).get(MyJobsViewModel.class);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.rcv.setVisibility(View.INVISIBLE);
        parent = (HomeActivity) getActivity();
        binding.swipeContainer.setOnRefreshListener(() -> myJobsViewModel.getMyJobs(preferenceHelper.getString(Constant.TOKEN, "")));
        myJobsViewModel.getlistJobRes().removeObservers(this);
        myJobsViewModel.getShowLoading().removeObservers(this);
        myJobsViewModel.getShowEmpty().removeObservers(this);
        myJobsViewModel.getjobDetailLiveData().removeObservers(this);
        myJobsViewModel.getlistJobRes().observe(this, listJobRes -> {
            hideLoading();
            if (listJobRes != null && listJobRes.size() > 0) {
                shipmentInfoList = listJobRes;
                //if (parent.getDashBoard_frag().getViewModel() != null)
                //    parent.getDashBoard_frag().getViewModel().setListMutableLiveData(shipmentInfoList);
                hideEmpty();
                bindingData();
            }
        });
        myJobsViewModel.getShowLoading().observe(this, aBoolean -> {
            if (aBoolean) showLoading();
            else hideLoading();
        });
        myJobsViewModel.getShowEmpty().observe(this, aBoolean -> {
            if (!aBoolean) hideEmpty();
            else showEmpty();
        });
        myJobsViewModel.getjobDetailLiveData().observe(this, jobDetailRes -> {
            hideLoading();
            this.jobDetailRes = jobDetailRes;
            if (clicked) {
                Intent intent = new Intent(getContext(), JobProcessingActivityV3.class);
                intent.putExtra("jobDetail", this.jobDetailRes);
                clicked = false;
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        showLoading();
        myJobsViewModel.getMyJobs(preferenceHelper.getString(Constant.TOKEN, ""));
        /*if (shipmentInfoList.size() > 0) {
            binding.CoffeeView.setVisibility(View.GONE);
            binding.rcv.setVisibility(View.VISIBLE);
            adapter = new ListJobAdapterV2(getContext(), shipmentInfoList, listener);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            layoutManager.setOrientation(RecyclerView.VERTICAL);
            binding.rcv.setLayoutManager(layoutManager);
            binding.rcv.setAdapter(adapter);
            doClearFilter();
        }*/
    }

    public void doFilterShipmentInfo(String newText) {
        if (shipmentInfoList.size() > 0)
            adapter.getFilter().filter(newText);
    }

    public void doStatusFilterShipmentInfo(int status) {
        if (shipmentInfoList.size() > 0)
            adapter.getStatusFilter().filter(String.valueOf(status));
    }

    public void doClearFilter() {
        adapter = new ListJobAdapterV2(getContext(), shipmentInfoList, listener);
        if (binding != null)
            binding.rcv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    ListJobAdapterV2.JobClickListenerV2 listener = shipmentInfo -> {
        clicked = true;
        showLoading();
        myJobsViewModel.getDetailJob(preferenceHelper.getString(Constant.TOKEN, ""), shipmentInfo.getHashId());
        this.shipmentInfo = shipmentInfo;
    };

    public void showLoading() {
        if (!(dialogWait != null && dialogWait.isShowing())) {
            dialogWait = new DialogWait2(getActivity());
            dialogWait.show();
        }
    }

    public void hideLoading() {
        if (dialogWait != null)
            dialogWait.dismiss();
        binding.swipeContainer.setRefreshing(false);
    }

    public void showEmpty() {
        binding.CoffeeView.setVisibility(View.VISIBLE);
        binding.rcv.setVisibility(View.GONE);
    }

    public void hideEmpty() {
        binding.CoffeeView.setVisibility(View.GONE);
        binding.rcv.setVisibility(View.VISIBLE);
    }

    public void bindingData() {
        adapter = new ListJobAdapterV2(getContext(), shipmentInfoList, listener);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.rcv.setLayoutManager(layoutManager);
        binding.rcv.setAdapter(adapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        hideLoading();
    }

}
