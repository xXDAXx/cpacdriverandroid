package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AppVersionCheckReq implements Serializable {
    @SerializedName("appId")
    @Expose
    private String appId;

    @SerializedName("versionNumber")
    @Expose
    private String versionNumber;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public AppVersionCheckReq(String appId, String versionNumber) {
        this.appId = appId;
        this.versionNumber = versionNumber;
    }
}
