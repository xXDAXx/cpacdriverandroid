package driver.cpac.co.th.view.jobprocessing.Documents;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.Documents;
import com.tmg.th.R;
import com.tmg.th.databinding.ItemDocumentLayoutBinding;

import java.util.ArrayList;
import java.util.List;

public class ListDocumentsAdapter extends RecyclerView.Adapter<ListDocumentsAdapter.MyViewHolder> {
    protected List<Documents> documentsList;
    private Context context;
    private DocumentClickListener mListener;

    public ListDocumentsAdapter(Context context, List<Documents> documentsList, DocumentClickListener listener) {
        this.context = context;
        this.documentsList = documentsList;
        mListener = listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemDocumentLayoutBinding binding;

        public MyViewHolder(ItemDocumentLayoutBinding binding, DocumentClickListener listener) {
            super(binding.getRoot());
            mListener = listener;
            this.binding = binding;
        }

        public void bind(Documents item) {
            binding.documentName.setText(item.getDocumentName());
            if (item.getFileType().toLowerCase().equals("pdf")) App.self().getPicasso().load(R.drawable.pdf_icon).into(binding.fileImg);
            else if (item.getFileType().toLowerCase().equals("jpg"))  App.self().getPicasso().load(R.drawable.jpg_icon).into(binding.fileImg);
            else if (item.getFileType().toLowerCase().equals("png")) App.self().getPicasso().load(R.drawable.png_icon).into(binding.fileImg);
            else if (item.getFileType().toLowerCase().equals("docx")) App.self().getPicasso().load(R.drawable.docx_icon).into(binding.fileImg);
            else App.self().getPicasso().load(R.drawable.file).into(binding.fileImg);
            binding.documentName.setSelected(true);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemDocumentLayoutBinding itemBinding =
                ItemDocumentLayoutBinding.inflate(layoutInflater, viewGroup, false);
        return new MyViewHolder(itemBinding, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        Documents documents = documentsList.get(position);
        MyViewHolder shipmentHolder = viewHolder;
        shipmentHolder.itemView.setOnClickListener(v -> {
            mListener.passContent(documents);
        });
        viewHolder.bind(documents);
    }

    @Override
    public int getItemCount() {
        return documentsList == null ? 0 : documentsList.size();
    }

    public void updateAdapter(ArrayList<Documents> arrayList) {
        this.documentsList.clear();
        this.documentsList = arrayList;
        notifyDataSetChanged();
    }

}
