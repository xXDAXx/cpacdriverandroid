package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JobStatusUpdateReq implements Serializable {
    @SerializedName("waybillId")
    @Expose
    private String waybillId;

    @SerializedName("waybillStatus")
    @Expose
    private short waybillStatus;

    @SerializedName("note")
    @Expose
    private String note;

    public JobStatusUpdateReq(String waybillId, short waybillStatus, String note) {
        this.waybillId = waybillId;
        this.waybillStatus = waybillStatus;
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getWaybillId() {
        return waybillId;
    }

    public void setWaybillId(String waybillId) {
        this.waybillId = waybillId;
    }

    public short getWaybillStatus() {
        return waybillStatus;
    }

    public void setWaybillStatus(short waybillStatus) {
        this.waybillStatus = waybillStatus;
    }
}
