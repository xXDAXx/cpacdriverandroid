package driver.cpac.co.th.util.enumClass;

public enum FcmNotificationType {
    NEW_JOB(1),
    ISSUED_JOB(2);

    private final int value;

    private FcmNotificationType(int value) {
        this.value = value;
    }

    public int getValue() {
        return (int) value;
    }

    public static NotificationType getEnumType(int value) {
        for(NotificationType type:NotificationType.values()){
            if (type.getValue()==value) return type;
        }
        return null;
    }
}
