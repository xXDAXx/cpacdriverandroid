package driver.cpac.co.th.Model.google;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Bounds implements Serializable{
    @SerializedName("southwest")
    @Expose
    private Southwest southwest;

    @SerializedName("northeast")
    @Expose
    private Northeast northeast;

    public Bounds(Southwest southwest, Northeast northeast) {
        this.southwest = southwest;
        this.northeast = northeast;
    }

    public Southwest getSouthwest() {
        return southwest;
    }

    public void setSouthwest(Southwest southwest) {
        this.southwest = southwest;
    }

    public Northeast getNortheast() {
        return northeast;
    }

    public void setNortheast(Northeast northeast) {
        this.northeast = northeast;
    }
}
