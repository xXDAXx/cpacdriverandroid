package driver.cpac.co.th.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;


import com.tmg.th.BuildConfig;
import com.tmg.th.R;
import com.tmg.th.databinding.DialogUpdateBinding;

public class DialogUpdateVersion extends Dialog {
    public Context context;
    public Activity activity;
    public boolean isForceUpdate;
    public String latestVersionName;
    private DialogUpdateBinding binding;
    DialogUpdateCallback callBackDialogCom;

    public DialogUpdateVersion(Activity a, boolean isForceUpdate, String latestVersionName, DialogUpdateCallback callBackDialogCom) {
        super(a);
        this.activity = a;
        this.isForceUpdate = isForceUpdate;
        this.latestVersionName = latestVersionName;
        this.latestVersionName = latestVersionName;
        this.callBackDialogCom = callBackDialogCom;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_update, (ViewGroup) activity.findViewById(android.R.id.content), false);
        setContentView(binding.getRoot());
        binding.cancelBtn.setOnClickListener(v -> callBackDialogCom.btnCancelClick());
        binding.okBtn.setOnClickListener(view -> {
            goToStore();
            dismiss();
        });
        binding.currentVer.setText(activity.getString(R.string.versionDialog_check_current) + " " + BuildConfig.VERSION_NAME);
        binding.latestVer.setText(activity.getString(R.string.versionDialog_check_latest) + " " + latestVersionName);
        if (isForceUpdate) {
            this.setCancelable(false);
            this.setCanceledOnTouchOutside(false);
            binding.cancelBtn.setVisibility(View.GONE);
            binding.divider.setVisibility(View.GONE);
        } else {
            binding.cancelBtn.setVisibility(View.VISIBLE);
            binding.divider.setVisibility(View.VISIBLE);
        }
    }

    public void goToStore() {
        this.dismiss();
        final String appPackageName = activity.getPackageName(); // getPackageName() from Context or Activity object
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public interface DialogUpdateCallback {
        void btnCancelClick();
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            //super.onBackPressed();
            activity.finish();
            activity.moveTaskToBack(true);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(activity, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

}
