package driver.cpac.co.th.view.NewJobs;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import driver.cpac.co.th.Model.JobDetailResV2;
import com.tmg.th.R;
import com.tmg.th.databinding.FragmentNewjobsBinding;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.view.Home.HomeActivity;
import driver.cpac.co.th.view.MyJobs.ListJobAdapterV2;
import driver.cpac.co.th.view.dialog.DialogWait;
import driver.cpac.co.th.view.dialog.DialogWait2;
import driver.cpac.co.th.view.jobdetail.JobDetailActivityV3;

import java.util.ArrayList;
import java.util.List;

public class NewJobsFragment extends Fragment {
    private NewJobsViewModel myJobsViewModel;
    private static View view;
    private FragmentNewjobsBinding binding;
    private HomeActivity parent;
    private DialogWait2 dialogWait;
    private ListJobAdapterV2 adapter;
    private List<JobDetailResV2> shipmentInfoList = new ArrayList<>();
    PreferenceHelper preferenceHelper;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        preferenceHelper = new PreferenceHelper(getContext());
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_newjobs, container, false);
        view = binding.getRoot();
        dialogWait = new DialogWait2(getActivity());
        myJobsViewModel = ViewModelProviders.of(this).get(NewJobsViewModel.class);
        binding.setLifecycleOwner(this);
        binding.rcv.setVisibility(View.INVISIBLE);
        SwipeToRefreshSetup();
        ObserverSetup();
        loadNewJobs();
        return view;
    }

    public void SwipeToRefreshSetup() {
        binding.swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadNewJobs();
                hideLoading();
            }
        });
    }

    public void ObserverSetup() {
        myJobsViewModel.getlistJobRes().observe(this, listJobRes -> {
            binding.swipeContainer.setRefreshing(false);
                    hideLoading();
                    if (listJobRes!=null&&listJobRes.size() > 0) {
                        shipmentInfoList = listJobRes;
                        hideEmpty();
                        bindingData();
                    }
                }
        );

        myJobsViewModel.getShowLoading().observe(this, aBoolean -> {
            if (!aBoolean) hideLoading();
        });
        myJobsViewModel.getShowEmpty().observe(this, aBoolean -> {
            if (!aBoolean) hideEmpty();
            else showEmpty();
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        hideLoading();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadNewJobs();
        if (shipmentInfoList.size() > 0) {
            binding.CoffeeView.setVisibility(View.GONE);
            binding.rcv.setVisibility(View.VISIBLE);
            adapter = new ListJobAdapterV2(getContext(), shipmentInfoList, listener);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            layoutManager.setOrientation(RecyclerView.VERTICAL);
            binding.rcv.setLayoutManager(layoutManager);
            binding.rcv.setAdapter(adapter);
        }
    }

    public void loadNewJobs() {
        showLoading();
        myJobsViewModel.getNewJobs(preferenceHelper.getString(Constant.TOKEN, ""));
    }

    public void showLoading() {
        if (!(dialogWait != null && dialogWait.isShowing())) {
            dialogWait = new DialogWait2(getActivity());
            dialogWait.show();
        }
    }

    public void hideLoading() {
        if (dialogWait != null)
            dialogWait.dismiss();
        binding.swipeContainer.setRefreshing(false);
    }

    ListJobAdapterV2.JobClickListenerV2 listener = new ListJobAdapterV2.JobClickListenerV2() {
        @Override
        public void passContent(JobDetailResV2 shipmentInfo) {
            //Intent intent = new Intent(getContext(), JobDetailActivity.class);
            Intent intent = new Intent(getContext(), JobDetailActivityV3.class);
            intent.putExtra("content", shipmentInfo);
            startActivity(intent);
        }
    };

    public void doFilterShipmentInfo(String newText) {
        if (shipmentInfoList.size() > 0)
            adapter.getFilter().filter(newText);
    }

    public void doClearFilter(){
        adapter = new ListJobAdapterV2( getContext(),shipmentInfoList, listener);
        if(binding!=null)
            binding.rcv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void showEmpty() {
        binding.CoffeeView.setVisibility(View.VISIBLE);
        binding.rcv.setVisibility(View.GONE);
    }

    public void hideEmpty() {
        binding.CoffeeView.setVisibility(View.GONE);
        binding.rcv.setVisibility(View.VISIBLE);
    }

    public void bindingData() {
        adapter = new ListJobAdapterV2(getContext(), shipmentInfoList, listener);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.rcv.setLayoutManager(layoutManager);
        binding.rcv.setAdapter(adapter);
    }
}
