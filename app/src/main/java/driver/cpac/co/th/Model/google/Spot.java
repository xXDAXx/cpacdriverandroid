package driver.cpac.co.th.Model.google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Spot implements Serializable{
    @SerializedName("lng")
    @Expose
    private Double lng;

    @SerializedName("lat")
    @Expose
    private Double lat;

    @SerializedName("name")
    @Expose
    private String name;

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Spot(Double lng, Double lat, String name) {
        this.lng = lng;
        this.lat = lat;
        this.name = name;
    }
}
