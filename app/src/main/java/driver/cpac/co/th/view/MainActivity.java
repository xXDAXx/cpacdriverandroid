package driver.cpac.co.th.view;

import android.os.Bundle;

import com.tmg.th.R;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.login.Login_Fragment;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends OldBaseActivity {
    private static FragmentManager fragmentManager;
    private static FragmentTransaction fragmentTransaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        super.onCreate(savedInstanceState);
        super.setMyView(findViewById(android.R.id.content));
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
        //If savedinstnacestate is null then replace login fragment

        if (savedInstanceState == null) {
            Login_Fragment login_fragmen = new Login_Fragment();
            //EnableGPS(false);
            fragmentManager
                    .beginTransaction()
                    //.replace(R.id.fragmentlayout, new Login_Fragment(), null).commit();
                    .replace(R.id.fragmentlayout, login_fragmen, null).commit();
        }

        // On close icon click finish activity
        //test hash
        //       String abc= (BCrypt.hashpw("123456",BCrypt.gensalt()));
        //       Boolean test = BCrypt.checkpw("123456","$2a$10$8pLiXdahIbGUOk5og5JnvuQLpxqR44pos74aLSQrErAuKksdamOMu");
        }

    @Override
    protected void onResume() {
        super.onResume();
        EnableGPS(false);
    }

}

