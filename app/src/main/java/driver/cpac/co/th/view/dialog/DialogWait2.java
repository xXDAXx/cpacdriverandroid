package driver.cpac.co.th.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;

import com.tmg.th.R;

public class DialogWait2 extends Dialog {

    public Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_wait2);
        getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        this.setCancelable(false);
    }

    public DialogWait2(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
    }
}
