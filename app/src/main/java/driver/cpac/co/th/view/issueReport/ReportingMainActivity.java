package driver.cpac.co.th.view.issueReport;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tmg.th.R;
import com.tmg.th.databinding.ActivityReportingMainBinding;

import java.util.ArrayList;
import java.util.List;

import driver.cpac.co.th.Model.DriverListIssueRes;
import driver.cpac.co.th.Model.IssueDetailRes;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogConfirm;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;

import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;

public class ReportingMainActivity extends OldBaseActivity implements DialogConfirm.CallBackDialogCom, DialogNoticeCallback.CallBackDialogNotice {
    PreferenceHelper preferenceHelper;
    private ActivityReportingMainBinding binding;
    //private IssueDetailRes jobDetailViewModel;
    DialogNoticeCallback dialogNotice;
    DialogConfirm dialogConfirm;
    DialogNoticeCallback dialogNoticeCallback;
    IssueReportingMainViewModel viewModel;
    List<DriverListIssueRes> list;
    IssueListAdapter adapter;
    public Activity activity;

    public JobDetailResV2 jobDetail;

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.getIssueList(preferenceHelper.getString(Constant.TOKEN, ""));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reporting_main);
        setSupportActionBar(binding.appBarLayout.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.issueReport_mainActivity_lblTittleMain));
        preferenceHelper = new PreferenceHelper(this);
        activity = this;
        viewModel = ViewModelProviders.of(this).get(IssueReportingMainViewModel.class);
        ObserverSetup();
        jobDetail = (JobDetailResV2) getIntent().getSerializableExtra("jobDetail");
        viewModel.getIssueList(preferenceHelper.getString(Constant.TOKEN, ""));
        list = new ArrayList<>();



        binding.btnAdd.setOnClickListener(view -> {
            Intent myIntent;
            myIntent = new Intent(this, ReportingStep1Activity.class);
            startActivity(myIntent);
        });
        binding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (adapter != null)
                    adapter.getFilter().filter(query);
                binding.searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (adapter != null)
                    adapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void ObserverSetup() {
        viewModel.getLoading().observe(this, aBoolean -> {
            if (aBoolean == true) {
                showLoading();
            } else if (aBoolean == false) {
                hideLoading();
            }
        });

        viewModel.getListIssueLiveData().observe(this, listIssueRes -> {
            if (listIssueRes.size() > 0) {
                if(jobDetail != null)
                {
                    int waybillId = jobDetail.getId();
                    list = filterListByWaybillID(waybillId,listIssueRes);
                } else
                    list = listIssueRes;

                if(jobDetail != null && list.size() == 0) {
                    Intent myIntent;
                    myIntent = new Intent(this, ReportingStep1Activity.class);
                    myIntent.putExtra("jobDetail", jobDetail);
                    myIntent.putExtra("backToJobDetail", true);
                    myIntent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                    startActivity(myIntent);
                }   else {
                    binding.rcv.setVisibility(View.VISIBLE);
                    adapter = new IssueListAdapter(this, list, listener);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                    layoutManager.setOrientation(RecyclerView.VERTICAL);
                    binding.rcv.setLayoutManager(layoutManager);
                    binding.rcv.setAdapter(adapter);
                }

            } else {

            }
        });
    }

    @Override
    public void BtnOkClick() {

    }

    @Override
    public void BtnOkDiaComClick() {

    }

    IssueListAdapter.IssueClickListener listener = item -> {
        Intent intent = new Intent(this, ReportingStep2Activity.class);
        intent.putExtra("viewIssue", item);
        startActivity(intent);
    };

    public List<DriverListIssueRes> filterListByWaybillID(int waybillId, List<DriverListIssueRes> originalList) {
        List<DriverListIssueRes> newList =  new ArrayList<>();
        for (DriverListIssueRes issue: originalList) {
            if (issue.getWaybillId() == waybillId)
                newList.add(issue);
        }

        return newList;
    }
}
