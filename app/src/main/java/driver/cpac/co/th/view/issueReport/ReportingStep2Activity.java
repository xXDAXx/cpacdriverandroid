package driver.cpac.co.th.view.issueReport;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.tmg.th.R;

import driver.cpac.co.th.Model.DriverIssueDocumentRes;
import driver.cpac.co.th.Model.DriverListIssueRes;
import driver.cpac.co.th.Model.DriverNewIssueDocumentReq;
import driver.cpac.co.th.Model.DriverNewIssueReq;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.view.dialog.DialogChooseWaybill;
import driver.cpac.co.th.view.jobdetail.JobDetailViewModel;
import driver.cpac.co.th.view.jobprocessing.Documents.ListDocumentsAdapterV2;
import com.tmg.th.databinding.ActivityReportingStep2Binding;

import org.w3c.dom.Document;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Driver;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import driver.cpac.co.th.Model.Documents;
import driver.cpac.co.th.util.GenericFileProvider;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.enumClass.IssueType;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogConfirm;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;
import driver.cpac.co.th.view.documentViewer.WebViewerActivity;
import driver.cpac.co.th.view.jobprocessing.JobProcessingActivityV3;

public class ReportingStep2Activity extends OldBaseActivity implements DialogConfirm.CallBackDialogCom, DialogNoticeCallback.CallBackDialogNotice, DialogChooseWaybill.CallBackDialogChooseWaybill {
    PreferenceHelper preferenceHelper;
    static final int REQUEST_IMAGE_CAPTURE = 1888;

    private ActivityReportingStep2Binding binding;
    DialogNoticeCallback dialogNotice;
    DialogConfirm dialogConfirm;
    DialogChooseWaybill dialogChooseWaybill;
    IssueReportingViewModel viewModel;
    String uploadFileType = null;
    private Uri mImageUri;
    public Activity activity;
    ItemNewIssue itemNewIssue;
    DriverListIssueRes viewIssue;
    List<Documents> documentsList;
    ListDocumentsAdapterV2 mAdapter;
    JobDetailResV2 waybillChoosen;
    JobDetailResV2 jobDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reporting_step2);
        setSupportActionBar(binding.appBarLayout.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.menu_list_issueReport));
        preferenceHelper = new PreferenceHelper(this);
        activity = this;
        viewModel = ViewModelProviders.of(this).get(IssueReportingViewModel.class);
        binding.lblChooseWayBill.setVisibility(View.GONE);
        ObserverSetup();

        itemNewIssue = (ItemNewIssue) getIntent().getSerializableExtra("NewIssue");
        if(itemNewIssue!=null) {
            binding.issueImage.setImageResource(itemNewIssue.type);
            binding.textView6.setText(itemNewIssue.title);
            binding.textView6.setTextColor(IssueType.GetIssueTypeColor(itemNewIssue.issueType.getValue(),this));
            binding.issueImage.setColorFilter(IssueType.GetIssueTypeColor(itemNewIssue.issueType.getValue(),this));
            setupRcvDocument();
        }

        jobDetail = (JobDetailResV2) getIntent().getSerializableExtra("jobDetail");
        if(jobDetail!=null) {
           binding.cardViewChooseWaybill.setVisibility(View.GONE);
            binding.lblChooseWayBill.setVisibility(View.VISIBLE);
            binding.lblWayBillID.setText("ID#" + jobDetail.getId());
        }

        viewIssue = (DriverListIssueRes) getIntent().getSerializableExtra("viewIssue");
        if(viewIssue!=null) {
            binding.issueImage.setImageResource(IssueType.GetIssueTypeImg(viewIssue.getType(),this));
            binding.textView6.setText(IssueType.GetIssueTypeName(viewIssue.getType(),this));
            binding.textView6.setTextColor(IssueType.GetIssueTypeColor(viewIssue.getType(),this));
            binding.issueImage.setColorFilter(IssueType.GetIssueTypeColor(viewIssue.getType(),this));
            binding.acceptBtn.setVisibility(View.GONE);
            binding.edtContent.setEnabled(false);
            binding.edtContent.setText(viewIssue.getContent());
            if(viewIssue.getWaybillId()>0) {
                binding.lblChooseWayBill.setVisibility(View.VISIBLE);
                binding.lblWayBillID.setText("ID#" + viewIssue.getWaybillId());
            }
            else {
                binding.lblWayBillID.setVisibility(View.GONE);
            }
            documentsList = new ArrayList<>();
            documentsList = DriverIssueDocumentRes.covertToDocuments(viewIssue.getListDocument());
            mAdapter = new ListDocumentsAdapterV2(this, documentsList, viewClickListener, deleteClickListener, WaybillStatus.finished);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(RecyclerView.HORIZONTAL);
            binding.rcvDocuments.setLayoutManager(layoutManager);
            binding.rcvDocuments.setAdapter(mAdapter);
            binding.cardViewChooseWaybill.setVisibility(View.GONE);

        }
        binding.acceptBtn.setOnClickListener(view -> {
            if(binding.edtContent.getText().length()>0) {
                dialogConfirm = new DialogConfirm(getString(R.string.issueReport_step2_lblTittleDialogConfirm), getString(R.string.issueReport_step2_contentDialogConfirm), this, this);
                dialogConfirm.show();
            }
            else {
                dialogNotice = new DialogNoticeCallback(getString(R.string.issueReport_step2_lblDescribeIssueInformations),this, this);
                dialogNotice.show();
            }
        });
        viewModel.getMyJobs(preferenceHelper.getString(Constant.TOKEN, ""));
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void ObserverSetup() {
        viewModel.getLoading().observe(this, aBoolean -> {
            if (aBoolean == true) {
                showLoading();
            } else if (aBoolean == false) {
                hideLoading();
            }
        });

        viewModel.getIssueCreated().observe(this,aBoolean -> {
            if (aBoolean == true) {
                startActivity(new Intent(this, ReportingMainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP));
            } else if (aBoolean == false) {
                dialogNotice = new DialogNoticeCallback(getString(R.string.issueReport_step2_submitFailedNotice),this,this);
                dialogNotice.show();
            }
        });

        viewModel.getUploadFileResponse().observe(this, uploadFileResponse -> {
            if(uploadFileResponse!= null) {
                Documents newDoc = new Documents(
                        0,
                        0,
                        uploadFileResponse.getFileUrl(),
                        uploadFileResponse.getFileName(),
                        "IssueDoc",
                        uploadFileResponse.getFileType());
                documentsList.add(newDoc);
                mAdapter.notifyDataSetChanged();
            }
        });

        viewModel.getjobDetailLiveData().observe(this,jobDetailResV2s -> {
            if(jobDetailResV2s.size()>0) {
                dialogChooseWaybill =  new DialogChooseWaybill(jobDetailResV2s,this,this);
                binding.cardViewChooseWaybill.setOnClickListener(view -> {
                    dialogChooseWaybill.show();
                });
            } else {
                binding.lblChooseWayBill.setVisibility(View.GONE);
                binding.lblWayBillID.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void BtnOkClick() {
        List<DriverNewIssueDocumentReq> docs = new ArrayList<>();
        if(documentsList.size()>1) {
            for (Documents document: documentsList) {
                if (!document.getDocumentType().equals("addnew")) {
                    DriverNewIssueDocumentReq docReq = DriverNewIssueDocumentReq.convertDtbIssueToReq(document);
                    docs.add(docReq);
                }
            }
        }
        int waybillID;
        if (waybillChoosen != null) {
            waybillID = waybillChoosen.getId();
        } else if (jobDetail != null ) {
            waybillID = jobDetail.getId();
        } else
            waybillID = 0;

        DriverNewIssueReq req = new DriverNewIssueReq(waybillID,binding.edtContent.getText().toString(),itemNewIssue.getIssueType().getValue(),(short)0,docs);
        viewModel.newIssueSubmit(req, preferenceHelper.getString(Constant.TOKEN, ""));
    }

    @Override
    public void BtnOkDiaComClick() {
       /* List<DriverNewIssueDocumentReq> docs = new ArrayList<>();
        if(documentsList.size()>1) {
            for (Documents document: documentsList) {
                if (!document.getDocumentType().equals("addnew")) {
                    DriverNewIssueDocumentReq docReq = DriverNewIssueDocumentReq.convertDtbIssueToReq(document);
                    docs.add(docReq);
                }
            }
        }
        DriverNewIssueReq req = new DriverNewIssueReq(0,binding.edtContent.getText().toString(),itemNewIssue.getIssueType().getValue(),(short)0,docs);
        viewModel.newIssueSubmit(req, preferenceHelper.getString(Constant.TOKEN, ""));*/
    }

    public void setupRcvDocument() {
        documentsList = new ArrayList<>();
        mAdapter = new ListDocumentsAdapterV2(this, documentsList, viewClickListener, deleteClickListener, WaybillStatus.loaded);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        binding.rcvDocuments.setLayoutManager(layoutManager);
        binding.rcvDocuments.setAdapter(mAdapter);
    }

    ListDocumentsAdapterV2.DocumentViewClickListener viewClickListener = documents -> {
        if (documents.getDocumentType() != null && documents.getDocumentType().equals("addnew")) {
            addDocumentBtnClick();
        } else {
            Intent intent = new Intent(this, WebViewerActivity.class);
            intent.putExtra("content", documents);
            startActivity(intent);
        }
    };

    ListDocumentsAdapterV2.DocumentDeleteClickListener deleteClickListener = documents -> {

    };

    public void addDocumentBtnClick() {

        uploadFileType = "ISSUEDOC";
        Dexter.withActivity(this).withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new PermissionListener() {
        @Override
        public void onPermissionGranted (PermissionGrantedResponse response){
            IntendCameraAction();
        }

        @Override
        public void onPermissionDenied (PermissionDeniedResponse response){/* ... */}

        @Override
        public void onPermissionRationaleShouldBeShown (PermissionRequest
        permission, PermissionToken token){
            token.continuePermissionRequest();
        }
    }
                ).check();

}

    public void IntendCameraAction() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = null;
        try {
            photo = createTemporaryFile("picture", ".jpg");
            photo.delete();
        } catch (Exception e) {
            Log.v("ABC", "Can't create file to take picture!");
            Toast.makeText(this, "Please check SD card! Image shot is impossible!", Toast.LENGTH_SHORT);
        }
        mImageUri = GenericFileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".fileprovider", photo);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        //start camera intent
        if (intent.resolveActivity(this.getPackageManager()) != null) {
            this.startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private File createTemporaryFile(String part, String ext) throws Exception {

        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode,  resultCode,  data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            try {
                showLoading();
                String fileName = uploadFileType + getCurrentTime();
                Dexter.withActivity(this)
                        .withPermission(Manifest
                                .permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener() {
                                          @Override
                                          public void onPermissionGranted(PermissionGrantedResponse response) {
                                              saveImgToStorage(fileName,getApplicationContext());
                                          }

                                          @Override
                                          public void onPermissionDenied(PermissionDeniedResponse response) {/* ... */}

                                          @Override
                                          public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                              token.continuePermissionRequest();
                                          }
                                      }
                        ).check();
                ContextWrapper cw = new ContextWrapper(this.getApplicationContext());
                File directory = cw.getDir("documents", Context.MODE_PRIVATE);
                File file = new File(directory, fileName + ".jpg");
                PreferenceHelper preferenceHelper = new PreferenceHelper(this);
                viewModel.newUploadDocument(file, uploadFileType, preferenceHelper.getString(Constant.TOKEN, ""));
            } catch (Exception ex) {
                Log.println(1, "222", ex.toString());
            }
        }
    }

    public static String getCurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public void saveImgToStorage(String filename, Context context) {
        try {
            context.getContentResolver().notifyChange(mImageUri, null);
            ContentResolver cr = context.getContentResolver();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(cr, mImageUri);
            } catch (Exception e) {
                Toast.makeText(context, "Failed to load", Toast.LENGTH_SHORT).show();
                Log.d("ABC", "Failed to load", e);
            }
            ContextWrapper cw = new ContextWrapper(context.getApplicationContext());
            File directory = cw.getDir("documents", Context.MODE_PRIVATE);
            File f = new File(directory, filename + ".jpg");
            File mypath = new File(directory, filename + ".jpg");
            FileOutputStream fos = null;
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            getResizedBitmap(bitmap, 3000).compress(Bitmap.CompressFormat.JPEG, 80, fos);
        } catch (Exception e) {
            Log.println(1, "222", e.toString());
        }
    }

    @Override
    public void WaybillChosen(JobDetailResV2 jobDetailResV2) {
        binding.lblChooseWayBill.setVisibility(View.VISIBLE);
        binding.lblWayBillID.setVisibility(View.VISIBLE);
        waybillChoosen = jobDetailResV2;
        binding.lblWayBillID.setText("ID#" + jobDetailResV2.getId());
    }
}
