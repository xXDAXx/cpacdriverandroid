package driver.cpac.co.th.view.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.tmg.th.R;
import com.tmg.th.databinding.ActivitySetNewPasswordBinding;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.SetPasswordReq;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.base.SplashActivity;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;

public class SetPasswordActivity extends OldBaseActivity implements DialogNoticeCallback.CallBackDialogNotice{

    ActivitySetNewPasswordBinding binding;
    LoginViewModel viewModel;
    DialogNoticeCallback dialogNoticeCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_set_new_password);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        preferenceHelper = new PreferenceHelper(this);
        //
        viewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        OserverSetup();

        binding.buttonContinue.setOnClickListener(view -> continueClick());

        binding.showHidePassword.setOnCheckedChangeListener((compoundButton, b) -> {
            if(b) {
                binding.password.setInputType(InputType.TYPE_CLASS_TEXT);
                binding.passwordConfirm.setInputType(InputType.TYPE_CLASS_TEXT);
            }
            else {
                binding.password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                binding.passwordConfirm.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            }
        });
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void OserverSetup() {
        viewModel.getPasswordSuccess().observe(this,aBoolean -> {
            hideLoading();
            if (aBoolean) {
                dialogNoticeCallback = new DialogNoticeCallback(getString(R.string.login_activeNewUser_setPwdSuccessfully), this, this);
                dialogNoticeCallback.show();
            }
            else {
                NoticeShow(getString(R.string.commonErrorMsg),this);
            }
        });

    }

    public void continueClick() {

        String pwd = binding.password.getText().toString();
        String pwdConfirm = binding.passwordConfirm.getText().toString();
        if(checkPwd(pwd,pwdConfirm)) {
            viewModel.setPasswordCall(new SetPasswordReq(App.self().getMobileNumber(), App.self().getActiveCode(), pwd, pwdConfirm));
            showLoading();
        }
    }

    public boolean checkPwd(String pwd,String pwdConfirm) {
        if(!pwd.equals(pwdConfirm)) {
            binding.passwordConfirm.setError(getString(R.string.changePassword_errorMsg_confrimNotMatch));
            binding.passwordConfirm.requestFocus();
            return false;
        }
        else if(!checkRulePassword(pwd)) {
            binding.password.setError(getString(R.string.changePassword_errorMsg_passwordError));
            binding.password.requestFocus();
            return false;
        }
        return true;
    }

    public boolean checkRulePassword(String pwd) {
        String regexp = "^(?=.*[a-zA-Z0-9]).{4,20}$";
        Pattern pattern;
        pattern = Pattern.compile(regexp);
        Matcher m = pattern.matcher(pwd);
        if (m.matches()) {
            return true;
        }
        return false;
    }

    @Override
    public void BtnOkDiaComClick() {
        //Move to Login
        Intent mStartActivity = new Intent(this, SplashActivity.class);
        mStartActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mStartActivity);
    }
}
