package driver.cpac.co.th.view.account.profile;

import androidx.lifecycle.MutableLiveData;

import driver.cpac.co.th.Model.UploadFileResponse;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.viewModel.BaseViewModel;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileViewModel extends BaseViewModel {

    private CargoLinkAPI cargoLinkAPI;
    public MutableLiveData<Boolean> uploadSuccessfull;
    public MutableLiveData<Boolean> updateSuccessfull;


    public MutableLiveData<Boolean> getUpdateSuccessfull() {
        if(updateSuccessfull == null)
            updateSuccessfull = new MutableLiveData<>();
        return updateSuccessfull;
    }

    public MutableLiveData<Boolean> getUploadSuccessfull() {
        if(uploadSuccessfull == null)
            uploadSuccessfull = new MutableLiveData<>();
        return uploadSuccessfull;
    }

    public void uploadAvatar(String Token, File f) {
        cargoLinkAPI = ApiUtils.getAPIService();
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/jpg"), f);
        MultipartBody.Part part = MultipartBody.Part.createFormData("file", f.getName(), fileReqBody);
        cargoLinkAPI.uploadPublic(Token, LangUtils.getCurrentLanguage(), part, null).enqueue(new Callback<UploadFileResponse>() {

            @Override
            public void onResponse(Call<UploadFileResponse> call, Response<UploadFileResponse> response) {
                if(response.isSuccessful()) {
                    UploadFileResponse res = response.body();
                    updateAvatar(Token,res);
                } else {
                    updateSuccessfull.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<UploadFileResponse> call, Throwable t) {
                updateSuccessfull.setValue(false);
            }
        });
    }

    public void updateAvatar(String Token, UploadFileResponse req) {
        cargoLinkAPI = ApiUtils.getAPIService();
        cargoLinkAPI.updateAvatar(Token,LangUtils.getCurrentLanguage(),req).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    updateSuccessfull.setValue(true);
                } else
                {
                    updateSuccessfull.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                updateSuccessfull.setValue(false);
            }
        });

    }
}
