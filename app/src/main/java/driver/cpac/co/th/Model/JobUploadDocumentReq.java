package driver.cpac.co.th.Model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JobUploadDocumentReq implements Serializable {
    @SerializedName("waybillId") @Expose
    private String waybillId;

    @Nullable  @SerializedName("detailWaybillId") @Expose
    private String detailWaybillId;

    @SerializedName("type") @Expose
    private String type;

    @SerializedName("documentName") @Expose
    private String documentName;

    @SerializedName("fileDetail") @Expose
    private UploadFileResponse fileDetail;

    public String getDetailWaybillId() {
        return detailWaybillId;
    }

    public void setDetailWaybillId(String detailWaybillId) {
        this.detailWaybillId = detailWaybillId;
    }

    public String getWaybillId() {
        return waybillId;
    }

    public void setWaybillId(String waybillId) {
        this.waybillId = waybillId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public UploadFileResponse getFileDetail() {
        return fileDetail;
    }

    public void setFileDetail(UploadFileResponse fileDetail) {
        this.fileDetail = fileDetail;
    }

    public JobUploadDocumentReq(String waybillId, @Nullable  String detailWaybillId, String type, String documentName) {
        this.waybillId = waybillId;
        this.detailWaybillId = detailWaybillId;
        this.type = type;
        this.documentName = documentName;
    }

    public JobUploadDocumentReq(String waybillId, @Nullable String detailWaybillId, String type, String documentName, UploadFileResponse fileDetail) {
        this.waybillId = waybillId;
        this.detailWaybillId = detailWaybillId;
        this.type = type;
        this.documentName = documentName;
        this.fileDetail = fileDetail;
    }
}
