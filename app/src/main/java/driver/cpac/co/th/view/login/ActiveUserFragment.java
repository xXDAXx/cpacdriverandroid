package driver.cpac.co.th.view.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.tmg.th.R;
import com.tmg.th.databinding.FragmentActiveUserBinding;

import driver.cpac.co.th.App;
import driver.cpac.co.th.view.BaseActivity;
import driver.cpac.co.th.view.dialog.DialogNotice;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;
import driver.cpac.co.th.view.dialog.DialogWait;
import driver.cpac.co.th.view.dialog.DialogWait2;

public class ActiveUserFragment extends Fragment implements DialogNoticeCallback.CallBackDialogNotice {

    FragmentActiveUserBinding binding;
    private LoginViewModel loginViewModel;
    public Fragment thisFragment;
    private static View view;
    private DialogWait2 dialogWait;
    private DialogNoticeCallback dialogNotice;
    private static FragmentTransaction fragmentTransaction;
    ActiveUserActivity.ActiveMode mode;

    @Override
    public void BtnOkDiaComClick() {
        moveToActiveSMSCodeActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_active_user, container, false);
        view = binding.getRoot();
        thisFragment = this;
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        binding = DataBindingUtil.setContentView(this.getActivity(), R.layout.fragment_active_user);
        binding.setLifecycleOwner(this);

        if (mode == ActiveUserActivity.ActiveMode.ACTIVE_MODE) {
            binding.lblAction.setText(getText(R.string.login_activeNewUser_label));
            binding.forgotButton.setText(getText(R.string.login_activeNewUser_btnActive));
        } else if (mode == ActiveUserActivity.ActiveMode.FORGOT_PASSWORD_MODE) {
            binding.lblAction.setText(getText(R.string.forgotBig));
            binding.forgotButton.setText(getText(R.string.login_activeNewUser_btnContinue));
        }

        binding.backToLoginBtn.setOnClickListener(view1 -> BackClick());
        binding.forgotButton.setOnClickListener(view1 -> activeClick());
        observerSetup();
        return view;
    }

    public void observerSetup() {
        loginViewModel.getVerifiedMobileNumber().observe(thisFragment, aBoolean -> {
            dialogWait.dismiss();
        if(aBoolean) {
            dialogNotice = new DialogNoticeCallback(getString(R.string.login_activeNewUser_instructionLabel3),getActivity(),this);
            dialogNotice.show();
            }
        });

        loginViewModel.getErrorActiveRes().observe(this,activeErrMsg -> {
            dialogWait.dismiss();
            DialogNotice dialogNotice = new DialogNotice(activeErrMsg.getMessage(),getActivity());
            dialogNotice.show();
        });
    }

    public void activeClick() {
        if (checkMobileNumber()) {
            //Call somethings
            App.self().setMobileNumber(binding.ccp.getFullNumberWithPlus() + binding.phoneNumber.getText().toString());
            if (mode == ActiveUserActivity.ActiveMode.ACTIVE_MODE)
                moveToActiveSMSCodeActivity();
            else if(mode == ActiveUserActivity.ActiveMode.FORGOT_PASSWORD_MODE) {
                dialogWait = new DialogWait2(getActivity());
                dialogWait.show();
                loginViewModel.verifyActiveDriverPhoneNumber(binding.ccp.getFullNumberWithPlus() + binding.phoneNumber.getText().toString());
            }
        }
    }

    public boolean checkMobileNumber() {
        boolean check = false;
        if (binding.phoneNumber.getText().length() == 0)
            binding.phoneNumber.setError(getString(R.string.login_errorMsg_emptyMobileNumber));
        else
            check = true;
        return check;
    }

    public void BackClick() {
        fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.RFogot_layout, new Login_Fragment()).commit();
        fragmentTransaction.addToBackStack(null);
    }

    public void moveToActiveSMSCodeActivity() {
        Context ctx = getContext();
        Intent myIntent;
        myIntent = new Intent(ctx, ActiveUserActivity.class);
        myIntent.putExtra("MODE", mode);
        loginViewModel.getErrorActiveRes().removeObservers(this);
        //if(mode == ActiveUserActivity.ActiveMode.FORGOT_PASSWORD_MODE) loginViewModel.getVerifiedMobileNumber().removeObservers(thisFragment);
        startActivity(myIntent);
    }

}