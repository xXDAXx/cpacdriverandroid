package driver.cpac.co.th.view.jobprocessing.JobProcess;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.JobUploadDocumentReq;
import driver.cpac.co.th.Model.UploadFileResponse;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.WaybillStatus;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobProcessViewModel extends ViewModel {
    private CargoLinkAPI cargoLinkAPI;
    public MutableLiveData<Boolean> loading;
    public MutableLiveData<JobDetailResV2> jobDetailRefreshData;
    UploadFileResponse uploadFileResponse;
    public MutableLiveData<Boolean> getLoading() {
        if (loading == null) {
            loading = new MutableLiveData<>();
        }
        return loading;
    }

    public MutableLiveData<JobDetailResV2> getJobDetailRefreshData() {
        if (jobDetailRefreshData == null) {
            jobDetailRefreshData = new MutableLiveData<>();
        }
        return jobDetailRefreshData;
    }

    public void refreshDetailJob(String Token, String jobId) {
        cargoLinkAPI = ApiUtils.getAPIService();
        cargoLinkAPI.getDetailWaybill(Token, LangUtils.getCurrentLanguage(), jobId)
                .enqueue(new Callback<JobDetailResV2>() {
                    @Override
                    public void onResponse(Call<JobDetailResV2> call, Response<JobDetailResV2> response) {
                        loading.setValue(false);
                        if (response.body() != null) {
                            jobDetailRefreshData.setValue(response.body());
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<JobDetailResV2> call, Throwable t) {
                        loading.setValue(false);
                    }
                });
    }

    public void uploadDocument(File file, JobDetailResV2 jobDetailRes, String uploadFileType, String Token, UploadFileResponse uploadFileResponse) {
        JobUploadDocumentReq jobUploadDocumentReq = null;
        cargoLinkAPI = ApiUtils.getAPIService();
        if (jobDetailRes.getStatus() == WaybillStatus.arrived.getValue()) {
            jobUploadDocumentReq = new JobUploadDocumentReq(jobDetailRes.getHashId(), "0", uploadFileType, file.getName(),uploadFileResponse);
        } else if (jobDetailRes.getStatus() == WaybillStatus.loaded.getValue()) {
            jobUploadDocumentReq = new JobUploadDocumentReq(jobDetailRes.getHashId(), Util.GetNextDeliveryPoint(jobDetailRes).getHashId(), uploadFileType, file.getName(),uploadFileResponse);
        }
        cargoLinkAPI.updateJobDocuments(Token, LangUtils.getCurrentLanguage(), jobUploadDocumentReq).
                enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.isSuccessful()) {
                            refreshDetailJob(Token, jobDetailRes.getHashId());
                        } else
                            loading.setValue(false);
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        loading.setValue(false);
                    }
                });
    }

    public void newUploadDocument(File file, JobDetailResV2 jobDetailRes, String uploadFileType, String Token) {
        loading.setValue(true);
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/jpg"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), fileReqBody);
        cargoLinkAPI = ApiUtils.getAPIService();
        cargoLinkAPI.upload(Token, LangUtils.getCurrentLanguage(), part, null).
                enqueue(new Callback<UploadFileResponse>() {
                    @Override
                    public void onResponse(Call<UploadFileResponse> call, Response<UploadFileResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                uploadFileResponse = response.body();
                                uploadDocument(file, jobDetailRes, uploadFileType, Token, uploadFileResponse);
                            }
                        } else
                            loading.setValue(false);
                    }

                    @Override
                    public void onFailure(Call<UploadFileResponse> call, Throwable t) {
                        loading.setValue(false);
                    }
                });
    }
}
