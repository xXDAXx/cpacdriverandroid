package driver.cpac.co.th.view.MapView;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import driver.cpac.co.th.Model.DirectionReq;
import driver.cpac.co.th.Model.google.Directions;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapViewModel extends ViewModel {

    private CargoLinkAPI cargoLinkAPI;
    public MutableLiveData<Directions> directionsLiveData;
    public MutableLiveData<Boolean> loading;

    public MutableLiveData<Directions> getDirectionsLiveData() {
        if (directionsLiveData == null) {
            directionsLiveData = new MutableLiveData<>();
        }
        return directionsLiveData;
    }

    public void getDirectionApi(DirectionReq directionReq){
        cargoLinkAPI = ApiUtils.getGGMAPI();
        cargoLinkAPI.getDirections(directionReq.getOrigin(),directionReq.getDestination(), Constant.GGM_DIRECTION_API_KEY)
                .enqueue(new Callback<Directions>() {
                    @Override
                    public void onResponse(Call<Directions> call, Response<Directions> response) {
                        if (response.isSuccessful()) {
                            Directions directions = response.body();
                            if(directions.getStatus().equals("OK")) {
                                directionsLiveData.setValue(directions);
                            }else{directionsLiveData.setValue(null); }

                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<Directions> call, Throwable t) {

                    }
                });
    }

    public void getDirectionWithWaypointsApi(DirectionReq directionReq,String wayPoints){
        cargoLinkAPI = ApiUtils.getGGMAPI();
        cargoLinkAPI.getDirectionsWithWayPoints(directionReq.getOrigin(),directionReq.getDestination(),wayPoints , Constant.GGM_DIRECTION_API_KEY)
                .enqueue(new Callback<Directions>() {
                    @Override
                    public void onResponse(Call<Directions> call, Response<Directions> response) {
                        if (response.isSuccessful()) {
                            Directions directions = response.body();
                            if(directions.getStatus().equals("OK")) {
                                directionsLiveData.setValue(directions);
                            }else{directionsLiveData.setValue(null); }

                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<Directions> call, Throwable t) {
                        Log.d("abc",t.toString());
                    }
                });
    }
}
