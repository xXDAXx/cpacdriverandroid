package driver.cpac.co.th.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.databinding.DataBindingUtil;

import com.tmg.th.R;
import com.tmg.th.databinding.DialogFilterBinding;
import driver.cpac.co.th.util.enumClass.WaybillStatus;


public class DialogFilter  extends Dialog {
    public Activity activity;
    public Dialog dialog;
    private DialogFilterBinding binding;
    CallBackDialogFilter callBackDialogFilter;
    public String mode;
    public int selected;

    public DialogFilter(String mode, Activity a, CallBackDialogFilter callBackDialogFilter){
        super(a);
        this.activity = a;
        this.callBackDialogFilter = callBackDialogFilter;
        this.mode = mode;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_filter, (ViewGroup) activity.findViewById(android.R.id.content), false);
        setContentView(binding.getRoot());
        binding.filterAll.setOnClickListener(v->{selected=0;callBackDialogFilter.FilterChosen();});
        setupModeFilter();
        binding.btnApply.setVisibility(View.GONE);
        binding.divider.setVisibility(View.GONE);
    }

    public void setupModeFilter()
    {
        switch (mode){
            case "MY_JOB":
                binding.linearLayoutMyJobFilter.setVisibility(View.VISIBLE);
                binding.linearLayoutHistoryFilter.setVisibility(View.GONE);
                binding.filterWaybillPending.setOnClickListener(v->{selected = WaybillStatus.driverAccepted.getValue();callBackDialogFilter.FilterChosen();});
                binding.filterWaybillIssued.setOnClickListener(v->{selected = WaybillStatus.issued.getValue();callBackDialogFilter.FilterChosen();});
                binding.filterLoaded.setOnClickListener(v->{selected = WaybillStatus.loaded.getValue();callBackDialogFilter.FilterChosen();});
                binding.filterStarted.setOnClickListener(v->{selected = WaybillStatus.arrived.getValue();callBackDialogFilter.FilterChosen();});
                break;
            case "HISTORY":
                break;
        }
    }

        public interface CallBackDialogFilter {
        void FilterChosen();
    }
}
