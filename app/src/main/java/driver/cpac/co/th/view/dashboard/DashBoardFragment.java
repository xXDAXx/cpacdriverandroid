package driver.cpac.co.th.view.dashboard;

import android.content.Intent;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.DashboardInfo;
import driver.cpac.co.th.Model.JobDetailResV2;
import com.tmg.th.R;
import com.tmg.th.databinding.FragmentDashboardV3Binding;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.view.Home.HomeActivity;
import driver.cpac.co.th.view.MailBox.MailBoxActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DashBoardFragment extends Fragment {
    FragmentDashboardV3Binding binding;
    private DashboardViewModel viewModel;
    PreferenceHelper preferenceHelper;
    private static View view;
    HomeActivity parent;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        preferenceHelper = new PreferenceHelper(getContext());
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard_v3, container, false);
        parent = (HomeActivity) getActivity();
        view = binding.getRoot();
        binding.badgeIcon.setVisibility(View.GONE);
        setupCardViewFinished(0, 0, 0);
        binding.ratringBar.setIndicator(true);
        binding.ratringBar.setEnabled(false);
        binding.lblName.setText(preferenceHelper.getString(Constant.FULLNAME,""));
        binding.emailImg.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), MailBoxActivity.class);
            startActivity(intent);
        });
        binding.myJobCounter.setOnClickListener(v -> {
            parent.setPageViewerIndex(1);
        });
        binding.newJobsCounter.setOnClickListener(v -> {
            parent.setPageViewerIndex(2);
        });
        viewModel = ViewModelProviders.of(this).get(DashboardViewModel.class);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        SetupAmbientImage();
        //TestNews();
        binding.newsIcon.setVisibility(View.GONE);

        ObserverSetup();
        getDashboardInfo();
        return view;
    }

    public void getDashboardInfo() {
        String Token = preferenceHelper.getString(Constant.TOKEN, "");
        viewModel.getMstType(Token);
        viewModel.getDashboardInfo(Token);
        viewModel.countUnreadNotifications(Token);
    }

    public void SetupAmbientImage() {
        Calendar rightNow = Calendar.getInstance();
        int now = rightNow.get(Calendar.HOUR_OF_DAY);
        int imgRes = R.drawable.db_morrning;
        if (0 < now && now <= 5) {
            imgRes = R.drawable.db_midnight;
            binding.lblWelcome.setTextColor(ContextCompat.getColor(getContext(), R.color.cargolink_white));
            binding.lblName.setTextColor(ContextCompat.getColor(getContext(), R.color.cargolink_white));
        } else if (5 < now && now <= 13) {
            imgRes = R.drawable.db_morrning;
            binding.lblWelcome.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            binding.lblName.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        } else if (13 < now && now <= 18) {
            imgRes = R.drawable.db_afternoon;
            binding.lblWelcome.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            binding.lblName.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
        } else if (18 < now && now <= 23) {
            imgRes = R.drawable.db_evening;
            binding.lblWelcome.setTextColor(ContextCompat.getColor(getContext(), R.color.cargolink_white));
            binding.lblName.setTextColor(ContextCompat.getColor(getContext(), R.color.cargolink_white));
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        binding.ambientImg.getLayoutParams().width = width + 2;
        binding.ambientImg.requestLayout();
        App.self().getPicasso().load(imgRes).into(binding.ambientImg);

    }

    public void setupCardViewFinished(int weekFinished, int monthFinished, int yearFinished) {
        binding.cvFinishedJob.setMode(1);
        binding.cvFinishedJob.setWeekCounter(weekFinished);
        binding.cvFinishedJob.Setup();
        binding.cvFinishedJob.binding.lblWeek.setOnClickListener(v -> {
            binding.cvFinishedJob.setMode(1);
            binding.cvFinishedJob.setWeekCounter(weekFinished);
            binding.cvFinishedJob.Setup();
        });
        binding.cvFinishedJob.binding.lblMonth.setOnClickListener(v -> {
            binding.cvFinishedJob.setMode(2);
            binding.cvFinishedJob.setMonthCounter(monthFinished);
            binding.cvFinishedJob.Setup();
        });
        binding.cvFinishedJob.binding.lblYear.setOnClickListener(v -> {
            binding.cvFinishedJob.setMode(3);
            binding.cvFinishedJob.setYearCounter(yearFinished);
            binding.cvFinishedJob.Setup();
        });
    }

    public void RemoveOldObserver() {
        viewModel.getDashboardInfoLiveData().removeObservers(this);
        viewModel.getListMutableLiveData().removeObservers(this);
    }

    public void ObserverSetup() {
        RemoveOldObserver();
        viewModel.getDashboardInfoLiveData().observe(this, dashboardInfo -> {
            SetupJobCounter(dashboardInfo);
        });

        viewModel.getListMutableLiveData().observe(this, jobDetailResV2List -> {
            List<JobDetailResV2> newList = jobDetailResV2List;

            JobReminderAdapter adapter = new JobReminderAdapter(getContext(), newList);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            layoutManager.setOrientation(RecyclerView.HORIZONTAL);
            binding.rcvReminder.setLayoutManager(layoutManager);
            binding.rcvReminder.setAdapter(adapter);
            if(adapter.getItemCount() > 0) {
                binding.rcvReminder.setVisibility(View.VISIBLE);
                binding.reminderIcon.setVisibility(View.VISIBLE);
                binding.reminderTxt.setVisibility(View.VISIBLE);
            } else {
                binding.rcvReminder.setVisibility(View.GONE);
                binding.reminderIcon.setVisibility(View.GONE);
                binding.reminderTxt.setVisibility(View.GONE);
            }
        });

        viewModel.getCountUnRead().observe(this, integer -> {
            if (integer > 0) {
                binding.badgeIcon.setVisibility(View.VISIBLE);
                binding.badgeIcon.setNumber(integer);
            }
        });
    }

    public void SetupJobCounter(DashboardInfo dashboardInfo) {
        Util.animateRoundedTextView(Integer.parseInt(binding.myJobCounter.getTitleText()), dashboardInfo.getMyJobCount(), 500, binding.myJobCounter);
        Util.animateRoundedTextView(Integer.parseInt(binding.newJobsCounter.getTitleText()), dashboardInfo.getNewJobCount(), 500, binding.newJobsCounter);
        int totalJob = dashboardInfo.getMyJobCount() + dashboardInfo.getFinishedJobCount() + dashboardInfo.getRejectedJobCount();
        Util.animateRoundedTextViewRate(0, dashboardInfo.getRejectedJobCount(), totalJob, 500, binding.rejectCounter);
        binding.ratringBar.setRating((float) dashboardInfo.getAverageRating());
        binding.ratingNumber.setText(dashboardInfo.getAverageRating() + "/" + dashboardInfo.getRateCount() + getString(R.string.dashboard_label_rates));
        setupCardViewFinished(dashboardInfo.getFinishedJobWeekCount(), dashboardInfo.getFinishedJobMonthCount(), dashboardInfo.getFinishedJobYearCount());
    }

    public void TestNews() {
        List<String> listImg = new ArrayList<String>();
        listImg.add("https://ik.qualitylogoproducts.com/blog/wp-content/uploads/2012/04/236057_HowCelebrityUsed2_053018-2.png");
        listImg.add("https://www.globalerainfotech.com/images/specialization/web-promotion.jpg");
        listImg.add("https://www.ccsj.edu/cms/shavings/wp-content/uploads/sites/52/2018/01/social_media-600x400.jpg");
        listImg.add("https://cdn.prdaily.com/wp-content/uploads/2019/01/Nonprofit_Social_Media-2.jpg");
        NewAdapter adapter = new NewAdapter(getContext(), listImg);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        binding.rcvNews.setLayoutManager(layoutManager);
        binding.rcvNews.setAdapter(adapter);
    }

    public DashboardViewModel getViewModel() {
        return viewModel;
    }

    @Override
    public void onResume() {
        super.onResume();
        //getDashboardInfo();
        SetupAmbientImage();
    }
}