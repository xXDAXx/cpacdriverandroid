package driver.cpac.co.th.util;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import androidx.core.content.ContextCompat;

import com.github.pavlospt.roundedletterview.RoundedLetterView;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;
import driver.cpac.co.th.Model.DetailWaybill;
import driver.cpac.co.th.Model.GetMstTypeRes;
import driver.cpac.co.th.Model.JobDetailResV2;

import com.google.gson.Gson;
import com.tmg.th.R;

import driver.cpac.co.th.Model.MstTypeItem;
import driver.cpac.co.th.util.enumClass.CargoType;
import driver.cpac.co.th.util.enumClass.TruckType;
import driver.cpac.co.th.util.enumClass.UnitType;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.base.SplashActivity;

import java.net.InetAddress;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLContext;

import static android.content.Context.NOTIFICATION_SERVICE;

public class Util {

    //Email Validation pattern
    public static final String regEx = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";

    //Fragments Tags
    public static final String Login_Fragment = "Login_Fragment";
    public static final String ForgotPassword_Fragment = "ForgotPassword_Fragment";
    public static ProgressDialog progressDialog;

    public static void returnLogin(Activity activity) {
        Intent intent = new Intent(activity, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

    private static Util util = null;

    public static Util getInstance() {
        if (util == null) {
            util = new Util();
        }
        return util;
    }

    public boolean checkNetwork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            return cm.getActiveNetworkInfo() != null;
        }
        return false;
    }

    public void openUrl(String url, Context context) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    public void simulateDelay() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static int JobStatusColorHelper(short jobStatus, Context context) {
        int colorCode = 0;
        if (jobStatus == WaybillStatus.waitingDriver.getValue()) {
            colorCode = (ContextCompat.getColor(context, R.color.job_status_new));
        } else if (jobStatus == WaybillStatus.issued.getValue()) {
            colorCode = (ContextCompat.getColor(context, R.color.job_status_waybill_issued));
        } else if (jobStatus == WaybillStatus.arrived.getValue()) {
            colorCode = (ContextCompat.getColor(context, R.color.job_status_started));
        } else if (jobStatus == WaybillStatus.driverAccepted.getValue()) {
            colorCode = (ContextCompat.getColor(context, R.color.job_status_waybill_pending));
        } else if (jobStatus == WaybillStatus.loaded.getValue()) {
            colorCode = (ContextCompat.getColor(context, R.color.job_status_loaded));
        } else if (jobStatus == WaybillStatus.finished.getValue()) {
            colorCode = (ContextCompat.getColor(context, R.color.job_status_done));
        } else if (jobStatus == WaybillStatus.cancelled.getValue()) {
            colorCode = (ContextCompat.getColor(context, R.color.job_status_failed));
        } else if (jobStatus == WaybillStatus.driverRejected.getValue()) {
            colorCode = (ContextCompat.getColor(context, R.color.job_status_rejected));
        } else if (jobStatus == WaybillStatus.shipperRejected.getValue()) {
            colorCode = (ContextCompat.getColor(context, R.color.job_status_failed));
        } else if (jobStatus == WaybillStatus.started.getValue()) {
            colorCode = (ContextCompat.getColor(context, R.color.job_status_new));
        }
        return colorCode;
    }

    /*
    public static String GetCargoType(int cargoType, Context context) {
        String returnCargotype = null;
        for (CargoType type : CargoType.values()) {
            if (cargoType == type.getValue()) {
                String prefix = "cargoType.label.";
                String resName = prefix + type.name();
                int id = context.getResources().getIdentifier(resName, "string", context.getPackageName());
                returnCargotype = context.getString(id);
            }
        }
        return returnCargotype;
    }
    */

    /*
    public static String GetTruckType(int truckType, Context context) {
        String returnTruckType = null;
        for (TruckType type : TruckType.values()) {
            if (truckType == type.getValue()) {
                String prefix = "truckType.label.";
                String resName = prefix + type.name();
                int id = context.getResources().getIdentifier(resName, "string", context.getPackageName());
                returnTruckType = context.getString(id);
            }
        }
        if(returnTruckType == null)
            returnTruckType = context.getString(R.string.truckType_label_others);
        return returnTruckType;
    }

    public static int getTruckTypeImageId (int truckType, Context context) {
        int truckImageId = 0;
        for (TruckType type : TruckType.values()) {
            if (truckType == type.getValue()) {
                String prefix = "";
                String resName = prefix + type.name().toLowerCase();
                truckImageId = context.getResources().getIdentifier(resName, "drawable", context.getPackageName());
                break;
            }
        }
        return truckImageId;
    }
    */

    public static String GetWaybillStatus(short status, Context context) {
        String returnStatus = null;
        for (WaybillStatus sts : WaybillStatus.values()) {
            if (status == sts.getValue()) {
                String prefix = "waybillStatus.label.";
                String resName = prefix + sts.name();
                int id = context.getResources().getIdentifier(resName, "string", context.getPackageName());
                return context.getString(id);
            }
        }
        return returnStatus;
    }

    /*
    public static String GetUnitType(short type, Context context) {
        String returnStatus = null;
        for (UnitType unit : UnitType.values()) {
            if (type == unit.getValue()) {
                String prefix = "unitType.label.";
                String resName = prefix + unit.name();
                int id = context.getResources().getIdentifier(resName, "string", context.getPackageName());
                return context.getString(id);
            }
        }
        return returnStatus;
    }*/

    public static String GetUnitType(short unitType, Context context) {
        String typeRet = "";
        PreferenceHelper preferenceHelper = new PreferenceHelper(context);
        Gson gson = new Gson();

        GetMstTypeRes res = gson.fromJson(preferenceHelper.getString(Constant.MST_TYPE,""),GetMstTypeRes.class);
        if(res != null) {
            for (MstTypeItem item : res.getUnitType()) {
                if (item.getType() == unitType) {
                    if (LangUtils.getCurrentLanguage().equals("en")) {
                        typeRet = item.getNameEng();
                        break;
                    } else if (LangUtils.getCurrentLanguage().equals("th")) {
                        typeRet = item.getNameThai();
                        break;
                    }
                }
            }
        }
        return typeRet;
    }

    public static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_locaction_updates";

    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The {@link Context}.
     */
    public static boolean requestingLocationUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }

    /**
     * Stores the location updates state in SharedPreferences.
     *
     * @param requestingLocationUpdates The location updates state.
     */
    public static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                .apply();
    }

    /**
     * Returns the {@code location} object as a human readable string.
     *
     * @param location The {@link Location}.
     */
    public static String getLocationText(Location location) {
        return location == null ? "Unknown location" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }

    public static String getLocationTitle(Context context) {
        return context.getString(R.string.location_updated,
                DateFormat.getDateTimeInstance().format(new Date()));
    }

    public static String TimeRemainingTitlefromString(String time, short status, Context context) {
        String text, tmp;
        Date loadingDate = new Date(convertStringToTimestamp(time).getTime());
        Date toDay = new Date((System.currentTimeMillis()));
        WaybillStatus waybillStatus = WaybillStatus.getEnumType(status);
        long diff = 0;
        diff = loadingDate.getTime() - toDay.getTime();
        tmp = context.getString(R.string.time_countdown_loadingTemplate);
        if (waybillStatus == WaybillStatus.loaded) {
            tmp = context.getString(R.string.time_countdown_deliveryTemplate);
        } else if (waybillStatus == WaybillStatus.finished) {
            diff = toDay.getTime() - loadingDate.getTime();
            tmp = context.getString(R.string.time_countdown_finishTemplate);
        }
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = diff / daysInMilli;
        diff = diff % daysInMilli;
        long elapsedHours = diff / hoursInMilli;
        diff = diff % hoursInMilli;
        long elapsedMinutes = diff / minutesInMilli;
        text = String.format(tmp, elapsedDays, elapsedHours, elapsedMinutes);
        return text;
    }

    public static Timestamp convertStringToTimestamp(String value) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            Date parsedDate = dateFormat.parse(value);
            Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
            return timestamp;
        } catch (Exception e) {
        }
        return null;
    }

    public static String GetFriendlyTimeText(Timestamp timestamp) {
        Date date = utcToLocal(new Date(timestamp.getTime()));
        Calendar calendar = Calendar.getInstance();
        Date toDay = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        Date yesterday = calendar.getTime();
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        Calendar cal3 = Calendar.getInstance();
        calendar.setTime(toDay);
        cal1.setTime(tomorrow);
        cal2.setTime(date);
        cal3.setTime(yesterday);
        String prefixDate;
        if (calendar.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR))
            prefixDate = "Today, ";
        else if (cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR))
            prefixDate = "Tomorrow, ";
        else if (cal3.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR))
            prefixDate = "Yesterday, ";
        else prefixDate = new SimpleDateFormat("dd/MM/yyyy ").format(date);
        SimpleDateFormat timefm = new SimpleDateFormat("hh:mm aa");
        String timeString = timefm.format(date);
        return (prefixDate + timeString);
    }

    public static Date utcToLocal(Date utcDate) {
        return new Date(utcDate.getTime()+ TimeZone.getDefault().getOffset(utcDate.getTime()));
    }

    public static DetailWaybill GetNextDeliveryPoint(JobDetailResV2 jobDetailResV2) {
        DetailWaybill detailWaybill = null;
        if (jobDetailResV2.getStatus() == WaybillStatus.loaded.getValue()) {
            for (DetailWaybill waybill : jobDetailResV2.getListDetailWaybil()) {
                if (waybill.getStatus() != WaybillStatus.loaded.getValue()&& waybill.getStatus() != WaybillStatus.finished.getValue()) {
                    detailWaybill = waybill;
                    break;
                }
            }
        }
        return detailWaybill;
    }

    public static boolean isLastDeliveryPoint(JobDetailResV2 jobDetailResV2) {
       if(Util.GetNextDeliveryPoint(jobDetailResV2).getId() == jobDetailResV2.getListDetailWaybil().get(jobDetailResV2.getListDetailWaybil().size()-1).getId())
           return true;
       else
           return false;
    }

    public static void animateRoundedTextView(int initialValue, int finalValue, int duration, final RoundedLetterView textview) {
        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(duration);
        valueAnimator.addUpdateListener(valueAnimator1 -> textview.setTitleText(valueAnimator1.getAnimatedValue().toString()));
        valueAnimator.start();

    }

    public static void animateRoundedTextViewRate(int initialRejectedValue, int finalRejectedValue, int Sum,  int duration, final RoundedLetterView textview) {
        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialRejectedValue, finalRejectedValue);
        valueAnimator.setDuration(duration);
        valueAnimator.addUpdateListener(valueAnimator1 -> textview.setTitleText(valueAnimator1.getAnimatedValue().toString() + "/" + String.valueOf(Sum)));
        valueAnimator.start();

    }

    public static boolean isAppOnForeground(Context context, String appPackageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = appPackageName;
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                //                Log.e("app",appPackageName);
                return true;
            }
        }
        return false;
    }

    public static boolean jobBelongToUser( JobDetailResV2 jobDetailResV2, int userId) {
        boolean result = true;
        int primaryDriverId = jobDetailResV2.getPrimaryDriver().getId();
        if ( jobDetailResV2.getSecondaryDriver() != null) {
            int secondaryDriverId = jobDetailResV2.getSecondaryDriver().getId();
            if(userId != primaryDriverId && userId != secondaryDriverId) return false;
        } else if(userId != primaryDriverId) return false;
        return result;
    }

    public static void DismissAllNotificaion(Context context) {
        NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        manager.cancelAll();
    }

    public static boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }
    }

    public static String convertToTitleCaseIteratingChars(String text) {
        if (text == null || text.isEmpty()) {
            return text;
        }

        StringBuilder converted = new StringBuilder();

        boolean convertNext = true;
        for (char ch : text.toCharArray()) {
            if (Character.isSpaceChar(ch)) {
                convertNext = true;
            } else if (convertNext) {
                ch = Character.toTitleCase(ch);
                convertNext = false;
            } else {
                ch = Character.toLowerCase(ch);
            }
            converted.append(ch);
        }

        return converted.toString();
    }

    public static void initializeSSLContext(Context mContext){
        try {
            SSLContext.getInstance("TLSv1.2");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            ProviderInstaller.installIfNeeded(mContext.getApplicationContext());
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public static String getFileNameByUrl(String url) {
        String fileName = null;
        if(url != null) {
            String[] abc = url.split("/");
            fileName = abc[abc.length-1];
        }
        return fileName;
    }

    public static  boolean checkRulePassword(String pwd) {
        String regexp = "^(?=.*[a-zA-Z0-9]).{4,20}$";
        Pattern pattern = Pattern.compile(regexp);
        Matcher m = pattern.matcher(pwd);
        if (m.matches()) {
            return true;
        }
        return false;
    }

}
