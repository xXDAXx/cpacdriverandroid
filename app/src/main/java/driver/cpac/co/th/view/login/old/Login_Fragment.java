package driver.cpac.co.th.view.login.old;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.tmg.th.R;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.view.Home.HomeActivity;
import com.tmg.th.databinding.LoginLayoutBinding;
import driver.cpac.co.th.view.MainActivity;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogChangeLanguage;
import driver.cpac.co.th.view.dialog.DialogNotice;
import driver.cpac.co.th.view.dialog.DialogWait;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import driver.cpac.co.th.view.dialog.DialogWait2;
import okhttp3.ResponseBody;
import driver.cpac.co.th.view.dialog.ProgressDialogFragment;

public class Login_Fragment extends Fragment implements DialogChangeLanguage.DialogDissmiss {
    private LoginViewModel loginViewModel;
    private LoginLayoutBinding binding;
    //private static FragmentManager fragmentManager;
    public Fragment thisFragment;
    private static View view;
    private static FragmentTransaction fragmentTransaction;
    DialogChangeLanguage dialogChangeLanguage;
    private AlertDialog alertDialog;
    private DialogWait2 dialogWait;
    private DialogNotice dialogNotice;
    private static LinearLayout thislayout;
    private String deviceToken;
    private int deviceType;
    PreferenceHelper preferenceHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.login_layout, container, false);
        view = binding.getRoot();
        thisFragment = this;
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        binding = DataBindingUtil.inflate(
                inflater, R.layout.login_layout, container, false);
        binding = DataBindingUtil.setContentView(this.getActivity(), R.layout.login_layout);
        //binding.setLifecycleOwner(this.getActivity());
        binding.setLifecycleOwner(this);
        binding.setLoginViewModel(loginViewModel);
        preferenceHelper = new PreferenceHelper(getContext());
        dialogChangeLanguage = new DialogChangeLanguage((OldBaseActivity)getActivity(), this);
        binding.imgLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogChangeLanguage.show();
            }
        });
        //init layout
        thislayout = (LinearLayout) this.getActivity().findViewById(R.id.Rlogin_layout);
        dialogWait = new DialogWait2(getActivity());

        loginViewModel.getUser().observe(this, loginUser -> {

            //hide keyboard
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(thislayout.getWindowToken(), 0);
            //
            if (TextUtils.isEmpty(Objects.requireNonNull(loginUser).getStrEmailAddress())) {
                binding.txtEmailAddress.setError(getString(R.string.login_errorMsg_emptyMail));
                binding.txtEmailAddress.requestFocus();
            } else if (!loginUser.isEmailValid()) {
                binding.txtEmailAddress.setError(getString(R.string.login_errorMsg_invalidMail));
                binding.txtEmailAddress.requestFocus();
            } else if (TextUtils.isEmpty(Objects.requireNonNull(loginUser).getStrPassword())) {
                binding.txtPassword.setError(getString(R.string.login_errorMsg_emptyPassword));
                binding.txtPassword.requestFocus();
            } else if (!loginUser.isPasswordLengthGreaterThan5()) {
                binding.txtPassword.setError(getString(R.string.login_errorMsg_passwordIsShort));
                binding.txtPassword.requestFocus();
            } else {
                dialogWait.show();
                loginUser.setDeviceToken(deviceToken);
                loginUser.setDeviceType(1); // 0 - iOS, 1 - Android
                loginViewModel.callLogin(loginUser);

            }
        });

        loginViewModel.getMove2Forgot().observe(this, move2Forgot -> {
            if (move2Forgot) {
                fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.Rlogin_layout, new ForgotPassword_Fragment()).commit();
                fragmentTransaction.addToBackStack(null);
            }
        });

        loginViewModel.getJwt().observe(this, Jwt -> {
            //dialogWait.hide();
            if (Jwt == null) {
                dialogNotice = new DialogNotice(getString(R.string.login_errorMsg_loginFailed), getActivity());
                dialogNotice.show();
            } else // success
            {
                if (preferenceHelper.getString(Constant.AVATAR, "").length() > 1) {
                    LoadAvatarFromUrl(preferenceHelper.getString(Constant.AVATAR, ""));
                } else {
                    Context ctx = getContext();
                    Intent myIntent;
                    myIntent = new Intent(ctx, HomeActivity.class);
                    myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    dialogWait.hide();
                    startActivity(myIntent);
                }
            }
        });

        loginViewModel.getResponseDownloadAvatar().observe(this, responseBody -> saveBitmapToStorage(responseBody));


        loginViewModel.getLoginFailed().observe(this, aBoolean -> {
            if (aBoolean == false) {
                dialogWait.hide();
                dialogNotice = new DialogNotice(getString(R.string.login_errorMsg_loginFailed), getActivity());
                dialogNotice.show();
            }
        });

        // FireBase
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(ProgressDialogFragment.TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        deviceToken = task.getResult().getToken();
                        Log.d(ProgressDialogFragment.TAG, deviceToken);
                        //Toast.makeText(getContext(), deviceToken, Toast.LENGTH_SHORT).show();
                    }
                });
        return view;
    }


    @Override
    public void RefreshContent() {
        binding.btnLogin.setText(R.string.logout_btn);
        binding.forgotPassword.setText(R.string.forgot);
        binding.showHidePassword.setText(R.string.show_pwd);
        binding.txtEmailAddress.setHint(R.string.email);
        binding.txtPassword.setHint(R.string.password);
        binding.lblLogin.setText(R.string.login);
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void LoadAvatarFromUrl(String url) {
        loginViewModel.downloadAvatar(url);
    }

    public void saveBitmapToStorage(ResponseBody body) {
        try {
            if (body != null)
                dialogWait.hide();
                Dexter.withActivity(thisFragment.getActivity())
                        .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener() {
                                          @Override
                                          public void onPermissionGranted(PermissionGrantedResponse response) {
                                              ContextWrapper cw = new ContextWrapper(getContext());
                                              // path to /data/data/yourapp/app_data/imageDir
                                              File directory = cw.getDir("avatars", Context.MODE_PRIVATE);
                                              // Create imageDir
                                              File mypath = new File(directory, "avatar0.jpg");
                                              try {
                                                  mypath.createNewFile();
                                              } catch (IOException e) {
                                                  e.printStackTrace();
                                              }
                                              FileOutputStream fos = null;
                                              try {
                                                  InputStream in = body.byteStream();
                                                  fos = new FileOutputStream(mypath);
                                                  // Use the compress method on the BitMap object to write image to the OutputStream
                                                  int c;

                                                  while ((c = in.read()) != -1) {
                                                      fos.write(c);
                                                  }
                                              } catch (Exception e) {
                                                  e.printStackTrace();
                                              } finally {
                                                  try {
                                                      fos.close();
                                                  } catch (IOException ex) {
                                                  }
                                              }
                                          }

                                          @Override
                                          public void onPermissionDenied(PermissionDeniedResponse response) {
                                              Log.println(1, "abc", "permission denied");
                                          }

                                          @Override
                                          public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                              Log.println(1, "abc", "onPermissionRationaleShouldBeShown");
                                          }
                                      }
                        ).check();
            Context ctx = getContext();
            Intent myIntent;
            myIntent = new Intent(ctx, HomeActivity.class);
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(myIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //DOWNLOAD AVATAR

}