package driver.cpac.co.th.view.jobprocessing.JobProcess;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.Documents;
import driver.cpac.co.th.Model.JobDetailResV2;
import com.tmg.th.R;
import com.tmg.th.databinding.FragmentJobProcessingV2Binding;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.GenericFileProvider;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.CargoType;
import driver.cpac.co.th.util.enumClass.TruckType;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.MapView.MapViewActivity;
import driver.cpac.co.th.view.dialog.DialogNotice;
import driver.cpac.co.th.view.documentViewer.WebViewerActivity;
import driver.cpac.co.th.view.issueReport.ReportingMainActivity;
import driver.cpac.co.th.view.issueReport.ReportingStep1Activity;
import driver.cpac.co.th.view.jobdetail.DestinationListAdapter;
import driver.cpac.co.th.view.jobprocessing.Documents.ListDocumentsAdapterV2;
import driver.cpac.co.th.view.jobprocessing.JobProcessingActivityV3;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class JobProcessFragmentV2 extends Fragment {
    static final int REQUEST_IMAGE_CAPTURE = 1888;
    private FragmentJobProcessingV2Binding binding;
    private JobProcessViewModel jobProcessViewModel;
    public JobDetailResV2 jobDetail;
    private JobProcessMode mode;
    private static View view;
    JobProcessingActivityV3 parent;
    String uploadFileType = null;
    private Uri mImageUri;
    PreferenceHelper preferenceHelper;
    DestinationListAdapter adapter;

    public enum JobProcessMode {
        LOAD(1),
        DELIVERY(2),
        HISTORY(3);
        private final int value;

        JobProcessMode(int value) {
            this.value = value;
        }

        public int getValue() {
            return (int) value;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_job_processing_v2, container, false);
        view = binding.getRoot();
        jobProcessViewModel = ViewModelProviders.of(this).get(JobProcessViewModel.class);
        binding.setLifecycleOwner(this);
        preferenceHelper = new PreferenceHelper(getContext());
        //Get bunndle
        parent = (JobProcessingActivityV3) getActivity();
        jobDetail = (JobDetailResV2) getArguments().getSerializable("jobDetail");
        if (jobDetail.getStatus() == WaybillStatus.loaded.getValue()) mode = JobProcessMode.DELIVERY;
        if (jobDetail.getStatus() == WaybillStatus.arrived.getValue()) mode = JobProcessMode.LOAD;
        if (jobDetail.getStatus() == WaybillStatus.started.getValue()) binding.addDocCardView.setVisibility(View.GONE);
        //Binding Data
        onDisplay_Detail_Shipment();
        setupRcvDocument(jobDetail);
        binding.dropList.cardViewMapButton.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), MapViewActivity.class);
            intent.putExtra("jobDetail", jobDetail);
            startActivity(intent);
        });

        /*
        binding.addDocCardView.setOnClickListener(view1 -> {
            if (binding.rcvDocuments.getVisibility() == View.GONE) {
                binding.rcvDocuments.setVisibility(View.VISIBLE);
                parent.getBinding().scrollView.setFillViewport(true);
                binding.scrollView.post(new Runnable() {
                    public void run() {
                        parent.getBinding().scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                });
            } else {
                binding.rcvDocuments.setVisibility(View.GONE);
                parent.getBinding().scrollView.setFillViewport(true);
            }
        });
        */

        binding.dropList.cardViewIssueButton.setOnClickListener(v ->{
            Intent myIntent;
            //myIntent = new Intent(getContext(), ReportingStep1Activity.class);
            myIntent = new Intent(getContext(), ReportingMainActivity.class);
            myIntent.putExtra("jobDetail", jobDetail);
            startActivity(myIntent);
        });

        ObserverSetup();
        return view;
    }

    public void ObserverSetup() {

        jobProcessViewModel.getJobDetailRefreshData().observe(this, jobDetailRes -> {
            jobDetail = jobDetailRes;
            parent.setJobDetailRes(jobDetailRes);
            onDisplay_Detail_Shipment();
            setupRcvDocument(jobDetailRes);
            DialogNotice dialogNotice = new DialogNotice(getString(R.string.jobProcess_dialogContent_noticeUploadedDocument), getActivity());
            dialogNotice.show();
        });

        jobProcessViewModel.getLoading().observe(this, aBoolean -> {
            if (aBoolean) parent.showLoading();
            else parent.hideLoading();
        });


    }

    public void setupRcvDocument(JobDetailResV2 jobDetailRes) {

        List<Documents> documentsList = jobDetailRes.getDocuments();
        driver.cpac.co.th.view.jobprocessing.Documents.ListDocumentsAdapterV2 mAdapter = new driver.cpac.co.th.view.jobprocessing.Documents.ListDocumentsAdapterV2(getContext(), documentsList, viewClickListener, deleteClickListener, WaybillStatus.getEnumType(jobDetailRes.getStatus()));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        binding.rcvDocuments.setLayoutManager(layoutManager);
        binding.rcvDocuments.setAdapter(mAdapter);
        parent.getBinding().scrollView.setFillViewport(true);
        binding.scrollView.post(() -> parent.getBinding().scrollView.fullScroll(View.FOCUS_DOWN));
    }

    public void onDisplay_Detail_Shipment() {
        if (jobDetail.getStatus() == WaybillStatus.started.getValue()
                || jobDetail.getStatus() == WaybillStatus.draft.getValue()
                || jobDetail.getStatus() == WaybillStatus.waitingDriver.getValue()
                || jobDetail.getStatus() == WaybillStatus.driverAccepted.getValue())
        binding.addDocCardView.setVisibility(View.GONE);
        else binding.addDocCardView.setVisibility(View.VISIBLE);

        if (jobDetail.getEstimateDistance() == null || jobDetail.getEstimateDistance().isEmpty()) {
            binding.dropList.distanceInfo.setVisibility(View.GONE);
            binding.dropList.distanceIcon.setVisibility(View.GONE);
        } else
            binding.dropList.distanceInfo.setText(getString(R.string.common_est) + " " + jobDetail.getEstimateDistance());
        //Binding list Destination
        ((SimpleItemAnimator) binding.dropList.rcvDestinationList.getItemAnimator()).setSupportsChangeAnimations(true);
        adapter = new DestinationListAdapter(getContext(), jobDetail, true, false);
        binding.dropList.rcvDestinationList.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.dropList.rcvDestinationList.setAdapter(adapter);
        binding.dropList.cardViewDriverInfo.setVisibility(View.GONE);

        // GOODS INFO
        String goodInfo = jobDetail.getTotalWeight().toString() + " " + getString(R.string.common_tons)  // 20 tons
                + " " + CargoType.GetCargoType(jobDetail.getTypeOfCargo(), getContext());  // FOOODS
        if (jobDetail.getQuantity() > 0) {
            goodInfo += " - " + jobDetail.getQuantity().toString() + " " + Util.GetUnitType(jobDetail.getUnit(), getContext()).toLowerCase()
                    + " ";
            if (!(jobDetail.getSizeOfCargo().equals("0 x 0 x 0m") || jobDetail.getSizeOfCargo().equals("- x - x -m")
            ||jobDetail.getSizeOfCargo().equals("0.0 x 0.0 x 0.0m") ||jobDetail.getSizeOfCargo().equals("0.00 x 0.00 x 0.00m")))
                goodInfo += jobDetail.getSizeOfCargo();
        }
        binding.dropList.goodsInfo.setText(goodInfo);

        //Truck Info
        binding.dropList.truckInfo.setText(TruckType.GetTruckType(jobDetail.getTruckType(), getContext()).toUpperCase() + " - " + jobDetail.getTruckNumber().toUpperCase());
        App.self().getPicasso().load(TruckType.GetTruckTypeImg(jobDetail.getTruckType(),getContext())).placeholder(R.drawable.others).into(binding.dropList.truckIcon);
        //Driver Info
        int userId = preferenceHelper.getInt(Constant.USER_ID, 0);
        if (jobDetail.getSecondaryDriver() != null) {   // IF TRIP HAVE 2 DRIVER
            if (userId == jobDetail.getSecondaryDriver().getId()) { // DRIVER IS PRIMARY DRIVER --> SHOW SECOND DRIVER INFO
                if (jobDetail.getPrimaryDriver().getAvaUrl() != null)
                    App.self().getPicasso().load(jobDetail.getPrimaryDriver().getAvaUrl()).placeholder(R.drawable.account).into(binding.dropList.imgAvatar);
                binding.dropList.driverName.setText(jobDetail.getPrimaryDriver().getDriverName());
                binding.dropList.driverTel.setText(getText(R.string.common_tel) + jobDetail.getPrimaryDriver().getDriverContact());
                binding.dropList.driverRole.setText(R.string.common_label_primaryDriver);
                binding.dropList.btnDrvCall.setOnClickListener(v -> {
                    String phone = jobDetail.getPrimaryDriver().getDriverContact();
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                    startActivity(intent);
                });
                binding.dropList.btnDrvSms.setOnClickListener(v -> {
                    String number = jobDetail.getPrimaryDriver().getDriverContact();  // The number on which you want to send SMS
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                });
            } else {  // DRIVER IS PRIMARY DRIVER --> SHOW SECOND DRIVER INFO
                if (jobDetail.getSecondaryDriver().getAvaUrl() != null)
                    App.self().getPicasso().load(jobDetail.getSecondaryDriver().getAvaUrl()).placeholder(R.drawable.account).into(binding.dropList.imgAvatar);
                binding.dropList.driverName.setText(jobDetail.getSecondaryDriver().getDriverName());
                binding.dropList.driverTel.setText(getText(R.string.common_tel)  + jobDetail.getSecondaryDriver().getDriverContact());
                binding.dropList.driverRole.setText(R.string.common_label_secondDriver);
                binding.dropList.btnDrvCall.setOnClickListener(v -> {
                    String phone = jobDetail.getSecondaryDriver().getDriverContact();
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                    startActivity(intent);
                });
                binding.dropList.btnDrvSms.setOnClickListener(v -> {
                    String number = jobDetail.getSecondaryDriver().getDriverContact();  // The number on which you want to send SMS
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                });

            }
        }
    }

    driver.cpac.co.th.view.jobprocessing.Documents.ListDocumentsAdapterV2.DocumentViewClickListener viewClickListener = documents -> {
        if (documents.getDocumentType() != null && documents.getDocumentType().equals("addnew")) {
            addDocumentBtnClick();
        } else {
            Intent intent = new Intent(getContext(), WebViewerActivity.class);
            intent.putExtra("content", documents);
            startActivity(intent);
        }
    };

    ListDocumentsAdapterV2.DocumentDeleteClickListener deleteClickListener = documents -> {

    };

    public void addDocumentBtnClick() {
        switch (mode) {
            case LOAD:
                uploadFileType = "POL";
                break;
            case DELIVERY:
                uploadFileType = "POD";
                break;
            case HISTORY:
                break;
        }
        Dexter.withActivity(getActivity())
                .withPermission(Manifest
                        .permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                                  @Override
                                  public void onPermissionGranted(PermissionGrantedResponse response) {
                                      IntendCameraAction();
                                  }

                                  @Override
                                  public void onPermissionDenied(PermissionDeniedResponse response) {/* ... */}

                                  @Override
                                  public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                      token.continuePermissionRequest();
                                  }
                              }
                ).check();
    }

    public void IntendCameraAction() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = null;
        try {
            photo = createTemporaryFile("picture", ".jpg");
            photo.delete();
        } catch (Exception e) {
            Log.v("ABC", "Can't create file to take picture!");
            Toast.makeText(getContext(), "Please check SD card! Image shot is impossible!", Toast.LENGTH_SHORT);
        }
        mImageUri = GenericFileProvider.getUriForFile(getContext(), getContext().getApplicationContext().getPackageName() + ".fileprovider", photo);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        //start camera intent
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            getActivity().startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            try {
                parent.showLoading();
                String fileName = uploadFileType + getCurrentTime();
                Dexter.withActivity(getActivity())
                        .withPermission(Manifest
                                .permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener() {
                                          @Override
                                          public void onPermissionGranted(PermissionGrantedResponse response) {
                                              saveImgToStorage(fileName);
                                          }

                                          @Override
                                          public void onPermissionDenied(PermissionDeniedResponse response) {/* ... */}

                                          @Override
                                          public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                              token.continuePermissionRequest();
                                          }
                                      }
                        ).check();
                ContextWrapper cw = new ContextWrapper(getActivity().getApplicationContext());
                File directory = cw.getDir("documents", Context.MODE_PRIVATE);
                File file = new File(directory, fileName + ".jpg");
                PreferenceHelper preferenceHelper = new PreferenceHelper(getContext());
                jobProcessViewModel.newUploadDocument(file, jobDetail, uploadFileType, preferenceHelper.getString(Constant.TOKEN, ""));
            } catch (Exception ex) {
                Log.println(1, "222", ex.toString());
            }
        }


    }

    public void saveImgToStorage(String filename) {
        try {
            getContext().getContentResolver().notifyChange(mImageUri, null);
            ContentResolver cr = getContext().getContentResolver();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(cr, mImageUri);
            } catch (Exception e) {
                Toast.makeText(getContext(), "Failed to load", Toast.LENGTH_SHORT).show();
                Log.d("ABC", "Failed to load", e);
            }
            ContextWrapper cw = new ContextWrapper(getActivity().getApplicationContext());
            File directory = cw.getDir("documents", Context.MODE_PRIVATE);
            File f = new File(directory, filename + ".jpg");
            File mypath = new File(directory, filename + ".jpg");
            FileOutputStream fos = null;
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            getResizedBitmap(bitmap, 3000).compress(Bitmap.CompressFormat.JPEG, 80, fos);
        } catch (Exception e) {
            Log.println(1, "222", e.toString());
        }
    }

    public static final String DATE_FORMAT_1 = "yyyyMMddHHmmss";

    public static String getCurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_1);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    private File createTemporaryFile(String part, String ext) throws Exception {

        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

}
