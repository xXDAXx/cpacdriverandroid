package driver.cpac.co.th.view.MyJobs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import driver.cpac.co.th.Model.ListJobRes;

import com.squareup.picasso.Picasso;
import com.tmg.th.R;
import com.tmg.th.databinding.ItemShipmentLayoutBinding;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.CargoType;
import driver.cpac.co.th.util.enumClass.TruckType;

import java.util.ArrayList;
import java.util.List;

public class ListJobAdapter extends RecyclerView.Adapter<ListJobAdapter.MyViewHolder> implements Filterable {

    protected List<ListJobRes> jobList;
    protected List<ListJobRes> originalList;
    private Context context;
    private JobClickListener mListener;
    private ItemShipmentLayoutBinding binding;

    public ListJobAdapter(Context context, List<ListJobRes> jobList, JobClickListener listener) {
        this.context = context;
        this.jobList = jobList;
        this.originalList = jobList;
        mListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemShipmentLayoutBinding itemBinding =
                ItemShipmentLayoutBinding.inflate(layoutInflater, viewGroup, false);
        return new MyViewHolder(itemBinding, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        ListJobRes currentShipment = jobList.get(position);
        MyViewHolder shipmentHolder = (MyViewHolder) viewHolder;
        shipmentHolder.itemView.setOnClickListener(v -> {
            mListener.passContent(currentShipment);
        });
        viewHolder.bind(currentShipment);
    }

    @Override
    public int getItemCount() {
        return jobList == null ? 0 : jobList.size();
    }

    public void updateAdapter(ArrayList<ListJobRes> arrayList) {
        this.jobList.clear();
        this.jobList = arrayList;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemShipmentLayoutBinding binding;

        public MyViewHolder(ItemShipmentLayoutBinding binding, JobClickListener listener) {
            super(binding.getRoot());
            mListener = listener;
            this.binding = binding;
        }

        public void bind(ListJobRes item) {
            binding.shipmentId.setText(context.getString(R.string.waybill_label_prefix) + item.getId().toString().toUpperCase() + " - " + item.getShipperName().toUpperCase());
            binding.shipmentSts.setText(Util.GetWaybillStatus(item.getStatus(),context));
            binding.pickupAddr.setText(item.getLoadingAddress());
            binding.dropAddr.setText(item.getDeliveryAddress());
            binding.pickupTime.setText(item.getTimeFromPickupOrg() + " -> " + item.getTimeToPickupOrg());
            binding.dropTime.setText(item.getTimeFromDeliveryOrg() + " -> " + item.getTimeToDeliveryOrg());
            binding.goodsInfo.setText(CargoType.GetCargoType(item.getTypeOfCargo(),context) + " - " + item.getTotalWeight().toString() + " tons");
            binding.executePendingBindings();
            binding.dashline.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            //color status
            binding.shipmentSts.setBackgroundColor(Util.JobStatusColorHelper(item.getStatus(), context));
            binding.truckInfo.setText(TruckType.GetTruckType(item.getTypeOfTruck(),context).toUpperCase() + " - " + item.getTruckNumber().toUpperCase());
            binding.shipmentId.setSelected(true);
            binding.lblEstimate2.setText(context.getText(R.string.common_est)+ " "+ item.getEstimateDistance());
            //binding.cargoIcon.setImageResource(CargoType.GetCargoTypeImg(item.getTypeOfCargo(),context));
            Picasso.get().load(CargoType.GetCargoTypeImg(item.getTypeOfCargo(), context)).placeholder(R.drawable.others).into(binding.cargoIcon);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                jobList = (List<ListJobRes>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<ListJobRes> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = jobList;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    public Filter getStatusFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                jobList = (List<ListJobRes>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<ListJobRes> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = jobList;
                } else {
                    filteredResults = getStatusFilteredResults(Short.valueOf(constraint.toString().toLowerCase()));
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    protected List<ListJobRes> getFilteredResults(String constraint) {
        List<ListJobRes> results = new ArrayList<>();

        for (ListJobRes item : originalList) {
            if (item.getShipperName().toLowerCase().contains(constraint) ||
                    item.getId().toString().toLowerCase().contains(constraint) ||
                    item.getTimeFromDeliveryOrg().toLowerCase().contains(constraint) ||
                    item.getTimeToDeliveryOrg().toLowerCase().contains(constraint) ||
                    item.getTimeFromPickupOrg().toLowerCase().contains(constraint) ||
                    item.getTimeToPickupOrg().toLowerCase().contains(constraint) ||
                    item.getTruckNumber().toLowerCase().contains(constraint) ||
                    item.getDeliveryAddress().toLowerCase().contains(constraint) ||
                    item.getLoadingAddress().toLowerCase().contains(constraint)) {
                results.add(item);
            }
        }
        return results;
    }

    protected List<ListJobRes> getStatusFilteredResults(int status) {
        List<ListJobRes> results = new ArrayList<>();

        for (ListJobRes item : originalList) {
            if (item.getStatus() == status) {
                results.add(item);
            }
        }
        return results;
    }

}

