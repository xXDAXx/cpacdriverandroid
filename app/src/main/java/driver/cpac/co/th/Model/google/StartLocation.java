package driver.cpac.co.th.Model.google;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class StartLocation implements Serializable{
    @SerializedName("lng")
    @Expose
    private Double lng;

    @SerializedName("lat")
    @Expose
    private Double lat;

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public StartLocation(Double lng, Double lat) {
        this.lng = lng;
        this.lat = lat;
    }
}
