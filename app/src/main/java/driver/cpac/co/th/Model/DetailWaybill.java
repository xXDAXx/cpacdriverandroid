package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DetailWaybill implements Serializable, Cloneable, Comparable<DetailWaybill> {
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("hashId")
    @Expose
    private String hashId;

    @SerializedName("waybillId")
    @Expose
    private int waybillId;

    @SerializedName("shipmentId")
    @Expose
    private int shipmentId;

    @SerializedName("quantity")
    @Expose
    private int quantity;

    @SerializedName("unit")
    @Expose
    private short unit;

    @SerializedName("totalWeight")
    @Expose
    private double totalWeight;

    @SerializedName("note")
    @Expose
    private String note;

    @SerializedName("addressDest")
    @Expose
    private String addressDest; // deliveryAddress

    @SerializedName("longitudeDest")
    @Expose
    private double longitudeDest;

    @SerializedName("latitudeDest")
    @Expose
    private double latitudeDest;

    @SerializedName("estimateDistance")
    @Expose
    private String estimateDistance;

    @SerializedName("fullnameDest")
    @Expose
    private String fullnameDest;

    @SerializedName("phoneDest")
    @Expose
    private String phoneDest;

    @SerializedName("deliveryDatetime")
    @Expose
    private String deliveryDatetime;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("shouldBeExpanded")
    @Expose
    private boolean shouldBeExpanded;

    @SerializedName("isExpandable")
    @Expose
    private boolean isExpandable;

    @SerializedName("doneTime")
    @Expose
    private String doneTime;

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public void setTotalWeight(double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public boolean isShouldBeExpanded() {
        return shouldBeExpanded;
    }

    public String getDoneTime() {
        return doneTime;
    }

    public void setDoneTime(String doneTime) {
        this.doneTime = doneTime;
    }

    public boolean isExpandable() {
        return isExpandable;
    }

    public void setExpandable(boolean expandable) {
        isExpandable = expandable;
    }

    public boolean getShouldBeExpanded() {
        return shouldBeExpanded;
    }

    public void setShouldBeExpanded(boolean expanded) {
        shouldBeExpanded = expanded;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getWaybillId() {
        return waybillId;
    }

    public void setWaybillId(int waybillId) {
        this.waybillId = waybillId;
    }

    public int getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(int shipmentId) {
        this.shipmentId = shipmentId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public short getUnit() {
        return unit;
    }

    public void setUnit(short unit) {
        this.unit = unit;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAddressDest() {
        return addressDest;
    }

    public void setAddressDest(String addressDest) {
        this.addressDest = addressDest;
    }

    public double getLongitudeDest() {
        return longitudeDest;
    }

    public void setLongitudeDest(double longitudeDest) {
        this.longitudeDest = longitudeDest;
    }

    public double getLatitudeDest() {
        return latitudeDest;
    }

    public void setLatitudeDest(double latitudeDest) {
        this.latitudeDest = latitudeDest;
    }

    public String getEstimateDistance() {
        return estimateDistance;
    }

    public void setEstimateDistance(String estimateDistance) {
        this.estimateDistance = estimateDistance;
    }

    public String getFullnameDest() {
        return fullnameDest;
    }

    public void setFullnameDest(String fullnameDest) {
        this.fullnameDest = fullnameDest;
    }

    public String getPhoneDest() {
        return phoneDest;
    }

    public void setPhoneDest(String phoneDest) {
        this.phoneDest = phoneDest;
    }

    public String getDeliveryDatetime() {
        return deliveryDatetime;
    }

    public void setDeliveryDatetime(String deliveryDatetime) {
        this.deliveryDatetime = deliveryDatetime;
    }

    @Override
    public int compareTo(DetailWaybill o) {
        return this.getId() - o.getId();
    }
}
