package driver.cpac.co.th.Model.RealmObject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TrackingInfo extends RealmObject implements Serializable {
    @SerializedName("vehicle") @Expose private String vehicle;
    @PrimaryKey @SerializedName("datetime") @Expose long datetime;
    @SerializedName("driver") @Expose private String driver;
    @SerializedName("speed") @Expose private float speed;
    @SerializedName("longitude") @Expose private Double longitude;
    @SerializedName("latitude") @Expose private Double latitude;
    @SerializedName("heading") @Expose private Double heading;
    @SerializedName("uid") @Expose private String uid;

    public TrackingInfo() {
    }

    public TrackingInfo(String vehicle, long datetime, String driver, float speed, Double longitude, Double latitude, Double heading, String uid) {
        this.vehicle = vehicle;
        this.datetime = datetime;
        this.driver = driver;
        this.speed = speed;
        this.longitude = longitude;
        this.latitude = latitude;
        this.heading = heading;
        this.uid = uid;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public long getDatetime() {
        return datetime;
    }

    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getHeading() {
        return heading;
    }

    public void setHeading(Double heading) {
        this.heading = heading;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
