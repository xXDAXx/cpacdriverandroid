package driver.cpac.co.th.Model.RealmObject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class NewWaybillId extends RealmObject implements Serializable {
    @PrimaryKey @SerializedName("waybillId") @Expose private String waybillId;

    public NewWaybillId() {
    }

    public NewWaybillId(String waybillId) {
        this.waybillId = waybillId;
    }

    public String getWaybillId() {
        return waybillId;
    }

    public void setWaybillId(String waybillId) {
        this.waybillId = waybillId;
    }
}
