package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;

public class ChangePass {
    @Expose
    private String currentPassword;
    @Expose
    private String newPassword;
    @Expose
    private String confirmPassword;
    @Expose
    private String loginId;

    public ChangePass(String loginId, String currentPassword, String newPassword, String confirmPassword) {
        this.loginId = loginId;
        this.currentPassword = currentPassword;
        this.newPassword = newPassword;
        this.confirmPassword = confirmPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }
}
