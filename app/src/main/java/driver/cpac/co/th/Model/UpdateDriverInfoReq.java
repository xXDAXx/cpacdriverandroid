package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class UpdateDriverInfoReq {
    @Expose
    private int id;
    @Expose
    private String phoneNumber;
    @Expose
    private String driverLicenseNumber;
    @Expose
    private List<Integer> truckTypes;
    @Expose
    private int drivingLicenseType;

    public UpdateDriverInfoReq(int id , String phoneNumber,String driverLicenseNumber, List<Integer> truckTypes, int drivingLicenseType) {
        this.setId(id);
        this.setPhoneNumber(phoneNumber);
        this.setDriverLicenseNumber(driverLicenseNumber);
        this.setTruckTypes(truckTypes);
        this.drivingLicenseType = drivingLicenseType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDriverLicenseNumber() {
        return driverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        this.driverLicenseNumber = driverLicenseNumber;
    }

    public List<Integer> getTruckTypes() {
        return truckTypes;
    }

    public void setTruckTypes(List<Integer> truckTypes) {
        this.truckTypes = truckTypes;
    }
}
