package driver.cpac.co.th.util;

import com.tmg.th.BuildConfig;

public @interface Constant {
    String GPS_ENABLED = "GPS_ENABLED";
    String HEADER_TOKEN = "Authorization";
    String CONTENT_TYPE_JSON = "Content-Type: application/json";
    String CONTENT_TYPE_MULTI_PART = "Content-Type: multipart/form-data";
    String ACCEPT_LANG_HEADER_PREFIX = "Accept-Language";

    //String API_BASE_URL = "http://192.168.1.71:6688/api/";
    //String API_PATH = "api/";
    String MEDIA_PATH = "web/v1/media/file/";
    //Local
    //String API_BASE_URL = "http://192.168.0.3:6688/api/";
    //DEV
    //String API_BASE_URL = "http://izicargo.vn:8000/api/";
    //PROD - HTTPS:
    //String API_BASE_URL = "https://cargolink.co.th/api/";
    String API_BASE_URL = BuildConfig.BASE_URL;
    //DEV - HTTPS
    //String API_BASE_URL = "https://izicargo.vn:8443/api/";
    /*Prod base url*/
    //String API_BASE_URL = " ";
    //String API_BASE_URL = "http://192.168.1.114:6688/api/";
    //DEV DU
    //String API_BASE_URL = "http://192.168.7.125:6688/api/";
    //DEV_VIET 2
    //String API_BASE_URL = "http://192.168.7.121:6688/api/";
    //DEV_VIET
    //String API_BASE_URL = "http://192.168.1.71:6688/api/";
    //DEV_VIET_2
    //String API_BASE_URL = "http://192.168.1.54:6688/api/";

    //Stagging
    //String GPS_API_URL = "http://13.229.208.208:9999";

    //Prod
    String GPS_API_URL = "http://gps.cargolink.co.th";


    String GGM_API_URL = "https://maps.googleapis.com/maps/api/";

    //TEST
    //String GGM_DIRECTION_API_KEY = "AIzaSyCo0k-daFGyHWR4PkLt8C5Rg5EKbe2kIOo";

    //PROD
    String GGM_DIRECTION_API_KEY = "AIzaSyBaJVKpM93FWYl8prKWHTR9UcTa9mcEDzQ";

    boolean IS_DEBUG = true;
    String TOKEN_PREFIX = "Bearer ";
    String USER_ID = "user_id";
    String USER_NAME = "user_name";
    String PREFS_NAME = "multi_language_active";
    String LANGUAGE = "language";
    String TOKEN = "token";
    String TITLE = "title";
    String TYPE = "type";
    String STATUS = "status";
    String INTERNAL = "internal";
    String MSGLIST = "msgList";
    String VALIDMSGLST = "validMsgList";
    String FULLNAME = "FULLNAME";
    String LICENSE_NUMBER = "LICENSE_NUMBER";
    String AVATAR = "AVATAR";
    String PHONE_NUMBER = "PHONE_NUMBER";
    String CARRIER_ID = "CARRIER_ID";
    String DEVICE_TOKEN = "DEVICE_TOKEN";
    String VEHICLE_NUMBER = "VEHICLE_NUMBER";
    String EMAIL = "EMAIL";
    String TRUCK_TYPE = "TRUCK_TYPE";
    String DRIVER_LICENCE = "DRIVER_LICENCE";
    String MST_TYPE = "MST_TYPE";


}
