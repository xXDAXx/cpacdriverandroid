package driver.cpac.co.th.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.tmg.th.R;

public class DialogConfirm extends Dialog {
    public Activity activity;
    public Dialog dialog;
    public Button ok, cancel;
    public String MODE, btnCancelText, btnOkText;
    TextView titleTv, contentTv;
    String title, content;
    CallBackDialogCom callBackDialogCom;
    CallBackCameraConfirm callBackCameraConfirm;

    public DialogConfirm(String title, String content, Activity a, CallBackDialogCom callBackDialogCom) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
        this.content = (content);
        this.title = (title);
        this.callBackDialogCom = callBackDialogCom;
    }

    public DialogConfirm(String title, String content, Activity a, CallBackDialogCom callBackDialogCom, CallBackCameraConfirm callBackCameraConfirm) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
        this.content = (content);
        this.title = (title);
        this.callBackDialogCom = callBackDialogCom;
        this.callBackCameraConfirm = callBackCameraConfirm;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_confirm);
        titleTv = (TextView) findViewById(R.id.dlg_confirm_tittle);
        contentTv = (TextView) findViewById(R.id.dlg_confirm_content);
        ok = (Button) findViewById(R.id.ok_btn);
        cancel = (Button) findViewById(R.id.cancel_btn);
        if (MODE != null && MODE.equals("CAMERA")) {
            ok.setText(btnOkText);
            cancel.setText(btnCancelText);
            ok.setText("CAMERA");
            ok.setBackgroundColor(activity.getResources().getColor(R.color.cargolink_green));
            cancel.setText("LATER");
            cancel.setBackgroundColor(activity.getResources().getColor(R.color.cargolink_red));
        }
        titleTv.setText(title);
        contentTv.setText(content);
        ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MODE!=null&&MODE.equals("CAMERA")) {
                    callBackCameraConfirm.BtnCameraClick();
                    dismiss();
                } else {
                    callBackDialogCom.BtnOkClick();
                    dismiss();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface CallBackDialogCom {
        void BtnOkClick();
    }

    public interface CallBackCameraConfirm {
        void BtnCameraClick();

        void BtnLaterClick();
    }

}


