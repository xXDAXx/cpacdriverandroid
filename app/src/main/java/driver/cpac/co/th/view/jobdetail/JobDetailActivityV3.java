package driver.cpac.co.th.view.jobdetail;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.SimpleItemAnimator;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.google.Directions;
import com.tmg.th.R;
import com.tmg.th.databinding.ActivityJobDetailV3Binding;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.CargoType;
import driver.cpac.co.th.util.enumClass.TruckType;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.Home.HomeActivity;
import driver.cpac.co.th.view.MapView.MapViewActivity;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogConfirm;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;
import driver.cpac.co.th.view.dialog.DialogRejectReason;

import java.util.List;


public class JobDetailActivityV3 extends OldBaseActivity implements DialogConfirm.CallBackDialogCom,
        DialogNoticeCallback.CallBackDialogNotice, DialogRejectReason.CallBackDialogReject {
    PreferenceHelper preferenceHelper;
    private ActivityJobDetailV3Binding binding;
    private JobDetailViewModel jobDetailViewModel;
    DialogNoticeCallback dialogNotice;
    DialogConfirm dialogConfirm;
    DialogRejectReason dialogRejectReason;
    JobDetailResV2 shipmentInfo;
    DialogNoticeCallback dialogNoticeCallback;

    public Activity activity;
    public Directions directions;
    private DestinationListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_job_detail_v3);
        setSupportActionBar(binding.appBarLayout.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        preferenceHelper = new PreferenceHelper(this);
        activity = this;
        directions = null;
        binding.acceptBtn.setOnClickListener(v -> AcceptJobClick());
        binding.rejectBtn.setOnClickListener(v -> RejectJobClick());
        shipmentInfo = (JobDetailResV2) getIntent().getSerializableExtra("content");
        jobDetailViewModel = ViewModelProviders.of(this).get(JobDetailViewModel.class);
        ObserverSetup();
        if (shipmentInfo != null) {
            /*getSupportActionBar().setTitle(getString(R.string.waybill_label_prefix) + shipmentInfo.getId().toString());
            onDisplay_Detail_Shipment(shipmentInfo);
            binding.cardViewMapButton.setOnClickListener(v -> {
                Intent intent = new Intent(this, MapViewActivity.class);
                intent.putExtra("jobDetail", shipmentInfo);
                startActivity(intent);
            });*/
            String jobId = shipmentInfo.getHashId();
            if (!jobId.equals("0")) {
                showLoading();
                jobDetailViewModel.getDetailJob(preferenceHelper.getString(Constant.TOKEN, ""), jobId);
            }
        } else {
            Util.DismissAllNotificaion(this);
            String jobId = getIntent().getStringExtra("jobId");
            if (!jobId.equals("0")) {
                showLoading();
                jobDetailViewModel.getDetailJob(preferenceHelper.getString(Constant.TOKEN, ""), jobId);
            }
        }

    }

    public void ObserverSetup() {
        jobDetailViewModel.getJobAccepted().observe(this, aBoolean -> {
            hideLoading();
            if (aBoolean == true) {
                ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
                List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
                if (taskList.get(0).numActivities == 1 && taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
                    Intent mainIntent = new Intent(JobDetailActivityV3.this, HomeActivity.class);
                    mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(mainIntent);
                } else
                    this.finish();
            }
        });
        jobDetailViewModel.getShowLoading().observe(this, aBoolean -> {
            if (aBoolean == true) {
                showLoading();
            } else if (aBoolean == false) {
                hideLoading();
            }
        });
        jobDetailViewModel.getJobRejected().observe(this, aBoolean -> {
            hideLoading();
            ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
            if (taskList.get(0).numActivities == 1 && taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
                Intent mainIntent = new Intent(JobDetailActivityV3.this, HomeActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(mainIntent);
            } else
                this.finish();
        });
        jobDetailViewModel.getjobDetailLiveData().observe(this, jobDetailResV2 -> {
            shipmentInfo = jobDetailResV2;
            getSupportActionBar().setTitle(getString(R.string.waybill_label_prefix) + shipmentInfo.getId().toString());
            onDisplay_Detail_Shipment(shipmentInfo);
            binding.cardViewMapButton.setOnClickListener(v -> {
                Intent intent = new Intent(this, MapViewActivity.class);
                intent.putExtra("jobDetail", shipmentInfo);
                startActivity(intent);
            });
        });
    }

    public void showRejectedNotice(WaybillStatus status) {
        String content = getString(R.string.jobDetail_dialogContent_waybillRejected);
        dialogNoticeCallback = new DialogNoticeCallback(content,this,this);
        dialogNoticeCallback.show();
    }

    @Override
    public void BtnOkDiaComClick() {
        Intent mainIntent = new Intent(this, HomeActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
    }

    public void onDisplay_Detail_Shipment(JobDetailResV2 shipmentInfo) {
        //Check if user have right to access

        WaybillStatus status = WaybillStatus.getEnumType(shipmentInfo.getStatus());
        int waybillType = shipmentInfo.getWaybillType();
        int userId = preferenceHelper.getInt(Constant.USER_ID,0);
        if( status == WaybillStatus.cancelled || status == WaybillStatus.driverRejected || status == WaybillStatus.shipperRejected || !Util.jobBelongToUser(shipmentInfo,userId)) {
            showRejectedNotice(status);
        }

        //RCV Destination:

        ((SimpleItemAnimator) binding.rcvDestinationList.getItemAnimator()).setSupportsChangeAnimations(true);
        adapter = new DestinationListAdapter(this, shipmentInfo, false, waybillType == 0? false: true);
        binding.rcvDestinationList.setLayoutManager(new LinearLayoutManager(this));
        binding.rcvDestinationList.setAdapter(adapter);
        if (shipmentInfo.getEstimateDistance() == null || shipmentInfo.getEstimateDistance().isEmpty()) {
            binding.distanceInfo.setVisibility(View.GONE);
            binding.distanceIcon.setVisibility(View.GONE);
        } else
            binding.distanceInfo.setText(getString(R.string.common_est) + " " + shipmentInfo.getEstimateDistance());
        //SHIPPER INFO
        if (shipmentInfo.getShipperAvatarUrl() != null && !shipmentInfo.getShipperAvatarUrl().isEmpty())
            App.self().getPicasso().load(shipmentInfo.getShipperAvatarUrl()).placeholder(R.drawable.account_white).into(binding.imgAvatarShipper);
        binding.shipperName.setText(shipmentInfo.getShipperName());

        //Truck Info
        binding.truckInfo.setText(TruckType.GetTruckType(shipmentInfo.getTruckType(), this).toUpperCase() + " - " + shipmentInfo.getTruckNumber().toUpperCase());
        App.self().getPicasso().load(TruckType.GetTruckTypeImg(shipmentInfo.getTruckType(),this)).placeholder(R.drawable.others).into(binding.truckIcon);
        // GOODS INFO
        String goodInfo = shipmentInfo.getTotalWeight().toString() + " " + getString(R.string.common_tons)  // 20 tons
                + " " + CargoType.GetCargoType(shipmentInfo.getTypeOfCargo(), this);  // FOOODS
        if (shipmentInfo.getQuantity() > 0) {
            goodInfo += " - " + shipmentInfo.getQuantity().toString() + " " + Util.GetUnitType(shipmentInfo.getUnit(), this).toLowerCase()
                    + " ";
            if (!(shipmentInfo.getSizeOfCargo().equals("0 x 0 x 0m") || shipmentInfo.getSizeOfCargo().equals("- x - x -m"))
                    ||(shipmentInfo.getSizeOfCargo().equals("0.0 x 0.0 x 0.0m")) ||(shipmentInfo.getSizeOfCargo().equals("0.00 x 0.00 x 0.00m")))
                goodInfo += shipmentInfo.getSizeOfCargo();
        }
        binding.goodsInfo.setText(goodInfo);

        //binding.txtNoteToDriver.setText(jobDetailRes.getHandlingInstruction());

        //Driver Info
        if (shipmentInfo.getSecondaryDriver() != null) {   // IF TRIP HAVE 2 DRIVER
            if (userId == shipmentInfo.getSecondaryDriver().getId()) { // DRIVER IS PRIMARY DRIVER --> SHOW SECOND DRIVER INFO
                if (shipmentInfo.getPrimaryDriver().getAvaUrl() != null)
                    App.self().getPicasso().load(shipmentInfo.getPrimaryDriver().getAvaUrl()).placeholder(R.drawable.account).into(binding.imgAvatar);
                binding.driverName.setText(shipmentInfo.getPrimaryDriver().getDriverName());
                binding.driverTel.setText(getString(R.string.common_tel) + shipmentInfo.getPrimaryDriver().getDriverContact());
                binding.driverRole.setText(R.string.common_label_primaryDriver);
                binding.btnDrvCall.setOnClickListener(v -> {
                    String phone = shipmentInfo.getPrimaryDriver().getDriverContact();
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                    startActivity(intent);
                });
                binding.btnDrvSms.setOnClickListener(v -> {
                    String number = shipmentInfo.getPrimaryDriver().getDriverContact();  // The number on which you want to send SMS
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                });
            } else {  // DRIVER IS PRIMARY DRIVER --> SHOW SECOND DRIVER INFO
                if (shipmentInfo.getSecondaryDriver().getAvaUrl() != null)
                    App.self().getPicasso().load(shipmentInfo.getSecondaryDriver().getAvaUrl()).placeholder(R.drawable.account).into(binding.imgAvatar);
                binding.driverName.setText(shipmentInfo.getSecondaryDriver().getDriverName());
                binding.driverRole.setText(R.string.common_label_secondDriver);
                binding.driverTel.setText(getString(R.string.common_tel) + shipmentInfo.getSecondaryDriver().getDriverContact());
                binding.btnDrvCall.setOnClickListener(v -> {
                    String phone = shipmentInfo.getSecondaryDriver().getDriverContact();
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                    startActivity(intent);
                });
                binding.btnDrvSms.setOnClickListener(v -> {
                    String number = shipmentInfo.getSecondaryDriver().getDriverContact();  // The number on which you want to send SMS
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                });
            }
        } else {
            binding.cardViewDriverInfo.setVisibility(View.GONE);
        }
        if(((shipmentInfo.isPrimaryDriverAccepted() && shipmentInfo.getPrimaryDriver().getId() == preferenceHelper.getInt(Constant.USER_ID, 0))
                || (shipmentInfo.isSecondaryDriverAccepted() && shipmentInfo.getSecondaryDriver().getId() == preferenceHelper.getInt(Constant.USER_ID, 0)))
                &&shipmentInfo.getSecondaryDriver()!=null) {
            binding.linearlayoutButton.setVisibility(View.INVISIBLE);
            binding.linearlayoutWaiting.setVisibility(View.VISIBLE);
        }

        if (shipmentInfo.getStatus() != WaybillStatus.waitingDriver.getValue()) {
            binding.linearlayoutButton.setVisibility(View.GONE);
            if (shipmentInfo.getStatus() == WaybillStatus.driverRejected.getValue())
                binding.waitBtn.setText(getText(R.string.jobDetail_buttonLabel_driverRejected));
            else if (shipmentInfo.getStatus() == WaybillStatus.shipperRejected.getValue())
                binding.waitBtn.setText(getText(R.string.jobDetail_buttonLabel_shiperRejected));
            else if (shipmentInfo.getStatus() == WaybillStatus.cancelled.getValue())
                binding.waitBtn.setText(getText(R.string.jobDetail_buttonLabel_CarrierCanceled));
        }
    }

    public boolean onSupportNavigateUp() {
        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
        if (taskList.get(0).numActivities == 1 && taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            Intent mainIntent = new Intent(JobDetailActivityV3.this, HomeActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(mainIntent);
        } else
            onBackPressed();
        return true;
    }

    public void AcceptJobClick() {
        dialogConfirm = new DialogConfirm(getString(R.string.app_name), getString(R.string.jobDetail_dialog_jobAcceptConfirm), this, this);
        dialogConfirm.MODE = "ACCEPT";
        dialogConfirm.show();
    }

    @Override
    public void BtnOkClick() {
        jobDetailViewModel.postAcceptJob(preferenceHelper.getString(Constant.TOKEN, ""), shipmentInfo.getHashId());
    }

    public void RejectJobClick() {
        dialogRejectReason = new DialogRejectReason(this, this);
        dialogRejectReason.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            if (dialogConfirm != null) {
                dialogConfirm.dismiss();
                dialogConfirm = null;
            }
            if (dialogNotice != null) {
                dialogNotice.dismiss();
                dialogNotice = null;
            }
        }
    }

    @Override
    public void BtnRejectSubmitClick(String reason) {
        jobDetailViewModel.postRejectJob(preferenceHelper.getString(Constant.TOKEN, ""), shipmentInfo.getHashId(), reason);
    }
}

