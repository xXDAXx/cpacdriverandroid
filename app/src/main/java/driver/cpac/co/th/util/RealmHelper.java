package driver.cpac.co.th.util;

import driver.cpac.co.th.Model.RealmObject.NewWaybillId;
import driver.cpac.co.th.Model.RealmObject.TrackingInfo;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class RealmHelper {
    private Realm realm;

    public static void saveTrackingInfoToRealm(TrackingInfo trackingInfo) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        TrackingInfo trackingInfoToSave = realm.copyToRealm(trackingInfo);
        realm.commitTransaction();
    }

    public static void deleteOfflineTrackingInfo() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(TrackingInfo.class);
        realm.commitTransaction();
    }

    public static RealmResults<TrackingInfo> getOfflineTrackingInfo() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<TrackingInfo> results1 =
                realm.where(TrackingInfo.class).findAll();
        return  results1;
    }

    public static List<TrackingInfo> copyToListTrackingInfo(RealmResults<TrackingInfo> realmResults) {
        Realm realm = Realm.getDefaultInstance();
        return realm.copyFromRealm(realmResults);
    }

    public static void saveNewJobToRealm(NewWaybillId newWaybillId) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        NewWaybillId newWaybillIdInfoToSave = realm.copyToRealm(newWaybillId);
        realm.commitTransaction();
    }

    public static void deleteOfflineNewJob() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(NewWaybillId.class);
        realm.commitTransaction();
    }

    public static RealmResults<NewWaybillId> getOfflineNewJob() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<NewWaybillId> results1 =
                realm.where(NewWaybillId.class).findAll();
        return  results1;
    }

    public static List<NewWaybillId> copyToListNewJob(RealmResults<NewWaybillId> realmResults) {
        Realm realm = Realm.getDefaultInstance();
        return realm.copyFromRealm(realmResults);
    }

    public Realm getRealm() {
        if (realm == null || realm.isClosed()) {
            realm = Realm.getDefaultInstance();
        }

        return realm;
    }
}
