package driver.cpac.co.th.view.account.profile;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.tmg.th.R;
import com.tmg.th.databinding.ItemTruckTypeBinding;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.TruckType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TruckTypeAdapter extends RecyclerView.Adapter<TruckTypeAdapter.MyViewHolder> {
    protected List<Integer> driverTruckTypeList;
    protected List<TruckType> originalList;
    private Context context;
    private ItemTruckTypeBinding binding;
    private Boolean enableCheckBox;
    private List<Integer> checkedList;

    public TruckTypeAdapter(Context context, Boolean enableCheckBox, List<Integer> driverTruckTypeList) {
        checkedList = new ArrayList<>();
        checkedList = driverTruckTypeList;
        this.context = context;
        this.originalList = Arrays.asList(TruckType.values());
        this.enableCheckBox = enableCheckBox;
        this.driverTruckTypeList = driverTruckTypeList;
    }

    @NonNull
    @Override
    public TruckTypeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemTruckTypeBinding itemBinding =
                ItemTruckTypeBinding.inflate(layoutInflater, viewGroup, false);
        return new TruckTypeAdapter.MyViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull TruckTypeAdapter.MyViewHolder viewHolder, int position) {
        TruckType currentTruckType = originalList.get(position);
        TruckTypeAdapter.MyViewHolder shipmentHolder = (TruckTypeAdapter.MyViewHolder) viewHolder;
        viewHolder.bind(currentTruckType, position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return originalList == null ? 0 : originalList.size();
    }

    public void updateAdapter(ArrayList<TruckType> arrayList) {
        this.originalList.clear();
        this.originalList = arrayList;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemTruckTypeBinding binding;

        public MyViewHolder(ItemTruckTypeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(TruckType item, int position) {
            binding.executePendingBindings();
            Log.i("Pos", String.valueOf(position));
            Picasso.get().load(TruckType.GetTruckTypeImg(item.getIntValue(), context))
                    .placeholder(R.drawable.truck_icon).into(binding.truckIcon);
            binding.truckName.setText(TruckType.GetTruckType(item.getIntValue(), context));
             if(driverTruckTypeList.contains(item.getIntValue()))
                binding.checkBox.setChecked(true);
             else
                 binding.checkBox.setChecked(false);
            if (enableCheckBox != null && enableCheckBox == true) {
                binding.checkBox.setEnabled(true);
                binding.checkBox.setOnCheckedChangeListener(null);
                binding.checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    if (isChecked) checkedList.add(item.getIntValue());
                    else checkedList.remove(Integer.valueOf(item.getIntValue()));
                });
                binding.layoutRoot.setOnClickListener(v -> {
                    if (binding.checkBox.isChecked()) {
                        binding.checkBox.setChecked(false);
                    } else {
                        binding.checkBox.setChecked(true);
                    }
                });
            } else
                binding.checkBox.setEnabled(false);
        }

    }

    public void enableCheckBox(boolean bool) {
        enableCheckBox = bool;
        notifyDataSetChanged();
    }

    public List<Integer> getCheckedList() {
        return checkedList;
    }
}