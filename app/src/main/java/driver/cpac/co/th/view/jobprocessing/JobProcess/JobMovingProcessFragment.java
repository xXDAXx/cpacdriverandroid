package driver.cpac.co.th.view.jobprocessing.JobProcess;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.tmg.th.R;
import com.tmg.th.databinding.FragmentJobProcessingV2Binding;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.Documents;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.GenericFileProvider;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.CargoType;
import driver.cpac.co.th.util.enumClass.TruckType;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.MapView.MapViewActivity;
import driver.cpac.co.th.view.dialog.DialogNotice;
import driver.cpac.co.th.view.documentViewer.WebViewerActivity;
import driver.cpac.co.th.view.issueReport.ReportingMainActivity;
import driver.cpac.co.th.view.jobdetail.DestinationListAdapter;
import driver.cpac.co.th.view.jobprocessing.Documents.ListDocumentsAdapterV2;
import driver.cpac.co.th.view.jobprocessing.JobProcessingActivityV3;


public class JobMovingProcessFragment extends Fragment {
    static final int REQUEST_IMAGE_CAPTURE = 1888;
    private FragmentJobProcessingV2Binding binding;
    private JobProcessViewModel jobProcessViewModel;
    public JobDetailResV2 jobDetail;
    private JobProcessMode mode;
    private static View view;
    JobProcessingActivityV3 parent;
    PreferenceHelper preferenceHelper;
    DestinationListAdapter adapter;

    public enum JobProcessMode {
        LOAD(1),
        DELIVERY(2),
        HISTORY(3);
        private final int value;

        JobProcessMode(int value) {
            this.value = value;
        }

        public int getValue() {
            return (int) value;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_job_processing_v2, container, false);
        view = binding.getRoot();
        jobProcessViewModel = ViewModelProviders.of(this).get(JobProcessViewModel.class);
        binding.setLifecycleOwner(this);
        preferenceHelper = new PreferenceHelper(getContext());
        //Get bunndle
        parent = (JobProcessingActivityV3) getActivity();
        jobDetail = (JobDetailResV2) getArguments().getSerializable("jobDetail");
        if (jobDetail.getStatus() == WaybillStatus.loaded.getValue()) mode = JobProcessMode.DELIVERY;
        if (jobDetail.getStatus() == WaybillStatus.arrived.getValue()) mode = JobProcessMode.LOAD;
        binding.addDocCardView.setVisibility(View.GONE);
        //Binding Data
        onDisplay_Detail_Shipment();
        //setupRcvDocument(jobDetail);
        binding.dropList.cardViewMapButton.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), MapViewActivity.class);
            intent.putExtra("jobDetail", jobDetail);
            startActivity(intent);
        });

        binding.dropList.cardViewIssueButton.setOnClickListener(v ->{
            Intent myIntent;
            //myIntent = new Intent(getContext(), ReportingStep1Activity.class);
            myIntent = new Intent(getContext(), ReportingMainActivity.class);
            myIntent.putExtra("jobDetail", jobDetail);
            startActivity(myIntent);
        });

        ObserverSetup();
        return view;
    }

    public void ObserverSetup() {

        jobProcessViewModel.getJobDetailRefreshData().observe(this, jobDetailRes -> {
            jobDetail = jobDetailRes;
            parent.setJobDetailRes(jobDetailRes);
            onDisplay_Detail_Shipment();
            DialogNotice dialogNotice = new DialogNotice(getString(R.string.jobProcess_dialogContent_noticeUploadedDocument), getActivity());
            dialogNotice.show();
        });

        jobProcessViewModel.getLoading().observe(this, aBoolean -> {
            if (aBoolean) parent.showLoading();
            else parent.hideLoading();
        });


    }

    public void onDisplay_Detail_Shipment() {
        if (jobDetail.getEstimateDistance() == null || jobDetail.getEstimateDistance().isEmpty()) {
            binding.dropList.distanceInfo.setVisibility(View.GONE);
            binding.dropList.distanceIcon.setVisibility(View.GONE);
        } else
            binding.dropList.distanceInfo.setText(getString(R.string.common_est) + " " + jobDetail.getEstimateDistance());
        //Binding list Destination
        ((SimpleItemAnimator) binding.dropList.rcvDestinationList.getItemAnimator()).setSupportsChangeAnimations(true);
        adapter = new DestinationListAdapter(getContext(), jobDetail, true, false);
        binding.dropList.rcvDestinationList.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.dropList.rcvDestinationList.setAdapter(adapter);
        binding.dropList.cardViewDriverInfo.setVisibility(View.GONE);

        //Truck Info
        binding.dropList.truckInfo.setText(TruckType.GetTruckType(jobDetail.getTruckType(), getContext()).toUpperCase() + " - " + jobDetail.getTruckNumber().toUpperCase());
        App.self().getPicasso().load(TruckType.GetTruckTypeImg(jobDetail.getTruckType(),getContext())).placeholder(R.drawable.others).into(binding.dropList.truckIcon);
        //Driver Info
        int userId = preferenceHelper.getInt(Constant.USER_ID, 0);
        if (jobDetail.getSecondaryDriver() != null) {   // IF TRIP HAVE 2 DRIVER
            if (userId == jobDetail.getSecondaryDriver().getId()) { // DRIVER IS PRIMARY DRIVER --> SHOW SECOND DRIVER INFO
                if (jobDetail.getPrimaryDriver().getAvaUrl() != null)
                    App.self().getPicasso().load(jobDetail.getPrimaryDriver().getAvaUrl()).placeholder(R.drawable.account).into(binding.dropList.imgAvatar);
                binding.dropList.driverName.setText(jobDetail.getPrimaryDriver().getDriverName());
                binding.dropList.driverTel.setText(getText(R.string.common_tel) + jobDetail.getPrimaryDriver().getDriverContact());
                binding.dropList.driverRole.setText(R.string.common_label_primaryDriver);
                binding.dropList.btnDrvCall.setOnClickListener(v -> {
                    String phone = jobDetail.getPrimaryDriver().getDriverContact();
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                    startActivity(intent);
                });
                binding.dropList.btnDrvSms.setOnClickListener(v -> {
                    String number = jobDetail.getPrimaryDriver().getDriverContact();  // The number on which you want to send SMS
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                });
            } else {  // DRIVER IS PRIMARY DRIVER --> SHOW SECOND DRIVER INFO
                if (jobDetail.getSecondaryDriver().getAvaUrl() != null)
                    App.self().getPicasso().load(jobDetail.getSecondaryDriver().getAvaUrl()).placeholder(R.drawable.account).into(binding.dropList.imgAvatar);
                binding.dropList.driverName.setText(jobDetail.getSecondaryDriver().getDriverName());
                binding.dropList.driverTel.setText(getText(R.string.common_tel)  + jobDetail.getSecondaryDriver().getDriverContact());
                binding.dropList.driverRole.setText(R.string.common_label_secondDriver);
                binding.dropList.btnDrvCall.setOnClickListener(v -> {
                    String phone = jobDetail.getSecondaryDriver().getDriverContact();
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                    startActivity(intent);
                });
                binding.dropList.btnDrvSms.setOnClickListener(v -> {
                    String number = jobDetail.getSecondaryDriver().getDriverContact();  // The number on which you want to send SMS
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                });

            }
        }
    }
}
