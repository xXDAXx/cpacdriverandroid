package driver.cpac.co.th.util;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import android.util.DisplayMetrics;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.Language;
import com.tmg.th.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class LangUtils extends android.content.ContextWrapper {

    public LangUtils(Context base) {
        super(base);
    }

    private static String sCurrentLanguage = null;

    public static String getCurrentLanguage() {
        if (sCurrentLanguage == null) {
            sCurrentLanguage = initCurrentLanguage();
        }
        return sCurrentLanguage;
    }

    /**
     * check language exist in SharedPrefs, if not exist then default language is English
     */
    private static String initCurrentLanguage() {
        PreferenceHelper preferenceHelper = new PreferenceHelper(App.self().getBaseContext());
        String currentLanguage = preferenceHelper.getString(Constant.LANGUAGE, null);

        if (currentLanguage != null) {
            return currentLanguage;
        }else {
            currentLanguage = App.self().getString(R.string.language_english_code);
            preferenceHelper.putString(Constant.LANGUAGE, currentLanguage);
        }
        return currentLanguage;
    }

    /**
     * return language list from string.xml
     */
    public static List<Language> getLanguageData() {
        List<Language> languageList = new ArrayList<>();
        List<String> languageNames =
                Arrays.asList(App.self().getResources().getStringArray(R.array.language_names));
        List<String> languageCodes =
                Arrays.asList(App.self().getResources().getStringArray(R.array.language_codes));
        if (languageNames.size() != languageCodes.size()) {
            // error, make sure these arrays are same size
            return languageList;
        }
        for (int i = 0, size = languageNames.size(); i < size; i++) {
            languageList.add(new Language(i, languageNames.get(i), languageCodes.get(i)));
        }
        return languageList;
    }

    /**
     * load current locale and change language
     */
    public static void loadLocale(Context context) {
        changeLanguage(context,initCurrentLanguage());
    }

    /**
     * change app language
     */
    @SuppressWarnings("deprecation")
    public static void changeLanguage(Context context,String language) {
        PreferenceHelper preferenceHelper = new PreferenceHelper(context);
       /*
        Locale locale = new Locale(language.toLowerCase());
        Locale.setDefault(locale);
        LocaleChanger.setLocale(locale);
        preferenceHelper.putString(Constant.LANGUAGE, language);*/

        preferenceHelper.putString(Constant.LANGUAGE, language);
        sCurrentLanguage = language;
        Locale locale = new Locale(sCurrentLanguage.toLowerCase());
        Locale.setDefault(locale);
        Configuration config = context.getResources().getConfiguration();
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(locale);
            LocaleList localeList = new LocaleList(locale);
            LocaleList.setDefault(localeList);
            config.setLocales(localeList);
        }

           else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(locale);
            //configuration.locale = locale;
        } else {
            config.locale = locale;

        }
        context.getResources().updateConfiguration(config,displayMetrics);
    }

    public static ContextWrapper wrap(Context context, Locale newLocale) {
        Locale sysLocale;
        Resources res = context.getResources();
        Configuration configuration = res.getConfiguration();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sysLocale = configuration.getLocales().get(0);
        } else {
            sysLocale = configuration.locale;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            configuration.setLocale(newLocale);

            LocaleList localeList = new LocaleList(newLocale);
            LocaleList.setDefault(localeList);
            configuration.setLocales(localeList);
            context = context.createConfigurationContext(configuration);


        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(newLocale);
            context = context.createConfigurationContext(configuration);

        } else {
            configuration.locale = newLocale;
            res.updateConfiguration(configuration, res.getDisplayMetrics());
        }

        return new ContextWrapper(context);
    }

    public static int getLanguageCode() {
        int languageCode = 0; //English by default)
        if(getCurrentLanguage().equals("th"))
            languageCode = 1;
        return languageCode;
    }
}

