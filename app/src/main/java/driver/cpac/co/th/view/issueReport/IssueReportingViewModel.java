package driver.cpac.co.th.view.issueReport;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.File;
import java.util.List;

import driver.cpac.co.th.Model.DriverNewIssueDocumentReq;
import driver.cpac.co.th.Model.DriverNewIssueReq;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.UploadFileResponse;
import driver.cpac.co.th.Model.google.Directions;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.LangUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssueReportingViewModel extends ViewModel {

    private CargoLinkAPI cargoLinkAPI;
    public MutableLiveData<Boolean> showLoading;
    public MutableLiveData<Boolean> issueCreated ;
    public MutableLiveData<UploadFileResponse> uploadFileResponse;
    public MutableLiveData<Boolean> showEmpty = new MutableLiveData<>();
    public MutableLiveData<List<JobDetailResV2>> jobDetailLiveData;

    public MutableLiveData<Boolean> getLoading() {
        if (showLoading == null) {
            showLoading = new MutableLiveData<>();
        }
        return showLoading;
    }

    public MutableLiveData<Boolean> getIssueCreated() {
        if (issueCreated == null) {
            issueCreated = new MutableLiveData<>();
        }
        return issueCreated;
    }

    public MutableLiveData<UploadFileResponse> getUploadFileResponse() {
        if (uploadFileResponse == null) {
            uploadFileResponse = new MutableLiveData<>();
        }
        return uploadFileResponse;
    }

    public MutableLiveData<List<JobDetailResV2>> getjobDetailLiveData() {
        if (jobDetailLiveData == null) {
            jobDetailLiveData = new MutableLiveData<>();
        }
        return jobDetailLiveData;
    }

    public MutableLiveData<Boolean> getShowEmpty() {
        if (showEmpty == null) {
            showEmpty = new MutableLiveData<>();
        }
        return showEmpty;
    }

    public void newUploadDocument(File file, String uploadFileType, String Token) {
        showLoading.setValue(true);
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/jpg"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), fileReqBody);
        cargoLinkAPI = ApiUtils.getAPIService();
        cargoLinkAPI.upload(Token, LangUtils.getCurrentLanguage(), part, null).
                enqueue(new Callback<UploadFileResponse>() {
                    @Override
                    public void onResponse(Call<UploadFileResponse> call, Response<UploadFileResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                showLoading.setValue(false);
                                uploadFileResponse.setValue(response.body());
                            }
                        } else
                            showLoading.setValue(false);
                    }

                    @Override
                    public void onFailure(Call<UploadFileResponse> call, Throwable t) {
                        showLoading.setValue(false);
                    }
                });
    }

    public void newIssueSubmit(DriverNewIssueReq req, String Token) {
        showLoading.setValue(true);
        cargoLinkAPI = ApiUtils.getAPIService();
        cargoLinkAPI.createNewIssueReport(Token, req).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                        showLoading.setValue(false);
                        issueCreated.setValue(true);
                } else {
                    showLoading.setValue(false);
                    issueCreated.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                showLoading.setValue(false);
                issueCreated.setValue(false);
            }
        });
    }

    public void getMyJobs(String Token) {
        showLoading.setValue(true);
        cargoLinkAPI = ApiUtils.getAPIService();
        cargoLinkAPI.getMyJobs(Token, LangUtils.getCurrentLanguage()).enqueue(new Callback<List<JobDetailResV2>>() {
            @Override
            public void onResponse(Call<List<JobDetailResV2>> call, Response<List<JobDetailResV2>> response) {
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        jobDetailLiveData.setValue(response.body());
                        showLoading.setValue(false);
                    } else {
                        showEmpty.setValue(true);
                        showLoading.setValue(false);
                    }
                } else {
                    showEmpty.setValue(true);
                    showLoading.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<List<JobDetailResV2>> call, Throwable t) {
                showEmpty.setValue(true);
                showLoading.setValue(false);
            }
        });

    }
}
