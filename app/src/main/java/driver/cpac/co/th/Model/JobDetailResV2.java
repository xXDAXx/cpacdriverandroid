package driver.cpac.co.th.Model;

import java.math.BigDecimal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import java.util.List;

public class JobDetailResV2 implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("hashId")
    @Expose
    private String hashId;
    @SerializedName("status")
    @Expose
    private short status;
    @SerializedName("shipperName")
    @Expose
    private String shipperName;
    @SerializedName("loadingAddress")
    @Expose
    private String loadingAddress;
    @SerializedName("loadingTime")
    @Expose
    private String loadingTime;
    @SerializedName("truckNumber")
    @Expose
    private String truckNumber;
    @SerializedName("truckType")
    @Expose
    private int truckType;
    @SerializedName("typeOfCargo")
    @Expose
    private int typeOfCargo;
    @SerializedName("totalWeight")
    @Expose
    private BigDecimal totalWeight;
    @SerializedName("loadingLat")
    @Expose
    private double loadingLat;
    @SerializedName("loadingLng")
    @Expose
    private double loadingLng;
    @SerializedName("estimateDistance")
    @Expose
    private String estimateDistance;

    @SerializedName("quantity")
    @Expose
    private Integer quantity;

    @SerializedName("listDetailWaybil")
    @Expose
    private List<DetailWaybill> listDetailWaybil;

    @SerializedName("documents")
    @Expose
    private List<Documents> documents;
    @SerializedName("primaryDriver")
    @Expose
    private DriverInJob primaryDriver;
    @SerializedName("secondaryDriver")
    @Expose
    private DriverInJob secondaryDriver;
    @SerializedName("shipperAvatarUrl")
    @Expose
    private String shipperAvatarUrl;
    @SerializedName("primaryDriverAccepted")
    @Expose
    private boolean primaryDriverAccepted;
    @SerializedName("secondaryDriverAccepted")
    @Expose
    private boolean secondaryDriverAccepted;
    @SerializedName("sizeOfCargo")
    @Expose
    private String sizeOfCargo;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("handlingInstruction")
    @Expose
    private String handlingInstruction;

    @SerializedName("nameOfCargo")
    @Expose
    private String nameOfCargo;

    @SerializedName("unit")
    @Expose
    private short unit;

    @SerializedName("fullnameOrg")
    @Expose
    private String fullnameOrg;

    @SerializedName("phoneOrg")
    @Expose
    private String phoneOrg;

    @SerializedName("doneTime")
    @Expose
    private String doneTime;

    @SerializedName("loadedTime")
    @Expose
    private String loadedTime;

    @SerializedName("history")
    @Expose
    private List<WaybillHistoryRes> history;

    @SerializedName("waybillType")
    @Expose
    private Integer waybillType;

    public Integer getWaybillType() {
        return waybillType;
    }

    public void setWaybillType(Integer waybillType) {
        this.waybillType = waybillType;
    }

    private Boolean accepted;

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public List<WaybillHistoryRes> getHistory() {
        return history;
    }

    public void setHistory(List<WaybillHistoryRes> history) {
        this.history = history;
    }

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public String getLoadedTime() {
        return loadedTime;
    }

    public void setLoadedTime(String loadedTime) {
        this.loadedTime = loadedTime;
    }

    public String getDoneTime() {
        return doneTime;
    }

    public void setDoneTime(String doneTime) {
        this.doneTime = doneTime;
    }

    public List<DetailWaybill> getListDetailWaybil() {
        return listDetailWaybil;
    }

    public void setListDetailWaybil(List<DetailWaybill> listDetailWaybil) {
        this.listDetailWaybil = listDetailWaybil;
    }

    public double getLoadingLat() {
        return loadingLat;
    }

    public void setLoadingLat(double loadingLat) {
        this.loadingLat = loadingLat;
    }

    public double getLoadingLng() {
        return loadingLng;
    }

    public void setLoadingLng(double loadingLng) {
        this.loadingLng = loadingLng;
    }

    public String getEstimateDistance() {
        return estimateDistance;
    }

    public void setEstimateDistance(String estimateDistance) {
        this.estimateDistance = estimateDistance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public String getLoadingAddress() {
        return loadingAddress;
    }

    public void setLoadingAddress(String loadingAddress) {
        this.loadingAddress = loadingAddress;
    }

    public String getLoadingTime() {
        return loadingTime;
    }

    public void setLoadingTime(String loadingTime) {
        this.loadingTime = loadingTime;
    }

    public String getTruckNumber() {
        return truckNumber;
    }

    public void setTruckNumber(String truckNumber) {
        this.truckNumber = truckNumber;
    }

    public int getTruckType() {
        return truckType;
    }

    public void setTruckType(int truckType) {
        this.truckType = truckType;
    }

    public int getTypeOfCargo() {
        return typeOfCargo;
    }

    public void setTypeOfCargo(int typeOfCargo) {
        this.typeOfCargo = typeOfCargo;
    }

    public BigDecimal getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(BigDecimal totalWeight) {
        this.totalWeight = totalWeight;
    }

    public List<Documents> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Documents> documents) {
        this.documents = documents;
    }

    public DriverInJob getPrimaryDriver() {
        return primaryDriver;
    }

    public void setPrimaryDriver(DriverInJob primaryDriver) {
        this.primaryDriver = primaryDriver;
    }

    public DriverInJob getSecondaryDriver() {
        return secondaryDriver;
    }

    public void setSecondaryDriver(DriverInJob secondaryDriver) {
        this.secondaryDriver = secondaryDriver;
    }

    public String getShipperAvatarUrl() {
        return shipperAvatarUrl;
    }

    public void setShipperAvatarUrl(String shipperAvatarUrl) {
        this.shipperAvatarUrl = shipperAvatarUrl;
    }

    public boolean isPrimaryDriverAccepted() {
        return primaryDriverAccepted;
    }

    public void setPrimaryDriverAccepted(boolean primaryDriverAccepted) {
        this.primaryDriverAccepted = primaryDriverAccepted;
    }

    public boolean isSecondaryDriverAccepted() {
        return secondaryDriverAccepted;
    }

    public void setSecondaryDriverAccepted(boolean secondaryDriverAccepted) {
        this.secondaryDriverAccepted = secondaryDriverAccepted;
    }

    public String getSizeOfCargo() {
        return sizeOfCargo;
    }

    public void setSizeOfCargo(String sizeOfCargo) {
        this.sizeOfCargo = sizeOfCargo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getHandlingInstruction() {
        return handlingInstruction;
    }

    public void setHandlingInstruction(String handlingInstruction) {
        this.handlingInstruction = handlingInstruction;
    }

    public String getNameOfCargo() {
        return nameOfCargo;
    }

    public void setNameOfCargo(String nameOfCargo) {
        this.nameOfCargo = nameOfCargo;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public short getUnit() {
        return unit;
    }

    public void setUnit(short unit) {
        this.unit = unit;
    }

    public String getFullnameOrg() {
        return fullnameOrg;
    }

    public void setFullnameOrg(String fullnameOrg) {
        this.fullnameOrg = fullnameOrg;
    }

    public String getPhoneOrg() {
        return phoneOrg;
    }

    public void setPhoneOrg(String phoneOrg) {
        this.phoneOrg = phoneOrg;
    }
}