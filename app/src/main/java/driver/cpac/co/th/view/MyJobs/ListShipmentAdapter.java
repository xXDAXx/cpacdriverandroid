package driver.cpac.co.th.view.MyJobs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.core.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tmg.th.databinding.ItemShipmentLayoutBinding;
import driver.cpac.co.th.Model.ShipmentInfo;
import com.tmg.th.R;

import java.util.ArrayList;
import java.util.List;

public class ListShipmentAdapter extends RecyclerView.Adapter<ListShipmentAdapter.MyViewHolder> implements Filterable {

    protected List<ShipmentInfo> shipmentslist;
    protected List<ShipmentInfo> originallist;
    private Context context;
    private ShipmentClickListener mListener;

    public ListShipmentAdapter(Context context, List<ShipmentInfo> shipmentInfoList, ShipmentClickListener listener) {
        this.context = context;
        this.shipmentslist = shipmentInfoList;
        this.originallist = shipmentInfoList;
        mListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemShipmentLayoutBinding itemBinding =
                ItemShipmentLayoutBinding.inflate(layoutInflater, viewGroup, false);
        return new MyViewHolder(itemBinding, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        ShipmentInfo currentShipment = shipmentslist.get(position);
        MyViewHolder shipmentHolder = (MyViewHolder) viewHolder;
        shipmentHolder.itemView.setOnClickListener(v -> {
            mListener.passContent(currentShipment);
        });
        viewHolder.bind(currentShipment);
    }

    @Override
    public int getItemCount() {
        return shipmentslist == null ? 0 : shipmentslist.size();
    }

    public void updateAdapter(ArrayList<ShipmentInfo> arrayList) {
        this.shipmentslist.clear();
        this.shipmentslist = arrayList;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemShipmentLayoutBinding binding;

        public MyViewHolder(ItemShipmentLayoutBinding binding, ShipmentClickListener listener) {
            super(binding.getRoot());
            mListener = listener;
            this.binding = binding;
        }

        public void bind(ShipmentInfo item) {
            binding.shipmentId.setText(item.getBooking_number());
            binding.shipmentSts.setText(item.getStatus());
            binding.pickupAddr.setText(item.getPickup());
            binding.dropAddr.setText(item.getDropoff());
            binding.pickupTime.setText(item.getPickup_time());
            binding.dropTime.setText(item.getDropoff_time());
            binding.executePendingBindings();
            binding.dashline.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            //color status
            switch (item.getStatus()){
                case "New":
                    binding.shipmentSts.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_new));
                    //binding.viewHeader.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_new));
                    break;
                case "Waybill pending":
                    binding.shipmentSts.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_waybill_pending));
                    //binding.viewHeader.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_waybill_pending));
                    break;
                case "Started":
                    binding.shipmentSts.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_started));
                    //binding.viewHeader.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_started));
                    break;
                case "Waybill issued":
                    binding.shipmentSts.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_waybill_issued));
                    //binding.viewHeader.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_waybill_issued));
                    break;
                case "Loaded":
                    binding.shipmentSts.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_loaded));
                    //binding.viewHeader.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_loaded));
                    break;
                case "Done":
                    binding.shipmentSts.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_done));
                    //binding.viewHeader.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_done));
                    break;
                case "Failed":
                    binding.shipmentSts.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_failed));
                    //binding.viewHeader.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_failed));
                    break;
                case "Rejected":
                    binding.shipmentSts.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_rejected));
                    break;
                case "Cancelled":
                    binding.shipmentSts.setBackgroundColor(ContextCompat.getColor(context,R.color.job_status_failed));
                    break;
            }
            binding.shipmentId.setSelected(true);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                shipmentslist = (List<ShipmentInfo>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<ShipmentInfo> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = shipmentslist;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    public Filter getStatusFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                shipmentslist = (List<ShipmentInfo>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<ShipmentInfo> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = shipmentslist;
                } else {
                    filteredResults = getStatusFilteredResults(constraint.toString().toLowerCase());
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    protected List<ShipmentInfo> getFilteredResults(String constraint) {
        List<ShipmentInfo> results = new ArrayList<>();

        for (ShipmentInfo item : originallist) {
            if (item.getBooking_number().toLowerCase().contains(constraint) ||
                    item.getDropoff().toLowerCase().contains(constraint) ||
                    item.getDropoff_time().toLowerCase().contains(constraint) ||
                    item.getPickup().toLowerCase().contains(constraint) ||
                    item.getPickup_time().toLowerCase().contains(constraint) ||
                    item.getStatus().toLowerCase().contains(constraint)) {
                results.add(item);
            }
        }
        return results;
    }

    protected List<ShipmentInfo> getStatusFilteredResults(String constraint) {
        List<ShipmentInfo> results = new ArrayList<>();

        for (ShipmentInfo item : originallist) {
            if (item.getStatus().toLowerCase().contains(constraint)) {
                results.add(item);
            }
        }
        return results;
    }

}

