package driver.cpac.co.th.view.dashboard;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.DetailWaybill;
import driver.cpac.co.th.Model.JobDetailResV2;

import com.tmg.th.R;
import com.tmg.th.databinding.ItemReminderLayoutBinding;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.jobprocessing.JobProcessingActivityV3;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class JobReminderAdapter extends RecyclerView.Adapter<JobReminderAdapter.MyViewHolder> {
    protected List<JobDetailResV2> jobList;
    protected List<JobDetailResV2> originalList;
    protected List<NextActions> nextActionsList;
    private Context context;
    private ItemReminderLayoutBinding binding;

    public JobReminderAdapter(Context context, List<JobDetailResV2> jobDetailResV2) {
        this.context = context;
        this.jobList = jobDetailResV2;
        this.originalList = jobDetailResV2;
        //Filter job need to remind
        this.nextActionsList = getListNextAction();
    }

    @NonNull
    @Override
    public JobReminderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemReminderLayoutBinding itemBinding =
                ItemReminderLayoutBinding.inflate(layoutInflater, viewGroup, false);
        return new JobReminderAdapter.MyViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull JobReminderAdapter.MyViewHolder viewHolder, int position) {
        NextActions currentAction = nextActionsList.get(position);
        JobReminderAdapter.MyViewHolder shipmentHolder = (JobReminderAdapter.MyViewHolder) viewHolder;
        viewHolder.bind(currentAction, position);
    }

    @Override
    public int getItemCount() {
        return nextActionsList == null ? 0 : nextActionsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemReminderLayoutBinding binding;

        public MyViewHolder(ItemReminderLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(NextActions item, int position) {
            int height = binding.rootLayout.getLayoutParams().height;
            if(getItemCount()==1) {
                binding.rootLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            } else {

            }
            binding.executePendingBindings();
            binding.andressTxt.setText(item.getNextDestination());
            binding.timeTxt.setText(Util.GetFriendlyTimeText(item.nextTime));
            String urlImg = jobList.get(item.jobPosition).getPhoto();
            if (urlImg != null && !urlImg.isEmpty())
                App.self().getPicasso().load(urlImg).placeholder(R.drawable.carrgo).into(binding.photoCargo);
            else
                App.self().getPicasso().load(R.drawable.carrgo).into(binding.photoCargo);
            binding.rootLayout.setOnClickListener(v -> {
                Intent intent = new Intent(context, JobProcessingActivityV3.class);
                intent.putExtra("jobDetail", jobList.get(item.getJobPosition()));
                context.startActivity(intent);
            });
        }
    }

    public List<NextActions> getListNextAction() {
        List<NextActions> listNextActions = new ArrayList<NextActions>();
        for (JobDetailResV2 jobDetailResV2 : jobList) {
            NextActions nextActions;
            WaybillStatus status = WaybillStatus.getEnumType(jobDetailResV2.getStatus());
            switch (status) {
                case issued:
                    nextActions = new NextActions(jobDetailResV2.getLoadingAddress(), Util.convertStringToTimestamp(jobDetailResV2.getLoadingTime()), jobDetailResV2.getStatus(), jobDetailResV2.getId(), jobList.indexOf(jobDetailResV2));
                    listNextActions.add(nextActions);
                    break;
                case arrived:
                    nextActions = new NextActions(jobDetailResV2.getLoadingAddress(), Util.convertStringToTimestamp(jobDetailResV2.getLoadingTime()), jobDetailResV2.getStatus(), jobDetailResV2.getId(), jobList.indexOf(jobDetailResV2));
                    listNextActions.add(nextActions);
                    break;
                case loaded:
                    nextActions = getNextActionFromLoadedJob(jobDetailResV2, jobList.indexOf(jobDetailResV2));
                    listNextActions.add(nextActions);
                    break;
            }
        }
        Collections.sort(listNextActions);
        if (listNextActions.size() < 3) {
            return listNextActions;
        } else {
            return (listNextActions.subList(0, 3));
        }
    }

    public class NextActions  implements Comparable<NextActions>{
        private String nextDestination;
        private Timestamp nextTime;
        private short status;
        private Integer id;

        public Integer getJobPosition() {
            return jobPosition;
        }

        public void setJobPosition(Integer jobPosition) {
            this.jobPosition = jobPosition;
        }

        private Integer jobPosition;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getNextDestination() {
            return nextDestination;
        }

        public void setNextDestination(String nextDestination) {
            this.nextDestination = nextDestination;
        }

        public Timestamp getNextTime() {
            return nextTime;
        }

        public void setNextTime(Timestamp nextTime) {
            this.nextTime = nextTime;
        }

        public short getStatus() {
            return status;
        }

        public void setStatus(short status) {
            this.status = status;
        }

        public NextActions(String nextDestination, Timestamp nextTime, short status, Integer id, Integer jobPosition) {
            this.nextDestination = nextDestination;
            this.nextTime = nextTime;
            this.status = status;
            this.id = id;
            this.jobPosition = jobPosition;
        }

        @Override
        public int compareTo(NextActions o) {
            return getNextTime().compareTo(o.getNextTime());
        }
    }

    public NextActions getNextActionFromLoadedJob(JobDetailResV2 job, int jobPosition) {
        NextActions nextActions;
        if (job.getStatus() == WaybillStatus.loaded.getValue()) {
            List<DetailWaybill> detailWaybillList = job.getListDetailWaybil();
            Iterator<DetailWaybill> it = detailWaybillList.iterator();
            while (it.hasNext()) {
                DetailWaybill i = it.next();
                if (i.getStatus() == WaybillStatus.finished.getValue())
                    it.remove();
            }
            DetailWaybill firstWaybill = detailWaybillList.get(0);
            nextActions = new NextActions(firstWaybill.getAddressDest(), Util.convertStringToTimestamp(firstWaybill.getDeliveryDatetime()), job.getStatus(), firstWaybill.getId(), jobPosition);
            for (DetailWaybill detailWaybill : detailWaybillList) {
                Timestamp detailWaybillTime = Util.convertStringToTimestamp(detailWaybill.getDeliveryDatetime());
                if (detailWaybillTime.before(nextActions.nextTime)) {
                    nextActions = new NextActions(detailWaybill.getAddressDest(), Util.convertStringToTimestamp(detailWaybill.getDeliveryDatetime()), job.getStatus(), detailWaybill.getId(), jobPosition);
                }
            }

            return nextActions;
        } else
            return null;
    }
}

