package driver.cpac.co.th.util.enumClass;

public enum NotificationType {
    jobNotification(1),
    systemNotification(2),
    others(3);

    private final int value;

    private NotificationType(int value) {
        this.value = value;
    }

    public short getValue() {
        return (short) value;
    }

    public static NotificationType getEnumType(int value) {
       for(NotificationType type:NotificationType.values()){
           if (type.getValue()==value) return type;
        }
       return null;
    }
}

