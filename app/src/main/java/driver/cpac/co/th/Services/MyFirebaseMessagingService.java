package driver.cpac.co.th.Services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tmg.th.R;

import driver.cpac.co.th.Model.RealmObject.NewWaybillId;
import driver.cpac.co.th.Model.RealmObject.TrackingInfo;
import driver.cpac.co.th.base.NotificationActivity;
import driver.cpac.co.th.util.RealmHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.FcmNotificationType;
import driver.cpac.co.th.view.base.SplashActivity;
import driver.cpac.co.th.view.jobdetail.JobDetailActivityV3;
import driver.cpac.co.th.view.jobprocessing.JobProcessingActivityV3;


import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String NOTIFICATION_ID_EXTRA = "notificationId";
    private static final String IMAGE_URL_EXTRA = "imageUrl";
    private static final String ADMIN_CHANNEL_ID = "admin_channel";
    private static int count = 0;
    private NotificationManager notificationManager;

    private static final String CHANNEL_ID = "Job Notification Channel";

    @Override
    public void onNewToken(String token) {

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //RemoteMessage.Notification notification = remoteMessage.getNotification();
        Map<String, String> data = remoteMessage.getData();
        String notifyType = data.get("notifyType");
        String dataStr = data.get("listData");
        String[] listData = dataStr.split("\\|\\|\\|");
        if (notifyType.equals("1") || notifyType.equals("2")) {
            String jobId = listData[0];
            if(notifyType.equals("1")) {
                NewWaybillId info = new NewWaybillId(jobId);
                try {
                    RealmHelper.saveNewJobToRealm(info);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            if (Util.isAppOnForeground(this, getApplicationContext().getPackageName())) {
                sendInAppNotification( jobId, notifyType);
            }
            else {
                sendJobNotification( jobId, notifyType);
            }
        }
    }

    private void sendJobNotification( String jobId, String notifyType) {
        // View Detail intent
        Intent intent = new Intent(this, SplashActivity.class);
        String title = "CargoLink Notification";
        String content = "";
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //Add Any key-value to pass extras to intent
        intent.putExtra("jobId", jobId);
        intent.putExtra("notifyType", notifyType);
        if (Integer.parseInt(notifyType) == FcmNotificationType.NEW_JOB.getValue()) {
            content = "Your Waybill was issued";
        } else if (Integer.parseInt(notifyType) == FcmNotificationType.NEW_JOB.getValue()) {
            content = "You have assigned a new job";
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //For Android Version Orio and greater than orio.

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "CargoLink", importance);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.YELLOW);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mNotifyManager.createNotificationChannel(mChannel);
        }

         Notification notification1 = new NotificationCompat.Builder(this)
                .addAction(R.drawable.truck_icon2, "VIEW DETAIL", pendingIntent)
                .setContentText(content)
                .setContentTitle(title)
                //.setOngoing(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setTimeoutAfter(86400000)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.mipmap.ic_launcher).build();
        //.setTicker(content)
        //.setWhen(System.currentTimeMillis());

        mNotifyManager.notify(count, notification1);
        count++;
    }

    private void sendInAppNotification( String jobId, String notifyType) {
        //Dismiss intent
        PendingIntent dismissIntent = NotificationActivity.getDismissIntent(count, this);
        //Create the PendingIntent
        // View Detail intent
        String title ="";
        String content ="";
        Intent intent =null;
        if (notifyType.equals("2")) {
            intent = new Intent(this, JobProcessingActivityV3.class);
            title = getString(R.string.firebaseNotification_issueJob_notifyTitle);//notification.getTitle();
            content = String.format(getString(R.string.firebaseNotification_issueJob_notifyBody),jobId);//notification.getBody();
        } else if (notifyType.equals("1")) {
            intent = new Intent(this, JobDetailActivityV3.class);
            title = getString(R.string.firebaseNotification_newJob_notifyTitle);//notification.getTitle();
            content = String.format(getString(R.string.firebaseNotification_newJob_notifyBody),jobId);//notification.getBody();
        }
        intent.putExtra("notificationID",CHANNEL_ID);
        intent.putExtra("jobId",jobId );
        intent.putExtra("jobId", jobId);
        intent.putExtra("notifyType", notifyType);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //For Android Version Orio and greater than orio.

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "CargoLink", importance);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.YELLOW);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mNotifyManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .addAction(R.drawable.truck_icon2, "VIEW DETAIL", pendingIntent)
                .addAction(R.drawable.close_ic, "DISMISS", dismissIntent)
                .setContentText(content)
                .setContentTitle(title)
                .setChannelId(CHANNEL_ID)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.mipmap.ic_launcher);
        //.setTicker(content)
        //.setWhen(System.currentTimeMillis());
        mNotifyManager.notify(0, builder.build());
    }

    public class ButtonReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int notificationId = intent.getIntExtra("notificationId", 0);
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(notificationId);
        }
    }
}

