package driver.cpac.co.th.Model.google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class StepsItem implements Serializable {
    @SerializedName("duration")
    @Expose
    private Duration duration;

    @SerializedName("start_location")
    @Expose
    private StartLocation startLocation;

    @SerializedName("distance")
    @Expose
    private Distance distance;

    @SerializedName("travel_mode")
    @Expose
    private String travelMode;

    @SerializedName("html_instructions")
    @Expose
    private String htmlInstructions;

    @SerializedName("end_location")
    @Expose
    private EndLocation endLocation;

    @SerializedName("polyline")
    @Expose
    private Polyline polyline;

    public StepsItem(Duration duration, StartLocation startLocation, Distance distance, String travelMode, String htmlInstructions, EndLocation endLocation, Polyline polyline) {
        this.duration = duration;
        this.startLocation = startLocation;
        this.distance = distance;
        this.travelMode = travelMode;
        this.htmlInstructions = htmlInstructions;
        this.endLocation = endLocation;
        this.polyline = polyline;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public StartLocation getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(StartLocation startLocation) {
        this.startLocation = startLocation;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public String getTravelMode() {
        return travelMode;
    }

    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }

    public String getHtmlInstructions() {
        return htmlInstructions;
    }

    public void setHtmlInstructions(String htmlInstructions) {
        this.htmlInstructions = htmlInstructions;
    }

    public EndLocation getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(EndLocation endLocation) {
        this.endLocation = endLocation;
    }

    public Polyline getPolyline() {
        return polyline;
    }

    public void setPolyline(Polyline polyline) {
        this.polyline = polyline;
    }
}