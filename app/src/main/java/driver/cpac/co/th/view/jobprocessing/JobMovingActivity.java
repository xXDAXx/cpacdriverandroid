package driver.cpac.co.th.view.jobprocessing;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.tmg.th.R;
import com.tmg.th.databinding.ActivityJobProcessingV2Binding;

import java.util.List;

import driver.cpac.co.th.Model.DetailWaybill;
import driver.cpac.co.th.Model.Documents;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.Home.HomeActivity;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogConfirm;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;
import driver.cpac.co.th.view.jobprocessing.JobHistory.JobHistoryFragment;
import driver.cpac.co.th.view.jobprocessing.JobProcess.JobMovingProcessFragment;
import driver.cpac.co.th.view.jobprocessing.JobProcess.JobProcessFragmentV2;
import driver.cpac.co.th.view.jobprocessing.JobStart.JobMovingStartFragment;
import driver.cpac.co.th.view.jobprocessing.JobStart.JobStartFragmentV2;

public class JobMovingActivity extends OldBaseActivity implements DialogConfirm.CallBackDialogCom, DialogNoticeCallback.CallBackDialogNotice {

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    PreferenceHelper preferenceHelper;

    public ActivityJobProcessingV2Binding getBinding() {
        return binding;
    }

    private ActivityJobProcessingV2Binding binding;
    public JobProcessingViewModel jobProcessingViewModel;
    DialogNoticeCallback dialogNotice;
    DialogConfirm dialogConfirm;
    public JobDetailResV2 jobDetailRes;
    FragmentManager fragmentManager;
    JobMovingProcessFragment jobMovingProcessFragment;

    public void setJobDetailRes(JobDetailResV2 jobDetailRes1) {
        this.jobDetailRes = jobDetailRes1;
    }

    private Activity activity;

    public JobDetailResV2 getJobDetailRes() {
        return jobDetailRes;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_job_processing_v2);
        super.setMyView(binding.layoutRoot);
        setSupportActionBar(binding.appBarLayout.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        jobDetailRes = (JobDetailResV2) getIntent().getSerializableExtra("jobDetail");
        activity = this;
        preferenceHelper = new PreferenceHelper(this);
        jobProcessingViewModel = ViewModelProviders.of(this).get(JobProcessingViewModel.class);
        setupObserver();
        fragmentManager = getSupportFragmentManager();
        binding.jobStepper.setVisibility(View.GONE);
                if (jobDetailRes != null) {
            //binding.jobStepper.SetupWithStatus(jobDetailRes.getStatus());
            getSupportActionBar().setTitle(getString(R.string.waybill_label_prefix) + jobDetailRes.getId().toString());
            setupFragmentManager();
            setupActionButtonandTimeBar();
        } else {
            Util.DismissAllNotificaion(this);
            String jobId = getIntent().getStringExtra("jobId");
            if (!jobId.equals("0")) {
                showLoading();
                jobProcessingViewModel.getDetailJob(preferenceHelper.getString(Constant.TOKEN, ""),jobId);
            }
        }
    }

    public void setupObserver() {
        jobProcessingViewModel.getJobDetailRefreshData().observe(this, jobDetailRes1 -> {
            this.jobDetailRes = jobDetailRes1;
            binding.jobStepper.SetupWithStatus(jobDetailRes.getStatus());
            FragmentReplace();
            setupActionButtonandTimeBar();
            if(jobDetailRes1.getStatus() == WaybillStatus.arrived.getValue()) {
                preferenceHelper.putString(Constant.VEHICLE_NUMBER, jobDetailRes1.getTruckNumber());
                //Util.setRequestingLocationUpdates(this,true);
                //checkGPS(WaybillStatus.getEnumType(jobDetailRes1.getStatus()));
            }else if(jobDetailRes1.getStatus() == WaybillStatus.finished.getValue()) {
                //Util.setRequestingLocationUpdates(this, false);
                //checkGPS(WaybillStatus.getEnumType(jobDetailRes1.getStatus()));
            }
        });
        jobProcessingViewModel.getLoading().observe(this, aBoolean -> {
            if (aBoolean) showLoading();
            else hideLoading();

        });
        jobProcessingViewModel.getjobDetailLiveData().observe(this, jobDetailRes1 -> {
            hideLoading();
            this.jobDetailRes = jobDetailRes1;
            binding.jobStepper.SetupWithStatus(jobDetailRes.getStatus());
            getSupportActionBar().setTitle(getString(R.string.waybill_label_prefix) + jobDetailRes.getId().toString());
            setupFragmentManager();
            setupActionButtonandTimeBar();
        });
    }

    public void setupActionButtonandTimeBar() {
        WaybillStatus status = WaybillStatus.getEnumType(jobDetailRes.getStatus());
        binding.jobStepper.SetupWithStatus(jobDetailRes.getStatus());

        switch (status) {
            case driverAccepted:
                binding.waitBtn.setVisibility(View.VISIBLE);
                binding.linearlayoutButton.setVisibility(View.INVISIBLE);
                binding.waitBtn.setText(getString(R.string.jobProcess_buttonLabel_waitingIssue));
                //binding.jobStepper.setVisibility(View.GONE);
                binding.txtTimeRemaining.setText(Util.TimeRemainingTitlefromString(jobDetailRes.getLoadingTime(), jobDetailRes.getStatus(), this));
                break;
            case issued:
                binding.linearlayoutButton.setVisibility(View.VISIBLE);
                binding.linearlayoutWaiting.setVisibility(View.GONE);
                binding.acctionBtn.setText(getString(R.string.jobProcess_buttonLabel_clickToStart));
                binding.jobStepper.setVisibility(View.VISIBLE);
                binding.txtTimeRemaining.setText(Util.TimeRemainingTitlefromString(jobDetailRes.getLoadingTime(), jobDetailRes.getStatus(), this));
                break;
            case started:
                binding.linearlayoutButton.setVisibility(View.VISIBLE);
                binding.linearlayoutWaiting.setVisibility(View.GONE);
                binding.acctionBtn.setText(getString(R.string.jobProcess_buttonLabel_clickToArrive));
                binding.jobStepper.setVisibility(View.VISIBLE);
                binding.txtTimeRemaining.setText(Util.TimeRemainingTitlefromString(jobDetailRes.getLoadingTime(), jobDetailRes.getStatus(), this));
                break;
            case arrived:
                binding.linearlayoutButton.setVisibility(View.VISIBLE);
                binding.linearlayoutWaiting.setVisibility(View.GONE);
                binding.acctionBtn.setText(getString(R.string.jobProcess_buttonLabel_clickToFinishLoad));
                binding.jobStepper.setVisibility(View.VISIBLE);
                binding.txtTimeRemaining.setText(Util.TimeRemainingTitlefromString(jobDetailRes.getLoadingTime(), jobDetailRes.getStatus(), this));
                break;
            case loaded:
                binding.linearlayoutButton.setVisibility(View.VISIBLE);
                binding.linearlayoutWaiting.setVisibility(View.GONE);
                binding.acctionBtn.setText(getString(R.string.jobProcess_buttonLabel_clickToFinishDelivery));
                binding.jobStepper.setVisibility(View.VISIBLE);
                if (Util.GetNextDeliveryPoint(jobDetailRes) != null)
                    binding.txtTimeRemaining.setText(Util.TimeRemainingTitlefromString(Util.GetNextDeliveryPoint(jobDetailRes).getDeliveryDatetime(), status.getValue(), this));
                break;
            case finished:
                binding.txtTimeRemaining.setText(getText(R.string.common_label_deliveredSucessfullyAt) + ": " +  jobDetailRes.getDoneTime());
                binding.linearlayoutButton.setVisibility(View.GONE);
                binding.linearlayoutWaiting.setVisibility(View.GONE);
                binding.jobStepper.setVisibility(View.GONE);
                break;
        }
        binding.acctionBtn.setOnClickListener(v -> startClick());
        int userId = preferenceHelper.getInt(Constant.USER_ID, 0);
        if (jobDetailRes.getSecondaryDriver() != null && userId == jobDetailRes.getSecondaryDriver().getId()) {
            binding.linearlayoutButton.setVisibility(View.GONE);
            binding.linearlayoutWaiting.setVisibility(View.GONE);
        }
    }

    public void setupFragmentManager() {
        Fragment oldFragment = getSupportFragmentManager().findFragmentByTag("OnlyOneFragment");
        if (oldFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(oldFragment).commit();
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, GetFragmentByStatus(), "OnlyOneFragment");
        fragmentTransaction.commit();
    }

    public void FragmentReplace() {
        Fragment oldFragment = getSupportFragmentManager().findFragmentByTag("OnlyOneFragment");
        if (oldFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(oldFragment).commit();
        }
        FragmentTransaction fragmentTransaction =  fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, GetFragmentByStatus(), "OnlyOneFragment");
        fragmentTransaction.commit();
    }

    public Fragment GetFragmentByStatus() {
        WaybillStatus status = WaybillStatus.getEnumType(jobDetailRes.getStatus());
        Bundle bundle = new Bundle();
        bundle.putSerializable("jobDetail", jobDetailRes);
        Fragment myFragment;
        if (status == WaybillStatus.driverAccepted || status == WaybillStatus.issued) {
            myFragment = new JobMovingStartFragment();
            myFragment.setArguments(bundle);
            return myFragment;
        } else if (status == WaybillStatus.loaded || status == WaybillStatus.arrived||status == WaybillStatus.started) {
            jobMovingProcessFragment = new JobMovingProcessFragment();
            jobMovingProcessFragment.setArguments(bundle);
            return jobMovingProcessFragment;
        } else if (status == WaybillStatus.finished) {
            //
            myFragment = new JobHistoryFragment();
            myFragment.setArguments(bundle);
            //myFragment = new JobHistoryFragment();
            //myFragment.setArguments(bundle);
            return myFragment;
        } else
            return null;
    }

    public boolean onSupportNavigateUp() {
        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
        if (taskList.get(0).numActivities == 1 && taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            Intent mainIntent = new Intent(JobMovingActivity.this, HomeActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(mainIntent);
        } else
            onBackPressed();
        onBackPressed();
        return true;
    }

    public void startClick() {
        if (jobDetailRes.getStatus() == WaybillStatus.issued.getValue()) {
            dialogConfirm = new DialogConfirm(
                    getString(R.string.jobProcess_dialogLabel_jobStartConfirmation), getString(R.string.jobProcess_dialogContent_jobStartConfirmation), activity, this);
            dialogConfirm.show();
        } else {
            if (jobDetailRes.getStatus() == WaybillStatus.started.getValue()) {
                dialogConfirm = new DialogConfirm(
                        getString(R.string.jobProcess_dialogLabel_arrivedConfirmation), getString(R.string.jobProcess_dialogContent_arrivedStartingPoint), activity, this);
                dialogConfirm.show();
            }
            else if (true) {
                if (jobDetailRes.getStatus() == WaybillStatus.arrived.getValue()) {
                    dialogConfirm = new DialogConfirm(
                            getString(R.string.jobProcess_dialogLabel_arrivedConfirmation), getString(R.string.jobProcess_dialogContent_arrivedLoadingPoint),activity, this);
                    dialogConfirm.show();
                }
            }
        }
    }

    @Override
    public void BtnOkClick() {
        showLoading();
        preferenceHelper = new PreferenceHelper();
        String token = preferenceHelper.getString(Constant.TOKEN, "");
        if (jobDetailRes.getStatus() == WaybillStatus.issued.getValue()) {
            jobProcessingViewModel.startJob(token, jobDetailRes.getHashId());
        } else if (jobDetailRes.getStatus() == WaybillStatus.started.getValue()) {
            jobProcessingViewModel.arriveJob(token, jobDetailRes.getHashId());
        } else if (jobDetailRes.getStatus() == WaybillStatus.arrived.getValue()) {
            DetailWaybill currentWaybill = Util.GetNextDeliveryPoint(jobDetailRes);
            jobProcessingViewModel.finishDetailWaybill(token, currentWaybill.getHashId(), Util.isLastDeliveryPoint(jobDetailRes), jobDetailRes.getHashId());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void BtnOkDiaComClick() {

    }
}
