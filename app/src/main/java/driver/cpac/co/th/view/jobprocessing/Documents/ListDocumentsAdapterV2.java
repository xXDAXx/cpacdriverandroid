package driver.cpac.co.th.view.jobprocessing.Documents;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.Documents;
import com.tmg.th.R;
import com.tmg.th.databinding.ItemDocumentLayoutV3Binding;
import driver.cpac.co.th.util.enumClass.WaybillStatus;

import java.util.ArrayList;
import java.util.List;

public class ListDocumentsAdapterV2 extends RecyclerView.Adapter<ListDocumentsAdapterV2.MyViewHolder> {
    protected List<Documents> documentsList;
    private Context context;
    private DocumentViewClickListener mViewListener;
    private DocumentDeleteClickListener mDeleteClickListener;
    private WaybillStatus jobStatus;

    public ListDocumentsAdapterV2(Context context, List<Documents> documentsList, DocumentViewClickListener viewListener, DocumentDeleteClickListener deleteClickListener, WaybillStatus status) {
        this.context = context;
        this.documentsList = documentsList;
        this.jobStatus = status;
        if (status != WaybillStatus.finished)
            AddDocumentBtnToList();
        this.mViewListener = viewListener;
        this.mDeleteClickListener = deleteClickListener;
    }

    public void AddDocumentBtnToList() {
        Documents doc = new Documents(0, 0, "cargolink", "cargolink", "addnew", "btn");
        if (documentsList == null) documentsList = new ArrayList<Documents>();
        documentsList.add(0, doc);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemDocumentLayoutV3Binding binding;

        public MyViewHolder(ItemDocumentLayoutV3Binding binding, DocumentViewClickListener viewListener, DocumentDeleteClickListener deleteClickListener) {
            super(binding.getRoot());
            mViewListener = viewListener;
            mDeleteClickListener = deleteClickListener;
            this.binding = binding;
        }

        public void bind(Documents item, int position) {
            if (position == 0 && jobStatus != WaybillStatus.finished) {
                binding.documentName.setVisibility(View.GONE);
                binding.addImg.setVisibility(View.VISIBLE);
                binding.docImg.setVisibility(View.GONE);
                binding.closeBtn.setVisibility(View.GONE);
            } else {
                //
                binding.closeBtn.setVisibility(View.GONE);
                //
                binding.documentName.setVisibility(View.VISIBLE);
                binding.addImg.setVisibility(View.GONE);
                binding.docImg.setVisibility(View.VISIBLE);
                binding.closeBtn.setVisibility(View.GONE);
                binding.documentName.setText(item.getDocumentName());
                //String url = Constant.API_BASE_URL + Constant.MEDIA_PATH + item.getDocumentUrl();
                String url = item.getDocumentUrl();
                if (item.getFileType().toLowerCase().equals("pdf")) {
                    App.self().getPicasso().load(R.drawable.pdf_icon).into(binding.docImg);
                    binding.closeBtn.setVisibility(View.GONE);
                } else if (item.getFileType().toLowerCase().equals("jpg"))
                    App.self().getPicasso().load(url).into(binding.docImg);
                else if (item.getFileType().toLowerCase().equals("png"))
                    App.self().getPicasso().load(url).into(binding.docImg);
                else if (item.getFileType().toLowerCase().equals("docx")) {
                    App.self().getPicasso().load(R.drawable.docx_icon).into(binding.docImg);
                    binding.closeBtn.setVisibility(View.GONE);
                } else {
                    App.self().getPicasso().load(R.drawable.file).into(binding.docImg);
                    binding.closeBtn.setVisibility(View.GONE);
                }
                binding.documentName.setSelected(true);
            }

        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemDocumentLayoutV3Binding itemBinding =
                ItemDocumentLayoutV3Binding.inflate(layoutInflater, viewGroup, false);
        return new MyViewHolder(itemBinding, mViewListener, mDeleteClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        Documents documents = documentsList.get(position);
        MyViewHolder shipmentHolder = viewHolder;
        if (position == 0)
            shipmentHolder.itemView.setOnClickListener(v -> {
                mViewListener.passViewDocument(documents);
            });
        else
            shipmentHolder.binding.docImg.setOnClickListener(v -> {
                mViewListener.passViewDocument(documents);
            });
        shipmentHolder.binding.closeBtn.setOnClickListener(v -> {
            mDeleteClickListener.passDeleteDocument(documents);
        });
        viewHolder.bind(documents, position);
    }

    @Override
    public int getItemCount() {
        return documentsList == null ? 0 : documentsList.size();
    }

    public void updateAdapter(ArrayList<Documents> arrayList) {
        this.documentsList.clear();
        this.documentsList = arrayList;
        notifyDataSetChanged();
    }

    public interface DocumentViewClickListener {
        void passViewDocument(Documents documents);
    }

    public interface DocumentDeleteClickListener {
        void passDeleteDocument(Documents documents);
    }


}
