package driver.cpac.co.th.view.NewJobs;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import driver.cpac.co.th.Model.BaseResponse;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.ShipmentInfo;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.LangUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewJobsViewModel extends ViewModel {
    private ShipmentInfo shipmentInfo;
    public MutableLiveData<List<JobDetailResV2>> listShipmentInfo;
    public MutableLiveData<List<JobDetailResV2>> listJobRes;
    public MutableLiveData<Boolean> showLoading = new MutableLiveData<>();
    public MutableLiveData<Boolean> showEmpty = new MutableLiveData<>();
    public MutableLiveData<BaseResponse> baseResponse;
    private CargoLinkAPI mCargoLinkAPI;

    public MutableLiveData<BaseResponse> getBaseRes() {
        if (baseResponse == null) {
            baseResponse = new MutableLiveData<>();
        }
        return baseResponse;
    }

    public MutableLiveData<List<JobDetailResV2>> getListShipmentInfo() {
        if (listShipmentInfo == null) {
            listShipmentInfo = new MutableLiveData<>();
        }
        return listShipmentInfo;
    }

    public MutableLiveData<List<JobDetailResV2>> getlistJobRes() {
        if (listJobRes == null) {
            listJobRes = new MutableLiveData<>();
        }
        return listJobRes;
    }

    public MutableLiveData<Boolean> getShowLoading() {
        if (showLoading == null) {
            showLoading = new MutableLiveData<>();
        }
        return showLoading;
    }

    public MutableLiveData<Boolean> getShowEmpty() {
        if (showEmpty == null) {
            showEmpty = new MutableLiveData<>();
        }
        return showEmpty;
    }


    //API getJob
    public void getNewJobs(String Token) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.getNewJobs(Token, LangUtils.getCurrentLanguage()).enqueue(new Callback<List<JobDetailResV2>>() {
            @Override
            public void onResponse(Call<List<JobDetailResV2>> call, Response<List<JobDetailResV2>> response) {
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        listJobRes.setValue(response.body());
                        showEmpty.setValue(false);
                    } else {
                        showLoading.setValue(false);
                        showEmpty.setValue(true);
                        listJobRes.setValue(null);
                    }
                } else {
                    showLoading.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<List<JobDetailResV2>> call, Throwable t) {
                Log.d("dadas",t.toString());
            }
        });

    }


}
