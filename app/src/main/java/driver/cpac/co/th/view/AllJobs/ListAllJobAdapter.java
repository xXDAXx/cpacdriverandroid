package driver.cpac.co.th.view.AllJobs;
//aaaaaa
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.tmg.th.R;
import com.tmg.th.databinding.ItemShipmentLayoutBinding;

import java.util.ArrayList;
import java.util.List;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.DetailWaybill;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.CargoType;
import driver.cpac.co.th.util.enumClass.TruckType;
import driver.cpac.co.th.util.enumClass.WaybillStatus;

public class ListAllJobAdapter extends PagedListAdapter<JobDetailResV2,ListAllJobAdapter.MyViewHolder> implements Filterable {

    protected List<JobDetailResV2> jobList;
    protected List<JobDetailResV2> originalList;
    private Context context;
    private JobClickListenerV2 mListener;
    private ItemShipmentLayoutBinding binding;

    protected ListAllJobAdapter(@NonNull DiffUtil.ItemCallback<JobDetailResV2> diffCallback) {
        super(diffCallback);
    }

    protected ListAllJobAdapter(@NonNull AsyncDifferConfig<JobDetailResV2> config) {
        super(config);
    }

    /*public ListAllJobAdapter(Context context) {
        //super(Article.DIFF_CALLBACK);
        this.context = context;
        //this.jobList = jobList;
        //this.originalList = jobList;
        //mListener = listener;
    }*/
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemShipmentLayoutBinding itemBinding =
                ItemShipmentLayoutBinding.inflate(layoutInflater, viewGroup, false);
        return new MyViewHolder(itemBinding, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        JobDetailResV2 currentShipment = jobList.get(position);
        MyViewHolder shipmentHolder = (MyViewHolder) viewHolder;
        shipmentHolder.itemView.setOnClickListener(v -> {
            mListener.passContent(currentShipment);
        });
        viewHolder.bind(currentShipment);
    }

    @Override
    public int getItemCount() {
        return jobList == null ? 0 : jobList.size();
    }

    public void updateAdapter(ArrayList<JobDetailResV2> arrayList) {
        this.jobList.clear();
        this.jobList = arrayList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemShipmentLayoutBinding binding;

        public MyViewHolder(ItemShipmentLayoutBinding binding, JobClickListenerV2 listener) {
            super(binding.getRoot());
            mListener = listener;
            this.binding = binding;
        }

        public void bind(JobDetailResV2 item) {
            binding.shipmentId.setText(context.getString(R.string.waybill_label_prefix) + item.getId().toString().toUpperCase() + " - " + item.getShipperName().toUpperCase());
            binding.shipmentSts.setText(Util.GetWaybillStatus(item.getStatus(),context));
            binding.pickupAddr.setText(item.getLoadingAddress());
            binding.dropAddr.setText(item.getListDetailWaybil().get(item.getListDetailWaybil().size() - 1).getAddressDest());
            binding.pickupTime.setText(item.getLoadingTime());
            binding.dropTime.setText(item.getListDetailWaybil().get(item.getListDetailWaybil().size() - 1).getDeliveryDatetime());
            if(item.getWaybillType() != 0) {
                binding.estimateDistance2.setVisibility(View.VISIBLE);
                binding.goodsInfo.setVisibility(View.VISIBLE);
                binding.cargoIcon.setVisibility(View.VISIBLE);
                binding.goodsInfo.setText(CargoType.GetCargoType(item.getTypeOfCargo(),context) + " - " + item.getTotalWeight().toString() + context.getString(R.string.common_tons));
                binding.executePendingBindings();
                binding.dashline.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                //Distance info
                if (item.getEstimateDistance() == null || item.getEstimateDistance().isEmpty()) {
                    binding.estimateDistance2.setVisibility(View.GONE);
                    binding.estimateDistance2.setVisibility(View.GONE);
                } else
                    binding.lblEstimate2.setText(context.getText(R.string.common_est) + " " + item.getEstimateDistance());
                if(item.getEstimateDistance()!=null)
                    binding.lblEstimate2.setText(context.getText(R.string.common_est)+ " "+ item.getEstimateDistance());
                App.self().getPicasso().get().load(CargoType.GetCargoTypeImg(item.getTypeOfCargo(), context)).placeholder(R.drawable.others).into(binding.cargoIcon);
                App.self().getPicasso().get().load(TruckType.GetTruckTypeImg(item.getTruckType(), context)).into(binding.truckIcon);

            } else if (item.getWaybillType() == 0) {
                binding.estimateDistance2.setVisibility(View.GONE);
                binding.goodsInfo.setVisibility(View.GONE);
                binding.cargoIcon.setVisibility(View.GONE);
            }
            //color status
            //binding.shipmentSts.setBackgroundColor(Util.JobStatusColorHelper(item.getStatus(), context));
            binding.truckInfo.setText(TruckType.GetTruckType(item.getTruckType(),context).toUpperCase() + " - " + item.getTruckNumber().toUpperCase());

            //binding.lblEstimate1.setText(context.getText(R.string.common_est)+ " "+ item.getEstimateDistance());

            TextView listTview[] ={binding.shipmentId,binding.shipmentSts,binding.pickupAddr,binding.dropAddr,binding.pickupTime,binding.dropTime, binding.goodsInfo, binding.truckInfo};
            if(item.getStatus() == WaybillStatus.waitingDriver.getValue()) {
                binding.cardViewParent.setCardBackgroundColor(ContextCompat.getColor(context, R.color.job_status_new));
                for (TextView tv: listTview) { tv.setTextColor(Color.WHITE);}
                binding.shipmentSts.setBackground(context.getDrawable(R.drawable.white_border_round_corner_tview));
            } else {
                binding.cardViewParent.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white));
                for (TextView tv: listTview) { tv.setTextColor(ContextCompat.getColor(context,R.color.gray3dot));}
                binding.shipmentSts.setTextColor((ContextCompat.getColor(context,R.color.cargolink_yellow)));
                binding.shipmentId.setTextColor((ContextCompat.getColor(context,R.color.gray3dot)));
                binding.shipmentSts.setBackground(context.getDrawable(R.drawable.round_corner_tview));
            }
            binding.shipmentId.setSelected(true);
            binding.goodsInfo.setSelected(true);
            binding.truckInfo.setSelected(true);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                jobList = (List<JobDetailResV2>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<JobDetailResV2> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = jobList;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    public Filter getStatusFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                jobList = (List<JobDetailResV2>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<JobDetailResV2> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = jobList;
                } else {
                    filteredResults = getStatusFilteredResults(Short.valueOf(constraint.toString().toLowerCase()));
                }
                FilterResults results = new FilterResults();
                results.values = filteredResults;
                return results;
            }
        };
    }

    protected List<JobDetailResV2> getFilteredResults(String constraint) {
        List<JobDetailResV2> results = new ArrayList<>();

        for (JobDetailResV2 item : originalList) {
            if (item.getShipperName().toLowerCase().contains(constraint) ||
                    item.getId().toString().toLowerCase().contains(constraint) ||
                    item.getLoadingTime().toLowerCase().contains(constraint) ||
                    item.getTruckNumber().toLowerCase().contains(constraint) ||
                    item.getLoadingAddress().toLowerCase().contains(constraint) ||
                    ListDestinationCheckContain(constraint,item.getListDetailWaybil())) {
                results.add(item);
            }
        }
        return results;
    }

    protected List<JobDetailResV2> getStatusFilteredResults(int status) {
        List<JobDetailResV2> results = new ArrayList<>();
        for (JobDetailResV2 item : originalList) {
            if (item.getStatus() == status) {
                results.add(item);
            }
        }
        return results;
    }

    public interface JobClickListenerV2 {
        void passContent(JobDetailResV2 listJobRes);
    }

    public boolean ListDestinationCheckContain(String string, List<DetailWaybill> list) {
        boolean result = false;
        for (DetailWaybill detailWabill:list) {
            if (detailWabill.getAddressDest().toLowerCase().contains(string) ||
                    detailWabill.getDeliveryDatetime().contains(string) ||
                    detailWabill.getFullnameDest().contains(string) )
                return true;
        }
        return result;
    }

}