package driver.cpac.co.th.Model.google;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class Polyline implements Serializable{

    @SerializedName("points")
    @Expose
    private String points;

    public Polyline(String points) {
        this.points = points;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
