package driver.cpac.co.th.view.AllJobs;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tmg.th.R;
import com.tmg.th.databinding.FragmentAllJobsBinding;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.RealmObject.NewWaybillId;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.RealmHelper;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.Home.HomeActivity;
import driver.cpac.co.th.view.MyJobs.ListJobAdapterV2;
import driver.cpac.co.th.view.dialog.DialogNewJob;
import driver.cpac.co.th.view.dialog.DialogNotice;
import driver.cpac.co.th.view.dialog.DialogWait2;
import driver.cpac.co.th.view.jobdetail.JobDetailActivityV3;
import driver.cpac.co.th.view.jobprocessing.JobMovingActivity;
import driver.cpac.co.th.view.jobprocessing.JobProcessingActivityV3;
import io.realm.RealmResults;

public class AllJobsFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    private AllJobsViewModel allJobsViewModel;
    private static View view;
    private FragmentAllJobsBinding binding;
    private DialogWait2 dialogWait;
    private DialogNotice dialogNotice;
    private ListJobAdapterV2 adapter;
    private List<JobDetailResV2> shipmentInfoList = new ArrayList<>();
    private List<JobDetailResV2> newJobList = new ArrayList<>();
    private int i = 0;
    private HomeActivity parent;
    DatePickerDialog datePickerDialog;
    DialogNewJob dialogNewJob;
    int dateSwitcher = 0;
    PreferenceHelper preferenceHelper;
    private JobDetailResV2 jobDetailRes;
    private JobDetailResV2 shipmentInfo;
    private List<JobDetailResV2> listNewJob;
    boolean clicked;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        preferenceHelper = new PreferenceHelper(getContext());
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_all_jobs, container, false);
        view = binding.getRoot();
        allJobsViewModel = ViewModelProviders.of(this).get(AllJobsViewModel.class);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.rcv.setVisibility(View.INVISIBLE);
        parent = (HomeActivity) getActivity();
        ObserverSetup();
        ComponentSetup();

        setupStatusFilter();
        return view;
    }

    public void ComponentSetup() {
        binding.swipeContainer.setOnRefreshListener(() -> allJobsViewModel.getMyJobs(preferenceHelper.getString(Constant.TOKEN, "")));
        binding.searchBox.searchEdt.setOnClickListener(v -> {
            showSearchBox(true);
        });
        binding.searchBox.imgSearch.setOnClickListener(v -> {
            showSearchBox(true);
        });
        binding.searchBox.imgClose.setOnClickListener(v -> {
            AllJobsFragment.this.showSearchBox(false);
            hideKeyboardFrom(getContext(), getView());
        });
        binding.searchBox.searchBtn.setOnClickListener(b -> {
            String searchTxt = binding.searchBox.searchEdt.getText().toString() + "^#^"
                    + binding.searchBox.destEdt.getText().toString() + "#^"
                    + binding.searchBox.loadEdt.getText().toString() + "#^"
                    + binding.searchBox.loadDate.getText().toString() + "#^"
                    + binding.searchBox.deliveryDate.getText().toString();
            adapter.getAdvancedFilter().filter(searchTxt);
        });
        binding.searchBox.loadDate.setOnClickListener(v -> {
            dateSwitcher = 0;
            showCalendarDialog();
        });
        binding.searchBox.deliveryDate.setOnClickListener(v -> {
            dateSwitcher = 1;
            showCalendarDialog();

        });
        binding.imageView.setOnClickListener(v -> {
            DialogNewJob dialogNewJob = new DialogNewJob(this.getActivity(), shipmentInfoList);
            dialogNewJob.show();
            Window window = dialogNewJob.getWindow();
            window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT);
        });
    }

    public void showCalendarDialog() {
        hideKeyboardFrom(getContext(), getView());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            final Calendar newCalendar = Calendar.getInstance();
            datePickerDialog = new DatePickerDialog(getContext(), this::onDateSet, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }
    }

    public void showSearchBox(boolean bool) {
        if (bool) {
            binding.searchBox.searchAdvanceExtender.setVisibility(View.VISIBLE);
            binding.searchBox.imgClose.setVisibility(View.VISIBLE);
        } else {
            doClearFilter();
            binding.searchBox.searchAdvanceExtender.setVisibility(View.GONE);
            binding.searchBox.imgClose.setVisibility(View.GONE);
            //binding.searchBox.searchView.setIconified(true);
            binding.searchBox.searchEdt.clearFocus();
            binding.searchBox.deliveryDate.setText("");
            binding.searchBox.loadDate.setText("");
            binding.searchBox.loadEdt.setText("");
            binding.searchBox.destEdt.setText("");
            binding.searchBox.searchEdt.setText("");
        }
    }

    public void ObserverSetup() {
        allJobsViewModel.getlistJobRes().removeObservers(this);
        allJobsViewModel.getShowLoading().removeObservers(this);
        allJobsViewModel.getShowEmpty().removeObservers(this);
        allJobsViewModel.getjobDetailLiveData().removeObservers(this);
        allJobsViewModel.getlistJobRes().observe(this, listJobRes -> {
            hideLoading();
            if (listJobRes != null && listJobRes.size() > 0) {
                binding.swipeContainer.setRefreshing(false);
                shipmentInfoList = listJobRes;
                newJobList = listJobRes;
                //if (parent.getDashBoard_frag().getViewModel() != null)
                //parent.getDashBoard_frag().getViewModel().setListMutableLiveData(shipmentInfoList);
                parent.setBadge(0, countNewJobs(listJobRes));
                hideEmpty();
                bindingData();
                initStatusFilter();
            }
        });
        allJobsViewModel.getShowLoading().observe(this, aBoolean -> {
            if (aBoolean) showLoading();
            else hideLoading();
        });
        allJobsViewModel.getShowEmpty().observe(this, aBoolean -> {
            if (!aBoolean) hideEmpty();
            else showEmpty();
        });
        allJobsViewModel.getjobDetailLiveData().observe(this, jobDetailRes -> {
            hideLoading();

            this.jobDetailRes = jobDetailRes;
            if (clicked) {
                if (jobDetailRes.getStatus() == WaybillStatus.waitingDriver.getValue()) {
                    Intent intent = new Intent(getContext(), JobDetailActivityV3.class);
                    intent.putExtra("content", jobDetailRes);
                    clicked = false;
                    startActivity(intent);
                } else {
                    Intent intent;
                    if (jobDetailRes.getWaybillType() == 0) {
                        intent = new Intent(getContext(), JobMovingActivity.class);
                    } else {
                        intent = new Intent(getContext(), JobProcessingActivityV3.class);
                    }
                    intent.putExtra("jobDetail", this.jobDetailRes);
                    clicked = false;
                    startActivity(intent);
                }
            }

        });
        allJobsViewModel.getCountUnRead().observe(this, number -> {
            parent.setBadge(1, number);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        showLoading();
        allJobsViewModel.getMyJobs(preferenceHelper.getString(Constant.TOKEN, ""));
        getMstInfo();
        getDashboardInfo();
        /*if (shipmentInfoList.size() > 0) {
            binding.CoffeeView.setVisibility(View.GONE);
            binding.rcv.setVisibility(View.VISIBLE);
            adapter = new ListJobAdapterV2(getContext(), shipmentInfoList, listener);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            layoutManager.setOrientation(RecyclerView.VERTICAL);
            binding.rcv.setLayoutManager(layoutManager);
            binding.rcv.setAdapter(adapter);
            doClearFilter();
        }*/
    }

    public void doFilterShipmentInfo(String newText) {
        if (shipmentInfoList.size() > 0)
            adapter.getFilter().filter(newText);
    }

    public void doStatusFilterShipmentInfo(int status) {
        if (shipmentInfoList.size() > 0)
            adapter.getStatusFilter().filter(String.valueOf(status));
    }

    public void doClearFilter() {
        adapter = new ListJobAdapterV2(getContext(), shipmentInfoList, listener);
        if (binding != null)
            binding.rcv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    ListJobAdapterV2.JobClickListenerV2 listener = shipmentInfo -> {
        clicked = true;
        showLoading();
        allJobsViewModel.getDetailJob(preferenceHelper.getString(Constant.TOKEN, ""), shipmentInfo.getHashId());
        this.shipmentInfo = shipmentInfo;
    };

    public void showLoading() {
        parent.showLoading();
    }

    public void hideLoading() {
        parent.hideLoading();
    }

    public void showEmpty() {
        binding.CoffeeView.setVisibility(View.VISIBLE);
        binding.rcv.setVisibility(View.GONE);
    }

    public void hideEmpty() {
        binding.CoffeeView.setVisibility(View.GONE);
        binding.rcv.setVisibility(View.VISIBLE);
    }

    public void bindingData() {
        adapter = new ListJobAdapterV2(getContext(), shipmentInfoList, listener);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.rcv.setLayoutManager(layoutManager);
        binding.rcv.setAdapter(adapter);

        //Show Dialog NewJob popup
        if (getListNewJob(newJobList).size() > 0) {
            if (dialogNewJob != null && !dialogNewJob.isShowing()) {
                dialogNewJob = new DialogNewJob(this.getActivity(), getListNewJob(newJobList));
                dialogNewJob.show();
                RealmHelper.deleteOfflineNewJob();
            } else if (dialogNewJob == null)
                dialogNewJob = new DialogNewJob(this.getActivity(), getListNewJob(newJobList));
            dialogNewJob.show();
            Window window = dialogNewJob.getWindow();
            window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        hideLoading();
    }

    public void getMstInfo() {
        String Token = preferenceHelper.getString(Constant.TOKEN, "");
        allJobsViewModel.getMstType(Token);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String textTime = (dayOfMonth < 10 ? "0" : "") + String.valueOf(dayOfMonth) + "-" + (month < 9 ? "0" : "") + String.valueOf(month + 1) + "-" + String.valueOf(year);
        if (dateSwitcher == 0) {
            binding.searchBox.loadDate.setText(textTime);
        } else if (dateSwitcher == 1) {
            binding.searchBox.deliveryDate.setText(textTime);
        }

    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void getDashboardInfo() {
        showLoading();
        String Token = preferenceHelper.getString(Constant.TOKEN, "");
        allJobsViewModel.countUnreadNotifications(Token);
    }

    public int countNewJobs(List<JobDetailResV2> list) {
        int i = 0;
        for (JobDetailResV2 job : list) {
            if (job.getStatus() == WaybillStatus.waitingDriver.getValue())
                i++;
        }
        return i;
    }

    public void setupStatusFilter() {
        List<TextView> arr = new ArrayList<>();
        arr.add(binding.lblAll);
        arr.add(binding.lblNew);
        arr.add(binding.lblInProgess);
        for (TextView tv : arr) {
            tv.setOnClickListener(v -> {
                showSearchBox(false);
                for (TextView tv2 : arr) {
                    tv2.setTextColor(ContextCompat.getColor(getContext(), R.color.gray3dot));
                    tv2.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
                }
                tv.setTextColor(Color.WHITE);
                tv.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dashboard_time_color));
                if (tv.equals(binding.lblAll)) {
                    adapter.getStatusFilter().filter("");
                } else if (tv.equals(binding.lblNew)) {
                    adapter.getStatusFilter().filter(String.valueOf(WaybillStatus.waitingDriver.getValue()));
                } else if (tv.equals(binding.lblInProgess)) {
                    adapter.getStatusFilter().filter("99");
                }
            });
        }
    }

    public void initStatusFilter() {
        List<TextView> arr = new ArrayList<>();
        arr.add(binding.lblAll);
        arr.add(binding.lblNew);
        arr.add(binding.lblInProgess);
        for (TextView tv2 : arr) {
            tv2.setTextColor(ContextCompat.getColor(getContext(), R.color.gray3dot));
            tv2.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        }
        binding.lblAll.setTextColor(Color.WHITE);
        binding.lblAll.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.dashboard_time_color));
        adapter.getStatusFilter().filter("");
    }

    protected List<JobDetailResV2> getListNewJob(List<JobDetailResV2> list) {
        List<JobDetailResV2> results = new ArrayList<>();
        List<NewWaybillId> listNewJobId = new ArrayList<>();
        RealmResults<NewWaybillId> realmResults = RealmHelper.getOfflineNewJob();
        if (realmResults.size() > 0) {
            listNewJobId = RealmHelper.copyToListNewJob(realmResults);
        }
        if (listNewJobId.size() > 0) {
            for (JobDetailResV2 item : list) {
                for (NewWaybillId newJob : listNewJobId) {
                    if (item.getStatus() == 1 && item.getId().toString().equals(newJob.getWaybillId())) {
                        results.add(item);
                    }
                }
            }
        }
        return results;
    }
}
