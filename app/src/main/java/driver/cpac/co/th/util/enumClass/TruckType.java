package driver.cpac.co.th.util.enumClass;

import android.content.Context;

import com.google.gson.Gson;

import driver.cpac.co.th.Model.GetMstTypeRes;
import driver.cpac.co.th.Model.MstTypeItem;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.util.PreferenceHelper;

public enum TruckType {
        many(-1),
        fourWheelsMinivan(1),
        fourWheelsOpenPickup(2),
        sixWheelsBox(3),
        sixWheelsCanvas(4),
        fourWheelsOpenTop(5),
        fourWheelsVan(6),
        fourWheelsFlatbed(7),
        tenWheelsMobileCrane(8),
        tenWheelsDump(9),
        twelveWheelsCarCarrier(10),
        fourWheelsRefrigerator(11),
        threeAxisTractorUnit(12),
        sixWheelsLiquidTank(13),
        tenWheelsChemicalLiquidTank(14),
        others(15),
        twoAxisTractorUnit(16),
        fourWheelsCabinetPickup(17),
        fourWheelsRefrigeratorPickup(18),
        fourWheelsCap(19),
        fourWheelsStall(20),
        sixWheelsCabinet(21),
        sixWheelsFlatbed(22),
        sixWheelsMotobikeCarrier(23),
        sixWheelsOpenTop(24),
        sixWheelsRefrigerator(25),
        tenWheelBox(26),
        tenWheelsCabinet(27),
        tenWheelsFlatbed(28),
        tenWheelsLiquidTank(29),
        tenWheelsRefrigerator(30),
        eighteenWheelsFlatbed(31),
        eighteenWheelsSemiTrailerBulkCement(32),
        twentyTrailer18WheelsCabinet(33),
        twentyTrailer18WheelsRefrigerator(34),
        twentyTrailer18WheelsBox(35),
        twentyTrainer18WheelsStallDump(36),
        fortyContainerTrailer(37),
        fortyRefrigeratorContainerTrailer(38),
        sixWheelsCrane(39);

        private final int value;

        private TruckType(int value) {
            this.value = value;
        }

        public short getValue() {
            return (short) value;
        }

        public int getIntValue() {
            return value;
        }

    public static String GetTruckTypeImg(int truckType, Context context) {
        String urlRet = "";
        PreferenceHelper preferenceHelper = new PreferenceHelper(context);
        Gson gson = new Gson();
        GetMstTypeRes res = gson.fromJson(preferenceHelper.getString(Constant.MST_TYPE,""),GetMstTypeRes.class);
        if(res != null) {
            for (MstTypeItem item : res.getTruckType()) {
                if (item.getType() == truckType) {
                    urlRet = Constant.API_BASE_URL.replace("api/","") + item.getImgUrl();
                    break;
                }
            }
        }
        return urlRet;
    }

    public static String GetTruckType(int truckType, Context context) {
        String typeRet = "";
        PreferenceHelper preferenceHelper = new PreferenceHelper(context);
        Gson gson = new Gson();

        GetMstTypeRes res = gson.fromJson(preferenceHelper.getString(Constant.MST_TYPE,""),GetMstTypeRes.class);
        if(res != null) {
            for (MstTypeItem item : res.getTruckType()) {
                if (item.getType() == truckType) {
                    if (LangUtils.getCurrentLanguage().equals("en")) {
                        typeRet = item.getNameEng();
                        break;
                    } else if (LangUtils.getCurrentLanguage().equals("th")) {
                        typeRet = item.getNameThai();
                        break;
                    }
                }
            }
        }
        return typeRet;
    }

}
