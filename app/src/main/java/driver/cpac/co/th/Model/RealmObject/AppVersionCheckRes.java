package driver.cpac.co.th.Model.RealmObject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AppVersionCheckRes implements Serializable {
    @SerializedName("appId")
    @Expose
    private String appID;

    @SerializedName("lastestVersion")
    @Expose
    private String lastestVersion;

    @SerializedName("haveUpdate")
    @Expose
    private boolean haveUpdate;

    @SerializedName("forceUpdate")
    @Expose
    private boolean forceUpdate;

    public AppVersionCheckRes(String appID, String lastestVersion, boolean haveUpdate, boolean forceUpdate) {
        this.appID = appID;
        this.lastestVersion = lastestVersion;
        this.haveUpdate = haveUpdate;
        this.forceUpdate = forceUpdate;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

    public String getLastestVersion() {
        return lastestVersion;
    }

    public void setLastestVersion(String lastestVersion) {
        this.lastestVersion = lastestVersion;
    }

    public boolean isHaveUpdate() {
        return haveUpdate;
    }

    public void setHaveUpdate(boolean haveUpdate) {
        this.haveUpdate = haveUpdate;
    }

    public boolean isForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }
}
