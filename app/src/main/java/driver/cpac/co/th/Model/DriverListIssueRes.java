/**
 * Copyright (c) 2018, gmenu Inc.<br>
 * All rights reserved.
 */
package driver.cpac.co.th.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class DriverListIssueRes implements Serializable {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("hashId")
    @Expose
    private String hashId;

    @SerializedName("content")
    @Expose
    private String content;

    @SerializedName("type")
    @Expose
    private int type;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("waybillId")
    @Expose
    private int waybillId;

    @SerializedName("waybillHashId")
    @Expose
    private String waybillHashId;

    @SerializedName("updatedBy")
    @Expose
    private String updatedBy;

    @SerializedName("updatedTime")
    @Expose
    private String updatedTime;

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    private List<DriverIssueDocumentRes> listDocument;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getWaybillId() {
        return waybillId;
    }

    public void setWaybillId(int waybillId) {
        this.waybillId = waybillId;
    }

    public String getWaybillHashId() {
        return waybillHashId;
    }

    public void setWaybillHashId(String waybillHashId) {
        this.waybillHashId = waybillHashId;
    }

    public String getUpdatedName() {
        return updatedBy;
    }

    public void setUpadtedName(String upadtedName) {
        this.updatedBy = upadtedName;
    }

    public List<DriverIssueDocumentRes> getListDocument() {
        return listDocument;
    }

    public void setListDocument(List<DriverIssueDocumentRes> listDocument) {
        this.listDocument = listDocument;
    }
}
