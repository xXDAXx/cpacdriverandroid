package driver.cpac.co.th.view.MailBox;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import driver.cpac.co.th.Model.DriverNotificationRes;
import com.tmg.th.databinding.ItemNotificationLayoutBinding;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.NotificationStatus;
import driver.cpac.co.th.util.enumClass.NotificationType;

import java.util.ArrayList;
import java.util.List;

public class ListNotificationAdapter extends RecyclerView.Adapter<ListNotificationAdapter.MyViewHolder> implements Filterable {

    protected List<DriverNotificationRes> notificationResList;
    protected List<DriverNotificationRes> originalList;
    private Context context;
    private NotifyClickListener mListener;

    public ListNotificationAdapter(Context context, List<DriverNotificationRes> jobList, NotifyClickListener listener) {
        this.context = context;
        this.notificationResList = jobList;
        this.originalList = jobList;
        mListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemNotificationLayoutBinding itemBinding =
                ItemNotificationLayoutBinding.inflate(layoutInflater, viewGroup, false);
        return new MyViewHolder(itemBinding, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        DriverNotificationRes currentNotit = notificationResList.get(position);
        MyViewHolder notificationHolder = (MyViewHolder) viewHolder;
        notificationHolder.itemView.setOnClickListener(v -> {
            mListener.passContent(currentNotit);
        });
        viewHolder.bind(currentNotit);
    }

    @Override
    public int getItemCount() {
        return notificationResList == null ? 0 : notificationResList.size();
    }

    public void updateAdapter(ArrayList<DriverNotificationRes> arrayList) {
        this.notificationResList.clear();
        this.notificationResList = arrayList;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemNotificationLayoutBinding binding;

        public MyViewHolder(ItemNotificationLayoutBinding binding, NotifyClickListener listener) {
            super(binding.getRoot());
            mListener = listener;
            this.binding = binding;
        }

        public void bind(DriverNotificationRes item) {
            binding.tittleNotification.setText(item.getTitle());
            binding.summaryNotification.setText(item.getSummary());
            binding.sentTime.setText(Util.GetFriendlyTimeText(Util.convertStringToTimestamp(item.getSentTime())));
            NotificationType type = NotificationType.getEnumType(item.getType());
            switch (type) {
                case jobNotification:

                case others:
                    break;
                case systemNotification:
                    break;
            }

            NotificationStatus status = NotificationStatus.getEnumType(item.getStatus());
            switch (status) {
                case read:
                    binding.tittleNotification.setTypeface(Typeface.DEFAULT);
                    binding.isNewIcon.setVisibility(View.GONE);
                    break;
                case unread:
                    binding.tittleNotification.setTypeface(Typeface.DEFAULT_BOLD);;
                    binding.isNewIcon.setVisibility(View.VISIBLE);
                    break;
                case delete:
                    break;
            }
        }
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                notificationResList = (List<DriverNotificationRes>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<DriverNotificationRes> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = notificationResList;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    public Filter getStatusFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                notificationResList = (List<DriverNotificationRes>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<DriverNotificationRes> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = notificationResList;
                } else {
                    filteredResults = getStatusFilteredResults(Short.valueOf(constraint.toString().toLowerCase()));
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    protected List<DriverNotificationRes> getFilteredResults(String constraint) {
        List<DriverNotificationRes> results = new ArrayList<>();

        for (DriverNotificationRes item : originalList) {
            /*
            if (item.getShipperName().toLowerCase().contains(constraint) ||
                    item.getId().toString().toLowerCase().contains(constraint) ||
                    item.getTimeFromDeliveryOrg().toLowerCase().contains(constraint) ||
                    item.getTimeToDeliveryOrg().toLowerCase().contains(constraint) ||
                    item.getTimeFromPickupOrg().toLowerCase().contains(constraint) ||
                    item.getTimeToPickupOrg().toLowerCase().contains(constraint) ||
                    item.getTruckNumber().toLowerCase().contains(constraint) ||
                    item.getDeliveryAddress().toLowerCase().contains(constraint) ||
                    item.getLoadingAddress().toLowerCase().contains(constraint)) {
                results.add(item);
            }
            */
        }
        return results;
    }

    protected List<DriverNotificationRes> getStatusFilteredResults(int status) {
        List<DriverNotificationRes> results = new ArrayList<>();

        for (DriverNotificationRes item : originalList) {
            if (item.getStatus() == status) {
                results.add(item);
            }
        }
        return results;
    }
}