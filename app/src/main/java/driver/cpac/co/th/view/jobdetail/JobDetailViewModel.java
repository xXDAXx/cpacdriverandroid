package driver.cpac.co.th.view.jobdetail;

import android.os.Handler;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import driver.cpac.co.th.Model.DirectionReq;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.JobStatusUpdateReq;
import driver.cpac.co.th.Model.google.Directions;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.util.enumClass.WaybillStatus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobDetailViewModel extends ViewModel {

    private CargoLinkAPI cargoLinkAPI;
    public MutableLiveData<Directions> directionsLiveData;
    public MutableLiveData<Boolean> jobAccepted ;
    public MutableLiveData<Boolean> jobRejected ;
    public MutableLiveData<Boolean> showLoading ;
    public MutableLiveData<JobDetailResV2> jobDetailLiveData;

    public MutableLiveData<Directions> getDirectionsLiveData() {

        if (directionsLiveData == null) {
            directionsLiveData = new MutableLiveData<>();
        }
        return directionsLiveData;
    }

    public MutableLiveData<JobDetailResV2> getjobDetailLiveData() {
        if (jobDetailLiveData == null) {
            jobDetailLiveData = new MutableLiveData<>();
        }
        return jobDetailLiveData;
    }

    public MutableLiveData<Boolean> getJobAccepted() {
        if (jobAccepted == null) {
            jobAccepted = new MutableLiveData<>();
        }
        return jobAccepted;
    }

    public MutableLiveData<Boolean> getJobRejected() {
        if (jobRejected == null) {
            jobRejected = new MutableLiveData<>();
        }
        return jobRejected;
    }

    public MutableLiveData<Boolean> getShowLoading() {
        if (showLoading == null) {
            showLoading = new MutableLiveData<>();
        }
        return showLoading;
    }

    public void getDirectionApi(DirectionReq directionReq){
        cargoLinkAPI = ApiUtils.getGGMAPI();
        cargoLinkAPI.getDirections(directionReq.getOrigin(),directionReq.getDestination(),directionReq.getKey())
                .enqueue(new Callback<Directions>() {
                    @Override
                    public void onResponse(Call<Directions> call, Response<Directions> response) {
                        if (response.isSuccessful()) {
                            Directions directions = response.body();
                            if(directions.getStatus().equals("OK")) {
                                directionsLiveData.setValue(directions);
                            }else{directionsLiveData.setValue(null); }

                        } else {
                            directionsLiveData.setValue(null);
                        }
                    }

                    @Override
                    public void onFailure(Call<Directions> call, Throwable t) {
                        directionsLiveData.setValue(null);
                    }
                });
    }

    // Call API accept job
    public void postAcceptJob(String Token,String jobId){
        showLoading.setValue(true);
        simulateDelay();
        cargoLinkAPI = ApiUtils.getAPIService();
        try {
            cargoLinkAPI.updateStatusWaybill(Token, LangUtils.getCurrentLanguage(),new JobStatusUpdateReq(jobId, WaybillStatus.driverAccepted.getValue(),null))
                    .enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            jobRejected.setValue(true);
                            if (response.isSuccessful()) {
                                jobRejected.setValue(true);
                            } else {
                                showLoading.setValue(false);
                            }
                        }
                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            showLoading.setValue(false);
                        }
                    });
        } catch (Exception ex){
            Log.println(0,"",ex.toString());
        }

    }

    public void postRejectJob(String Token,String jobId, String reason){
        showLoading.setValue(true);
        simulateDelay();
        cargoLinkAPI = ApiUtils.getAPIService();
        cargoLinkAPI.updateStatusWaybill(Token, LangUtils.getCurrentLanguage(),new JobStatusUpdateReq(jobId, WaybillStatus.driverRejected.getValue(),reason))
                .enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        showLoading.setValue(false);
                        if (response.isSuccessful()) {
                            jobRejected.setValue(true);
                        } else {

                        }
                    }
                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        showLoading.setValue(false);
                    }
                });
    }

    private void simulateDelay() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void waitasec(String Token){
        showLoading.setValue(true);
        cargoLinkAPI = ApiUtils.getAPIService();
        cargoLinkAPI.waitasec(Token, LangUtils.getCurrentLanguage())
                .enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.isSuccessful()) {
                            jobAccepted.setValue(true);
                        } else {
                            jobAccepted.setValue(true);
                        }
                    }
                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                           showLoading.setValue(false);
                    }
                });
    }

    public void getDetailJob(String Token,String jobId){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
        cargoLinkAPI = ApiUtils.getAPIService();
        cargoLinkAPI.getDetailWaybill(Token,LangUtils.getCurrentLanguage(),jobId)
                .enqueue(new Callback<JobDetailResV2>() {
                    @Override
                    public void onResponse(Call<JobDetailResV2> call, Response<JobDetailResV2> response) {
                        showLoading.setValue(false);
                        if (response.body()!=null){
                            jobDetailLiveData.setValue(response.body());
                        }else
                        {

                        }
                    }

                    @Override
                    public void onFailure(Call<JobDetailResV2> call, Throwable t) {
                        showLoading.setValue(false);
                    }
                });
            }
        }, 1000);
    }
}

