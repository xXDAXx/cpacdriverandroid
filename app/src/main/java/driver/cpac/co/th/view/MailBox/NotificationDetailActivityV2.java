package driver.cpac.co.th.view.MailBox;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import androidx.databinding.DataBindingUtil;

import com.tmg.th.R;
import driver.cpac.co.th.customComponent.ObservableScrollViewCallbacks;
import driver.cpac.co.th.customComponent.ScrollState;
import com.tmg.th.databinding.ActivityNotificationDetailV2Binding;
import driver.cpac.co.th.view.base.OldBaseActivity;

public class NotificationDetailActivityV2 extends OldBaseActivity implements ObservableScrollViewCallbacks {

    private ActivityNotificationDetailV2Binding binding;
    private int mFlexibleSpaceHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification_detail_v2);
        setSupportActionBar(binding.appBarLayout.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding.contenScrollView.setScrollViewCallbacks(this);
        mFlexibleSpaceHeight = 150;
        int flexibleSpaceAndToolbarHeight = mFlexibleSpaceHeight + getActionBarSize();
        binding.webview.setVisibility(View.GONE);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        updateFlexibleSpaceText(scrollY);
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    protected int getActionBarSize() {
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    private void updateFlexibleSpaceText(final int scrollY) {
        if (scrollY < 150)
            slideView(binding.transformerView,150,150-scrollY);
    }



    public static void slideView(View view,
                                 int currentHeight,
                                 int newHeight) {

        ValueAnimator slideAnimator = ValueAnimator
                .ofInt(currentHeight, newHeight)
                .setDuration(100);

        /* We use an update listener which listens to each tick
         * and manually updates the height of the view  */

        slideAnimator.addUpdateListener(animation1 -> {
            Integer value = (Integer) animation1.getAnimatedValue();
            view.getLayoutParams().height = value.intValue();
            view.requestLayout();
        });

        /*  We use an animationSet to play the animation  */

        AnimatorSet animationSet = new AnimatorSet();
        animationSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animationSet.play(slideAnimator);
        animationSet.start();
    }
}
