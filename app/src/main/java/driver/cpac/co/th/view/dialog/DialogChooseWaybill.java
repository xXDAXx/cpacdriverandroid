package driver.cpac.co.th.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tmg.th.R;
import com.tmg.th.databinding.DialogChooseWaybillBinding;

import java.util.List;

import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.view.MyJobs.ListJobAdapterV2;

public class DialogChooseWaybill extends Dialog {
    public Activity activity;
    public Dialog dialog;
    List<JobDetailResV2> content;
    private DialogChooseWaybillBinding binding;
    LangUtils langUtils;
    private ListJobAdapterV2 adapter;
    ListJobAdapterV2.JobClickListenerV2 listener;
    CallBackDialogChooseWaybill callback;

    public DialogChooseWaybill(List<JobDetailResV2> content, Activity a,CallBackDialogChooseWaybill callback) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
        this.content = content;
        this.callback =  callback;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_choose_waybill, (ViewGroup) activity.findViewById(android.R.id.content), false);
        setContentView(binding.getRoot());

        listener = shipmentInfo -> {
            callback.WaybillChosen(shipmentInfo);
            dismiss();
        };
        adapter = new ListJobAdapterV2(getContext(), content, listener);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.rcv.setLayoutManager(layoutManager);
        binding.rcv.setAdapter(adapter);
    }

    public interface CallBackDialogChooseWaybill {
        void WaybillChosen(JobDetailResV2 jobDetailResV2);
    }
}
