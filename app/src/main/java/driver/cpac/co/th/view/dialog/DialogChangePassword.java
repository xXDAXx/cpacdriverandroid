package driver.cpac.co.th.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import driver.cpac.co.th.Model.ChangePass;
import com.tmg.th.R;

import java.util.Objects;

public class DialogChangePassword extends Dialog {
    public Activity activity;
    public Dialog dialog;
    public Button ok, cancel;
    TextInputEditText cur_pwd, new_pwd, confirm_pwd;
    CallBackChangePassWord callBackChangePassWord;
    public ChangePass changePass;

    public DialogChangePassword(Activity a, CallBackChangePassWord callBackChangePassWord) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
        this.callBackChangePassWord = callBackChangePassWord;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_change_password);
        ok = (Button) findViewById(R.id.ok_btn);
        cancel = (Button) findViewById(R.id.cancel_btn);
        cur_pwd = (TextInputEditText) findViewById(R.id.current_pwd);
        new_pwd = (TextInputEditText) findViewById(R.id.new_pwd);
        confirm_pwd = (TextInputEditText) findViewById(R.id.new_pwd_confirm);
        ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Check inputpassword
                if (TextUtils.isEmpty(Objects.requireNonNull(cur_pwd.getText().toString()))) {
                    cur_pwd.setError(getContext().getString(R.string.login_errorMsg_emptyPassword));
                    cur_pwd.requestFocus();
                } else if (!(cur_pwd.getText().length() > 5)) {
                    cur_pwd.setError(getContext().getString(R.string.login_errorMsg_passwordIsShort));
                    cur_pwd.requestFocus();
                } else if (TextUtils.isEmpty(Objects.requireNonNull(new_pwd.getText().toString()))) {
                    new_pwd.setError(getContext().getString(R.string.login_errorMsg_emptyPassword));
                    new_pwd.requestFocus();
                } else if (!(new_pwd.getText().length() > 5)) {
                    new_pwd.setError(getContext().getString(R.string.login_errorMsg_passwordIsShort));
                    new_pwd.requestFocus();
                } else if (TextUtils.isEmpty(Objects.requireNonNull(confirm_pwd.getText().toString()))) {
                    confirm_pwd.setError(getContext().getString(R.string.login_errorMsg_emptyPassword));
                    confirm_pwd.requestFocus();
                } else if (!(confirm_pwd.getText().length() > 5)) {
                    confirm_pwd.setError(getContext().getString(R.string.login_errorMsg_passwordIsShort));
                    confirm_pwd.requestFocus();
                } else if (cur_pwd.getText().toString().equals(new_pwd.getText().toString())) {
                    new_pwd.setError(getContext().getString(R.string.changePassword_errorMsg_sameWithOld));
                    new_pwd.requestFocus();
                } else if (!confirm_pwd.getText().toString().equals(new_pwd.getText().toString())) {
                    confirm_pwd.setError(getContext().getString(R.string.changePassword_errorMsg_confrimNotMatch));
                    confirm_pwd.requestFocus();
                } else {
                    dismiss();
                    //changePass = new ChangePass(cur_pwd.getText().toString(),new_pwd.getText().toString(),confirm_pwd.getText().toString());
                    callBackChangePassWord.BtnChangeClick(new ChangePass(null,cur_pwd.getText().toString(),new_pwd.getText().toString(),confirm_pwd.getText().toString()));
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface CallBackChangePassWord {
        void BtnChangeClick(ChangePass changePass);
    }

}