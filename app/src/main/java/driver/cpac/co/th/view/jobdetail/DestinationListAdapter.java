package driver.cpac.co.th.view.jobdetail;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tmg.th.R;
import com.tmg.th.databinding.ItemListDestinationBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import driver.cpac.co.th.Model.DetailWaybill;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.WaybillStatus;

import static android.view.View.GONE;

public class DestinationListAdapter extends RecyclerView.Adapter<DestinationListAdapter.MyViewHolder> {
    protected List<DetailWaybill> jobList;
    protected List<DetailWaybill> originalList;
    private Context context;
    private ItemListDestinationBinding binding;
    private boolean showContact;
    private boolean expanable;

    public DestinationListAdapter(Context context, JobDetailResV2 jobDetailResV2, boolean showContact, boolean expanable) {
        this.context = context;
        this.jobList = jobDetailResV2.getListDetailWaybil();
        if (jobList.size() > 1)
            Collections.sort(jobList);
        AddLoadingPointToList(jobDetailResV2);
        this.originalList = jobList;
        this.showContact = showContact;
        this.expanable = expanable;
        if (!expanable) {
            ProcessDestinationList(jobDetailResV2);
        }
    }

    public void ProcessDestinationList(JobDetailResV2 jobResV2) {
        WaybillStatus status = WaybillStatus.getEnumType(jobResV2.getStatus());
        if (status == WaybillStatus.arrived)
            jobList.get(0).setShouldBeExpanded(true);
        else if (status == WaybillStatus.loaded) {
            for (DetailWaybill waybill : jobList) {
                if (waybill.getStatus() != WaybillStatus.finished.getValue() && waybill.getStatus() != WaybillStatus.loaded.getValue()) {
                    waybill.setShouldBeExpanded(true);
                    break;
                }
            }
        }
    }

    public void AddLoadingPointToList(JobDetailResV2 jobDetailResV2) {
        ///fix adđ duplicate loading point
        if(jobList.get(0).getId()!=0) {
            DetailWaybill loadingPoint = new DetailWaybill();
            loadingPoint.setId(0);
            loadingPoint.setAddressDest(jobDetailResV2.getLoadingAddress());
            loadingPoint.setDeliveryDatetime(jobDetailResV2.getLoadingTime());
            loadingPoint.setFullnameDest(jobDetailResV2.getFullnameOrg());
            loadingPoint.setPhoneDest(jobDetailResV2.getPhoneOrg());
            loadingPoint.setLatitudeDest(jobDetailResV2.getLoadingLat());
            loadingPoint.setLongitudeDest(jobDetailResV2.getLoadingLng());
            loadingPoint.setQuantity(jobDetailResV2.getQuantity());
            loadingPoint.setTotalWeight(jobDetailResV2.getTotalWeight().doubleValue());
            loadingPoint.setUnit(jobDetailResV2.getUnit());
            loadingPoint.setStatus(jobDetailResV2.getStatus());
            loadingPoint.setDoneTime(jobDetailResV2.getLoadedTime());
            loadingPoint.setNote(jobDetailResV2.getHandlingInstruction());
            jobList.add(0, loadingPoint);
        }
    }

    @NonNull
    @Override
    public DestinationListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemListDestinationBinding itemBinding =
                ItemListDestinationBinding.inflate(layoutInflater, viewGroup, false);
        return new DestinationListAdapter.MyViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DestinationListAdapter.MyViewHolder viewHolder, int position) {
        DetailWaybill currentWaybill = jobList.get(position);
        DestinationListAdapter.MyViewHolder shipmentHolder = (DestinationListAdapter.MyViewHolder) viewHolder;
        viewHolder.bind(currentWaybill, position);
    }

    @Override
    public int getItemCount() {
        return jobList == null ? 0 : jobList.size();
    }

    public void updateAdapter(ArrayList<DetailWaybill> arrayList) {
        this.jobList.clear();
        this.jobList = arrayList;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemListDestinationBinding binding;

        public MyViewHolder(ItemListDestinationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(DetailWaybill item, int position) {

            binding.executePendingBindings();
            binding.addressText.setText(item.getAddressDest());
            if (item.getStatus() != WaybillStatus.finished.getValue())
                binding.timeText.setText(item.getDeliveryDatetime());
            else {
                if (position == 0) {
                    binding.timeText.setText(context.getString(R.string.common_label_loadedat) + " " + item.getDoneTime());
                } else {
                    binding.timeText.setText(context.getString(R.string.common_label_deliveredat) + " " + item.getDoneTime());
                }
            }
            binding.positionText.setTitleText(String.valueOf(position));
            binding.dropOffImage.setImageResource(R.drawable.load_ic);
            binding.actionText.setText(context.getString(R.string.common_label_dropoff) + item.getQuantity() + " " + Util.GetUnitType(item.getUnit(), context).toLowerCase());
            binding.weightText.setText(item.getTotalWeight().toString() + context.getString(R.string.common_tons));
            binding.lblContact.setText(item.getFullnameDest());
            binding.txtTel.setText(item.getPhoneDest());
            binding.btnCall.setOnClickListener(v -> {
                String phone = item.getPhoneDest();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                context.startActivity(intent);
            });
            binding.btnSms.setOnClickListener(v -> {
                String number = item.getPhoneDest();  // The number on which you want to send SMS
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
            });
            boolean expanded = item.getShouldBeExpanded();
            binding.dropOffImage.setImageResource(R.drawable.delivery_ic);
            binding.expandableLayout.setVisibility(expanded ? View.VISIBLE : GONE);

            if (position == 0) {
                if (item.getNote()!=null && !item.getNote().isEmpty()) {
                    binding.noteIcon.setVisibility(View.VISIBLE);
                    binding.noteText.setVisibility(View.VISIBLE);
                    binding.noteText.setText(item.getNote());
                } else {
                    binding.noteIcon.setVisibility(View.GONE);
                    binding.noteText.setVisibility(View.GONE);
                }

                binding.dashlineTop.setVisibility(GONE);
                binding.positionText.setVisibility(GONE);
                binding.dashlineBottom.setVisibility(View.VISIBLE);
                binding.destinationIcon.setImageResource(R.drawable.destinattion_icon);
                binding.destinationIcon.setColorFilter(ContextCompat.getColor(context, R.color.cargolink_blue), PorterDuff.Mode.SRC_IN);
                binding.dropOffImage.setImageResource(R.drawable.load_ic);
                binding.actionText.setText(context.getString(R.string.common_label_pickup) + item.getQuantity() + " " + Util.GetUnitType(item.getUnit(), context).toLowerCase());
            } else {
                binding.noteIcon.setVisibility(GONE);
                binding.noteText.setVisibility(GONE);

                if (item.getShouldBeExpanded())
                    binding.destinationIcon.setColorFilter(ContextCompat.getColor(context, R.color.cargolink_yellow), PorterDuff.Mode.SRC_IN);
                else
                    binding.destinationIcon.setColorFilter(null);
                binding.dashlineTop.setVisibility(View.VISIBLE);
                binding.dashlineBottom.setVisibility(View.VISIBLE);
                //binding.dashlineExpand.setVisibility(View.VISIBLE);
                binding.destinationIcon.setImageResource(R.drawable.destinattion_icon);
                binding.positionText.setTitleText(String.valueOf(position));
                binding.positionText.setVisibility(View.VISIBLE);
            }
            if (expanable) {
                binding.showLayout.setOnClickListener(v -> {
                    if (item.getShouldBeExpanded() == true) {
                        item.setShouldBeExpanded(false);
                        notifyItemChanged(position);
                    } else
                        changeStateOfItemsInLayout(jobList, position);
                });
            }

            if (position == jobList.size() - 1) {
                //binding.dashlineExpand.setVisibility(GONE);
                //binding.dashlineExpand2.setVisibility(GONE);
                binding.dashlineBottom.setVisibility(GONE);
                binding.divider.setVisibility(GONE);
            }
            if (jobList.size() == 2) binding.positionText.setVisibility(GONE);

            if (!showContact) {
                binding.cardViewContactInfo.setVisibility(GONE);
            }
            if (item.getStatus() == WaybillStatus.loaded.getValue() || item.getStatus() == WaybillStatus.finished.getValue()) {
                binding.doneIcon.setVisibility(View.VISIBLE);
                binding.positionText.setVisibility(GONE);
            }
        }

        private void changeStateOfItemsInLayout(List<DetailWaybill> listItems, int p) {
            int idx = 0;
            for (DetailWaybill detailWaybill : listItems) {
                if (idx == p) {
                    detailWaybill.setShouldBeExpanded(true);
                    idx++;
                    notifyItemChanged(p);
                    continue;
                }
                if (detailWaybill.getShouldBeExpanded() == true)
                    notifyItemChanged(idx);
                detailWaybill.setShouldBeExpanded(false);
                idx++;
            }
        }


    }
}

