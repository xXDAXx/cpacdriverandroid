package driver.cpac.co.th.view.account;

import android.util.Log;

import androidx.lifecycle.ViewModel;

import com.google.gson.JsonObject;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.LangUtils;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AccountViewModel extends ViewModel {
    @Inject
    public AccountViewModel() {

    }

    private CargoLinkAPI mCargoLinkAPI;
    public void logout(String Token) {
        //set user login
        String token = (Token);
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.logoutcall(token,LangUtils.getCurrentLanguage()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                } else if (response.code() == 401) {
                    // Handle unauthorized
                } else if (response.code() == 400) {
                } else {
                    // Handle other responses
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                // TODO better error handling
                // Show error Dialog
                //simulateDelay();
            }
        });

    }
    //CHANGE LANGUAGE
    public void changeLanguage(String token, String languageId) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.changeLanguage(token,languageId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful())
                Log.i("abc","fail");
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("abc",t.toString());
            }
        });
    }
}