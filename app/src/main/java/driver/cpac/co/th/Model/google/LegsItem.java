package driver.cpac.co.th.Model.google;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

public class LegsItem implements Serializable{

    @SerializedName("duration")
    @Expose
    private Duration duration;

    @SerializedName("start_location")
    @Expose
    private StartLocation startLocation;

    @SerializedName("distance")
    @Expose
    private Distance distance;

    @SerializedName("start_address")
    @Expose
    private String startAddress;

    @SerializedName("steps")
    @Expose
    private List<StepsItem> steps;

    public LegsItem(Duration duration, StartLocation startLocation, Distance distance, String startAddress, List<StepsItem> steps) {
        this.duration = duration;
        this.startLocation = startLocation;
        this.distance = distance;
        this.startAddress = startAddress;
        this.steps = steps;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public StartLocation getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(StartLocation startLocation) {
        this.startLocation = startLocation;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public List<StepsItem> getSteps() {
        return steps;
    }

    public void setSteps(List<StepsItem> steps) {
        this.steps = steps;
    }
}
