package driver.cpac.co.th.WebApi;

import driver.cpac.co.th.util.Constant;


public class ApiUtils {
    private ApiUtils() {
    }

    public static final String BASE_URL = Constant.API_BASE_URL;
    public static final String GGMAPI_URL = Constant.GGM_API_URL;
    public static final String GPS_API_URL = Constant.GPS_API_URL;

    public static CargoLinkAPI getAPIService() {
        return RetrofitClient.getClient(BASE_URL).create(CargoLinkAPI.class);
    }

    public static CargoLinkAPI getGGMAPI() {
        return RetrofitClient.getGGMClient(GGMAPI_URL).create(CargoLinkAPI.class);
    }

    public static CargoLinkAPI getGpsAPI() {
        return RetrofitClient.getGPSClient(GPS_API_URL).create(CargoLinkAPI.class);
    }
}
