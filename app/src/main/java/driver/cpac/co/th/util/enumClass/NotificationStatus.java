package driver.cpac.co.th.util.enumClass;

public enum NotificationStatus {
    unread(1),
    read(2),
    delete(3);

    private final int value;

    private NotificationStatus(int value) {
        this.value = value;
    }

    public short getValue() {
        return (short) value;
    }

    public static NotificationStatus getEnumType(int value) {
        for(NotificationStatus type:NotificationStatus.values()){
            if (type.getValue()==value) return type;
        }
        return null;
    }
}
