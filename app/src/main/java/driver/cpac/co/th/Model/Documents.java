package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.io.Serializable;
public class Documents implements Serializable {
    @SerializedName("waybillId")
    @Expose
    private int waybillId;
    @SerializedName("detailWaybillId")
    @Expose
    private int detailWaybillId;
    @SerializedName("documentUrl")
    @Expose
    private String documentUrl;
    @SerializedName("documentName")
    @Expose
    private String documentName;
    @SerializedName("documentType")
    @Expose
    private String documentType;
    @SerializedName("fileType")
    @Expose
    private String fileType;

    public int getWaybillId() {
        return waybillId;
    }

    public void setWaybillId(int waybillId) {
        this.waybillId = waybillId;
    }

    public int getDetailWaybillID() {
        return detailWaybillId;
    }

    public void setDetailWaybillID(int detailWaybillId) {
        this.detailWaybillId = detailWaybillId;
    }

    public Documents(int waybillId, int detailWaybillId, String documentUrl, String documentName, String documentType, String fileType) {
        this.waybillId = waybillId;
        this.detailWaybillId = detailWaybillId;
        this.documentUrl = documentUrl;
        this.documentName = documentName;
        this.documentType = documentType;
        this.fileType = fileType;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentUrl() {
        return documentUrl;
    }

    public void setDocumentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
