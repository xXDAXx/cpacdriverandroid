package driver.cpac.co.th.util.enumClass;

import android.content.Context;
import android.util.Log;

import com.tmg.th.R;

public enum IssueType {
    trafficJam(0),
    roadAccident(1),
    carCrash(2),
    longLoadingQueue(3),
    raining(4),
    flood(5),
    naturalDisaster(6),
    getLost(7),
    wrongAddress(8),
    contactPerson(9),
    shipperCancelledJob(10),
    changePickUpPoint(11),
    changeDeliveryPoint(12),
    policeDetention(13),
    Others(14);

    private final int value;

    private IssueType(int value) {
        this.value = value;
    }

    public short getValue() {
        return (short) value;
    }

    public static IssueType getEnumType(int value) {
        for (IssueType type : IssueType.values()) {
            if (type.getValue() == value) return type;
        }
        return null;
    }

    public static int GetIssueTypeImg(int issueType, Context context) {
        IssueType type = IssueType.getEnumType(issueType);
        int drawableId = 0;
        switch (type) {

            case trafficJam:
                drawableId = R.drawable.issuetype_trafficjam;
                break;

            case roadAccident:
                drawableId = R.drawable.issuetype_roadaccident;
                break;

            case carCrash:
                drawableId = R.drawable.issuetype_carcrash;
                break;

            case longLoadingQueue:
                drawableId = R.drawable.issuetype_longloadingqueue;
                break;

            case raining:
                drawableId = R.drawable.issuetype_raining;
                break;

            case flood:
                drawableId = R.drawable.issuetype_flood;
                break;

            case naturalDisaster:
                drawableId = R.drawable.issuetype_naturaldisaster;
                break;

            case getLost:
                drawableId = R.drawable.issuetype_getlost;
                break;

            case wrongAddress:
                drawableId = R.drawable.issuetype_wrongaddress;
                break;

            case contactPerson:
                drawableId = R.drawable.issuetype_contactperson;
                break;

            case shipperCancelledJob:
                drawableId = R.drawable.issuetype_shippercancelledjob;
                break;

            case changePickUpPoint:
                drawableId = R.drawable.issuetype_changepickuppoint;
                break;

            case changeDeliveryPoint:
                drawableId = R.drawable.issuetype_changedeliverypoint;
                break;

            case policeDetention:
                drawableId = R.drawable.issuetype_policedetention;
                break;

            case Others:
                drawableId = R.drawable.issuetype_others;
                break;
        }
        return drawableId;
    }

    public static String GetIssueTypeName(int issueType, Context context) {
        IssueType type = IssueType.getEnumType(issueType);
        int stringID = 0;
        String prefix = "issueType.label.";
        String resName = prefix + type.name();
        stringID = context.getResources().getIdentifier(resName, "string", context.getPackageName());
        Log.d("abc",context.getString(stringID));
        return context.getString(stringID);
    }

    public static int GetIssueTypeColor(int issueType, Context context) {
        IssueType type = IssueType.getEnumType(issueType);
        int colorId = 0;
        switch (type) {

            case trafficJam:
                colorId = context.getResources().getColor(R.color.cargolink_yellow);
                break;

            case roadAccident:
                colorId = context.getResources().getColor(R.color.cargolink_dark_blue_bg);
                break;

            case carCrash:
                colorId = context.getResources().getColor(R.color.cargolink_red);
                break;

            case longLoadingQueue:
                colorId = context.getResources().getColor(R.color.cargolink_green);
                break;

            case raining:
                colorId = context.getResources().getColor(R.color.cargolink_blue);
                break;

            case flood:
                colorId = context.getResources().getColor(R.color.job_status_waybill_issued);
                break;

            case naturalDisaster:
                colorId = context.getResources().getColor(R.color.cargolink_violet);

            case getLost:
                colorId = context.getResources().getColor(R.color.cargolink_yellow);
                break;

            case wrongAddress:
                colorId = context.getResources().getColor(R.color.cargolink_dark_blue_bg);
                break;

            case contactPerson:
                colorId = context.getResources().getColor(R.color.cargolink_red);
                break;

            case shipperCancelledJob:
                colorId = context.getResources().getColor(R.color.cargolink_green);
                break;

            case changePickUpPoint:
                colorId = context.getResources().getColor(R.color.cargolink_blue);
                break;

            case changeDeliveryPoint:
                colorId = context.getResources().getColor(R.color.job_status_waybill_issued);
                break;


            case policeDetention:
                colorId = context.getResources().getColor(R.color.cargolink_yellow);
                break;


            case Others:
                colorId = context.getResources().getColor(R.color.cargolink_violet);
        }
        return colorId;
    }
}
