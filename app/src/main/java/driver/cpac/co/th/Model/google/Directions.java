package driver.cpac.co.th.Model.google;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class Directions implements Serializable{
    @SerializedName("routes")
    @Expose
    private List<RoutesItem> routes;

    @SerializedName("geocoded_waypoints")
    @Expose
    private List<GeocodedWaypointsItem> geocodedWaypoints;

    @SerializedName("status")
    @Expose
    private String status;

    public Directions(List<RoutesItem> routes, List<GeocodedWaypointsItem> geocodedWaypoints, String status) {
        this.routes = routes;
        this.geocodedWaypoints = geocodedWaypoints;
        this.status = status;
    }

    public List<RoutesItem> getRoutes() {
        return routes;
    }

    public void setRoutes(List<RoutesItem> routes) {
        this.routes = routes;
    }

    public List<GeocodedWaypointsItem> getGeocodedWaypoints() {
        return geocodedWaypoints;
    }

    public void setGeocodedWaypoints(List<GeocodedWaypointsItem> geocodedWaypoints) {
        this.geocodedWaypoints = geocodedWaypoints;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
