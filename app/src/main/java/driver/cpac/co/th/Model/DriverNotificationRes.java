package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DriverNotificationRes implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("hashId")
    @Expose
    private String hashId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("type")
    @Expose
    private short type;
    @SerializedName("status")
    @Expose
    private short status;
    @SerializedName("waybillId")
    @Expose
    private int waybillId;
    @SerializedName("waybillHashId")
    @Expose
    private String waybillHashId;
    @SerializedName("sentTime")
    @Expose
    private String sentTime;


    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public String getWaybillHashId() {
        return waybillHashId;
    }

    public void setWaybillHashId(String waybillHashId) {
        this.waybillHashId = waybillHashId;
    }

    public String getSentTime() {
        return sentTime;
    }

    public void setSentTime(String sentTime) {
        this.sentTime = sentTime;
    }

    public int getWaybillId() {
        return waybillId;
    }

    public void setWaybillId(int waybillId) {
        this.waybillId = waybillId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public DriverNotificationRes(int id, String title, String summary, String content, short type, short status, int waybillId) {
        this.id = id;
        this.title = title;
        this.summary = summary;
        this.content = content;
        this.type = type;
        this.status = status;
        this.waybillId = waybillId;
    }
}
