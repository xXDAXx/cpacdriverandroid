package driver.cpac.co.th.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.databinding.DataBindingUtil;

import com.tmg.th.R;
import com.tmg.th.databinding.DialogChangeLanguageBinding;

import java.util.Locale;

import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.view.base.OldBaseActivity;

public class DialogChangeLanguage extends Dialog {
    public OldBaseActivity activity;
    public Dialog dialog;
    DialogDissmiss dialogDissmiss;
    private DialogChangeLanguageBinding binding;
    LangUtils langUtils;

    public DialogChangeLanguage(OldBaseActivity a, DialogDissmiss dialogDissmiss) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
        this.dialogDissmiss = dialogDissmiss;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //binding = DataBindingUtil.setContentView(activity, R.layout.dialog_change_language);
        binding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_change_language,(ViewGroup) activity.findViewById(android.R.id.content) , false);
        setContentView( binding.getRoot());
        binding.engLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                langUtils = new LangUtils(activity);
                langUtils.changeLanguage(activity,getContext().getString(R.string.language_english_code));
                activity.updateLocale(new Locale(LangUtils.getCurrentLanguage()));
                dismiss();
                //dialogDissmiss.RefreshContent();
            }
        });
        binding.thaiLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                langUtils = new LangUtils(activity);
                langUtils.changeLanguage(activity,getContext().getString(R.string.language_thailand_code));
                activity.updateLocale(new Locale(LangUtils.getCurrentLanguage()));
                dismiss();
                //dialogDissmiss.RefreshContent();
            }
        });
    }

    public interface DialogDissmiss {
        void RefreshContent();
    }

}
