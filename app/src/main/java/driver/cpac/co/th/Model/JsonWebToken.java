package driver.cpac.co.th.Model;

import com.google.gson.JsonObject;
import driver.cpac.co.th.util.Constant;

public class JsonWebToken {
    private JsonObject jsonWebToken;
    private String StringToken;
    public JsonObject getJsonWebToken() {
        return jsonWebToken;
    }

    public void setJsonWebToken(JsonObject jsonWebToken) {
        this.jsonWebToken = jsonWebToken;
    }

    public String getStringToken()
    {
        if (jsonWebToken==null)
            return null;
        else
            return jsonWebToken.get(Constant.TOKEN).getAsString();
    }

    public void setStringToken(String stringToken) {
        StringToken = stringToken;
    }
}
