package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;

public class ListJobRes implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("status")
    @Expose
    private short status;
    @SerializedName("shipperName")
    @Expose
    private String shipperName;
    @SerializedName("loadingAddress")
    @Expose
    private String loadingAddress;
    @SerializedName("deliveryAddress")
    @Expose
    private String deliveryAddress;
    @SerializedName("timeFromPickupOrg")
    @Expose
    private String timeFromPickupOrg;
    @SerializedName("timeToPickupOrg")
    @Expose
    private String timeToPickupOrg;
    @SerializedName("timeFromDeliveryOrg")
    @Expose
    private String timeFromDeliveryOrg;
    @SerializedName("timeToDeliveryOrg")
    @Expose
    private String timeToDeliveryOrg;
    @SerializedName("truckNumber")
    @Expose
    private String truckNumber;
    @SerializedName("typeOfTruck")
    @Expose
    private int typeOfTruck;
    @SerializedName("typeOfCargo")
    @Expose
    private int typeOfCargo;
    @SerializedName("totalWeight")
    @Expose
    private BigDecimal totalWeight;
    @SerializedName("loadingLat")
    @Expose
    private double loadingLat;
    @SerializedName("loadingLng")
    @Expose
    private double loadingLng;
    @SerializedName("deliveryLat")
    @Expose
    private double deliveryLat;
    @SerializedName("deliveryLng")
    @Expose
    private double deliveryLng;
    @SerializedName("estimateDistance")
    @Expose
    private String estimateDistance;

    public double getLoadingLat() {
        return loadingLat;
    }

    public void setLoadingLat(double loadingLat) {
        this.loadingLat = loadingLat;
    }

    public double getLoadingLng() {
        return loadingLng;
    }

    public void setLoadingLng(double loadingLng) {
        this.loadingLng = loadingLng;
    }

    public double getDeliveryLat() {
        return deliveryLat;
    }

    public void setDeliveryLat(double deliveryLat) {
        this.deliveryLat = deliveryLat;
    }

    public double getDeliveryLng() {
        return deliveryLng;
    }

    public void setDeliveryLng(double deliveryLng) {
        this.deliveryLng = deliveryLng;
    }

    public String getEstimateDistance() {
        return estimateDistance;
    }

    public void setEstimateDistance(String estimateDistance) {
        this.estimateDistance = estimateDistance;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public String getLoadingAddress() {
        return loadingAddress;
    }

    public void setLoadingAddress(String loadingAddress) {
        this.loadingAddress = loadingAddress;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getTimeFromPickupOrg() {
        return timeFromPickupOrg;
    }

    public void setTimeFromPickupOrg(String timeFromPickupOrg) {
        this.timeFromPickupOrg = timeFromPickupOrg;
    }

    public String getTimeToPickupOrg() {
        return timeToPickupOrg;
    }

    public void setTimeToPickupOrg(String timeToPickupOrg) {
        this.timeToPickupOrg = timeToPickupOrg;
    }

    public String getTimeFromDeliveryOrg() {
        return timeFromDeliveryOrg;
    }

    public void setTimeFromDeliveryOrg(String timeFromDeliveryOrg) {
        this.timeFromDeliveryOrg = timeFromDeliveryOrg;
    }

    public String getTimeToDeliveryOrg() {
        return timeToDeliveryOrg;
    }

    public void setTimeToDeliveryOrg(String timeToDeliveryOrg) {
        this.timeToDeliveryOrg = timeToDeliveryOrg;
    }

    public String getTruckNumber() {
        return truckNumber;
    }

    public void setTruckNumber(String truckNumber) {
        this.truckNumber = truckNumber;
    }

    public int getTypeOfTruck() {
        return typeOfTruck;
    }

    public void setTypeOfTruck(int typeOfTruck) {
        this.typeOfTruck = typeOfTruck;
    }

    public int getTypeOfCargo() {
        return typeOfCargo;
    }

    public void setTypeOfCargo(int typeOfCargo) {
        this.typeOfCargo = typeOfCargo;
    }

    public BigDecimal getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(BigDecimal totalWeight) {
        this.totalWeight = totalWeight;
    }
}