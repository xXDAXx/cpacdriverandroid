package driver.cpac.co.th.view.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.tmg.th.R;
import com.tmg.th.databinding.ActivityActiveNewUserBinding;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.ActiveUserReqReq;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;

public class ActiveUserActivity extends OldBaseActivity implements DialogNoticeCallback.CallBackDialogNotice {
    ActivityActiveNewUserBinding binding;
    LoginViewModel viewModel;
    ActiveMode mode;
    DialogNoticeCallback dialogNoticeCallback;

    Handler mHandler;
    Runnable runnable = () -> updateCountDown();
    int countTime = 120;


    public enum ActiveMode {
        ACTIVE_MODE,
        FORGOT_PASSWORD_MODE;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_active_new_user);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //getSupportActionBar().setTitle(R.string.login_activeNewUser_lblActive);
        preferenceHelper = new PreferenceHelper(this);
        //
        viewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        mode = (ActiveMode) getIntent().getSerializableExtra("MODE");
        if (mode == ActiveMode.ACTIVE_MODE) binding.buttonResend.setVisibility(View.GONE);
        else binding.buttonResend.setVisibility(View.VISIBLE);
        //
        mHandler= new Handler();
        updateCountDown();
        OserverSetup();

        binding.buttonContinue.setOnClickListener(view -> continueClick());
        binding.buttonResend.setOnClickListener(view -> resendClick());
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void OserverSetup() {
        viewModel.getActiveSuccess().observe(this, aBoolean -> {
            hideLoading();
            if (aBoolean) {
                App.self().setActiveCode(binding.inputReason.getText().toString());
                if (mode == ActiveMode.ACTIVE_MODE)
                    dialogNoticeCallback = new DialogNoticeCallback(getString(R.string.login_activeNewUser_activeSuccessfully), this, this);
                else
                    dialogNoticeCallback = new DialogNoticeCallback(getString(R.string.login_activeNewUser_setPwdIntruction), this, this);
                dialogNoticeCallback.show();
            }
        });
        viewModel.getVerifiedMobileNumber().observe(this, aBoolean1-> {
            hideLoading();
            updateCountDown();
            if(!aBoolean1) {
                NoticeShow(getString(R.string.commonErrorMsg),this);
            }
        });
        viewModel.getErrorActiveRes().observe(this,activeErrMsg -> {
            hideLoading();
            NoticeShow(activeErrMsg.getMessage(),this);
        });
    }

    public void continueClick() {
        showLoading();
        if (mode == ActiveMode.ACTIVE_MODE) {
            viewModel.activeCall(new ActiveUserReqReq(App.self().getMobileNumber(), binding.inputReason.getText().toString(),0));
        } else if(mode == ActiveMode.FORGOT_PASSWORD_MODE){
            viewModel.activeCall(new ActiveUserReqReq(App.self().getMobileNumber(), binding.inputReason.getText().toString(),1));
        }
    }

    public void resendClick() {
        showLoading();
        viewModel.verifyActiveDriverPhoneNumber(App.self().getMobileNumber());
    }

    @Override
    public void BtnOkDiaComClick() {
        //Move to Login
        Intent mStartActivity = new Intent(this, SetPasswordActivity.class);
        startActivity(mStartActivity);
    }

    public void updateCountDown() {
        if(countTime>0) {
            binding.buttonResend.setText(getText(R.string.login_activeNewUser_btnResend) + "\n("+countTime+"s)");
            countTime--;
            binding.buttonResend.setEnabled(false);
            binding.buttonResend.setBackgroundColor(ContextCompat.getColor(this,(R.color.cargolink_gray)));
            mHandler.postDelayed(runnable, 1000);
        } else {
            mHandler = new Handler();
            countTime = 120;
            binding.buttonResend.setText(getText(R.string.login_activeNewUser_btnResend));
            binding.buttonResend.setEnabled(true);
            binding.buttonResend.setBackgroundColor(ContextCompat.getColor(this,(R.color.cargolink_yellow)));
        }

    }
}
