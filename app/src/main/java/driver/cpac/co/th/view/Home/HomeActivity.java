package driver.cpac.co.th.view.Home;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.notification.AHNotification;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.tmg.th.R;

import java.lang.reflect.Field;

import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.view.AllJobs.AllJobsFragment;
import driver.cpac.co.th.view.MailBox.MailBoxFragment;
import driver.cpac.co.th.view.MyJobs.MyJobsFragment;
import driver.cpac.co.th.view.NewJobs.NewJobsFragment;
import driver.cpac.co.th.view.account.AccountFragment;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dashboard.DashBoardFragment;
import driver.cpac.co.th.view.dialog.DialogFilter;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;
import driver.cpac.co.th.view.sos.SosFragment;

public class HomeActivity extends OldBaseActivity implements DialogNoticeCallback.CallBackDialogNotice {

    AHBottomNavigation navView;
    private ViewPager viewPager;
    private SearchView searchView;
    //Search + Filter
    Toolbar toolbar, searchtoolbar;
    Menu search_menu;
    MenuItem item_search, action_filter, action_search, action_delete;
    AppBarLayout appBarLayout;

    //Fragment
    MyJobsFragment myjobs_frag;
    AccountFragment account_frag;
    //DashBoardFragment dashBoard_frag;
    AllJobsFragment allJobsFrag;
    MyJobsFragment dashBoard_frag;
    NewJobsFragment newjobs_frag;
    MailBoxFragment mailBoxFragment;
    SosFragment sos_frag;
    DialogNoticeCallback dialogNoticeCallback;
    DialogFilter dialogFilter;
    ImageView deleteBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LangUtils.loadLocale(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        LangUtils.loadLocale(this);
        navView = findViewById(R.id.bottom_nav_view);
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(getResources().getString(R.string.title_myJobs), R.drawable.my_jobs_gray);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(getResources().getString(R.string.alert), R.drawable.email_outline);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(getResources().getString(R.string.title_account), R.drawable.account);
        navView.addItem(item1);
        navView.addItem(item2);
        navView.addItem(item3);
        navView.setAccentColor(ContextCompat.getColor(this,R.color.cargolink_yellow));
        navView.setInactiveColor(R.color.cargolink_gray);
        //navView.setColored(true);
        //
        viewPager = (ViewPager) findViewById(R.id.HomeViewPager);
        //mTextMessage = findViewById(R.id.message);
        navView.setOnTabSelectedListener((position, wasSelected) -> {
            viewPager.setCurrentItem(position);
            return true;
        });


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        allJobsFrag.doClearFilter();
                        navView.setCurrentItem(0);
                        break;
                    case 1:
                        allJobsFrag.doClearFilter();
                        navView.setCurrentItem(1);
                        break;
                    case 2:
                        allJobsFrag.doClearFilter();
                        navView.setCurrentItem(2);
                        break;
                    case 3:

                        break;
                    case 4:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        setupViewPager(viewPager);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        //dashBoard_frag = new MyJobsFragment();
        allJobsFrag = new AllJobsFragment();
        //myjobs_frag = new MyJobsFragment();
        mailBoxFragment = new MailBoxFragment();
        account_frag = new AccountFragment();
        //newjobs_frag = new NewJobsFragment();
        //sos_frag = new SosFragment();
        adapter.addFragment(allJobsFrag);
        adapter.addFragment(mailBoxFragment);
        //adapter.addFragment(newjobs_frag);
        //adapter.addFragment(sos_frag);
        adapter.addFragment(account_frag);
        viewPager.setAdapter(adapter);
    }


    public MyJobsFragment getDashBoard_frag() {
        return dashBoard_frag;
    }

    public void setPageViewerIndex(int i) {
        appBarLayout.setVisibility(View.VISIBLE);
        viewPager.setCurrentItem(i, true);
    }

    @Override
    public void BtnOkDiaComClick() {
        if(dialogNoticeCallback != null) dialogNoticeCallback.dismiss();
        if(mailBoxFragment != null)
            mailBoxFragment.deleteAllNotifications();
    }

    public void setBadge(int position, int counts) {
        if (counts > 0) {
            AHNotification notification = new AHNotification.Builder()
                    .setText(String.valueOf(counts))
                    .setBackgroundColor(ContextCompat.getColor(this, R.color.cargolink_red))
                    .setTextColor(ContextCompat.getColor(this, R.color.white))
                    .build();
            navView.setNotification(notification, position);
        }
    }
}