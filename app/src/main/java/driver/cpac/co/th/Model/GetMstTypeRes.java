package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetMstTypeRes implements Serializable {
    @SerializedName("cargoType")
    @Expose
    private List<MstTypeItem> cargoType;

    @SerializedName("truckType")
    @Expose
    private List<MstTypeItem> truckType;

    @SerializedName("unitType")
    @Expose
    private List<MstTypeItem> unitType;


    public List<MstTypeItem> getCargoType() {
        return cargoType;
    }

    public void setCargoType(List<MstTypeItem> cargoType) {
        this.cargoType = cargoType;
    }

    public List<MstTypeItem> getTruckType() {
        return truckType;
    }

    public void setTruckType(List<MstTypeItem> truckType) {
        this.truckType = truckType;
    }

    public List<MstTypeItem> getUnitType() {
        return unitType;
    }

    public void setUnitType(List<MstTypeItem> unitType) {
        this.unitType = unitType;
    }

    public GetMstTypeRes(List<MstTypeItem> cargoType, List<MstTypeItem> truckType, List<MstTypeItem> unitType) {
        this.cargoType = cargoType;
        this.truckType = truckType;
        this.unitType = unitType;
    }
}
