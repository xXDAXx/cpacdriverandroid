package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ActiveUserReqReq implements Serializable
    {
        @SerializedName("phoneNumber") @Expose private String mobileNumer;
        @SerializedName("verificationCode") @Expose private String activeCode;
        @SerializedName("actionType") @Expose private int actionType;

        public int getActionType() {
            return actionType;
        }

        public void setActionType(int actionType) {
            this.actionType = actionType;
        }

        public String getMobileNumer() {
            return mobileNumer;
        }

        public void setMobileNumer(String mobileNumer) {
            this.mobileNumer = mobileNumer;
        }

        public String getActiveCode() {
            return activeCode;
        }

        public void setActiveCode(String activeCode) {
            this.activeCode = activeCode;
        }

        public ActiveUserReqReq(String mobileNumer, String activeCode, int actionType) {
            this.mobileNumer = mobileNumer;
            this.activeCode = activeCode;
            this.actionType = actionType;
        }
    }
