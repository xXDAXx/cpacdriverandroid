package driver.cpac.co.th;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import com.google.gson.Gson;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.tmg.th.BuildConfig;
import com.tmg.th.R;
import com.zeugmasolutions.localehelper.LocaleAwareApplication;

import driver.cpac.co.th.WebApi.RetrofitClient;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.util.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class App extends LocaleAwareApplication {

    private static App sInstance;
    private Gson mGSon;
    public PreferenceHelper preferenceHelper;
    String mobileNumber;
    String activeCode;


    public static App self() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        mGSon = new Gson();
        Realm.init(this);;
        preferenceHelper = new PreferenceHelper(this);
    }


    public Gson getGSon() {
        return mGSon;
    }

    public  Picasso getPicasso() {
        Picasso picasso;
        OkHttpClient client;
        if (BuildConfig.allowClearText)
            client = RetrofitClient.getUnsafeOkHttpClient().newBuilder()
                    .addInterceptor(chain -> {
                        final Request original = chain.request();
                        final Request authorized = original.newBuilder()
                                .addHeader("Cookie", "cookie-name=cookie-value")
                                .addHeader(Constant.HEADER_TOKEN, App.self().preferenceHelper.getString(Constant.TOKEN,""))
                                .build();
                        return chain.proceed(authorized);
                    })
                    .build();
        else
            client = new OkHttpClient.Builder().addInterceptor(chain -> {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Cookie", "cookie-name=cookie-value")
                        .addHeader(Constant.HEADER_TOKEN, App.self().preferenceHelper.getString(Constant.TOKEN, ""))
                        .build();
                return chain.proceed(newRequest);
            })
                    .build();

        picasso = new Picasso.Builder(this)
                .downloader(new OkHttp3Downloader(client))
                .build();
        return picasso;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getActiveCode() {
        return activeCode;
    }

    public void setActiveCode(String activeCode) {
        this.activeCode = activeCode;
    }
}