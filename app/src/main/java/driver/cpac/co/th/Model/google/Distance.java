package driver.cpac.co.th.Model.google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
public class Distance implements Serializable {
    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("value")
    @Expose
    private int value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Distance(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
