package driver.cpac.co.th.util.enumClass;

public enum WaybillStatus {
    draft(0),
    waitingDriver(1),
    driverAccepted(2),
    issued(3),
    arrived(4),
    loaded(5),
    finished(6),
    cancelled(7),
    driverRejected(8),
    shipperRejected(9),
    started(10);
    private final int value;

    private WaybillStatus(int value) {
        this.value = value;
    }

    public short getValue() {
        return (short) value;
    }

    public static WaybillStatus getEnumType(int value) {
        for(WaybillStatus type:WaybillStatus.values()){
            if (type.getValue()==value) return type;
        }
        return null;
    }
}