package driver.cpac.co.th.view.MapView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.github.pavlospt.roundedletterview.RoundedLetterView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.PolyUtil;
import driver.cpac.co.th.Model.DetailWaybill;
import driver.cpac.co.th.Model.DirectionReq;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.google.Directions;
import com.tmg.th.R;
import com.tmg.th.databinding.ActivityMapBinding;
import driver.cpac.co.th.util.MapAnimator;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.base.OldBaseActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mumayank.com.airlocationlibrary.AirLocation;

public class MapViewActivity extends OldBaseActivity implements OnMapReadyCallback {
    private AirLocation airLocation;
    ActivityMapBinding binding;
    MapViewModel viewModel;
    private List<LatLng> gogoRoute;
    private GoogleMap mMap;
    private JobDetailResV2 jobDetail;
    public Directions directions;
    List<LatLng> listPoint;
    LatLng currentPosition;
    WaybillStatus status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_map);
        getSupportActionBar();
        preferenceHelper = new PreferenceHelper(this);
        viewModel = ViewModelProviders.of(this).get(MapViewModel.class);
        directions = null;
        jobDetail = (JobDetailResV2) getIntent().getSerializableExtra("jobDetail");
        status = WaybillStatus.getEnumType(jobDetail.getStatus());
        binding.cardViewBack.setOnClickListener(v -> {
            onBackPressed();
        });
        binding.btnNavigation.setOnClickListener(v -> {
            NavigationClick();
        });
        binding.btnOpenMap.setOnClickListener(v -> {
            GmapClick();
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);
        AirLocationSetup();
        ObserverSetup();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        SetupMapView();
    }

    public void AirLocationSetup() {
        if (status != WaybillStatus.finished) {
            airLocation = new AirLocation(this, true, true, new AirLocation.Callbacks() {
                @Override
                public void onSuccess(@NonNull Location location) {
                    currentPosition = new LatLng(location.getLatitude(), location.getLongitude());
                    SetupMapView();
                }

                @Override
                public void onFailed(@NonNull AirLocation.LocationFailedEnum locationFailedEnum) {
                    // do something
                }
            });
        }
    }

    public void ObserverSetup() {
        viewModel.getDirectionsLiveData().observe(this, directions -> {
            hideLoading();
            if (directions != null) {
                createRoute(directions);
            } else {
                //dialogNotice = new DialogNoticeCallback(getString(R.string.common_err), activity);
                //dialogNotice.show();
            }
        });
    }

    public void SetupMapView() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        setupPointer();
        for (LatLng latLng : listPoint) builder.include(latLng);
        LatLngBounds bounds = builder.build();
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 200);
        mMap.setOnMapLoadedCallback(() -> {
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));
            mMap.moveCamera(cu);
            mMap.animateCamera(cu);
            //addMaker(mMap, , getDrawable(R.drawable.bluemaker), null);
            //addMaker(mMap, pointB, getDrawable(R.drawable.redmaker), null);
            AddListPointMaker();
            CallDirectionAPI();

        });
    }

    public void AddListPointMaker() {
        List<String> listAddr = new ArrayList<>();
        for(DetailWaybill detailWaybill:jobDetail.getListDetailWaybil()) listAddr.add(detailWaybill.getAddressDest());
        for (int i = 0; i < listPoint.size(); i++) {
            addMaker(mMap, listPoint.get(i), getMarkerIconFromBitmap(CreateBitmapMarker(i)), listAddr.get(i));
        }
    }

    public Bitmap CreateBitmapMarker(int position) {
        View v = LayoutInflater.from(this).inflate(R.layout.item_marker_with_number,
                null);
        ImageView imageView = (ImageView) v.findViewById(R.id.markerImg);
        RoundedLetterView textView = (RoundedLetterView) v.findViewById(R.id.textNumber);
        if (position == 0) {
            textView.setVisibility(View.GONE);
            imageView.setColorFilter(ContextCompat.getColor(this, R.color.cargolink_blue), PorterDuff.Mode.SRC_IN);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setTitleText(String.valueOf(position));
        }

        v.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());

        final Bitmap clusterBitmap = Bitmap.createBitmap(v.getMeasuredWidth(),
                v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(clusterBitmap);
        v.draw(canvas);

        return clusterBitmap;
    }

    public void CallDirectionAPI() {
        List<DetailWaybill> list = jobDetail.getListDetailWaybil();
        if (list.size() == 2) {
            viewModel.getDirectionApi(new DirectionReq(list.get(0).getAddressDest(), list.get(1).getAddressDest(), null));
        } else {
            // Direction API with Waypoints
            String wayPoints = "";
            for (int i = 0; i < list.size(); i++) {
                if (i != 0 && i != list.size() - 1)
                    wayPoints += list.get(i).getAddressDest() + "|";
            }
            wayPoints = wayPoints.substring(0, wayPoints.length() - 1);
            viewModel.getDirectionWithWaypointsApi(new DirectionReq(list.get(0).getAddressDest(), list.get(list.size() - 1).getAddressDest(), null), wayPoints);
        }

    }

    public void setupPointer() {
        if (jobDetail != null) {
            listPoint = new ArrayList<>();
            //LatLng loadingPoint = new LatLng(jobDetail.getLoadingLat(), jobDetail.getLoadingLng());
            //listPoint.add(loadingPoint);
            for (DetailWaybill detailWaybill : jobDetail.getListDetailWaybil()) {
                LatLng latLng = new LatLng(detailWaybill.getLatitudeDest(), detailWaybill.getLongitudeDest());
                listPoint.add(latLng);
            }
        }
    }

    public void addMaker(GoogleMap googleMap, LatLng POINT, BitmapDescriptor bm, String tittle) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(POINT).icon(bm);
        if (tittle != null) markerOptions.title(tittle);
        googleMap.addMarker(markerOptions);
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int widthPhone = size.x;
        int height = widthPhone / 9;
        int width = widthPhone / 9;
        BitmapDrawable bitmapdraw = (BitmapDrawable) drawable;
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        Canvas canvas = new Canvas();
        canvas.setBitmap(smallMarker);
        drawable.setBounds(0, 0, smallMarker.getWidth(), smallMarker.getHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(smallMarker);
    }

    private BitmapDescriptor getMarkerIconFromBitmap(Bitmap bitmap) {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int widthPhone = size.x;
        int height = widthPhone / 9;
        int width = widthPhone / 9;
        Bitmap smallMarker = Bitmap.createScaledBitmap(bitmap, width, height, false);
        Canvas canvas = new Canvas();
        canvas.setBitmap(smallMarker);
        return BitmapDescriptorFactory.fromBitmap(smallMarker);
    }

    public String getAddressFromLatLng(LatLng point) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(point.latitude, point.longitude, 1);
        } catch (Exception ex) {
            Log.e("", ex.toString());
            return null;
        }
        String address = addresses.get(0).getAddressLine(0);
        return address;
    }

    private void startAnim() {
        if (mMap != null) {
            MapAnimator.getInstance().animateRoute(mMap, gogoRoute);
        } else {
            Toast.makeText(getApplicationContext(), "Map not ready", Toast.LENGTH_LONG).show();
        }
    }

    private void createRoute(Directions directions) {
        this.directions = directions;
        gogoRoute = new ArrayList<>();
        gogoRoute = PolyUtil.decode(directions.getRoutes().get(0).getOverviewPolyline().getPoints());
        if (gogoRoute.size() == 1) {
            gogoRoute = new ArrayList<>();
            gogoRoute.add(currentPosition);
            gogoRoute.add(listPoint.get(1));
        }
        startAnim();
    }

    public void NavigationClick() {
        if (status != WaybillStatus.finished) {
            String content = "google.navigation:q=";
            switch (status) {
                case arrived:
                    content += jobDetail.getLoadingAddress();
                    break;
                case loaded:
                    content += Util.GetNextDeliveryPoint(jobDetail).getAddressDest();
                    break;
                default:
                    content += jobDetail.getLoadingAddress();
                    break;

            }
            Uri gmmIntentUri = Uri.parse(content);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }
    }

    public void GmapClick() {
        String content = "geo:0,0?q=";
        switch (status) {
            case arrived:
                content += jobDetail.getLoadingAddress();
                break;
            case loaded:
                content += Util.GetNextDeliveryPoint(jobDetail).getAddressDest();
                break;
            default:
                content += jobDetail.getLoadingAddress();
                break;

        }
        Uri gmmIntentUri = Uri.parse(content);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        airLocation.onActivityResult(requestCode, resultCode, data);
    }

    // override and call airLocation object's method by the same name
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        airLocation.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }
}