package driver.cpac.co.th.view.MyJobs;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.DetailWaybill;
import driver.cpac.co.th.Model.JobDetailResV2;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.tmg.th.R;
import com.tmg.th.databinding.ItemShipmentLayoutBinding;

import driver.cpac.co.th.WebApi.MyGlide;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.CargoType;
import driver.cpac.co.th.util.enumClass.TruckType;
import driver.cpac.co.th.util.enumClass.WaybillStatus;

import java.util.ArrayList;
import java.util.List;

public class  ListJobAdapterV2 extends RecyclerView.Adapter<ListJobAdapterV2.MyViewHolder> implements Filterable {

    protected List<JobDetailResV2> jobList;
    protected List<JobDetailResV2> originalList;
    private Context context;
    private JobClickListenerV2 mListener;
    private ItemShipmentLayoutBinding binding;

    public ListJobAdapterV2(Context context, List<JobDetailResV2> jobList, JobClickListenerV2 listener) {
        this.context = context;
        this.jobList = jobList;
        this.originalList = jobList;
        mListener = listener;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemShipmentLayoutBinding itemBinding =
                ItemShipmentLayoutBinding.inflate(layoutInflater, viewGroup, false);
        return new MyViewHolder(itemBinding, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        JobDetailResV2 currentShipment = jobList.get(position);
        MyViewHolder shipmentHolder = (MyViewHolder) viewHolder;
        shipmentHolder.itemView.setOnClickListener(v -> {
            mListener.passContent(currentShipment);
        });
        viewHolder.bind(currentShipment);
    }

    @Override
    public int getItemCount() {
        return jobList == null ? 0 : jobList.size();
    }

    public void updateAdapter(ArrayList<JobDetailResV2> arrayList) {
        this.jobList.clear();
        this.jobList = arrayList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemShipmentLayoutBinding binding;

        public MyViewHolder(ItemShipmentLayoutBinding binding, JobClickListenerV2 listener) {
            super(binding.getRoot());
            mListener = listener;
            this.binding = binding;
        }

        public void bind(JobDetailResV2 item) {
            binding.shipmentId.setText(context.getString(R.string.waybill_label_prefix) + item.getId().toString().toUpperCase() + " - " + item.getShipperName().toUpperCase());
            binding.shipmentSts.setText(Util.GetWaybillStatus(item.getStatus(),context));
            binding.pickupAddr.setText(item.getLoadingAddress());
            binding.dropAddr.setText(item.getListDetailWaybil().get(item.getListDetailWaybil().size() - 1).getAddressDest());
            binding.pickupTime.setText(item.getLoadingTime());
            binding.dropTime.setText(item.getListDetailWaybil().get(item.getListDetailWaybil().size() - 1).getDeliveryDatetime());
            binding.goodsInfo.setText(CargoType.GetCargoType(item.getTypeOfCargo(),context) + " - " + item.getTotalWeight().toString() + context.getString(R.string.common_tons));
            binding.executePendingBindings();
            binding.dashline.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            //Distance info
            if (item.getEstimateDistance() == null || item.getEstimateDistance().isEmpty()) {
                binding.estimateDistance2.setVisibility(View.GONE);
                binding.estimateDistance2.setVisibility(View.GONE);
            } else
                binding.lblEstimate2.setText(context.getText(R.string.common_est) + " " + item.getEstimateDistance());
            //color status
            //binding.shipmentSts.setBackgroundColor(Util.JobStatusColorHelper(item.getStatus(), context));
            binding.truckInfo.setText(TruckType.GetTruckType(item.getTruckType(),context).toUpperCase() + " - " + item.getTruckNumber().toUpperCase());

            //binding.lblEstimate1.setText(context.getText(R.string.common_est)+ " "+ item.getEstimateDistance());
            if(item.getEstimateDistance()!=null)
                binding.lblEstimate2.setText(context.getText(R.string.common_est)+ " "+ item.getEstimateDistance());
            //App.self().getPicasso().get().load(CargoType.GetCargoTypeImg(item.getTypeOfCargo(), context)).placeholder(R.drawable.others).into(binding.cargoIcon);
            //App.self().getPicasso().get().load(TruckType.GetTruckTypeImg(item.getTruckType(), context)).into(binding.truckIcon);

            Glide.with(context).load(CargoType.GetCargoTypeImg(item.getTypeOfCargo(), context)).placeholder(R.drawable.carrgo).into(binding.cargoIcon);
            Glide.with(context).load(TruckType.GetTruckTypeImg(item.getTypeOfCargo(), context)).placeholder(R.drawable.others).into(binding.truckIcon);

            TextView listTview[] ={binding.shipmentId,binding.shipmentSts,binding.pickupAddr,binding.dropAddr,binding.pickupTime,binding.dropTime, binding.goodsInfo, binding.truckInfo};
            if(item.getStatus() == WaybillStatus.waitingDriver.getValue()) {
                binding.cardViewParent.setCardBackgroundColor(ContextCompat.getColor(context, R.color.job_status_new));
                for (TextView tv: listTview) { tv.setTextColor(Color.WHITE);}
                binding.shipmentSts.setBackground(context.getDrawable(R.drawable.white_border_round_corner_tview));
            } else {
                binding.cardViewParent.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white));
                for (TextView tv: listTview) { tv.setTextColor(ContextCompat.getColor(context,R.color.gray3dot));}
                binding.shipmentSts.setTextColor((ContextCompat.getColor(context,R.color.cargolink_yellow)));
                binding.shipmentId.setTextColor((ContextCompat.getColor(context,R.color.gray3dot)));
                binding.shipmentSts.setBackground(context.getDrawable(R.drawable.round_corner_tview));
            }
            binding.shipmentId.setSelected(true);
            binding.goodsInfo.setSelected(true);
            binding.truckInfo.setSelected(true);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                jobList = (List<JobDetailResV2>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<JobDetailResV2> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = originalList;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    public Filter getStatusFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                jobList = (List<JobDetailResV2>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<JobDetailResV2> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = originalList;
                } else {
                    filteredResults = getStatusFilteredResults(Short.valueOf(constraint.toString().toLowerCase()));
                }
                FilterResults results = new FilterResults();
                results.values = filteredResults;
                return results;
            }
        };
    }

    public Filter getAdvancedFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                jobList = (List<JobDetailResV2>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<JobDetailResV2> filteredResults = null;
                String quickSearch = "";
                String dest = "";
                String loadingPoint = "";
                String loadDate = "";
                String deliveryDate = "";
                String[] arr = (constraint.toString().toLowerCase()).split("#");

                quickSearch = arr[0].replace("^","");
                dest = arr[1].replace("^","");
                loadingPoint = arr[2].replace("^","");
                loadDate = arr[3].replace("^","");
                deliveryDate = arr[4].replace("^","");
                if ((quickSearch.length() == 0 && dest.length() == 0 && loadingPoint.length() == 0 && loadDate.length() == 0 && deliveryDate.length() == 0) ||
                        (quickSearch == null && dest == null && loadingPoint == null && loadDate == null && deliveryDate == null)){
                    filteredResults = jobList;
                } else {
                    filteredResults = advancedJobFilterResults(quickSearch,dest,loadingPoint,loadDate,deliveryDate);
                }
                FilterResults results = new FilterResults();
                results.values = filteredResults;
                return results;
            }
        };
    }

    protected List<JobDetailResV2> getFilteredResults(String constraint) {
        List<JobDetailResV2> results = new ArrayList<>();

        for (JobDetailResV2 item : originalList) {
            if (item.getShipperName().toLowerCase().contains(constraint) ||
                    item.getId().toString().toLowerCase().contains(constraint) ||
                    item.getLoadingTime().toLowerCase().contains(constraint) ||
                    item.getTruckNumber().toLowerCase().contains(constraint) ||
                    item.getLoadingAddress().toLowerCase().contains(constraint) ||
                    ListDestinationCheckContain(constraint,item.getListDetailWaybil())) {
                results.add(item);
            }
        }
        return results;
    }

    protected List<JobDetailResV2> advancedJobFilterResults(String quickSearch, String dest, String loadingPoint, String loadDate, String deliveryDate) {
        List<JobDetailResV2> results = new ArrayList<>();

        for (JobDetailResV2 item : originalList) {
            if ((item.getShipperName().toLowerCase().contains(quickSearch) ||
                    item.getId().toString().toLowerCase().contains(quickSearch) ||
                    item.getLoadingTime().toLowerCase().contains(quickSearch) ||
                    item.getTruckNumber().toLowerCase().contains(quickSearch) ||
                    item.getLoadingAddress().toLowerCase().contains(quickSearch) ||
                    ListDestinationCheckContain(quickSearch,item.getListDetailWaybil()))
                    && ListDestinationCheckContain((dest == null ? "" : dest),item.getListDetailWaybil())
                    && item.getLoadingTime().toLowerCase().contains((loadDate == null ? "" : loadDate))
                    && item.getLoadingAddress().toLowerCase().contains((loadingPoint == null? "" : loadingPoint))
                    && ListDestinationCheckContain((deliveryDate == null ? "" : deliveryDate),item.getListDetailWaybil()))
            {
                results.add(item);
            }
        }
        return results;
    }

    protected List<JobDetailResV2> getStatusFilteredResults(int status) {
        List<JobDetailResV2> results = new ArrayList<>();
        for (JobDetailResV2 item : originalList) {
            if (status == 99) {
                if (item.getStatus() != WaybillStatus.waitingDriver.getValue()) {
                    results.add(item);
                }
            }else if (item.getStatus() == status) {
                results.add(item);
            }
        }
        return results;
    }

    public interface JobClickListenerV2 {
        void passContent(JobDetailResV2 listJobRes);
    }

    public boolean ListDestinationCheckContain(String string, List<DetailWaybill> list) {
        boolean result = false;
        for (DetailWaybill detailWabill:list) {
            if (detailWabill.getAddressDest().toLowerCase().contains(string) ||
                    detailWabill.getDeliveryDatetime().contains(string) ||
                    detailWabill.getFullnameDest().contains(string) )
                return true;
        }
        return result;
    }

}

