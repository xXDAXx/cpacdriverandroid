package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DriverIssueDocumentRes implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("hashId")
    @Expose
    private String hashId;

    @SerializedName("documentType")
    @Expose
    private String documentType;

    @SerializedName("documentUrl")
    @Expose
    private String documentUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentUrl() {
        return documentUrl;
    }

    public void setDocumentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
    }

    public static List<Documents> covertToDocuments(List<DriverIssueDocumentRes> res) {
        List<Documents> list = new ArrayList<>();
        if(res.size() >0) {
            for (DriverIssueDocumentRes doc: res) {
                Documents documents = new Documents(
                        0,0,doc.documentUrl,"Issue documents",doc.documentType,"jpg");
                list.add(documents);
            }
        }
        return list;
    }
}
