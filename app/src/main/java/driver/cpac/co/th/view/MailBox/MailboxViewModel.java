package driver.cpac.co.th.view.MailBox;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import driver.cpac.co.th.Model.DriverNotificationRes;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.ListJobRes;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.LangUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MailboxViewModel extends ViewModel {
    public MutableLiveData<List<DriverNotificationRes>> listMutableLiveData;
    private CargoLinkAPI mCargoLinkAPI;
    public MutableLiveData<ListJobRes> jobResMutableLiveData;
    public MutableLiveData<Boolean> showEmpty;
    public MutableLiveData<Boolean> loading;
    public MutableLiveData<JobDetailResV2> jobDetailLiveData;

    public MutableLiveData<JobDetailResV2> getjobDetailLiveData() {
        if (jobDetailLiveData == null) {
            jobDetailLiveData = new MutableLiveData<>();
        }
        return jobDetailLiveData;
    }

    public MutableLiveData<List<DriverNotificationRes>> getListMutableLiveData() {
        if (listMutableLiveData == null) {
            listMutableLiveData = new MutableLiveData<>();
        }
        return listMutableLiveData;

    }

    public MutableLiveData<Boolean> getShowEmpty() {
        if (showEmpty == null) {
            showEmpty = new MutableLiveData<>();
        }
        return showEmpty;

    }

    public MutableLiveData<Boolean> getLoading() {
        if (loading == null) {
            loading = new MutableLiveData<>();
        }
        return loading;

    }

    public MutableLiveData<ListJobRes> getJobResMutableLiveData() {
        if (jobResMutableLiveData == null) {
            jobResMutableLiveData = new MutableLiveData<>();
        }
        return jobResMutableLiveData;
    }

    public void getMyNotify(String Token) {
        loading.setValue(true);
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.getListNotifications(Token, LangUtils.getCurrentLanguage()).enqueue(new Callback<List<DriverNotificationRes>>() {
            @Override
            public void onResponse(Call<List<DriverNotificationRes>> call, Response<List<DriverNotificationRes>> response) {
                loading.setValue(false);
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        listMutableLiveData.setValue(response.body());
                        showEmpty.setValue(false);
                    } else
                        showEmpty.setValue(true);

                } else
                    showEmpty.setValue(true);
            }

            @Override
            public void onFailure(Call<List<DriverNotificationRes>> call, Throwable t) {
                //showEmpty.setValue(true);
                loading.setValue(false);
            }
        });
    }

    public void getDetailJob(String Token,String jobId){
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.getDetailWaybill(Token,LangUtils.getCurrentLanguage(),jobId)
                        .enqueue(new Callback<JobDetailResV2>() {
                            @Override
                            public void onResponse(Call<JobDetailResV2> call, Response<JobDetailResV2> response) {
                                if (response.body()!=null){
                                    jobDetailLiveData.setValue(response.body());
                                }else
                                {
                                }
                            }
                            @Override
                            public void onFailure(Call<JobDetailResV2> call, Throwable t) {
                            }
                        });
    }

    public void setReadNotify(String Token, String notificationId) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.setReadNotifications(Token, LangUtils.getCurrentLanguage(), notificationId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.println(1, "ABC", "OK");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.println(1, "ABC", "Failed");
            }
        });
    }

    public void deleteAllNotifications(String Token) {
        loading.setValue(true);
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.deleteAllNotifications(Token, LangUtils.getCurrentLanguage()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful())
                    showEmpty.setValue(true);
                loading.setValue(false);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.println(1, "ABC", "Failed");
                loading.setValue(false);
            }
        });
    }
}
