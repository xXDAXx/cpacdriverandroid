package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ShipmentInfo implements Serializable {
    public static String SERVER_STATUS_PENDING = "pending";
    public static String SERVER_STATUS_ACCEPTED = "accepted";
    public static String SERVER_STATUS_PICKED_UP = "picked";
    public static String SERVER_STATUS_DELIVERED = "delivered";

    @SerializedName("booking_number")
    @Expose
    private String booking_number;

    @SerializedName("pickup")
    @Expose
    private String pickup;

    @SerializedName("pickup_time")
    @Expose
    private String pickup_time;

    @SerializedName("dropoff_time")
    @Expose
    private String dropoff_time;

    @SerializedName("dropoff")
    @Expose
    private String dropoff;

    @SerializedName("extras")
    @Expose
    private String extras;

    @SerializedName("details_items")
    @Expose
    private String details_items;

    @SerializedName("vehicle_type")
    @Expose
    private String vehicle_type;

    @SerializedName("total_items")
    @Expose
    private String total_items;

    @SerializedName("status")
    @Expose
    private String status;


    public String getPickup() {
        return pickup;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    public String getPickup_time() {
        return pickup_time;
    }

    public void setPickup_time(String pickup_time) {
        this.pickup_time = pickup_time;
    }

    public String getDropoff_time() {
        return dropoff_time;
    }

    public void setDropoff_time(String dropoff_time) {
        this.dropoff_time = dropoff_time;
    }

    public String getDropoff() {
        return dropoff;
    }

    public void setDropoff(String dropoff) {
        this.dropoff = dropoff;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public String getDetails_items() {
        return details_items;
    }

    public void setDetails_items(String details_items) {
        this.details_items = details_items;
    }

    public String getVehicle_type() {
        return vehicle_type;
    }

    public void setVehicle_type(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    public String getTotal_items() {
        return total_items;
    }

    public void setTotal_items(String total_items) {
        this.total_items = total_items;
    }

    public ShipmentInfo(String booking_number, String pickup, String pickup_time, String dropoff, String dropoff_time, String vehicle_type, String status) {
        this.booking_number= booking_number;
        this.pickup= pickup;
        this.pickup_time = pickup_time;
        this.dropoff = dropoff;
        this.dropoff_time = dropoff_time;
        this.vehicle_type = vehicle_type;
        this.status = status;
    }

    public String getBooking_number() {
        return booking_number;
    }

    public void setBooking_number(String booking_number) {
        this.booking_number = booking_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
