package driver.cpac.co.th.view.login.old;

import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import driver.cpac.co.th.Model.BaseResponse;
import driver.cpac.co.th.Model.DriverInfo;
import driver.cpac.co.th.Model.JsonWebToken;
import driver.cpac.co.th.Model.Login;
import driver.cpac.co.th.Model.LoginUser;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.util.PreferenceHelper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import driver.cpac.co.th.util.Util;

public class LoginViewModel extends ViewModel {

    public MutableLiveData<String> EmailAddress = new MutableLiveData<>();
    public MutableLiveData<String> Password = new MutableLiveData<>();
    public MutableLiveData<Boolean> Move2Forgot = new MutableLiveData<>();
    public MutableLiveData<Boolean> Back2Login = new MutableLiveData<>();
    private final ObservableBoolean isChecked = new ObservableBoolean(false);
    private MutableLiveData<Boolean> showDialog = new MutableLiveData<>();
    private MutableLiveData<Boolean> loginFailed = new MutableLiveData<>();
    public MutableLiveData<ResponseBody> responseDownloadAvatar = new MutableLiveData<>();
    private MutableLiveData<LoginUser> userMutableLiveData;
    public MutableLiveData<JsonWebToken> Jwt;
    private CargoLinkAPI mCargoLinkAPI;
    DriverInfo driverInfo;

    public MutableLiveData<BaseResponse> baseResponse;

    public MutableLiveData<BaseResponse> getBaseRes() {

        if (baseResponse == null) {
            baseResponse = new MutableLiveData<>();
        }
        return baseResponse;
    }

    public MutableLiveData<LoginUser> getUser() {

        if (userMutableLiveData == null) {
            userMutableLiveData = new MutableLiveData<>();
        }
        return userMutableLiveData;

    }

    public MutableLiveData<ResponseBody> getResponseDownloadAvatar() {

        if (responseDownloadAvatar == null) {
            responseDownloadAvatar = new MutableLiveData<>();
        }
        return responseDownloadAvatar;

    }

    public MutableLiveData<Boolean> getLoginFailed() {
        if (loginFailed == null) {
            loginFailed = new MutableLiveData<>();
        }
        return loginFailed;
    }

    public MutableLiveData<JsonWebToken> getJwt() {
        if (Jwt == null) {
            Jwt = new MutableLiveData<>();
        }
        return Jwt;
    }

    public ObservableBoolean getIsChecked() {
        return isChecked;
    }

    public MutableLiveData<Boolean> getShowDialog() {
        if (showDialog == null) {
            showDialog = new MutableLiveData<>();
        }
        return showDialog;
    }

    public MutableLiveData<Boolean> getMove2Forgot() {

        if (Move2Forgot == null) {
            Move2Forgot = new MutableLiveData<>();
        }
        return Move2Forgot;

    }

    public MutableLiveData<Boolean> getBack2Login() {

        if (Back2Login == null) {
            Back2Login = new MutableLiveData<>();
        }
        return Back2Login;

    }

    public void onClick(View view) {

        LoginUser loginUser = new LoginUser(EmailAddress.getValue(), Password.getValue());
        userMutableLiveData.setValue(loginUser);
    }

    public void onForgotPwdClick(View view) {
        Move2Forgot.setValue(true);
    }

    public void onShowPwdClick(View view) {
        isChecked.set(((CheckBox) view).isChecked());
    }

    public void onBackClick(View view) {
        Back2Login.setValue(true);
    }


    //API LOGIN
    public void callLogin(LoginUser loginUser) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.logincall(LangUtils.getCurrentLanguage(), new Login(loginUser.getStrEmailAddress(), loginUser.getStrPassword(), loginUser.getDeviceToken(), loginUser.getDeviceType(),2)).enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonWebToken key = new JsonWebToken();
                    key.setJsonWebToken(response.body());
                    driverInfo = new Gson().fromJson(response.body().toString().trim(), DriverInfo.class);
                    if (driverInfo.isHasRoleDriver()) {
                        // Set sharePreferences here
                        String token = response.headers().get(Constant.HEADER_TOKEN).toString();
                        PreferenceHelper preferenceHelper = new PreferenceHelper();
                        preferenceHelper.putString(Constant.TOKEN, Constant.TOKEN_PREFIX + token);
                        preferenceHelper.putString(Constant.USER_NAME, loginUser.getStrEmailAddress());
                        preferenceHelper.putInt(Constant.USER_ID, driverInfo.getId());
                        preferenceHelper.putString(Constant.FULLNAME, driverInfo.getFullName());
                        preferenceHelper.putString(Constant.LICENSE_NUMBER, driverInfo.getLicenseNumberDocument());
                        preferenceHelper.putString(Constant.AVATAR, driverInfo.getAvatar());
                        preferenceHelper.putString(Constant.CARRIER_ID, driverInfo.getCarrierId());
                        preferenceHelper.putString(Constant.EMAIL, driverInfo.getEmail());
                        preferenceHelper.putString(Constant.PHONE_NUMBER, driverInfo.getPhoneNumber());
                        preferenceHelper.putString(Constant.DEVICE_TOKEN, driverInfo.getDeviceToken());
                        preferenceHelper.setList(Constant.TRUCK_TYPE, driverInfo.getTruckType());
                        //Change Language
                        changeLanguage((Constant.TOKEN_PREFIX + token), LangUtils.getCurrentLanguage());
                        //
                        Jwt.setValue(key);
                    } else {
                        loginFailed.setValue(false);
                    }
                } else
                    loginFailed.setValue(false);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                loginFailed.setValue(false);
            }
        });
    }

    //RESET PASSWORD

    public void resetPassword(LoginUser loginUser) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.resetpassword(LangUtils.getCurrentLanguage(), new Login(loginUser.getStrEmailAddress(), loginUser.getStrPassword(), "", 1,2)).enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    BaseResponse cPresponse = new BaseResponse();
                    cPresponse.setBaseResponse(response.body());
                    baseResponse.setValue(cPresponse);
                } else if (response.code() == 401) {
                    BaseResponse cPresponse = new BaseResponse();
                    cPresponse.setErrorResponse(response.errorBody());
                    baseResponse.setValue(cPresponse);
                } else if (response.code() == 400) {
                    BaseResponse cPresponse = new BaseResponse();
                    cPresponse.setErrorResponse(response.errorBody());
                    baseResponse.setValue(cPresponse);
                } else if (response.code() == 500) {
                    BaseResponse cPresponse = new BaseResponse();
                    cPresponse.setErrorResponse(response.errorBody());
                    baseResponse.setValue(cPresponse);
                } else {
                    baseResponse.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                baseResponse.setValue(null);
            }
        });
    }


    //CHANGE LANGUAGE
    public void changeLanguage(String token, String languageId) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.changeLanguage(token, languageId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful())
                    Log.i("abc", "fail");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("abc", t.toString());
            }
        });
    }

    public void downloadAvatar(String url) {
        PreferenceHelper preferenceHelper = new PreferenceHelper();
        mCargoLinkAPI = ApiUtils.getAPIService();
        String fileName = Util.getFileNameByUrl(url);
        mCargoLinkAPI.downloadImg(preferenceHelper.getString(Constant.TOKEN, ""), fileName).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    responseDownloadAvatar.setValue(response.body());
                } else {
                    responseDownloadAvatar.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseDownloadAvatar.setValue(null);
                Log.d("ABC", "error download");
            }
        });
    }
}
