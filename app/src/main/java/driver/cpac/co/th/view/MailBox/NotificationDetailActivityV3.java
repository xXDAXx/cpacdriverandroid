package driver.cpac.co.th.view.MailBox;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.appbar.AppBarLayout;
import driver.cpac.co.th.Model.DriverNotificationRes;
import driver.cpac.co.th.Model.JobDetailResV2;
import com.tmg.th.R;
import com.tmg.th.databinding.ActivityNotificationDetailV3Binding;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.NotificationType;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;
import driver.cpac.co.th.view.jobdetail.JobDetailActivityV3;
import driver.cpac.co.th.view.jobprocessing.JobProcessingActivityV3;

public class NotificationDetailActivityV3 extends OldBaseActivity implements AppBarLayout.OnOffsetChangedListener, DialogNoticeCallback.CallBackDialogNotice {

    private ActivityNotificationDetailV3Binding binding;
    private static final int PERCENTAGE_TO_SHOW_IMAGE = 20;
    private int mMaxScrollSize;
    private boolean mIsImageHidden;
    private MailboxViewModel viewModel;
    private JobDetailResV2 shipmentInfo;
    private DriverNotificationRes driverNotificationRes;
    DialogNoticeCallback dialogNoticeCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification_detail_v3);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding.appbar.addOnOffsetChangedListener(this);
        driverNotificationRes = (DriverNotificationRes) getIntent().getSerializableExtra("notify");
        //getSupportActionBar().setTitle(shipmentInfo.getId().toString() + " - " + shipmentInfo.getShipperName().toUpperCase());
        preferenceHelper = new PreferenceHelper(this);
        BindingNotify();
        binding.btnLayout.setVisibility(View.INVISIBLE);
        viewModel = ViewModelProviders.of(this).get(MailboxViewModel.class);
        viewModel.getDetailJob(preferenceHelper.getString(Constant.TOKEN, ""), driverNotificationRes.getWaybillHashId());
        OserverSetup();
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void OserverSetup() {
        viewModel.getjobDetailLiveData().observe(this, jobDetailResV2 -> {
            shipmentInfo = jobDetailResV2;
            binding.btnLayout.setVisibility(View.VISIBLE);
            WaybillStatus status = WaybillStatus.getEnumType(shipmentInfo.getStatus());
            int userId = preferenceHelper.getInt(Constant.USER_ID,0);
            if(status == WaybillStatus.waitingDriver) {
                binding.btnLayout.setOnClickListener(v-> {MoveToDetailActivity();});
            } else if(Util.jobBelongToUser(shipmentInfo,userId) && (status == WaybillStatus.driverAccepted || status == WaybillStatus.issued
                    || status == WaybillStatus.arrived || status == WaybillStatus.loaded
                    || status == WaybillStatus.finished)) {
                binding.btnLayout.setOnClickListener(v-> {MoveToProcessActivity();});
            } else if( status == WaybillStatus.cancelled || status == WaybillStatus.driverRejected
                    || status == WaybillStatus.shipperRejected) {
                binding.btnLayout.setOnClickListener(v-> {showRejectedNotice(status);});
            }
        });
    }

    public void BindingNotify() {
        NotificationType type = NotificationType.getEnumType(driverNotificationRes.getType());
        switch (type) {
            case jobNotification:
                binding.collapsing.setTitle(driverNotificationRes.getSummary());
                binding.sentTime.setText(Util.GetFriendlyTimeText(Util.convertStringToTimestamp(driverNotificationRes.getSentTime())));
                binding.contentBody.setText(driverNotificationRes.getContent());
                //binding.webview.setVisibility(View.GONE);
                binding.btnAction.setText(getString(R.string.jobProcess_dialog_viewDetailBtn));
                break;
            case systemNotification:


                break;
            case others:


                break;
        }
    }

    public void MoveToDetailActivity() {
        Intent detailActivityIntent = new Intent(this, JobDetailActivityV3.class);
        detailActivityIntent.putExtra("content",shipmentInfo );
        startActivity(detailActivityIntent);
    }

    public void MoveToProcessActivity() {
        Intent detailActivityIntent = new Intent(this, JobProcessingActivityV3.class);
        detailActivityIntent.putExtra("jobDetail",shipmentInfo );
        startActivity(detailActivityIntent);
    }

    public void showRejectedNotice(WaybillStatus status) {
        String content = "This waybill was canceled or rejected and could not be viewed.";
        dialogNoticeCallback = new DialogNoticeCallback(content,this,this);
        dialogNoticeCallback.show();
    }

    @Override
    public void BtnOkDiaComClick() {
        onBackPressed();
    }
    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int currentScrollPercentage = (Math.abs(i)) * 100
                / mMaxScrollSize;

        if (currentScrollPercentage >= PERCENTAGE_TO_SHOW_IMAGE) {
            if (!mIsImageHidden) {
                mIsImageHidden = true;

                ViewCompat.animate(binding.fab).scaleY(0).scaleX(0).start();
            }
        }

        if (currentScrollPercentage < PERCENTAGE_TO_SHOW_IMAGE) {
            if (mIsImageHidden) {
                mIsImageHidden = false;
                ViewCompat.animate(binding.fab).scaleY(1).scaleX(1).start();
            }
        }
    }
}
