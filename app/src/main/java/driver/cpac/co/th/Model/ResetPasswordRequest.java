package driver.cpac.co.th.Model;
import com.google.gson.annotations.Expose;
public class ResetPasswordRequest {
    @Expose
    String loginId;

    public ResetPasswordRequest(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }
}
