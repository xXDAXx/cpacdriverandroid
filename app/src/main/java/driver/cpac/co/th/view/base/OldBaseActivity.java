package driver.cpac.co.th.view.base;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.zeugmasolutions.localehelper.LocaleAwareCompatActivity;

import driver.cpac.co.th.Services.LocationUpdatesService;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.util.NetworkChangeReceiver;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.dialog.DialogNoInternet;
import driver.cpac.co.th.view.dialog.DialogNotice;
import driver.cpac.co.th.view.dialog.DialogWait;
import driver.cpac.co.th.view.dialog.DialogWait2;

import java.util.Locale;

import javax.inject.Inject;

public class OldBaseActivity extends LocaleAwareCompatActivity implements LifecycleOwner, SharedPreferences.OnSharedPreferenceChangeListener {
    @Inject
    protected PreferenceHelper preferenceHelper;
    private DialogWait2 dialogWait;
    private DialogNotice dialogNotice;
    private DialogNoInternet dialogNoInternet;
    protected Util util;
    public MyReceiver myReceiver;

    private static final String TAG = "resPMain";
    public static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    protected LocationUpdatesService mService = null;
    protected boolean mBound = false;
    private View myView;
    public Location myLocation;
    //public BroadcastReceiver myReceiver;

    /*@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
//        preferenceHelper = new PreferenceHelper(this);
        Context context = LangUtils.wrap(newBase, new Locale(LangUtils.getCurrentLanguage()));
        super.attachBaseContext(context);
        *//*newBase = LocaleChanger.configureBaseContext(newBase);
        super.attachBaseContext(newBase)*//*;
    }

    @Override
    public void applyOverrideConfiguration(Configuration config) {
        super.applyOverrideConfiguration(config);
    }*/

    public void showLoading() {
        if (dialogWait == null) {
            dialogWait = new DialogWait2(this);
        }
        dialogWait.show();
    }

    public void hideLoading() {
        if (dialogWait != null)
            dialogWait.dismiss();
    }

    public void showNotice(String content) {
        dialogNotice = new DialogNotice(content, this);
        dialogNotice.show();
    }

    public void setMyView(View view) {
        myView = view;
    }

    public boolean checkGPSPermissions() {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    public void requestGPSPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            /*Snackbar.make(
                    myView,
                    "Location permission need!",
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", view -> {
                        // Request permission
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_PERMISSIONS_REQUEST_CODE);
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".*/
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        if (dialogWait != null) {
            dialogWait.dismiss();
        }
        if (dialogNotice != null) {
            dialogNotice.dismiss();
        }
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (Util.requestingLocationUpdates(this)) {
                    mService.requestLocationUpdates();
                }

            } else {
                // Permission denied.
                /*Snackbar.make(
                        myView,
                        "GPS permission denied",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("SETTINGS", view -> {
                            // Build intent that displays the App settings screen.
                            Intent intent = new Intent();
                            intent.setAction(
                                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package",
                                    BuildConfig.APPLICATION_ID, null);
                            intent.setData(uri);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        })
                        .show();*/
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(Util.KEY_REQUESTING_LOCATION_UPDATES)) {
            //EnableGPS(false);
        }
    }

    protected final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            //if (jobDetailRes.getStatus() == WaybillStatus.arrived.getValue() || jobDetailRes.getStatus() == WaybillStatus.loaded.getValue())
            if (checkGPSPermissions()) {
                if (preferenceHelper.getString(Constant.TOKEN, "") != "")
                    EnableGPS(true);
            }
            //EnableGPS(true);
            else
                requestGPSPermissions();
            registerReceiver();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    public void registerReceiver() {
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(myReceiver, new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));
    }

    public void EnableGPS(boolean aBoolean) {
        if (aBoolean) {
            //mBound = false;
            if (!checkGPSPermissions()) {
                requestGPSPermissions();
            } else mService.requestLocationUpdates();
        } else if (!aBoolean) {
            if(mService != null)
                mService.removeLocationUpdates();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        preferenceHelper = new PreferenceHelper(this);
        myReceiver = new MyReceiver();
        bindService(new Intent(this, LocationUpdatesService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
        registerInternetCheckReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(myReceiver,
                new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));
    }

    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    public class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null)
                return;
            Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
            if (location != null) {
                myLocation = location;
                /*Toast.makeText(getApplicationContext(), Util.getLocationText(location),
                        Toast.LENGTH_SHORT).show();*/
            }
        }
    }
    ;

    public void checkGPS(WaybillStatus status) {
        if (status == WaybillStatus.arrived || status == WaybillStatus.loaded) {
            EnableGPS(true);
        } else if (status == WaybillStatus.finished)
            EnableGPS(false);
    }

    private void registerInternetCheckReceiver() {
        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(NetworkChangeReceiver.NETWORK_CHANGE_ACTION);
            registerReceiver(internalNetworkChangeReceiver, intentFilter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        try {
            // Make sure to unregister internal receiver in onDestroy().
            unregisterReceiver(internalNetworkChangeReceiver);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        super.onDestroy();
    }

    /**
     * This is internal BroadcastReceiver which get status from external receiver(NetworkChangeReceiver)
     */
    InternalNetworkChangeReceiver internalNetworkChangeReceiver = new InternalNetworkChangeReceiver();

    class InternalNetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String internetStatus = intent.getStringExtra("status");
            if (!internetStatus.equals("Internet Connected")) {
                dialogNoInternet = new DialogNoInternet(OldBaseActivity.this);
                dialogNoInternet.show();
            } else {

            }
        }
    }

    public void NoticeShow(String msg, Activity context) {
        DialogNotice dialogNotice = new DialogNotice(msg, context);
        dialogNotice.show();
    }

}

