package driver.cpac.co.th.Model;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import driver.cpac.co.th.Model.RealmObject.TrackingInfo;

import java.io.Serializable;
import java.util.List;

public class OfflineGpsInfo implements Serializable {
    @SerializedName("data")
    @Expose
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public OfflineGpsInfo(String data) {
        this.data = data;
    }

    public OfflineGpsInfo(List<TrackingInfo> trackingInfoList) {
        if (trackingInfoList.size() > 0) {
            String data = "";
            for (TrackingInfo trackingInfo : trackingInfoList) {
                Gson gson = new Gson();
                String json = gson.toJson(trackingInfo);
                data += json + System.lineSeparator();
            }
            int i = data.length();
            i = data.lastIndexOf('\n', i - 1);
            this.data = data.substring(0,i);;
        }
    }
}
