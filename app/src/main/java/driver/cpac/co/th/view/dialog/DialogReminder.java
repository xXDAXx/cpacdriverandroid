package driver.cpac.co.th.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;

import androidx.databinding.DataBindingUtil;

import driver.cpac.co.th.Model.DetailWaybill;
import com.tmg.th.R;
import com.tmg.th.databinding.DialogReminderLayoutBinding;
import driver.cpac.co.th.util.Util;

public class DialogReminder extends Dialog {

    public Activity activity;
    public Dialog dialog;
    public DetailWaybill detailWaybill;
    private DialogReminderLayoutBinding binding;
    CallBackDialogCom callBackDialogCom;

    public DialogReminder(DetailWaybill detailWaybill, Activity a, CallBackDialogCom callBackDialogCom) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
        this.detailWaybill = detailWaybill;
        this.callBackDialogCom = callBackDialogCom;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_reminder_layout, (ViewGroup) activity.findViewById(android.R.id.content), false);
        setContentView(binding.getRoot());
        binding.andressTxt.setText(detailWaybill.getAddressDest());
        binding.timeTxt.setText(Util.GetFriendlyTimeText(Util.convertStringToTimestamp(detailWaybill.getDeliveryDatetime())));
    }

    public interface CallBackDialogCom {
        void BtnDetailReminderClick();
    }
}
