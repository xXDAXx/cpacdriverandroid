package driver.cpac.co.th.util;

import android.view.View;

public interface RecyclerViewClickListener {
    void onClick(View view, int position);
}
