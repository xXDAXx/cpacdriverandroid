package driver.cpac.co.th.view.MailBox;

import driver.cpac.co.th.Model.DriverNotificationRes;

public interface NotifyClickListener {
    void passContent(DriverNotificationRes driverNotificationRes);
}
