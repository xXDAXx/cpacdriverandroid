package driver.cpac.co.th.view.history;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import driver.cpac.co.th.Model.JobDetailResV2;
import com.tmg.th.R;
import com.tmg.th.databinding.ActivityHistoryBinding;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.view.MyJobs.ListJobAdapterV2;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogFilter;
import driver.cpac.co.th.view.dialog.DialogNotice;
import driver.cpac.co.th.view.jobprocessing.JobProcessingActivityV3;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends OldBaseActivity {
    private ActivityHistoryBinding binding;
    private SearchView searchView;
    private HistoryViewModel historyViewModel;
    private List<JobDetailResV2> shipmentInfoList = new ArrayList<>();
    JobDetailResV2 listJobRes;
    JobDetailResV2 jobDetailRes;
    private DialogNotice dialogNotice;
    private ListJobAdapterV2 adapter;
    //Search + Filter
    Menu search_menu;
    MenuItem item_search, action_filter, action_search;
    DialogFilter dialogFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_history);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.title_history));
        setSearchtoolbar();
        historyViewModel = ViewModelProviders.of(this).get(HistoryViewModel.class);
        binding.setLifecycleOwner(this);
        showLoading();
        preferenceHelper = new PreferenceHelper(this);
        binding.swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                historyViewModel.getHistoryList(preferenceHelper.getString(Constant.TOKEN,""));
            }

        });
        historyViewModel.getHistoryList(preferenceHelper.getString(Constant.TOKEN,""));
        historyViewModel.getlistJobRes().observe(this,alistJobRes->{
            binding.swipeContainer.setRefreshing(false);
            hideLoading();
            if (alistJobRes!=null&&alistJobRes.size() > 0) {
                shipmentInfoList = alistJobRes;
                hideEmpty();
                bindingData();
            }
        });
        historyViewModel.getShowEmpty().observe(this, aBoolean -> {
            if (!aBoolean) hideEmpty();
            else showEmpty();
        });
        historyViewModel.getShowLoading().observe(this, aBoolean -> {
            binding.swipeContainer.setRefreshing(false);
            if (!aBoolean) hideLoading();
        });

        historyViewModel.getjobDetailLiveData().observe(this,jobDetailRes -> {
            hideLoading();
            this.jobDetailRes = jobDetailRes;
            Intent intent = new Intent(this, HistoryDetailActivity.class);
            intent.putExtra("jobDetail",jobDetailRes );
            startActivity(intent);
        });
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_history, menu);
        action_search = (MenuItem) menu.findItem(R.id.action_search);
        action_filter = (MenuItem) menu.findItem(R.id.action_filter);
        action_filter.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle status filter & action filter or search
        switch (item.getItemId()) {
            case R.id.action_search:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    circleReveal(binding.searchtoolbar.getId(), 1, true, true);
                else
                    binding.searchtoolbar.setVisibility(View.VISIBLE);

                item_search.expandActionView();
                return true;
            case R.id.action_filter:
                //dialogFilter = new DialogFilter("MY_JOB", this, this);
                //dialogFilter.show();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void setSearchtoolbar() {
            binding.searchtoolbar.inflateMenu(R.menu.menu_search);
            search_menu = binding.searchtoolbar.getMenu();

            binding.searchtoolbar.setNavigationOnClickListener((v) -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    circleReveal(R.id.searchtoolbar, 1, true, false);
                else
                    binding.searchtoolbar.setVisibility(View.GONE);
            });
            item_search = search_menu.findItem(R.id.action_filter_search);
            item_search.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        circleReveal(R.id.searchtoolbar, 1, true, false);
                    } else
                        binding.searchtoolbar.setVisibility(View.GONE);
                    return true;
                }
            });
            initSearchView();
    }
    public void initSearchView() {
        searchView =
                (SearchView) search_menu.findItem(R.id.action_filter_search).getActionView();
        // Enable/Disable Submit button in the keyboard

        searchView.setSubmitButtonEnabled(false);

        // set hint and the text colors
        EditText txtSearch = ((EditText) searchView.findViewById(androidx.appcompat.R.id.search_src_text));
        txtSearch.setHint(getText(R.string.common_label_searchhere));
        txtSearch.setHintTextColor(getResources().getColor(R.color.cargolink_yellow));
        txtSearch.setTextColor(getResources().getColor(R.color.cargolink_yellow));

        // Change search close button image
        ImageView closeButton = (ImageView) searchView.findViewById(androidx.appcompat.R.id.search_close_btn);
        closeButton.setImageResource(R.drawable.yellow_close);
        closeButton.setOnClickListener(v -> {doClearFilter(); txtSearch.setText("");});

        // set the cursor
        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.yellow_search_cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
            e.printStackTrace();
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                callSearch(query);

                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                callSearch(newText);
                return true;
            }
        });

    }
    public void callSearch(String query) {

        doFilterShipmentInfo(query);

    }

    public void doFilterShipmentInfo(String newText){
        if(shipmentInfoList.size()>0)
            adapter.getFilter().filter(newText);
    }

    public void doClearFilter(){
        adapter = new ListJobAdapterV2( this,shipmentInfoList, listener);
        binding.rcv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void circleReveal(int viewID, int posFromRight, boolean containsOverflow, final boolean isShow) {
        final View myView = findViewById(viewID);

        int width = myView.getWidth();

        if (posFromRight > 0)
            width -= (posFromRight * getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material)) - (getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) / 2);
        if (containsOverflow)
            width -= getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material);

        int cx = width;
        int cy = myView.getHeight() / 2;

        Animator anim;
        if (isShow)
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, (float) width);
        else
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, (float) width, 0);

        anim.setDuration((long) 360);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isShow) {
                    super.onAnimationEnd(animation);
                    myView.setVisibility(View.INVISIBLE);
                    doClearFilter();
                }
            }
        });

        // make the view visible and start the animation
        if (isShow)
            myView.setVisibility(View.VISIBLE);

        // start the animation
        anim.start();


    }
    public void showEmpty(){
        binding.CoffeeView.setVisibility(View.VISIBLE);
        binding.rcv.setVisibility(View.GONE);
    }
    public void hideEmpty(){
        binding.CoffeeView.setVisibility(View.GONE);
        binding.rcv.setVisibility(View.VISIBLE);
    }
    public void bindingData(){
        adapter = new ListJobAdapterV2( this,shipmentInfoList, listener);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.rcv.setLayoutManager(layoutManager);
        binding.rcv.setAdapter(adapter);
    }
    ListJobAdapterV2.JobClickListenerV2 listener = shipmentInfo -> {
        showLoading();
        historyViewModel.getDetailJob(preferenceHelper.getString(Constant.TOKEN,""),shipmentInfo.getHashId());
        this.listJobRes = shipmentInfo;
    };
}
