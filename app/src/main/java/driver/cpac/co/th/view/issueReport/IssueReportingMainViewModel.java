package driver.cpac.co.th.view.issueReport;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import driver.cpac.co.th.Model.DriverListIssueRes;
import driver.cpac.co.th.Model.DriverNewIssueReq;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssueReportingMainViewModel  extends ViewModel {

    private CargoLinkAPI cargoLinkAPI;
    public MutableLiveData<Boolean> showLoading;
    public MutableLiveData<List<DriverListIssueRes>>  listIssueLiveData;

    public MutableLiveData<Boolean> getLoading() {
        if (showLoading == null) {
            showLoading = new MutableLiveData<>();
        }
        return showLoading;
    }

    public MutableLiveData<List<DriverListIssueRes>> getListIssueLiveData() {
        if (listIssueLiveData == null) {
            listIssueLiveData = new MutableLiveData<>();
        }
        return listIssueLiveData;
    }


    public void getIssueList(String Token) {
        showLoading.setValue(true);
        cargoLinkAPI = ApiUtils.getAPIService();
        cargoLinkAPI.getIssueList(Token).enqueue(new Callback<List<DriverListIssueRes>>() {
            @Override
            public void onResponse(Call<List<DriverListIssueRes>> call, Response<List<DriverListIssueRes>> response) {
                if (response.isSuccessful()) {
                    showLoading.setValue(false);
                    if (response.body() != null) {
                        listIssueLiveData.setValue(response.body());
                    }
                } else {
                    showLoading.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<List<DriverListIssueRes>> call, Throwable t) {

            }
        });
    }
}
