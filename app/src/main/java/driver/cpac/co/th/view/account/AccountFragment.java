package driver.cpac.co.th.view.account;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.ItemSetting;
import com.tmg.th.R;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.view.Home.HomeActivity;
import driver.cpac.co.th.view.MailBox.MailBoxActivity;
import driver.cpac.co.th.view.account.changePassword.ChangePasswordActivity;
import driver.cpac.co.th.view.account.profile.Profile_activity;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.base.SplashActivity;
import driver.cpac.co.th.view.dialog.DialogChangeLanguage;
import driver.cpac.co.th.view.dialog.DialogConfirm;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.RecyclerViewClickListener;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.view.dialog.DialogNotice;
import driver.cpac.co.th.view.dialog.DialogWait;
import driver.cpac.co.th.view.dialog.DialogWait2;
import driver.cpac.co.th.view.history.HistoryActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import driver.cpac.co.th.view.issueReport.ReportingMainActivity;


public class AccountFragment extends Fragment implements DialogConfirm.CallBackDialogCom, DialogChangeLanguage.DialogDissmiss {
    private List<ItemSetting> functions = new ArrayList<>();
    private AccountViewModel accountViewModel;
    private Util util;
    DialogNotice dialogNotice;
    DialogWait2 dialogWait;
    DialogChangeLanguage dialogChangeLanguage;
    DialogConfirm dialogConfirm;
    RecyclerViewClickListener listener;
    RecyclerView funcs_listview;
    ListFunctionAdapter mRcvAdapter;
    CircleImageView avaImg;
    PreferenceHelper preferenceHelper;
    TextView textName;
    Button btn_logout;
    /*@Inject
    AccountViewModel accountViewModel;
*/
    /*@Override
    public int getLayoutId() {
        return R.layout.fragment_account;
    }
*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //accountViewModel = ViewModelProviders.of(this,viewModelFactory).get(AccountViewModel.class);
        util = Util.getInstance();
        setUserVisibleHint(false);
        accountViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);
        preferenceHelper = new PreferenceHelper(getContext());

        dialogWait = new DialogWait2(getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account, null);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        functions = initData();
        funcs_listview = (RecyclerView) view.findViewById(R.id.rcv);
        View profileView = (View) view.findViewById(R.id.viewbg);
        avaImg = (CircleImageView) view.findViewById(R.id.img_avatar);
        btn_logout = (Button) view.findViewById(R.id.btn_logout);
        textName = (TextView) view.findViewById(R.id.textname);
        loadImageFromStorage();
        dialogConfirm = new DialogConfirm(getString(R.string.logout_title), getString(R.string.logout_confirm), getActivity(), this);
        /*btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogConfirm.show();
            }
        });*/
        btn_logout.setVisibility(View.GONE);
        profileView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent;
                myIntent = new Intent(getContext(), Profile_activity.class);
                startActivity(myIntent);
            }
        });
        textName.setText(preferenceHelper.getString(Constant.FULLNAME, ""));
        mRcvAdapter = new ListFunctionAdapter(functions, getContext(), listener);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        funcs_listview.setLayoutManager(layoutManager);
        funcs_listview.setAdapter(mRcvAdapter);

        super.onViewCreated(view, savedInstanceState);
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listener = (view, position) -> {
            switch (position) {
                //Change Password
                case 0: {
                    Intent myIntent;
                    myIntent = new Intent(getContext(), ChangePasswordActivity.class);
                    startActivity(myIntent);
                    break;
                }
                //Change Language
                case 1: {
                    dialogChangeLanguage = new DialogChangeLanguage((OldBaseActivity)getActivity(), this);
                    dialogChangeLanguage.show();
                    break;
                }
                //History
                case 2: {
                    Intent myIntent;
                    myIntent = new Intent(getContext(), HistoryActivity.class);
                    startActivity(myIntent);
                    break;
                }
                //Mailbox
                case 3: {
                    //dialogChangeLanguage = new DialogChangeLanguage(getActivity(), this);
                    //dialogChangeLanguage.show();
                    Intent intent = new Intent(getContext(), MailBoxActivity.class);
                    startActivity(intent);
                    break;
                }
                //Report
                case 4: {
                    Intent myIntent;
                    myIntent = new Intent(getContext(), ReportingMainActivity.class);
                    startActivity(myIntent);
                    break;
                }
                //Logout
                case 5: {
                    dialogConfirm = new DialogConfirm(getString(R.string.logout_title), getString(R.string.logout_confirm), getActivity(), this);
                    dialogConfirm.show();
                    break;
                }
            }
        };
        mRcvAdapter = new ListFunctionAdapter(initData(), getActivity(), listener);
        funcs_listview.setAdapter(mRcvAdapter);
    }

    //Confirm Logout callback
    @Override
    public void BtnOkClick() {
        PreferenceHelper preferenceHelper = new PreferenceHelper();
        accountViewModel.logout(preferenceHelper.getString(Constant.TOKEN, ""));
        preferenceHelper.removeKey(Constant.TOKEN);
        preferenceHelper.removeKey(Constant.AVATAR);
        deleteOldAvatar();
        Util.DismissAllNotificaion(getContext()); // Clear all notification
        Intent mStartActivity = new Intent(getActivity(), SplashActivity.class);
        mStartActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mStartActivity);
    }


    @Override
    public void RefreshContent() {
        accountViewModel.changeLanguage(preferenceHelper.getString(Constant.TOKEN, ""), LangUtils.getCurrentLanguage());
        Intent intent = new Intent(getContext(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    //Back from avatar activity - reload avatar img
    @Override
    public void onResume() {
        super.onResume();
        loadImageFromStorage();
    }

    private List<ItemSetting> initData() {
        List<ItemSetting> funcstions = new ArrayList<>();
        ItemSetting item1 = new ItemSetting(getString(R.string.change_password), R.drawable.password_icon);
        ItemSetting item2 = new ItemSetting(getString(R.string.change_language_title), R.drawable.globe_filled_50);
        ItemSetting item3 = new ItemSetting(getString(R.string.title_history), R.drawable.baseline_history_black_18dp);
        ItemSetting item4 = new ItemSetting(getString(R.string.title_mailbox), R.drawable.email_outline);
        ItemSetting item5 = new ItemSetting(Util.convertToTitleCaseIteratingChars(getString(R.string.logout_btn)), R.drawable.logout);
        ItemSetting item6 = new ItemSetting(getString(R.string.menu_list_issueReport), R.drawable.alarm);

        /*
        String item5 = new String("test");
        String item6 = new String("test");
        String item7 = new String("test");
        String item8 = new String("test");
        String item9 = new String("test");
        String item10 = new String("test");
        String item11 = new String("test");
        String item12 = new String("test");
        String item13 = new String("test");
        String item14 = new String("test");*/


        funcstions.add(item1);
        funcstions.add(item2);
        funcstions.add(item3);
        funcstions.add(item4);
        funcstions.add(item6);
        funcstions.add(item5);
        /*funcstions.add(item3);
        funcstions.add(item4);
        funcstions.add(item5);
        funcstions.add(item6);
        funcstions.add(item7);
        funcstions.add(item8);
        funcstions.add(item9);
        funcstions.add(item10);
        funcstions.add(item11);
        funcstions.add(item12);
        funcstions.add(item13);*/
        return funcstions;
    }

    private void loadImageFromStorage() {

        try {
            ContextWrapper cw = new ContextWrapper(getContext());
            File directory = cw.getDir("avatars", Context.MODE_PRIVATE);
            File f = new File(directory, "avatar0.jpg");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            avaImg.setImageBitmap(b);
        } catch (FileNotFoundException e) {
            // e.printStackTrace();
            if (preferenceHelper.getString(Constant.AVATAR, "").length() > 1) {
                App.self().getPicasso().load(preferenceHelper.getString(Constant.AVATAR, ""))
                        .placeholder(R.drawable.ic_avatar_grey).into(avaImg);
            } else
                avaImg.setImageBitmap(BitmapFactory
                        .decodeResource(getResources(), R.drawable.ic_avatar_grey));
        }
    }

    public void deleteOldAvatar() {
        ContextWrapper cw = new ContextWrapper(getActivity().getApplicationContext());
        File directory = cw.getDir("avatars", Context.MODE_PRIVATE);
        File f = new File(directory, "avatar0.jpg");
        if (f.exists()) {
            f.delete();
        }
    }
}
