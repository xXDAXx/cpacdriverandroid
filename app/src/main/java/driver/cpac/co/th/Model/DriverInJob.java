package driver.cpac.co.th.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DriverInJob implements Parcelable, Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("driverName")
    @Expose
    private String driverName;
    @SerializedName("avaUrl")
    @Expose
    private String avaUrl;
    @SerializedName("driverContact")
    @Expose
    private String driverContact;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getAvaUrl() {
        return avaUrl;
    }

    public void setAvaUrl(String avaUrl) {
        this.avaUrl = avaUrl;
    }

    public String getDriverContact() {
        return driverContact;
    }

    public void setDriverContact(String driverContact) {
        this.driverContact = driverContact;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.driverName);
        dest.writeString(this.avaUrl);
        dest.writeString(this.driverContact);
    }

    public DriverInJob() {
    }

    protected DriverInJob(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.driverName = in.readString();
        this.avaUrl = in.readString();
        this.driverContact = in.readString();
    }

    public static final Parcelable.Creator<DriverInJob> CREATOR = new Parcelable.Creator<DriverInJob>() {
        @Override
        public DriverInJob createFromParcel(Parcel source) {
            return new DriverInJob(source);
        }

        @Override
        public DriverInJob[] newArray(int size) {
            return new DriverInJob[size];
        }
    };
}
