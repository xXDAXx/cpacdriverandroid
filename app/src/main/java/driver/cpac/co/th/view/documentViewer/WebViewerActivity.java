package driver.cpac.co.th.view.documentViewer;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.databinding.DataBindingUtil;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.Documents;
import com.tmg.th.R;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.view.base.OldBaseActivity;
import com.tmg.th.databinding.ActivityWebviewerBinding;

import java.util.HashMap;


public class WebViewerActivity extends OldBaseActivity {
    private ActivityWebviewerBinding binding;
    private Documents documents;
    PreferenceHelper preferenceHelper;
    HashMap<String, String> extraHeaders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_webviewer);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        PreferenceHelper preferenceHelper;
        extraHeaders = new HashMap<String, String>();
        preferenceHelper = new PreferenceHelper();
        extraHeaders.put(Constant.HEADER_TOKEN, preferenceHelper.getString(Constant.TOKEN,""));

        documents = (Documents) getIntent().getSerializableExtra("content");
        if (documents.getDocumentName() != null)
            getSupportActionBar().setTitle(documents.getDocumentName());
        preferenceHelper = new PreferenceHelper(this);
        //
        /*binding.webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                //Make the bar disappear after URL is loaded, and changes string to Loading...
                setTitle("Loading...");
                setProgress(progress * 100); //Make the bar disappear after URL is loaded

                // Return the app name after finish loading
                if (progress == 100)
                    setTitle(R.string.app_name);
            }
        });*/
        binding.webview.setWebViewClient(new HelloWebViewClient());
        //binding.webview.addView(binding.webview.get);
        binding.webview.getSettings().setJavaScriptEnabled(true);
        binding.webview.getSettings().setAllowFileAccess(true);
        if (documents.getDocumentUrl() != null)
            if (!documents.getFileType().equals("jpg") && !documents.getFileType().equals("png") && !documents.getFileType().equals("jpeg")&&!documents.getFileType().equals("image/jpg")) {
                binding.imageView.setVisibility(View.GONE);
                String doc = "https://docs.google.com/gview?embedded=true&url=" + Constant.API_BASE_URL + Constant.MEDIA_PATH + documents.getDocumentUrl();
                binding.webview.loadUrl(doc,extraHeaders);
                //binding.webview.loadData(doc, "text/html", "UTF-8");
            } else {
                binding.webview.setVisibility(View.GONE);
                binding.imageView.setVisibility(View.VISIBLE);
                App.self().getPicasso().load(documents.getDocumentUrl()).placeholder(R.drawable.jpg_icon).into(binding.imageView);
            }
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url,extraHeaders);
            return true;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && binding.webview.canGoBack()) {
            binding.webview.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}