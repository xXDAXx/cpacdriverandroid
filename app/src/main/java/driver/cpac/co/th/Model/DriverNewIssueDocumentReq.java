/**
 * Copyright (c) 2018, gmenu Inc.<br>
 * All rights reserved.
 */
package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

public class DriverNewIssueDocumentReq implements Serializable {

  @SerializedName("documentUrl")
  @Expose
  private String documentUrl;

  @SerializedName("documentName")
  @Expose
  private String documentName;

  @SerializedName("documentType")
  @Expose
  private String documentType;

  public String getDocumentUrl() {
    return documentUrl;
  }

  public void setDocumentUrl(String documentUrl) {
    this.documentUrl = documentUrl;
  }

  public String getDocumentName() {
    return documentName;
  }

  public void setDocumentName(String documentName) {
    this.documentName = documentName;
  }

  public String getDocumentType() {
    return documentType;
  }

  public void setDocumentType(String documentType) {
    this.documentType = documentType;
  }

  public DriverNewIssueDocumentReq(String documentUrl, String documentName, String documentType) {
    this.documentUrl = documentUrl;
    this.documentName = documentName;
    this.documentType = documentType;
  }

  public static DriverNewIssueDocumentReq convertDtbIssueToReq(Documents documents) {
    return new DriverNewIssueDocumentReq(
            documents.getDocumentUrl(),
            documents.getDocumentName(),
            documents.getDocumentType());
  }

}
