package driver.cpac.co.th.view.history;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.tmg.th.R;
import com.tmg.th.databinding.ActivityHistoryDetailBinding;

import java.util.List;

import driver.cpac.co.th.Model.Documents;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.CargoType;
import driver.cpac.co.th.util.enumClass.TruckType;
import driver.cpac.co.th.util.enumClass.WaybillStatus;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.documentViewer.WebViewerActivity;
import driver.cpac.co.th.view.jobprocessing.Documents.ListDocumentsAdapterV2;

public class HistoryDetailActivity extends OldBaseActivity {
    private ActivityHistoryDetailBinding binding;
    private HistoryViewModel historyViewModel;
    JobDetailResV2 jobDetailRes;
    private HistoryListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_history_detail);
        setSupportActionBar(binding.appBarLayout.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding.setLifecycleOwner(this);
        preferenceHelper = new PreferenceHelper(this);
        jobDetailRes = (JobDetailResV2) getIntent().getSerializableExtra("jobDetail");
        if (jobDetailRes != null) {
            getSupportActionBar().setTitle(getString(R.string.waybill_label_prefix) + jobDetailRes.getId().toString());
            binding.txtTimeRemaining.setText(getText(R.string.job_deliveredAt) + ": " + jobDetailRes.getDoneTime());
            onDisplay_Detail_Shipment();
        }
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void onDisplay_Detail_Shipment() {
        if (jobDetailRes != null) {
            //binding.dropList.cargoIcon.setImageResource(CargoType.GetCargoTypeImg(jobDetail.getTypeOfCargo(), this));
            Picasso.get().load(CargoType.GetCargoTypeImg(jobDetailRes.getTypeOfCargo(), this)).placeholder(R.drawable.others).into(binding.cargoIcon);
            //SHIPPER INFO

            /// GOODS INFO
            String goodInfo = jobDetailRes.getTotalWeight().toString() + " " + getString(R.string.common_tons)  // 20 tons
                    + " " + CargoType.GetCargoType(jobDetailRes.getTypeOfCargo(), this);  // FOOODS
            if (jobDetailRes.getQuantity() > 0) {
                goodInfo += " - " + jobDetailRes.getQuantity().toString() + " " + Util.GetUnitType(jobDetailRes.getUnit(), this).toLowerCase()
                        + " ";
                if (!(jobDetailRes.getSizeOfCargo().equals("0 x 0 x 0m") || jobDetailRes.getSizeOfCargo().equals("- x - x -m")
                        || jobDetailRes.getSizeOfCargo().equals("0.0 x 0.0 x 0.0m") || jobDetailRes.getSizeOfCargo().equals("0.00 x 0.00 x 0.00m")))
                    goodInfo += jobDetailRes.getSizeOfCargo();
            }
            binding.goodsInfo.setText(goodInfo);
            //binding.txtNoteToDriver.setText(jobDetailRes.getHandlingInstruction());

            //Driver Info
            int userId = preferenceHelper.getInt(Constant.USER_ID, 0);

            //DOcument list
            setupRcvDocument(jobDetailRes);

            //Binding list Destination
            ((SimpleItemAnimator) binding.rcvDocuments.getItemAnimator()).setSupportsChangeAnimations(true);
            adapter = new HistoryListAdapter(this, jobDetailRes, true, true);
            binding.rcvStatusHisstory.setLayoutManager(new LinearLayoutManager(this));
            binding.rcvStatusHisstory.setAdapter(adapter);

            //Truck Info
            binding.truckInfo.setText(TruckType.GetTruckType(jobDetailRes.getTruckType(), this).toUpperCase() + " - " + jobDetailRes.getTruckNumber().toUpperCase());

            binding.shipmentId.setText(this.getString(R.string.waybill_label_prefix) + jobDetailRes.getId().toString().toUpperCase() + " - " + jobDetailRes.getShipperName().toUpperCase());
            binding.pickupAddr.setText(jobDetailRes.getLoadingAddress());
            binding.dropAddr.setText(jobDetailRes.getListDetailWaybil().get(jobDetailRes.getListDetailWaybil().size() - 1).getAddressDest());
            binding.pickupTime.setText(jobDetailRes.getLoadingTime());
            binding.dropTime.setText(jobDetailRes.getListDetailWaybil().get(jobDetailRes.getListDetailWaybil().size() - 1).getDeliveryDatetime());
            binding.goodsInfo.setText(CargoType.GetCargoType(jobDetailRes.getTypeOfCargo(), this) + " - " + jobDetailRes.getTotalWeight().toString() + this.getString(R.string.common_tons));
            binding.executePendingBindings();
            binding.dashline.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            //Distance info
            if (jobDetailRes.getEstimateDistance() == null || jobDetailRes.getEstimateDistance().isEmpty()) {
                binding.estimateDistance2.setVisibility(View.GONE);
                binding.estimateDistance2.setVisibility(View.GONE);
            } else
                binding.lblEstimate2.setText(this.getText(R.string.common_est) + " " + jobDetailRes.getEstimateDistance());
            //color status
            //binding.shipmentSts.setBackgroundColor(Util.JobStatusColorHelper(item.getStatus(), context));
            binding.truckInfo.setText(TruckType.GetTruckType(jobDetailRes.getTruckType(), this).toUpperCase() + " - " + jobDetailRes.getTruckNumber().toUpperCase());

            //binding.lblEstimate1.setText(context.getText(R.string.common_est)+ " "+ item.getEstimateDistance());
            if (jobDetailRes.getEstimateDistance() != null)
                binding.lblEstimate2.setText(this.getText(R.string.common_est) + " " + jobDetailRes.getEstimateDistance());

            Glide.with(this).load(CargoType.GetCargoTypeImg(jobDetailRes.getTypeOfCargo(), this)).placeholder(R.drawable.carrgo).into(binding.cargoIcon);
            Glide.with(this).load(TruckType.GetTruckTypeImg(jobDetailRes.getTypeOfCargo(), this)).placeholder(R.drawable.others).into(binding.truckIcon);

            TextView listTview[] = {binding.shipmentId, binding.pickupAddr, binding.dropAddr, binding.pickupTime, binding.dropTime, binding.goodsInfo, binding.truckInfo};
            binding.cardViewParent.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
            for (TextView tv : listTview) {
                tv.setTextColor(ContextCompat.getColor(this, R.color.gray3dot));
            }
        }
        binding.shipmentId.setSelected(true);
        binding.goodsInfo.setSelected(true);
        binding.truckInfo.setSelected(true);

    }


    public void setupRcvDocument(JobDetailResV2 jobDetailRes) {
        List<Documents> documentsList = jobDetailRes.getDocuments();
        ListDocumentsAdapterV2 mAdapter = new ListDocumentsAdapterV2(this, documentsList, viewClickListener, null, WaybillStatus.getEnumType(jobDetailRes.getStatus()));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        binding.rcvDocuments.setLayoutManager(layoutManager);
        binding.rcvDocuments.setAdapter(mAdapter);
    }

    ListDocumentsAdapterV2.DocumentViewClickListener viewClickListener = documents -> {
        Intent intent = new Intent(this, WebViewerActivity.class);
        intent.putExtra("content", documents);
        startActivity(intent);
    };
}
