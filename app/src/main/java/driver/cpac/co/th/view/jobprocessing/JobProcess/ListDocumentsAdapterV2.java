package driver.cpac.co.th.view.jobprocessing.JobProcess;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import driver.cpac.co.th.Model.Documents;
import com.tmg.th.R;
import com.tmg.th.databinding.ItemDocumentLayoutBinding;
import com.tmg.th.databinding.ItemDocumentLayoutV2Binding;
import driver.cpac.co.th.view.jobprocessing.Documents.DocumentClickListener;

import java.util.ArrayList;
import java.util.List;

public class ListDocumentsAdapterV2 extends RecyclerView.Adapter<ListDocumentsAdapterV2.MyViewHolder> {
    protected List<Documents> documentsList;
    private Context context;
    private DocumentClickListener mListener;

    public ListDocumentsAdapterV2(Context context, List<Documents> documentsList, DocumentClickListener listener) {
        this.context = context;
        this.documentsList = documentsList;
        mListener = listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemDocumentLayoutV2Binding binding;

        public MyViewHolder(ItemDocumentLayoutV2Binding binding, DocumentClickListener listener) {
            super(binding.getRoot());
            mListener = listener;
            this.binding = binding;
        }

        public void bind(Documents item) {
            binding.documentName.setText(item.getDocumentName()+"."+item.getFileType());
            if (item.getFileType().toLowerCase().equals("pdf")) binding.fileImg.setImageResource(R.drawable.pdf_icon);
            else if (item.getFileType().toLowerCase().equals("jpg")) binding.fileImg.setImageResource(R.drawable.jpg_icon);
            else if (item.getFileType().toLowerCase().equals("png")) binding.fileImg.setImageResource(R.drawable.png_icon);
            else if (item.getFileType().toLowerCase().equals("docx")) binding.fileImg.setImageResource(R.drawable.docx_icon);
            else binding.fileImg.setImageResource(R.drawable.file);
                if (item.getDocumentType()!= null && item.getDocumentType().toLowerCase().equals("pod"))
                   binding.documentType.setText("Proof of delivery");
                else if (item.getDocumentType()!= null && item.getDocumentType().toLowerCase().equals("pol"))
                    binding.documentType.setText("Proof of loading");
                else binding.documentType.setText("Others");
            binding.documentName.setSelected(true);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemDocumentLayoutV2Binding itemBinding =
                ItemDocumentLayoutV2Binding.inflate(layoutInflater, viewGroup, false);
        return new MyViewHolder(itemBinding, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        Documents documents = documentsList.get(position);
        MyViewHolder shipmentHolder = viewHolder;
        shipmentHolder.itemView.setOnClickListener(v -> {
            mListener.passContent(documents);
        });
        viewHolder.bind(documents);
    }

    @Override
    public int getItemCount() {
        return documentsList == null ? 0 : documentsList.size();
    }

    public void updateAdapter(ArrayList<Documents> arrayList) {
        this.documentsList.clear();
        this.documentsList = arrayList;
        notifyDataSetChanged();
    }

}
