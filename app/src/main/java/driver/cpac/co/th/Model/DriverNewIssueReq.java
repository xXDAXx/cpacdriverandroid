/**
 * Copyright (c) 2018, gmenu Inc.<br>
 * All rights reserved.
 */
package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DriverNewIssueReq implements Serializable {

  @SerializedName("waybillId")
  @Expose
  private int waybillId;

  @SerializedName("content")
  @Expose
  private String content;

  @SerializedName("type")
  @Expose
  private short type;

  @SerializedName("status")
  @Expose
  private short status;

  @SerializedName("document")
  @Expose
  private List<DriverNewIssueDocumentReq> document;


  public int getWaybillId() {
    return waybillId;
  }

  public void setWaybillId(int waybillId) {
    this.waybillId = waybillId;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public short getType() {
    return type;
  }

  public void setType(short type) {
    this.type = type;
  }

  public short getStatus() {
    return status;
  }

  public void setStatus(short status) {
    this.status = status;
  }

  public List<DriverNewIssueDocumentReq> getDocument() {
    return document;
  }

  public void setDocument(List<DriverNewIssueDocumentReq> document) {
    this.document = document;
  }

  public DriverNewIssueReq(int waybillId, String content, short type, short status, List<DriverNewIssueDocumentReq> document) {
    this.waybillId = waybillId;
    this.content = content;
    this.type = type;
    this.status = status;
    this.document = document;
  }
}
