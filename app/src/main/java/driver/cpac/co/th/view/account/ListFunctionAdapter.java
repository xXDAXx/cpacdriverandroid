package driver.cpac.co.th.view.account;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import driver.cpac.co.th.Model.ItemSetting;
import com.tmg.th.R;
import driver.cpac.co.th.util.RecyclerViewClickListener;

import java.util.ArrayList;
import java.util.List;

public class ListFunctionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ItemSetting> funcs = new ArrayList<>();
    private RecyclerViewClickListener tListener;
    private Context mContext;
    private LayoutInflater layoutInflater;

    public ListFunctionAdapter(List<ItemSetting> funcs, Context mContext, RecyclerViewClickListener listener) {
        this.funcs = funcs;
        this.mContext = mContext;
        tListener = listener;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemfuncs = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_text_layout, viewGroup, false);
        return new FuncsViewHolder(itemfuncs,tListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        FuncsViewHolder funcsViewHolder = (FuncsViewHolder) holder;
        String itemfuncs = funcs.get(position).getTitle();
        funcsViewHolder.textView.setText(itemfuncs);
        int imageId = funcs.get(position).getType();
        funcsViewHolder.imageView.setImageResource(imageId);
    }

    @Override
    public int getItemCount() {
        return funcs.size();
    }

    public static class FuncsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RecyclerViewClickListener mListener;
        TextView textView;
        ImageView imageView;

        public FuncsViewHolder(View view, RecyclerViewClickListener listener) {
            super(view);
            mListener = listener;
            textView = (TextView) view.findViewById(R.id.action_Name);
            imageView = (ImageView) view.findViewById(R.id.action_image);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(view, getAdapterPosition());
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class ItemFunctions{

    }

}
