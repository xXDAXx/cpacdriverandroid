package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class DriverInfo {
    @Expose
    private
    int id;
    @Expose
    private
    String email;
    @Expose
    private
    String fullName;
    @Expose
    private
    boolean enable;
    @Expose
    private
    String equipment;
    @Expose
    private
    String licenseNumberDocument;
    @Expose
    private
    String phoneNumber;
    @Expose
    private
    String carrierId;
    @Expose
    private
    String avatar;
    @Expose
    private
    String platform;
    @Expose
    private
    String deviceToken;
    @Expose
    private
    List<Integer> truckType;
    @Expose
    private
    boolean deleted;
    @Expose
    private
    boolean hasRoleDriver;
    @Expose
    private
    int drivingLicenseType;

    public int getDrivingLicenseType() {
        return drivingLicenseType;
    }

    public void setDrivingLicenseType(int drivingLicenseType) {
        this.drivingLicenseType = drivingLicenseType;
    }

    public DriverInfo(int id, String email, String fullName, boolean enable, String equipment, String licenseNumberDocument, String phoneNumber, String carrierId, String avatar, String platform, String deviceToken, boolean deleted, boolean hasRoleDriver, List<Integer> truckType){
        this.setId(id);
        this.avatar = avatar;
        this.carrierId = carrierId;
        this.deleted = deleted;
        this.deviceToken = deviceToken;
        this.email = email;
        this.enable = enable;
        this.fullName = fullName;
        this.equipment = equipment;
        this.hasRoleDriver = hasRoleDriver;
        this.licenseNumberDocument = licenseNumberDocument;
        this.phoneNumber = phoneNumber;
        this.platform = platform;
        this.setTruckType(truckType);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getLicenseNumberDocument() {
        return licenseNumberDocument;
    }

    public void setLicenseNumberDocument(String licenseNumberDocument) {
        this.licenseNumberDocument = licenseNumberDocument;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isHasRoleDriver() {
        return hasRoleDriver;
    }

    public void setHasRoleDriver(boolean hasRoleDriver) {
        this.hasRoleDriver = hasRoleDriver;
    }

    public List<Integer> getTruckType() {
        return truckType;
    }

    public void setTruckType(List<Integer> truckTypes) {
        this.truckType = truckTypes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
