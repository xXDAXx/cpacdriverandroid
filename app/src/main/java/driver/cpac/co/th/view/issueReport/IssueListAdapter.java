package driver.cpac.co.th.view.issueReport;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tmg.th.databinding.ItemIssueLayoutBinding;

import java.util.ArrayList;
import java.util.List;

import driver.cpac.co.th.Model.DriverListIssueRes;
import driver.cpac.co.th.util.enumClass.IssueType;


public class IssueListAdapter extends RecyclerView.Adapter<IssueListAdapter.MyViewHolder> implements Filterable {

    protected List<DriverListIssueRes> issueList;
    protected List<DriverListIssueRes> originalList;
    private Context context;
    private IssueListAdapter.IssueClickListener mListener;
    private ItemIssueLayoutBinding binding;

    public IssueListAdapter(Context context, List<DriverListIssueRes> issueList, IssueListAdapter.IssueClickListener listener) {
        this.context = context;
        this.issueList = issueList;
        this.originalList = issueList;
        mListener = listener;
    }

    @NonNull
    @Override
    public IssueListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemIssueLayoutBinding itemBinding =
                ItemIssueLayoutBinding.inflate(layoutInflater, viewGroup, false);
        return new IssueListAdapter.MyViewHolder(itemBinding, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull IssueListAdapter.MyViewHolder viewHolder, int position) {
        DriverListIssueRes currentItem = issueList.get(position);
        IssueListAdapter.MyViewHolder shipmentHolder = (IssueListAdapter.MyViewHolder) viewHolder;
        shipmentHolder.itemView.setOnClickListener(v -> {
            mListener.passContent(currentItem);
        });
        viewHolder.bind(currentItem);
    }

    @Override
    public int getItemCount() {
        return issueList == null ? 0 : issueList.size();
    }

    public void updateAdapter(ArrayList<DriverListIssueRes> arrayList) {
        this.issueList.clear();
        this.issueList = arrayList;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemIssueLayoutBinding binding;

        public MyViewHolder(ItemIssueLayoutBinding binding, IssueListAdapter.IssueClickListener listener) {
            super(binding.getRoot());
            mListener = listener;
            this.binding = binding;
        }

        public void bind(DriverListIssueRes item) {
            binding.tvIssueName.setText(IssueType.GetIssueTypeName(item.getType(), context));
            binding.tvIssueName.setTextColor(IssueType.GetIssueTypeColor(item.getType(), context));
            binding.iconIssue.setImageResource(IssueType.GetIssueTypeImg(item.getType(), context));
            binding.iconIssue.setColorFilter(IssueType.GetIssueTypeColor(item.getType(), context));
            binding.tvDate.setText(item.getUpdatedTime());
            binding.tvIssuer.setText("By " + item.getUpdatedName());
            //App.self().getPicasso().load(Util.getTruckTypeImageId(item.getTruckType(),context)).placeholder(R.drawable.others).into(binding.truckIcon);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                issueList = (List<DriverListIssueRes>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<DriverListIssueRes> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = issueList;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
    }

    protected List<DriverListIssueRes> getFilteredResults(String constraint) {
        List<DriverListIssueRes> results = new ArrayList<>();

        for (DriverListIssueRes item : originalList) {
            if (
                    item.getContent().toLowerCase().contains(constraint) ||
                            item.getUpdatedName().toLowerCase().contains(constraint) ||
                            Integer.toString(item.getId()).contains(constraint) ||
                            Integer.toString(item.getStatus()).contains(constraint) ||
                            Integer.toString(item.getWaybillId()).contains(constraint) ||
                            String.valueOf(item.getType()).contains(constraint) ||
                            IssueType.GetIssueTypeName(item.getType(), context).contains(constraint)
            )
                results.add(item);
        }
        return results;
    }

    protected List<DriverListIssueRes> getStatusFilteredResults(int status) {
        List<DriverListIssueRes> results = new ArrayList<>();
        for (DriverListIssueRes item : originalList) {
            if (item.getStatus() == status) {
                results.add(item);
            }
        }
        return results;
    }

    public interface IssueClickListener {
        void passContent(DriverListIssueRes item);
    }

    public boolean ListDestinationCheckContain(String string, List<DriverListIssueRes> list) {
        boolean result = false;
        for (DriverListIssueRes item : list) {
            if (item.getContent().toLowerCase().contains(string) ||
                    item.getUpdatedName().contains(string) ||
                    String.valueOf(item.getWaybillId()).contains(string))
                return true;
        }
        return result;
    }

}

