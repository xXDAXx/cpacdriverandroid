package driver.cpac.co.th.customComponent;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import androidx.constraintlayout.widget.ConstraintLayout;


import com.tmg.th.R;
import com.tmg.th.databinding.ItemJobStepperBinding;
import driver.cpac.co.th.util.enumClass.WaybillStatus;

public class JobStepperBar extends ConstraintLayout {
    ItemJobStepperBinding binding;
    String txtStart;

    public String getTxtStart() {
        return txtStart;
    }

    public void setTxtStart(String txtStart) {
        this.txtStart = txtStart;
    }

    public String getTxtLoad() {
        return txtLoad;
    }

    public void setTxtLoad(String txtLoad) {
        this.txtLoad = txtLoad;
    }

    public String getTxtDelivery() {
        return txtDelivery;
    }

    public void setTxtDelivery(String txtDelivery) {
        this.txtDelivery = txtDelivery;
    }

    public int getColorStart() {
        return colorStart;
    }

    public void setColorStart(int colorStart) {
        this.colorStart = colorStart;
    }

    public int getColorDark() {
        return colorDark;
    }

    public void setColorDark(int colorDark) {
        this.colorDark = colorDark;
    }

    public int getColorDarker() {
        return colorDarker;
    }

    public void setColorDarker(int colorDarker) {
        this.colorDarker = colorDarker;
    }

    public int getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(int jobStatus) {
        this.jobStatus = jobStatus;
    }

    Context context;
    String txtLoad;
    String txtDelivery;
    int colorStart;
    int colorDark;
    int colorDarker;
    int jobStatus = 3;
    int colorTextHighlight;
    int colorTextNotHighlight;

    public JobStepperBar(Context context) {
        super(context);
        this.context = context;
    }

    public JobStepperBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
        setupComponents(attrs);
    }


    private void init() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(infService);
        binding = ItemJobStepperBinding.inflate(layoutInflater, this, true);
    }

    private void setupComponents(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.JobStepperBar);
        txtStart = a.getString(R.styleable.JobStepperBar_text_start);
        txtLoad = a.getString(R.styleable.JobStepperBar_text_load);
        txtDelivery = a.getString(R.styleable.JobStepperBar_text_delivery);
        colorStart = a.getInt(R.styleable.JobStepperBar_color_start, 0);
        colorDark = a.getInt(R.styleable.JobStepperBar_color_dark, 0);
        colorDarker = a.getInt(R.styleable.JobStepperBar_color_darker, 0);
        colorTextHighlight = a.getInt(R.styleable.JobStepperBar_text_highlight_color, 0);
        colorTextNotHighlight = a.getInt(R.styleable.JobStepperBar_text_not_highlight, 0);
        jobStatus = a.getInt(R.styleable.JobStepperBar_jobStatus, 0);
        SetupWithStatus(jobStatus);
    }

    public void SetupWithStatus(int status){
        this.jobStatus = status;
        binding.txtLoad.setText(txtLoad);
        binding.txtDelivery.setText(txtDelivery);
        binding.txtStart.setText(txtStart);
        if (status == WaybillStatus.driverAccepted.getValue() || jobStatus == WaybillStatus.issued.getValue()|| jobStatus == WaybillStatus.started.getValue()) {
            binding.txtStart.setBackgroundColor(colorStart);
            binding.endOfStart.setColorFilter(colorStart);
            binding.txtLoad.setBackgroundColor(colorDark);
            binding.endOfLoad.setColorFilter(colorDark);
            binding.txtDelivery.setBackgroundColor(colorDarker);
            binding.txtStart.setTextColor(colorTextHighlight);
            binding.txtLoad.setTextColor(colorTextNotHighlight);
            binding.txtDelivery.setTextColor(colorTextNotHighlight);
        } else if (status == WaybillStatus.arrived.getValue()) {
            binding.txtStart.setBackgroundColor(colorDark);
            binding.endOfStart.setColorFilter(colorDark);
            binding.txtLoad.setBackgroundColor(colorStart);
            binding.endOfLoad.setColorFilter(colorStart);
            binding.txtDelivery.setBackgroundColor(colorDark);
            binding.txtStart.setTextColor(colorTextNotHighlight);
            binding.txtLoad.setTextColor(colorTextHighlight);
            binding.txtDelivery.setTextColor(colorTextNotHighlight);
        } else if (status == WaybillStatus.loaded.getValue()) {
            binding.txtStart.setBackgroundColor(colorDarker);
            binding.endOfStart.setColorFilter(colorDarker);
            binding.txtLoad.setBackgroundColor(colorDark);
            binding.endOfLoad.setColorFilter(colorDark);
            binding.txtDelivery.setBackgroundColor(colorStart);
            binding.txtStart.setTextColor(colorTextNotHighlight);
            binding.txtLoad.setTextColor(colorTextNotHighlight);
            binding.txtDelivery.setTextColor(colorTextHighlight);
        }
    }
}
