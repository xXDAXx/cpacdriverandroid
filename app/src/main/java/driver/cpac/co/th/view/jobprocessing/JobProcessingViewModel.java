package driver.cpac.co.th.view.jobprocessing;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import driver.cpac.co.th.Model.DirectionReq;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.JobStatusUpdateReq;
import driver.cpac.co.th.Model.google.Directions;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.util.enumClass.WaybillStatus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobProcessingViewModel extends ViewModel {
    private CargoLinkAPI cargoLinkAPI;
    public MutableLiveData<Directions> directionsLiveData;
    public MutableLiveData<Boolean> isWaitFinished;
    public MutableLiveData<Boolean> loading;
    public MutableLiveData<JobDetailResV2> jobDetailLiveData;
    public MutableLiveData<JobDetailResV2> jobDetailRefreshData;

    public MutableLiveData<Directions> getDirectionsLiveData() {

        if (directionsLiveData == null) {
            directionsLiveData = new MutableLiveData<>();
        }
        return directionsLiveData;
    }

    public MutableLiveData<JobDetailResV2> getjobDetailLiveData() {
        if (jobDetailLiveData == null) {
            jobDetailLiveData = new MutableLiveData<>();
        }
        return jobDetailLiveData;
    }

    public MutableLiveData<Boolean> getLoading() {
        if (loading == null) {
            loading = new MutableLiveData<>();
        }
        return loading;
    }

    public MutableLiveData<JobDetailResV2> getJobDetailRefreshData() {
        if (jobDetailRefreshData == null) {
            jobDetailRefreshData = new MutableLiveData<>();
        }
        return jobDetailRefreshData;
    }

    public void getDirectionApi(DirectionReq directionReq) {
        cargoLinkAPI = ApiUtils.getGGMAPI();
        cargoLinkAPI.getDirections(directionReq.getOrigin(), directionReq.getDestination(), directionReq.getKey())
                .enqueue(new Callback<Directions>() {
                    @Override
                    public void onResponse(Call<Directions> call, Response<Directions> response) {
                        if (response.isSuccessful()) {
                            Directions directions = response.body();
                            if (directions.getStatus().equals("OK")) {
                                directionsLiveData.setValue(directions);
                            } else {
                                directionsLiveData.setValue(null);
                            }

                        } else {
                            directionsLiveData.setValue(null);
                        }
                    }

                    @Override
                    public void onFailure(Call<Directions> call, Throwable t) {
                        directionsLiveData.setValue(null);
                    }
                });
    }

    public void getDetailJob(String Token, String jobId) {
        cargoLinkAPI = ApiUtils.getAPIService();
        cargoLinkAPI.getDetailWaybill(Token, LangUtils.getCurrentLanguage(), jobId)
                .enqueue(new Callback<JobDetailResV2>() {
                    @Override
                    public void onResponse(Call<JobDetailResV2> call, Response<JobDetailResV2> response) {
                        if (response.body() != null) {
                            jobDetailLiveData.setValue(response.body());
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<JobDetailResV2> call, Throwable t) {
                        loading.setValue(false);
                    }
                });
    }

    public void refreshDetailJob(String Token, String jobId) {
        cargoLinkAPI = ApiUtils.getAPIService();
        cargoLinkAPI.getDetailWaybill(Token, LangUtils.getCurrentLanguage(), jobId)
                .enqueue(new Callback<JobDetailResV2>() {
                    @Override
                    public void onResponse(Call<JobDetailResV2> call, Response<JobDetailResV2> response) {
                        loading.setValue(false);
                        if (response.body() != null) {
                            jobDetailRefreshData.setValue(response.body());
                            //isWaitFinished.setValue(true);
                        } else {
                            loading.setValue(false);
                        }
                    }

                    @Override
                    public void onFailure(Call<JobDetailResV2> call, Throwable t) {
                        loading.setValue(false);
                    }
                });
    }

    public void startJob(String Token, String jobId) {
        cargoLinkAPI = ApiUtils.getAPIService();
        try {
            cargoLinkAPI.updateStatusWaybill(Token, LangUtils.getCurrentLanguage(), new JobStatusUpdateReq(jobId, WaybillStatus.started.getValue(), null))
                    .enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            loading.setValue(false);
                            if (response.isSuccessful()) {
                                refreshDetailJob(Token, jobId);
                            } else {
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            loading.setValue(false);
                        }
                    });
        } catch (Exception ex) {
            Log.println(0, "", ex.toString());
        }

    }

    public void arriveJob(String Token, String jobId) {
        cargoLinkAPI = ApiUtils.getAPIService();
        try {
            cargoLinkAPI.updateStatusWaybill(Token, LangUtils.getCurrentLanguage(), new JobStatusUpdateReq(jobId, WaybillStatus.arrived.getValue(), null))
                    .enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            loading.setValue(false);
                            if (response.isSuccessful()) {
                                refreshDetailJob(Token, jobId);
                            } else {
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            loading.setValue(false);
                        }
                    });
        } catch (Exception ex) {
            Log.println(0, "", ex.toString());
        }

    }

    public void loadedGoods(String Token, String jobId) {
        cargoLinkAPI = ApiUtils.getAPIService();
        try {
            cargoLinkAPI.updateStatusWaybill(Token, LangUtils.getCurrentLanguage(), new JobStatusUpdateReq(jobId, WaybillStatus.loaded.getValue(), null))
                    .enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            loading.setValue(false);
                            if (response.isSuccessful()) {
                                refreshDetailJob(Token, jobId);
                            } else {
                                loading.setValue(false);
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            loading.setValue(false);
                        }
                    });
        } catch (Exception ex) {
            Log.println(0, "", ex.toString());
        }

    }

    public void finishJob(String Token, String jobId) {
        cargoLinkAPI = ApiUtils.getAPIService();
        try {
            cargoLinkAPI.updateStatusWaybill(Token, LangUtils.getCurrentLanguage(), new JobStatusUpdateReq(jobId, WaybillStatus.finished.getValue(), null))
                    .enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            if (response.isSuccessful()) {
                                refreshDetailJob(Token, jobId);
                            } else {
                                loading.setValue(false);
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            ;
                            loading.setValue(false);
                        }
                    });
        } catch (Exception ex) {
            Log.println(0, "", ex.toString());
        }

    }

    public void finishDetailWaybill(String Token, String detailWaybillID, boolean isLastItem, String jobID) {
        cargoLinkAPI = ApiUtils.getAPIService();
        try {
            cargoLinkAPI.updateStatusDetailWaybill(Token, LangUtils.getCurrentLanguage(), new JobStatusUpdateReq(detailWaybillID, WaybillStatus.finished.getValue(), null))
                    .enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            if (response.isSuccessful()) {
                                if (isLastItem) {
                                    finishJob(Token, jobID);
                                } else
                                    refreshDetailJob(Token, jobID);
                            } else {
                                loading.setValue(false);
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            ;
                            loading.setValue(false);
                        }
                    });
        } catch (Exception ex) {
            Log.println(0, "", ex.toString());
        }
    }
}
