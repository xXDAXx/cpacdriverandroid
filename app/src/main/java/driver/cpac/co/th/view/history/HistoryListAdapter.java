package driver.cpac.co.th.view.history;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tmg.th.R;
import com.tmg.th.databinding.ItemListHistoryBinding;

import java.util.ArrayList;
import java.util.List;

import driver.cpac.co.th.Model.DetailWaybill;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.WaybillHistoryRes;
import driver.cpac.co.th.util.enumClass.WaybillHistoryStatus;

import static android.view.View.GONE;

public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.MyViewHolder> {
    protected List<WaybillHistoryRes> historyList;
    protected List<DetailWaybill> detailWaybillList;
    protected JobDetailResV2 jobDetail;
    private Context context;
    private ItemListHistoryBinding binding;

    public HistoryListAdapter(Context context, JobDetailResV2 jobDetailResV2, boolean showContact, boolean expanable) {
        this.context = context;
        this.historyList = jobDetailResV2.getHistory();
        this.detailWaybillList = jobDetailResV2.getListDetailWaybil();
        this.jobDetail = jobDetailResV2;
        //if (historyList.size() > 1)
        //    Collections.sort(historyList);
    }

    @NonNull
    @Override
    public HistoryListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(context);
        ItemListHistoryBinding itemBinding =
                ItemListHistoryBinding.inflate(layoutInflater, viewGroup, false);
        return new HistoryListAdapter.MyViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryListAdapter.MyViewHolder viewHolder, int position) {
        WaybillHistoryRes currentWaybill = historyList.get(position);
        HistoryListAdapter.MyViewHolder shipmentHolder = (HistoryListAdapter.MyViewHolder) viewHolder;
        viewHolder.bind(currentWaybill, position);
    }

    @Override
    public int getItemCount() {
        return historyList == null ? 0 : historyList.size();
    }

    public void updateAdapter(ArrayList<WaybillHistoryRes> arrayList) {
        this.historyList.clear();
        this.historyList = arrayList;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemListHistoryBinding binding;

        public MyViewHolder(ItemListHistoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(WaybillHistoryRes item, int position) {
            binding.executePendingBindings();
            binding.addressText.setText(WaybillHistoryStatus.GetWaybillHistoryStatus(item.getType(),context));
            binding.timeText.setText(item.getFormattedCreatedDate());
            if (position == 0) {
                binding.dashlineTop.setVisibility(GONE);
                binding.dashlineBottom.setVisibility(View.VISIBLE);
                binding.destinationIcon.setImageResource(R.drawable.origin_icon);
                binding.destinationIcon.setColorFilter(ContextCompat.getColor(context, R.color.cargolink_blue), PorterDuff.Mode.SRC_IN);
            } else {
                binding.dashlineTop.setVisibility(View.VISIBLE);
                binding.dashlineBottom.setVisibility(View.VISIBLE);
                binding.destinationIcon.setImageResource(R.drawable.redmaker);
                binding.destinationIcon.setColorFilter(ContextCompat.getColor(context, R.color.cargolink_red), PorterDuff.Mode.SRC_IN);
            }

            if (item.getType()== WaybillHistoryStatus.waybillArrived.getValue()) {
                String textToView = WaybillHistoryStatus.GetWaybillHistoryStatus(item.getType(),context) +
                        ": " + jobDetail.getLoadingAddress();
                binding.addressText.setText(textToView);
            }else if (item.getType()== WaybillHistoryStatus.waybillLoaded.getValue()) {
                String textToView = WaybillHistoryStatus.GetWaybillHistoryStatus(item.getType(),context) +
                        ": " + jobDetail.getLoadingAddress();
                binding.addressText.setText(textToView);
            }else if (item.getType()== WaybillHistoryStatus.waybillFinished.getValue()) {
                int waybillDetailIndex = position - (historyList.size() - detailWaybillList.size());
                String textToView = WaybillHistoryStatus.GetWaybillHistoryStatus(item.getType(),context) +
                        ": " + detailWaybillList.get(waybillDetailIndex).getAddressDest();
                binding.addressText.setText(textToView);
            }

            if (position == historyList.size() - 1) {
                binding.dashlineBottom.setVisibility(GONE);
                binding.divider.setVisibility(GONE);
            }
        }
    }
}

