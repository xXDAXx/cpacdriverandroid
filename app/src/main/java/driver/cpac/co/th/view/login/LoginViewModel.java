package driver.cpac.co.th.view.login;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.tmg.th.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import driver.cpac.co.th.App;
import driver.cpac.co.th.Model.ActiveErrMsg;
import driver.cpac.co.th.Model.ActiveUserReqReq;
import driver.cpac.co.th.Model.DriverInfo;
import driver.cpac.co.th.Model.ErrorResponse;
import driver.cpac.co.th.Model.Login;
import driver.cpac.co.th.Model.SetPasswordReq;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.Constant;
import driver.cpac.co.th.util.LangUtils;
import driver.cpac.co.th.util.PreferenceHelper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends ViewModel {
    private MutableLiveData<Boolean> loginSuccess = new MutableLiveData<>();
    private MutableLiveData<Boolean> activeSuccess = new MutableLiveData<>();
    private MutableLiveData<Boolean> passwordSuccess = new MutableLiveData<>();
    private MutableLiveData<Boolean> verifiedMobileNumber = new MutableLiveData<>();
    public MutableLiveData<ResponseBody> responseDownloadAvatar = new MutableLiveData<>();
    public MutableLiveData<ActiveErrMsg> errorActiveRes = new MutableLiveData<>();
    public MutableLiveData<ErrorResponse> errorValidLoginRes = new MutableLiveData<>();
    public DriverInfo driverInfo;
    private CargoLinkAPI mCargoLinkAPI;

    public MutableLiveData<ActiveErrMsg> getErrorActiveRes() {
        if (errorActiveRes == null)
            errorActiveRes = new MutableLiveData<>();
        return errorActiveRes;
    }

    public MutableLiveData<ResponseBody> getResponseDownloadAvatar() {

        if (responseDownloadAvatar == null) {
            responseDownloadAvatar = new MutableLiveData<>();
        }
        return responseDownloadAvatar;

    }

    public MutableLiveData<Boolean> getVerifiedMobileNumber() {

        if (verifiedMobileNumber == null) {
            verifiedMobileNumber = new MutableLiveData<>();
        }
        return verifiedMobileNumber;

    }

    public MutableLiveData<Boolean> getLoginSuccess() {
        if (loginSuccess == null) {
            loginSuccess = new MutableLiveData<>();
        }
        return loginSuccess;
    }

    public MutableLiveData<Boolean> getActiveSuccess() {
        if (activeSuccess == null) {
            activeSuccess = new MutableLiveData<>();
        }
        return activeSuccess;
    }

    public MutableLiveData<Boolean> getPasswordSuccess() {
        if (passwordSuccess == null) {
            passwordSuccess = new MutableLiveData<>();
        }
        return passwordSuccess;
    }

    //CHANGE LANGUAGE
    public void changeLanguage(String token, String languageId) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.changeLanguage(token, languageId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful())
                    Log.i("abc", "fail");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("abc", t.toString());
            }
        });
    }

    public void login(Login loginInfo) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.logincall(LangUtils.getCurrentLanguage(), loginInfo).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    driverInfo = new Gson().fromJson(response.body().toString().trim(), DriverInfo.class);
                    if (driverInfo.isHasRoleDriver()) {
                        // Set sharePreferences here
                        String token = response.headers().get(Constant.HEADER_TOKEN).toString();
                        PreferenceHelper preferenceHelper = new PreferenceHelper();
                        preferenceHelper.putString(Constant.TOKEN, Constant.TOKEN_PREFIX + token);
                        preferenceHelper.putString(Constant.USER_NAME, loginInfo.getEmail());
                        preferenceHelper.putInt(Constant.USER_ID, driverInfo.getId());
                        preferenceHelper.putString(Constant.FULLNAME, driverInfo.getFullName());
                        preferenceHelper.putString(Constant.LICENSE_NUMBER, driverInfo.getLicenseNumberDocument());
                        preferenceHelper.putString(Constant.AVATAR, driverInfo.getAvatar());
                        preferenceHelper.putString(Constant.CARRIER_ID, driverInfo.getCarrierId());
                        preferenceHelper.putString(Constant.EMAIL, driverInfo.getEmail());
                        preferenceHelper.putString(Constant.PHONE_NUMBER, driverInfo.getPhoneNumber());
                        preferenceHelper.putString(Constant.DEVICE_TOKEN, driverInfo.getDeviceToken());
                        preferenceHelper.setList(Constant.TRUCK_TYPE, driverInfo.getTruckType());
                        preferenceHelper.putInt(Constant.DRIVER_LICENCE,driverInfo.getDrivingLicenseType());
                        //Change Language
                        changeLanguage((Constant.TOKEN_PREFIX + token), LangUtils.getCurrentLanguage());
                        //
                        loginSuccess.setValue(true);
                    } else if (response.code() == 400) {
                        loginSuccess.setValue(false);
                    } else {
                        loginSuccess.setValue(false);
                    }
                } else
                    loginSuccess.setValue(false);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                loginSuccess.setValue(false);
            }
        });
    }

    public void validLogin(Login loginInfo) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.validLoginCall(LangUtils.getCurrentLanguage(), loginInfo).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                ErrorResponse res;
                ActiveErrMsg df = null;
                if (response.isSuccessful()) {
                    login(loginInfo);
                } else if (response.code() == 400) {
                        try {
                            res = App.self().getGSon().fromJson(response.errorBody().string(), ErrorResponse.class);
                            if(res.getValidMsgList() != null && res.getValidMsgList().getDriverAccLocked().size()>0)
                                df = new ActiveErrMsg("", res.getValidMsgList().getDriverAccLocked().get(0));
                            errorActiveRes.setValue(df);
                        } catch (IOException e) {
                            df = new ActiveErrMsg("", App.self().getString(R.string.commonErrorMsg));
                            errorActiveRes.setValue(df);
                        }
                } else
                    loginSuccess.setValue(false);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                loginSuccess.setValue(false);
            }
        });
    }

    public void activeCall(ActiveUserReqReq req) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.activeNewUser(LangUtils.getCurrentLanguage(), req).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                ActiveErrMsg res;
                if (response.isSuccessful()) {

                    activeSuccess.setValue(true);
                } else if (response.code() == 400) {
                    try {
                        res = App.self().getGSon().fromJson(response.errorBody().string(), ActiveErrMsg.class);
                        errorActiveRes.setValue(res);
                    } catch (IOException e) {
                        res = new ActiveErrMsg("", App.self().getString(R.string.commonErrorMsg));
                        errorActiveRes.setValue(res);
                    }
                } else {
                    res = new ActiveErrMsg("", App.self().getString(R.string.commonErrorMsg));
                    errorActiveRes.setValue(res);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                activeSuccess.setValue(false);
            }
        });
    }

    public void setPasswordCall(SetPasswordReq req) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.setPasswordUser(LangUtils.getCurrentLanguage(), req).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    ///

                    passwordSuccess.setValue(true);
                } else {
                    passwordSuccess.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                passwordSuccess.setValue(false);
            }
        });
    }

    public void verifyActiveDriverPhoneNumber(String req) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.verifyActiveDriverPhoneNumber(LangUtils.getCurrentLanguage(), req).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                ActiveErrMsg res;
                if (response.isSuccessful()) {
                    ///

                    verifiedMobileNumber.setValue(true);
                } else if (response.code() == 400) {
                    try {
                        res = App.self().getGSon().fromJson(response.errorBody().string(), ActiveErrMsg.class);
                        errorActiveRes.setValue(res);
                    } catch (IOException e) {
                        res = new ActiveErrMsg("", App.self().getString(R.string.commonErrorMsg));
                        errorActiveRes.setValue(res);
                    }
                } else {
                    verifiedMobileNumber.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                verifiedMobileNumber.setValue(false);
            }
        });
    }
}
