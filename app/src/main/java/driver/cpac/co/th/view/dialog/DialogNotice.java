package driver.cpac.co.th.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.databinding.DataBindingUtil;

import com.tmg.th.R;
import com.tmg.th.databinding.DialogNoticeBinding;
import driver.cpac.co.th.util.LangUtils;

public class DialogNotice extends Dialog {
    public Activity activity;
    public Dialog dialog;
    public String stringContent;
    private DialogNoticeBinding binding;
    LangUtils langUtils;

    public DialogNotice(String content, Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
        this.stringContent = content;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_notice, (ViewGroup) activity.findViewById(android.R.id.content), false);
        setContentView(binding.getRoot());
        binding.okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        binding.noticeContent.setText(stringContent);
    }
}
