package driver.cpac.co.th.view.history;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import driver.cpac.co.th.Model.BaseResponse;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.Model.ShipmentInfo;
import driver.cpac.co.th.WebApi.ApiUtils;
import driver.cpac.co.th.WebApi.CargoLinkAPI;
import driver.cpac.co.th.util.LangUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryViewModel extends ViewModel {
    public MutableLiveData<List<ShipmentInfo>> listShipmentInfo;
    public MutableLiveData<List<JobDetailResV2>> listJobRes;
    public MutableLiveData<Boolean> showLoading = new MutableLiveData<>();
    public MutableLiveData<Boolean> showEmpty = new MutableLiveData<>();
    public MutableLiveData<BaseResponse> baseResponse;
    public MutableLiveData<JobDetailResV2> jobDetailLiveData;
    private CargoLinkAPI mCargoLinkAPI;

    public MutableLiveData<List<JobDetailResV2>> getlistJobRes(){
        if(listJobRes == null){
            listJobRes = new MutableLiveData<>();
        }
        return listJobRes;
    }

    public MutableLiveData<Boolean> getShowLoading() {
        if(showLoading == null) {
            showLoading = new MutableLiveData<>();
        }
        return showLoading;
    }
    public MutableLiveData<Boolean> getShowEmpty() {
        if (showEmpty == null) {
            showEmpty = new MutableLiveData<>();
        }
        return showEmpty;
    }
    public MutableLiveData<JobDetailResV2> getjobDetailLiveData() {
        if (jobDetailLiveData == null) {
            jobDetailLiveData = new MutableLiveData<>();
        }
        return jobDetailLiveData;
    }

    //API getJob
    public void getHistoryList(String Token){
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.getJobsHistory(Token, LangUtils.getCurrentLanguage()).enqueue(new Callback<List<JobDetailResV2>>() {
            @Override
            public void onResponse(Call<List<JobDetailResV2>> call, Response<List<JobDetailResV2>> response) {
                if (response.isSuccessful()) {
                    if(response.body().size()>0) {
                        listJobRes.setValue(response.body());
                    }
                    else {
                        showEmpty.setValue(true);
                        showLoading.setValue(false);
                    }
                }
                else
                {
                    showEmpty.setValue(true);
                    showLoading.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<List<JobDetailResV2>> call, Throwable t) {
                showEmpty.setValue(true);
                showLoading.setValue(false);
            }
        });

    }

    public void getDetailJob(String Token, String jobId) {
        mCargoLinkAPI = ApiUtils.getAPIService();
        mCargoLinkAPI.getDetailWaybill(Token, LangUtils.getCurrentLanguage(), jobId)
                .enqueue(new Callback<JobDetailResV2>() {
                    @Override
                    public void onResponse(Call<JobDetailResV2> call, Response<JobDetailResV2> response) {
                        if (response.body() != null) {
                            jobDetailLiveData.setValue(response.body());
                        } else {
                            showLoading.setValue(false);
                        }
                    }

                    @Override
                    public void onFailure(Call<JobDetailResV2> call, Throwable t) {
                        showLoading.setValue(false);
                    }
                });
    }
}
