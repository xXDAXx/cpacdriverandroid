package driver.cpac.co.th.view.issueReport;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tmg.th.R;
import com.tmg.th.databinding.ActivityReportingStep1Binding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import driver.cpac.co.th.Model.ItemSetting;
import driver.cpac.co.th.Model.JobDetailResV2;
import driver.cpac.co.th.util.PreferenceHelper;
import driver.cpac.co.th.util.RecyclerViewClickListener;
import driver.cpac.co.th.util.Util;
import driver.cpac.co.th.util.enumClass.IssueType;
import driver.cpac.co.th.util.enumClass.TruckType;
import driver.cpac.co.th.view.base.OldBaseActivity;
import driver.cpac.co.th.view.dialog.DialogConfirm;
import driver.cpac.co.th.view.dialog.DialogNoticeCallback;
import driver.cpac.co.th.view.jobprocessing.JobProcessingActivityV3;

public class ReportingStep1Activity extends OldBaseActivity implements DialogConfirm.CallBackDialogCom,
        DialogNoticeCallback.CallBackDialogNotice {
    PreferenceHelper preferenceHelper;
    private ActivityReportingStep1Binding binding;
    //private IssueDetailRes jobDetailViewModel;
    DialogNoticeCallback dialogNotice;
    DialogConfirm dialogConfirm;
    DialogNoticeCallback dialogNoticeCallback;
    public JobDetailResV2 jobDetail;
    Boolean backToJobDetail;

    public Activity activity;
    RecyclerViewClickListener listener;
    List<ItemNewIssue> funcstions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reporting_step1);
        setSupportActionBar(binding.appBarLayout.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.issueReport_mainActivity_lblTittleMain));
        preferenceHelper = new PreferenceHelper(this);
        activity = this;
        ObserverSetup();

        jobDetail = (JobDetailResV2) getIntent().getSerializableExtra("jobDetail");
        backToJobDetail = (Boolean)  getIntent().getSerializableExtra("backToJobDetail");

        listener = (view, position) -> {
            funcstions.get(position);
            Intent intent = new Intent(this, ReportingStep2Activity.class);
            intent.putExtra("NewIssue", funcstions.get(position));
            if(jobDetail!=null) {
                intent.putExtra("jobDetail", jobDetail);
            }
            startActivity(intent);
        };
        Test();
    }

    public boolean onSupportNavigateUp() {
        if(backToJobDetail == null || backToJobDetail != true)
            onBackPressed();
        else if(backToJobDetail != null && backToJobDetail == true) {
            startActivity(new Intent(this,JobProcessingActivityV3.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP));
        }
        return true;
    }

    public void ObserverSetup() {

    }

    @Override
    public void BtnOkClick() {

    }

    @Override
    public void BtnOkDiaComClick() {

    }

    public void Test() {
        funcstions = new ArrayList<>();
        List<IssueType> listIssue = Arrays.asList(IssueType.values());
        for (IssueType type:listIssue) {
            ItemNewIssue item1 = new ItemNewIssue(IssueType.GetIssueTypeName(type.getValue(),this),
                    IssueType.GetIssueTypeImg(type.getValue(),this),
                    type);
            funcstions.add(item1);
        }

        /*
        ItemNewIssue item1 = new ItemNewIssue(getString(R.string.issueReport_issueType_waybill), R.drawable.issuetype_waybill, IssueType.Wallbill);
        ItemNewIssue item2 = new ItemNewIssue(getString(R.string.issueReport_issueType_traffic), R.drawable.issuetype_traffic,IssueType.Traffic);
        ItemNewIssue item3 = new ItemNewIssue(getString(R.string.issueReport_issueType_accidents), R.drawable.issuetype_accidents,IssueType.Accidents);
        ItemNewIssue item4 = new ItemNewIssue(getString(R.string.issueReport_issueType_document), R.drawable.issuetype_document,IssueType.Document);
        ItemNewIssue item5 = new ItemNewIssue(getString(R.string.issueReport_issueType_loading), R.drawable.issuetype_loading,IssueType.Loading);
        ItemNewIssue item6 = new ItemNewIssue(getString(R.string.issueReport_issueType_delivery), R.drawable.issuetype_delivery,IssueType.Delivery);
        ItemNewIssue item7 = new ItemNewIssue(getString(R.string.issueReport_issueType_others), R.drawable.issuetype_others,IssueType.Others);

        funcstions.add(item1);
        funcstions.add(item2);
        funcstions.add(item3);
        funcstions.add(item4);
        funcstions.add(item6);
        funcstions.add(item5);
        funcstions.add(item7);
         */

        binding.rcv.setVisibility(View.VISIBLE);
        ListnNewIssueAdapter adapter = new ListnNewIssueAdapter( funcstions,this, listener);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.rcv.setLayoutManager(layoutManager);
        binding.rcv.setAdapter(adapter);
    }
}
