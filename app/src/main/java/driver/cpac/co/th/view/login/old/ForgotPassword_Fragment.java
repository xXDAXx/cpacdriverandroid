package driver.cpac.co.th.view.login.old;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import driver.cpac.co.th.Model.BaseResponse;
import driver.cpac.co.th.Model.LoginUser;
import com.tmg.th.R;
import com.tmg.th.databinding.ForgotpasswordLayoutBinding;
import driver.cpac.co.th.view.dialog.DialogNotice;
import driver.cpac.co.th.view.dialog.DialogWait;
import driver.cpac.co.th.view.dialog.DialogWait2;

import java.util.Objects;


public class ForgotPassword_Fragment extends Fragment implements LifecycleOwner {
    private static View view;
    private LoginViewModel loginViewModel;
    private static FragmentManager fragmentManager;
    private ForgotpasswordLayoutBinding binding;
    private DialogNotice dialogNotice;
    private DialogWait2 dialogWait;

    private static FragmentTransaction fragmentTransaction;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentManager = getActivity().getSupportFragmentManager();
        binding = DataBindingUtil.inflate(
                inflater, R.layout.forgotpassword_layout, container, false);
        view = binding.getRoot();
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        binding.setLoginViewModel(loginViewModel);
        binding = DataBindingUtil.inflate(
                inflater, R.layout.forgotpassword_layout, container, false);
        binding = DataBindingUtil.setContentView(this.getActivity(), R.layout.forgotpassword_layout);
        //binding.setLifecycleOwner(this.getActivity());
        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.setLoginViewModel(loginViewModel);

        dialogWait = new DialogWait2(getActivity());

        loginViewModel.getUser().observe(this, new Observer<LoginUser>() {
            @Override
            public void onChanged(@Nullable LoginUser loginUser) {
                if (TextUtils.isEmpty(Objects.requireNonNull(loginUser).getStrEmailAddress())) {
                    binding.registeredEmailid.setError("Enter an E-Mail Address");
                    binding.registeredEmailid.requestFocus();
                } else if (!loginUser.isEmailValid()) {
                    binding.registeredEmailid.setError("Enter a Valid E-mail Address");
                    binding.registeredEmailid.requestFocus();
                } else {
                    dialogWait.show();
                    loginViewModel.resetPassword(loginUser);
                }
            }
        });

        loginViewModel.getBack2Login().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean back2Login) {
                if (back2Login) {
                    /*
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.login_layout,
                                    new ForgotPassword_Fragment(),
                                    Util.ForgotPassword_Fragment).commit();
                */
                    fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.RFogot_layout, new Login_Fragment()).commit();
                    fragmentTransaction.addToBackStack(null);
                }
            }
        });

        loginViewModel.getBaseRes().observe(this, new Observer<BaseResponse>() {
            @Override
            public void onChanged(BaseResponse baseResponse) {
                dialogWait.hide();
                if (baseResponse != null) {
                    dialogNotice = new DialogNotice(baseResponse.getStringNotice(), getActivity());
                } else {
                    dialogNotice = new DialogNotice(getString(R.string.commonErrorMsg), getActivity());
                }
                if (baseResponse.getStringStatus() == null) {
                    fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.RFogot_layout, new Login_Fragment()).commit();
                    fragmentTransaction.addToBackStack(null);
                }
                dialogNotice.show();
            }

        });

        return view;
    }
}