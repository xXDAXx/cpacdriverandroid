package driver.cpac.co.th.Model;

import com.google.gson.annotations.Expose;

public class ItemSetting {
    @Expose
    String title;
    @Expose
    int type;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ItemSetting(String title, int type) {
        this.title = title;
        this.type = type;
    }

    public ItemSetting() {
        this.title = "";
        this.type = 0;
    }
}


